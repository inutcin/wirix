<?php
    $query = $_D->link->real_escape_string($_H->get("query"));
    $limit = 10;
    $offset = intval($_H->get('offset'));
    
    $_D->search(
	array("a"=>"pages"),
	array(),array(),
	"`a`.`name` LIKE '%$query%' OR `a`.`anons` LIKE '%$query%' OR `a`.`text` LIKE '%$query%'","`a`.`publish_from` DESC",
	$limit,$offset,
	array(
	    "`a`.`id`"=>"id",
	    "`a`.`name`"=>"name"
	)
    );

    $this->data["result"] = $_D->rows;
    $this->data["pages"] = $_D->pages;
    $this->data["query"] = htmlspecialchars($query);
