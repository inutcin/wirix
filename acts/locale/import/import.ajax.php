<?php
    
    include_once($_SERVER['DOCUMENT_ROOT']."/wirix/libs/wirix.class.php");

    $answer = array();
    $answer['error'] = '';
    $answer['success'] = '';
    $answer['redirect'] = '/locale/import/';

    //$answer['error'] = print_r($_FILES, true);
    //$answer['error'] = str_replace("\n", "", $answer['error']);
    
    if(!$filename = $_FILES['pofile']['tmp_name']){
        $error = new wirix_error();
        $answer['error'] = _t($error->upload_error($_FILES['pofile']['error'], 'en'));
    }
    
    $locale = $_SERVER['wirix']->lib_load('locale');
    list($inserted, $updated) = $locale->import_from_po_file($filename, $_SERVER['http']->post('lang'));
    
    if($answer['error'] || $locale->error){
        $answer['error'] = $locale->error;
    }
    else{
        $answer['success'] = _t("Updated:")." ".$updated."."._t("Inserted:")." ".$inserted.".";
    }
    
    
    
    echo "<script>";
    if($answer['error']){
        echo "parent.$('#".$_SERVER['http']->post('form_id')." .alert-bar').css('display','table-row');\n";
        echo "parent.$('#".$_SERVER['http']->post('form_id').
            "_alert_error').attr('style','');\n";
        echo "parent.document.getElementById('".$_SERVER['http']->post('form_id').
            "_alert_error').innerHTML='".$answer['error']."';";
    }
    else{
        echo "parent.$('#".$_SERVER['http']->post('form_id')." .success-bar').css('display','table-row');\n";
        echo "parent.$('#".$_SERVER['http']->post('form_id').
            "_alert_success').attr('style','');\n";
        echo "parent.document.getElementById('".$_SERVER['http']->post('form_id').
            "_alert_success').innerHTML='".$answer['success']."';";
        
    }
    echo "</script>";


