<?php

    $form = $_SERVER['wirix']->lib_load('html_form');
    $form->id = 'locale_import_form';
    $form->title = '';
    $form->send_button_text = _t('Import');
    $form->use_ajax = false;
    $form->submit_url = "/wirix/acts/locale/import/import.ajax.php";

        $input  = $_SERVER['wirix']->lib_load("html_input");
        $input->name = 'pofile';
        $input->type = 'file';
        $input->title = _t('File name:');
        $input->default = _t('File name:');
        $input->comment = '';
        $form->add_input($input);
        unset($input);

        $input  = $_SERVER['wirix']->lib_load("html_input");
        $input->name = 'lang';
        $input->regexp = '/^.{2,8}$/';
        $input->type = 'select';
        $input->title = _t('Site Language');
        $input->default = _t('Site Language');
        $input->comment = '';
        
        $_SERVER['db']->search(array("a"=>"locale_langs"), array());
        $input->values = array();
        foreach($_SERVER['db']->rows as $row)
            $input->values[] = array("value"=>$row['lang'], "title"=>$row['name']." (".$row['country'].")"); 
        
        $form->add_input($input);
        unset($input);
    $form->commit();
    $this->data['locale_import_form'] = $form->fetch();
