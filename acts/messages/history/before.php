<?php

    $tmp    = explode(",", $_SERVER['http']->item);
    $from_id = isset($tmp[0])?intval($tmp[0]):0;
    $to_id   = isset($tmp[1])?intval($tmp[1]):0;
    $user_id    = $_SERVER['auth']->user['id'];
    
    if($user_id!=$from_id && $user_id!=$to_id){
        $_SERVER['http']->status(403);
        die;
    }
    
    $tmp = $messages->history($from_id, $to_id, intval($_SERVER['http']->action));
    
    $this->data['messages']     = $tmp['messages'];
    $this->data['pages']        = $tmp['pages'];

    $date = $_SERVER['wirix']->lib_load("dates");
    foreach($this->data['messages'] as $k=>$v){
        $this->data['messages'][$k]['from_fio'] = $v['from_fio']?$v['from_fio']:$v['from_username'];
        $this->data['messages'][$k]['to_fio'] = $v['to_fio']?$v['to_fio']:$v['to_username'];
        $this->data['messages'][$k]['date'] = $date->translate($v['date']);
        $this->data['messages'][$k]['read_date'] = $date->translate($v['read_date']);
    }
    unset($date);
