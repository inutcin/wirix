<?php

    $message = $messages->view(
        $message_id = intval($_SERVER['http']->action), 
        $_SERVER['auth']->user['id']
    );
    
    if($message_id && $_SERVER['auth']->user['id']==$message['to_id'])
        $messages->remove($message_id);
    
    $_SERVER['http']->status(
        302,
        $_SERVER['http']->get('return_url')?$_SERVER['http']->get('return_url'):'/'
    );
    die;
    
