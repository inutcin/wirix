<?php

    
    $this->data['message'] = $messages->view(
        intval($_SERVER['http']->action),
        $_SERVER['auth']->user['id']
    );
    $this->data['message']['body'] = str_replace('\n', "\n", $this->data['message']['body']);
    $this->data['message']['body'] = str_replace('\r', "\r", $this->data['message']['body']);
    if($_SERVER['http']->post("answer")){

        $messages->reply_message_id = $this->data['message']['id'];
        $messages->from = $_SERVER['auth']->user['id'];
        $messages->to = $this->data['message']['from_id'];
        $message_id = $messages->send(
            $_SERVER['http']->post("answer"),
            isset($_FILES['upload'])?$_FILES['upload']:array()
        );
        $_SERVER['http']->status(302, "/messages/view/".$message_id."/");
        die;
    }
    

    $this->data['message']['date'] = $dates->translate($this->data['message']['date']);
    $this->data['message']['body'] = explode("\n", $this->data['message']['body']);
    foreach($this->data['message']['body'] as $k=>$v)$this->data['message']['body'][$k] = "> ".$v;
    $this->data['message']['body'][] = "\n";
    $this->data['message']['body'] = implode("\n", $this->data['message']['body']);
    

