<?php

    $messages_menu = $_SERVER['wirix']->lib_load("html_menu");
    $messages_menu->type = 'nav nav-tabs';

    $messages = $_SERVER['wirix']->lib_load("message");
    
    $this->data['module']   = $_SERVER['http']->module;
    $this->data['section']  = $_SERVER['http']->section;
    $this->data['action']   = $_SERVER['http']->action;
    $this->data['item']     = $_SERVER['http']->item;
    
    $dates = $_SERVER['wirix']->lib_load("dates");
    
    if(!$_SERVER['http']->section)$_SERVER['http']->status(302,'/messages/unread/');
