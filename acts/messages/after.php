<?php
    $statistic = $messages->statistic($_SERVER['auth']->user['id']);

    $messages_menu->items = array(
        array(
            "text"=>_t('Unread').($statistic['unread']?" (".$statistic['unread'].")":""),
            "url"=>"/messages/unread/",
            "class"=>$_SERVER['http']->section=='unread'?"active":"",
            "icon"  => "icon-envelope"
        ),
        array(
            "text"=>_t('Inbox').($statistic['inbox']?" (".$statistic['inbox'].")":""),
            "url"=>"/messages/inbox/",
            "class"=>$_SERVER['http']->section=='inbox'?"active":"",
            "icon"  => "icon-inbox"
        ),
        array(
            "text"  =>_t('Sent').($statistic['sent']?" (".$statistic['sent'].")":""),
            "url"   =>"/messages/sent/",
            "class" =>$_SERVER['http']->section=='sent'?"active":"",
            "icon"  =>  "icon-share"
        ),
        array(
            "text"=>_t('Trash').($statistic['deleted']?" (".$statistic['deleted'].")":""),
            "url"=>"/messages/trash/",
            "class"=>$_SERVER['http']->section=='trash'?"active":"",
            "icon"  => "icon-trash"
        )
    );
    $this->data['messages_menu'] = $messages_menu->fetch();
