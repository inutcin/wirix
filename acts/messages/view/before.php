<?php

    $this->data['message'] = $messages->view(
        intval($_SERVER['http']->action), 
        $_SERVER['auth']->user['id']
    );

    $this->data['message']['date'] = $dates->translate($this->data['message']['date']);
    $this->data['message']['read_date'] = $dates->translate($this->data['message']['read_date']);
    $this->data['message']['body'] = str_replace('\n', "\n", $this->data['message']['body']);
    $this->data['message']['body'] = str_replace('\r', "\r", $this->data['message']['body']);
    $this->data['message']['body'] = html_entity_decode($this->data['message']['body']);
    $this->data['message']['body'] = str_replace("\n", "<br/>\n", $this->data['message']['body']);
    
    
