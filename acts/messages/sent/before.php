<?php

    $tmp = $messages->sent(isset($_SERVER['auth']->user['id'])?intval(
        $_SERVER['auth']->user['id']
    ):0,intval($_SERVER['http']->item));
    
    $this->data['messages'] = $tmp['messages'];
    $this->data['pages']    = $tmp['pages'];
    
    $date = $_SERVER['wirix']->lib_load("dates");
    foreach($this->data['messages'] as $k=>$v){
        $this->data['messages'][$k]['from_fio'] = $v['from_fio']?$v['from_fio']:$v['from_username'];
        $this->data['messages'][$k]['to_fio'] = $v['to_fio']?$v['to_fio']:$v['to_username'];
        $this->data['messages'][$k]['date'] = $date->translate($v['date']);
        $this->data['messages'][$k]['read_date'] = $date->translate($v['read_date']);
    }
