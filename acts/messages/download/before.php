<?php

    $file_obj = $_SERVER['wirix']->lib_load("file");
    
    $message_id = intval($_SERVER['http']->action);
    
    if(!$message_id){
        _w('Message ID is not defined');
        die;
    }
    
    $messages = $_SERVER['wirix']->lib_load("message");
    $message = $messages->view($message_id, $_SERVER['auth']->user['id'], 0);
    
    if(!$message){
        $_SERVER['http']->status(403);
        die;
    }
    
    $file_id = $message['file_id'];
    $filename = $message['file_info']['name'];
    $size = $message['file_info']['size_h'];

    $fd = $file_obj->get_file_descriptor($file_id);
    $_SERVER['http']->attach($filename);
    $_SERVER['http']->content_type($message['file_info']['mime']);
    header("Content-length:", $size);
    while(!feof($fd)){
        echo fread($fd, 1024);
    }
