<?php
    include("../../../libs/wirix.class.php");
    
    $answer = array();
    $answer['error'] = '';
    $answer['redirect'] = '/groups/';

    if(!$_SERVER['auth']->is_group(1))$_SERVER['http']->status(403);

    // Подключаем проверку полей
    include(dirname(__FILE__)."/../../../libs/h/html_form/check.ajax.php");
    
    // Проверка существования группы
    if(!$answer['error'] 
        && 
        $group_info = $_SERVER['auth']->group_exists_by_name($group_name = $_SERVER['http']->post('name')) 
    )
        $answer['error'] = "Группа <b>$group_name</b> уже существует";

    if(!$answer['error'])$_SERVER['auth']->group_add($group_name);
    
    $answer['ok'] = 1;
    echo json_encode($answer);
?>
