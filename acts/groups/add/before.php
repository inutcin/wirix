<?php
    if(!$_SERVER['auth']->is_group(1))$_SERVER['http']->status(403);

    $group_form = $_SERVER['wirix']->lib_load("html_form");
    $group_form->id = "group_form";
    $group_form->title = "Добавить группу";
    $group_form->send_button_text = 'Добавить';
    $group_form->submit_url = "/wirix/acts/groups/add/add.ajax.php";

    include(dirname(__FILE__)."/../group.fields.php");

    $this->data['group_form'] = $group_form->fetch();
?>
