<?php
    $this->breadcrumbs->add("Пользователи",'/users/');
    
    if(!$_SERVER['auth']->is_group(1))$_SERVER['http']->status(403);
    
    $groups_table = $_SERVER['wirix']->lib_load("html_db_table");
    $groups_table->title = 'Группы';
    $groups_table->id = 'groups';
    
    // Если не указано конкретное действие - выводим таблицу пользователей
    if(!$_SERVER['http']->section){
        $groups_table->tables = array("a"=>"groups");
        $groups_table->search();
    
        $column = array();
        $column['title']= 'ID';
        $column['text'] =   '"$row[id]"';
        $column['width']=   '10px';
        $groups_table->add_column($column);
        unset($column);
    
        $column = array();
        $column['title']= 'Группа';
        $column['text'] =   '"$row[name]"';
        $column['icon'] =   '"icon-user"';
        $column['url']  =   '"/groups/edit/".$row["id"]."/"';
        $column['width']=   '250px';
        $groups_table->add_column($column);
        unset($column);


        $column['icon'] =   '"icon-pencil"';
        $column['hint'] =   '"Редактировать"';
        $column['url']  =   '"/companies/edit/".$row["id"]."/"';

    
        $groups_table->add_action_button(
            $name       =   'add',     //!< Имя кнопки
            $title      =   'Добавить группу',     //!< Текст кнопки 
            $hint       =   'Добавить группу',     //!< Всплываюша подсказка
            $icon       =   'icon-plus',     //!< Иконка кнопки
            $url        =   '/groups/add/',     //!< URL кнопки
            $onclick    =   ''      //!< Событие на кнопке
        );
    
        $this->data['groups_table'] = $groups_table->fetch();
    }
    
?>
