<?php

    $input  = $_SERVER['wirix']->lib_load("html_input");
    $input->type='text';
    $input->title = 'Имя группы';
    $input->regexp = "/^.{1,128}$/";
    $input->comment = 'Имя группы - от 1 до 32 любых символов';
    $input->name = 'name';
    $input->value = isset($group_info['name']) && $group_info['name']?$group_info['name']:'';
    $input->default = '';
    $group_form->add_input($input);
    unset($input);
?>
