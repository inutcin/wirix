<?php
    include("../../../libs/wirix.class.php");
    
    $answer = array();
    $answer['error'] = '';
    $answer['redirect'] = '/groups/';
    
    if(!$_SERVER['auth']->is_group(1))$_SERVER['http']->status(403);
    
    $group_name = $_SERVER['http']->post("name");
    
    // Подключаем проверку полей
    include(dirname(__FILE__)."/../../../libs/h/html_form/check.ajax.php");
    
    // Проверка существования группы
    if(!$answer['error'] 
        && 
        $group_info = $_SERVER['auth']->group_exists_by_id($group_id = $_SERVER['http']->post('group_id')) 
    )
    if($_SERVER['http']->post('group_id')!=$group_info['id'])
        $answer['error'] = "Группа <b>$group_name</b> уже существует";

    if(!$answer['error'])
        $_SERVER['db']->update("groups", array("name"=>$group_name), array("id"=>$group_id));
    $answer['ok'] = 1;
    echo json_encode($answer);
?>
