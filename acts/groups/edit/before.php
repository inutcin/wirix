<?php

    if(!$_SERVER['auth']->is_group(1))$_SERVER['http']->status(403);
        
    $_SERVER['db']->search_one("groups",array("id"=>$_SERVER['http']->action));
    $group_info = $_SERVER['db']->record;

    $group_form = $_SERVER['wirix']->lib_load("html_form");
    $group_form->id = "edit_group_form";
    $group_form->title = "Редактировать группу";
    $group_form->send_button_text = 'Редактировать';
    $group_form->submit_url = "/wirix/acts/groups/edit/edit.ajax.php";

    include(dirname(__FILE__)."/../group.fields.php");

    $input  = $_SERVER['wirix']->lib_load("html_input");
    $input->value = $group_info['id'];
    $input->type='hidden';
    $input->name = 'group_id';
    $group_form->add_input($input);
    unset($input);

    $this->data['group_form'] = $group_form->fetch();
    
?>
