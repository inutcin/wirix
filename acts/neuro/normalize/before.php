<?php

if(!$datascheme = $_SERVER['wirix']->init_datascheme("neuro_normalize"))
    echo $_SERVER['wirix']->error;
$table = $_SERVER['wirix']->lib_load("html_datascheme_table");

$table->scheme = $datascheme;

$table->filter = array(1=>"`b`.`name`",2=>"`c`.`name`");

$this->data['neuro_table'] = $table->fetch();
$this->data['h1'] = "Нормализация критериев";
