<?php

if(!$datascheme = $_SERVER['wirix']->init_datascheme("neuro_reaction"))
    echo $_SERVER['wirix']->error;
$table = $_SERVER['wirix']->lib_load("html_datascheme_table");

$table->scheme = $datascheme;

$this->data['neuro_table'] = $table->fetch();
$this->data['h1'] = "Реакции нейросети";
