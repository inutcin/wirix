<?php
    global $engine;
    
    /*
        Если тэга с указанным ID нет - 404-я
    */
    if(!$_D->search_one("tags", array(
            "`id`"=>$tag_id=intval($_H->action),
            "`show`"=>'Y',
        )
    )){
        $_H->status(404);
        $engine->status = 404;
    }
    
    $this->data["tag"] = $_D->record;
    
    $_D->record = array();
    
    if($_D->search_one("pages", array(
        "id"=>$this->data["tag"]["page_id"]
    ))){
        $this->data["page"] = $_D->record;
        $this->data["page"]["text"] = html_entity_decode($this->data["page"]["text"], ENT_COMPAT, 'utf-8');
    }

    // Получаем все страницы тега
    $_D->search(
        array(
            "a"=>"tags",
            "b"=>"pages"
        ),
        array("`a`.`page_id`=`b`.`id`"=>"LEFT"),
        array(),
        "`a`.`parent_id`=".$tag_id." AND `a`.`show`='Y' AND (NOW() BETWEEN `b`.`publish_from` AND `b`.`publish_to`)",
        "`a`.`order_by` DESC",0,0,
        array(
            "`a`.`id`"=>"id",
            "`a`.`page_id`"=>"page_id",
            "`b`.`name`"=>"name",
            "`b`.`anons`"=>"anons",
            "`b`.`h1`"=>"h1",
            "`b`.`text`"=>"text",
        )
    );

    $this->data["tags"] = $_D->rows;
    
    $cms = $_W->lib_load("cms_pages");
    $this->data["pages"] = $cms->get_pages_by_tag($tag_id);
    $this->data["bread"] = $cms->get_bread();
