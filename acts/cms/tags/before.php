<?php
    
    $tags = $_W->lib_load("cms_pages");
    
    $tags->id = 'tags';
    if(!$_H->action || $tag_id=intval($_H->action)){
        if(!isset($tag_id))$tag_id = 0;
        
        if($_D->search_one("tags", array("id"=>$tag_id)))
            $this->data["tag"] = $_D->record;
        
        $_D->record = array();
        
        if($_D->search_one("mod_rewrite", array("to_url"=>"/tags/view/$tag_id/","status"=>200)))
            $this->data["tag"]["chpu"] = $_D->record["from_url"];
        
        
        if(
            isset($this->data["tag"]["page_id"]) 
            && $_D->search_one("pages", array("id"=>$this->data["tag"]["page_id"]))
        )
            $page = $this->data["page"] = $_D->record;
         
        

        if(isset($this->data["page"])){
            $pageform = $_W->lib_load("html_form");
            $pageform->id = 'tagpage';
            $pageform->title = '';
            $pageform->send_button_text = "Сохранить";
            $pageform->use_back_button = false;

            $input  = $_SERVER['wirix']->lib_load("html_input");    
            $input->name = 'name';                              
            $input->title = 'Имя раздела';                     
            $input->regexp = "/^.+$/";                    
            $input->placeholder = 'Имя элемента структуры';           
            $input->comment = '';
            $input->value=$this->data["tag"]["name"];
            $pageform->add_input($input);    
            unset($input);      

            $input  = $_SERVER['wirix']->lib_load("html_input");    
            $input->name = 'show';                              
            $input->title = 'Отображать на сайте';                     
            $input->regexp = "/^(0|1)$/";                    
            $input->type = "checkbox";                    
            $input->placeholder = 'ЧПУ имя каталога';           
            $input->comment = '';
            $input->value=$this->data["tag"]["show"]=='Y'?"Y":'';
            $pageform->add_input($input);    
            unset($input);      

            $input  = $_SERVER['wirix']->lib_load("html_input");    
            $input->name = 'menu';                              
            $input->title = 'Создать пункт меню';                     
            $input->regexp = "/^(0|1)$/";                    
            $input->type = "checkbox";                    
            $input->placeholder = 'Пункт в главном меню';           
            $input->comment = '';
            $input->value=$this->data["tag"]["menu"]=='Y'?"Y":'';
            $pageform->add_input($input);    
            unset($input);      

            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'text';
            $input->type = 'textarea';
            $input->title = 'Текст страницы';
            $input->placeholder = 'Текст страницы';
            $input->value = html_entity_decode($page["text"]);
            $input->regexp = "/^.*$/m";
            $pageform->add_input($input);
            unset($input);
            
            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'anons';
            $input->type = 'textarea';
            $input->title = 'Анонс страницы';
            $input->height = '100px';
            $input->placeholder = 'Анонс страницы   ';
            $input->wyswyg = false;
            $input->value = html_entity_decode ($page["anons"]);
            $input->regexp = "/^.*$/m";
            $pageform->add_input($input);
            unset($input);

            $input  = $_SERVER['wirix']->lib_load("html_input");    
            $input->name = 'folder';                              
            $input->title = 'Имя каталога (латиница, для ЧПУ)';                     
            $input->regexp = "/^[\d\w\-]+$/";                    
            $input->placeholder = 'ЧПУ';           
            $input->comment = '';
            $input->value=$this->data["tag"]["folder"];
            $pageform->add_input($input);    
            unset($input);      

            
            
            $input  = $_SERVER['wirix']->lib_load("html_input");    
            $input->name = 'chpu';                              
            $input->title = 'ЧПУ страницы<br/>(включая начальный "/")';                     
            $input->regexp = "/^\/[\d\w\-\/]*$/";                    
            $input->placeholder = 'ЧПУ';           
            $input->comment = '';
            $input->value=$this->data["tag"]["chpu"]
                ?
                $this->data["tag"]["chpu"]
                :
                $tags->get_tag_chpu($tag_id); 
            $pageform->add_input($input);    
            unset($input);      
            
            $input  = $_SERVER['wirix']->lib_load("html_input");    
            $input->name = 'h1';                              
            $input->title = 'H1';                     
            $input->regexp = "/^.{0,255}$/";                    
            $input->placeholder = '';           
            $input->comment = '';
            $input->value=$page["h1"]?$page["h1"]:$page["name"];
            $pageform->add_input($input);    
            unset($input);      

            $input  = $_SERVER['wirix']->lib_load("html_input");    
            $input->name = 'title';                              
            $input->title = 'Title';                     
            $input->regexp = "/^.{0,255}$/";                    
            $input->placeholder = '';           
            $input->comment = '';
            $input->value=isset($page["title"]) && $page["title"]?$page["title"]:$page["name"];
            $pageform->add_input($input);    
            unset($input);      


            $input  = $_SERVER['wirix']->lib_load("html_input");    
            $input->name = 'description';                              
            $input->title = 'Description';                     
            $input->regexp = "/^.{0,255}$/";                    
            $input->placeholder = '';           
            $input->comment = '';
            $input->value=isset($page["description"]) && $page["description"]?$page["description"]:htmlspecialchars($page["name"]);
            $pageform->add_input($input);    
            unset($input);      
            
            $input  = $_SERVER['wirix']->lib_load("html_input");    
            $input->name = 'keywords';                              
            $input->title = 'Keywords';                     
            $input->regexp = "/^.{0,255}$/";                    
            $input->placeholder = '';           
            $input->comment = '';
            $input->value=isset($page["keywords"]) && $page["keywords"]?$page["keywords"]:htmlspecialchars($page["name"]);
            $pageform->add_input($input);    
            unset($input);      
            

            
            
            $input  = $_SERVER['wirix']->lib_load("html_input");    
            $input->name = 'tag_id';                              
            $input->type = 'hidden';                              
            $input->regexp = "/^\d+$/";                    
            $input->comment = '';
            $input->value=$this->data["tag"]["id"];
            $pageform->add_input($input);    
            unset($input);      

            $pageform->add_group(4,5,"Контент",false);
            $pageform->add_group(6,10,"Seo",false);
            $pageform->submit_url = '/wirix/acts/cms/tags/edit.ajax.php';

            $this->data['pageform'] = $pageform->fetch();
        }
        
            
        if($tag_id)$this->data["pages_table"]= $tags->fetch_pages_table($tag_id);
    }
    elseif($_H->action=='add'){
        $parent_id = intval($_H->item);
        $this->data["tagsform"] = $tags->fetch_add_tag($parent_id);
    }
    elseif($_H->action=='edit'){
        $id = intval($_H->item);
        $this->data["tagsform"] = $tags->fetch_edit_tag($id);
    }
    elseif($_H->action=='delete'){
        $id = intval($_H->item);
        $parent_id = $tags->delete_tag($id);
        $_H->status('302','/cms/tags/'.$parent_id."/");
        die;
    }
