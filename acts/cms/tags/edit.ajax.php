<?php
include_once(dirname(__FILE__)."/../../../libs/wirix.class.php");

// Доступно только пользователям Admin
if(!$_A->is_group(1)){$_H->status(403);die;}


$answer = array();
$answer['redirect'] = '';
$answer['success'] = 'Раздел сохранён';
$answer['error'] = '';

// Валидация
foreach($_POST["regexp_fields"] as $name=>$regexp)
    if(!preg_match(base64_decode($regexp), $_H->post($name)))
        $answer['error'] = 'Неверно заполнено поле &quot;'.$_POST["title_fields"][$name].'&quot;';


// Сохранение элемента структуры
$tag_id = $_H->post("tag_id");
$upd = array(
    "show"=>$_H->post("show")?'Y':'N',
    "menu"=>$_H->post("menu")?'Y':'N',
    "name"=>$_D->link->real_escape_string($_H->post("name")),
    "folder"=>$_D->link->real_escape_string($_H->post("folder"))
);

// Создание ЧПУ элемента структуры
if($chpu = $_D->link->real_escape_string($_H->post("chpu"))){
    $cms_pages = $_W->lib_load("cms_pages");
    $real_url = "/tags/view/$tag_id/";
    $url_cond = array("to_url"=>$real_url);
    $ins_fields = array(
        "from_url"=>$chpu,
        "to_url"=>$real_url,
        "status"=>200,
        "regex"=>"",
    );
    if($_D->search_one("mod_rewrite",$url_cond))
        $_D->update("mod_rewrite",$ins_fields,$url_cond);
    else
        $_D->insert("mod_rewrite",$ins_fields);
}

// Чистим неактуальные SEO-правила
$_D->sql_query("DELETE FROM seo WHERE auto =  'Y' AND request_uri NOT IN (
SELECT  `from_url` 
FROM  `mod_rewrite` 
WHERE 1
)", "CHANGE");


// Создание СЕО
$url_cond = array("request_uri"=>$chpu);
$ins_fields = array(
    "title"=>$_D->link->real_escape_string($_H->post("title")),
    "description"=>$_D->link->real_escape_string($_H->post("description")),
    "keywords"=>$_D->link->real_escape_string($_H->post("keywords")),
    "request_uri"=>$chpu
);


if($_D->search_one("seo",$url_cond))
    $_D->update("seo",$ins_fields,$url_cond);
else
    $_D->insert("seo",$ins_fields,$url_cond);

$_D->update("tags",$upd,array("id"=>$tag_id));
$page_id = 0;
if($_D->search_one("tags",array("id"=>$tag_id)))
    $page_id = $_D->record['page_id'];

$upd = array(
    "name"  =>  $_D->link->real_escape_string($_H->post("name")),
    "h1"    => $_D->link->real_escape_string($_H->post("h1"))
);
$_D->update("pages",$upd,array("id"=>$page_id));

echo json_encode($answer);
