<?php
    
    // Доступно только пользователям Admin
    if(!$_A->is_group(1)){$_H->status(403);die;}
    
    $menu = $_W->lib_load("html_menu");
    
    
    $menu->items= array(
        array(
            "text"  =>  _t('Rewrites'),
            "url"   =>  "/cms/mod_rewrite/",
            "childs"=>array(
                array(
                    "text"  =>  _t('New rewrite rule'),
                    "url"   =>  "/cms/mod_rewrite/new/"
                )
            ),
            "class"=>$_H->section=='mod_rewrite'?"dropdown active":""
            
        ),
        array(
            "text"  =>  _t('Contents'),
            "url"   =>  "/cms/pages/",
            "childs"=>array(
                array(
                    "text"  =>  _t('New content'),
                    "url"   =>  "/cms/pages/new/"
                )
            ),
            "class"=>$_H->section=='pages'?"dropdown active":""
        ),
        array(
            "text"  =>  _t('Tags'),
            "url"   =>  "/cms/tags/",
            "class"=>$_H->section=='tags'?"dropdown active":""
        ),
        array(
            "text"  =>  _t("File"),
            "url"   =>  "/cms/filemanager/",
            "class"=>$_H->section=='filemanager'?"dropdown active":""
        ),
        array(
            "text"  =>  _t('SEO'),
            "url"   =>  "/cms/seo/",
            "childs"=>array(
                array(
                    "text"  =>  _t('New SEO rule'),
                    "url"   =>  "/cms/seo/new/"
                )
            ),
            "class"=>$_H->section=='seo'?"dropdown active":""
        ),
        array(
            "text"  =>  _t('Exit'),
            "url"   =>  "/exit/",
            "childs"=>array()
        )
    );
    
    $this->data["menu"] = $menu->fetch();
    
    $structure = $_W->lib_load("html_tree_db");
    $structure->title   = "Древо";
    $structure->id      = "structure";
    $structure->table   = "tags";
    $structure->url     = '/cms/tags/$id/';
    $structure->active_id =intval($_H->action);
    
    $structure->add_callback = '
global $_H;
global $_W;

$ins_fields = array(
    "name"=>$value,
    "text"=>"",
    "publish_from"=>date("Y-m-d H:i:s"),
    "publish_to"=>"2037-12-31 23:59:59"
);
$page_id = $_D->insert("pages",$ins_fields);

$_D->update("'.$structure->table.'",array("folder"=>$_H->translit($value),"page_id"=>$page_id),array("id"=>$id));

$cms_pages = $_W->lib_load("cms_pages");
$chpu = $cms_pages->get_tag_chpu($id);
$real_url = "/tags/view/$id/";
$url_cond = array("to_url"=>$real_url);
$ins_fields = array(
    "from_url"=>$chpu,
    "to_url"=>$real_url,
    "status"=>200,
    "regex"=>"",
);
$_D->insert("mod_rewrite",$ins_fields,$url_cond);

';



    $structure->save_callback = '
global $_H;
global $_W;
$_D->update("'.$structure->table.'",array("folder"=>$_H->translit($value)),array("id"=>$id));
$_D->update("pages",array("name"=>$value),array("id"=>$record["page_id"]));

$cms_pages = $_W->lib_load("cms_pages");
$chpu = $cms_pages->get_tag_chpu($id);
$real_url = "/tags/view/$id/";
$url_cond = array("to_url"=>$real_url);
$ins_fields = array(
    "from_url"=>$chpu,
    "to_url"=>$real_url,
    "status"=>200,
    "regex"=>"",
);
if($_D->search_one("mod_rewrite",$url_cond))
    $_D->update("mod_rewrite",$ins_fields,$url_cond);
else
    $_D->insert("mod_rewrite",$ins_fields,$url_cond);
    

';

    $structure->delete_callback = '
global $_W;

$cms_pages = $_W->lib_load("cms_pages");
$real_url = "/tags/view/$id/";
$url_cond = array("to_url"=>$real_url);

$_D->delete("mod_rewrite",$url_cond);
if(isset($record["page_id"]) && intval($record["page_id"]))
    $_D->delete("pages",array("id"=>$record["page_id"]));
';
  
    $structure->move_callback = '
global $_W;
$cms_pages = $_W->lib_load("cms_pages");
$chpu = $cms_pages->get_tag_chpu($id);
$real_url = "/tags/view/$id/";
$url_cond = array("to_url"=>$real_url);
$ins_fields = array(
    "from_url"=>$chpu,
    "to_url"=>$real_url,
    "status"=>200,
    "regex"=>"",
);
if($_D->search_one("mod_rewrite",$url_cond))
    $_D->update("mod_rewrite",$ins_fields,$url_cond);
else
    $_D->insert("mod_rewrite",$ins_fields,$url_cond);
    
';


    
    $this->data["structure_tree"] = $structure->fetch();
