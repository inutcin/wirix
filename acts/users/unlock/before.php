<?php

    $user_id = intval($_SERVER['http']->action);
    
    $_SERVER['db']->update("users", array("locked"=>''), array("id"=>$user_id));
    
    if($_SERVER['HTTP_REFERER'])
        $_SERVER['http']->status("302", $_SERVER['HTTP_REFERER']);
    else
        $_SERVER['http']->status("302", "/users");
