<?php

    $input  = $_SERVER['wirix']->lib_load("html_input");
    $input->type='text';
    $input->title = 'Имя пользователя';
    $input->regexp = "/^.{1,128}$/";
    $input->comment = 'Имя пользователя - от 1 до 32 любых символов';
    $input->name = 'username';
    $input->value = isset($user_info['name'])?$user_info['name']:'';
    $input->default = '';
    $user_form->add_input($input);
    unset($input);

    $input  = $_SERVER['wirix']->lib_load("html_input");
    $input->type='password';
    $input->title = 'Пароль';
    $input->regexp = "/^.{0,32}$/";
    $input->comment = 'Пароль - от 1 до 32 любых символов. Если пароль менять не надо - оставьте это поле пустым';
    $input->name = 'password';
    $input->default = '';
    $user_form->add_input($input);
    unset($input);
    
    $input  = $_SERVER['wirix']->lib_load("html_input");
    $input->type='password';
    $input->title = 'Повтор пароля';
    $input->regexp = "/^.{0,32}$/";
    $input->comment = 'Повтор пароля. Должен совпадать с паролем';
    $input->name = 'repassword';
    $input->default = '';
    $user_form->add_input($input);
    unset($input);

    $input  = $_SERVER['wirix']->lib_load("html_input");
    $input->type='checkbox';
    $input->title = 'Заблокирован';
    $input->regexp = "/^.{0,32}$/";
    $input->comment = 'Если данный флаг установлен, пользователь не сможет войти';
    $input->name = 'locked';
    $input->value = isset($user_info['locked'])?$user_info['locked']:'';
    $input->default = '';
    $user_form->add_input($input);
    unset($input);

    $input  = $_SERVER['wirix']->lib_load("html_input");
    $input->value = $value;
    $input->values = $values;
    $input->type='set';
    $input->title = 'Группы';
    $input->regexp = "/^.*$/";
    $input->comment = 'Выберите группы, в которых должен состоть пользователь';
    $input->name = 'groups';
    $input->default = '';
    $user_form->add_input($input);
    unset($input);

?>
