<?php

    if(!$_SERVER['auth']->is_group(1))$_SERVER['http']->status(403);

    $user_form = $_SERVER['wirix']->lib_load("html_form");
    $user_form->id = "edit_user_form";
    $user_form->title = "Добавить пользователя";
    $user_form->send_button_text = 'Сохранить';
    $user_form->submit_url = "/wirix/acts/users/add/add.ajax.php";

    $value = '';
    
    $_SERVER['db']->search(array("a"=>"groups"),array(),array());
    $values = array();
    foreach($_SERVER['db']->rows as $row)$values[] = array("value"=>$row['id'],"title"=>$row['name']);

    include(dirname(__FILE__)."/../user.fields.php");
    $user_form->commit();

    $this->data['add_user_form'] = $user_form->fetch();
?>
