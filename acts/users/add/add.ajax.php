<?php
    include("../../../libs/wirix.class.php");
    
    
    $answer = array();
    $answer['error'] = '';
    $answer['redirect'] = '/users/';
    
    if(!$_SERVER['auth']->is_group(1))$_SERVER['http']->status(403);
    
    // Подключаем проверку полей
    include(dirname(__FILE__)."/../../../libs/h/html_form/check.ajax.php");
    
    $password = $_SERVER['http']->post('password');
    $user_id = intval($_SERVER['http']->post('user_id'));
    if($password!=$_SERVER['http']->post('repassword'))
        $answer['error'] = "Пароль и подтверждение не совпадают";
    
    // Проверка существования группы
    if(!$answer['error'] 
        && 
        $user_info = $_SERVER['auth']->user_exists_by_username($username = $_SERVER['http']->post('username')) 
    )
        $answer['error'] = "Пользователь <b>$username</b> уже существует";

    if(!$answer['error']){

        $user_id = $_SERVER['auth']->account_create(
            $username, 
            $password,
            $enter = false
        );

        $_SERVER['auth']->account_save(
            $username, 
            $password, 
            $_SERVER['http']->post('locked'), 
            $user_id
        );
        
        
        // Сохраняем группы пользователя
        $groups = explode(",",$_SERVER['http']->post('groups'));
        
        foreach($groups as $k=>$group)if(!trim($group))unset($groups[$k]);
        // Убираем бывшие группы
        $_SERVER['db']->delete("groups_of_users", array("user_id"=>$user_id), "", 0);
        // Добавляем снова
        foreach($groups as $group_id)$_SERVER['auth']->user_group_add($user_id, $group_id);
    }
    
    $answer['ok'] = 1;
    echo json_encode($answer);
?>
