<?php
    $this->breadcrumbs->add("Пользователи",'/users/');
    
    $users_table = $_SERVER['wirix']->lib_load("html_db_table");
    $users_table->title = 'Пользователи';
    
    if(!$_SERVER['auth']->is_group(1))$_SERVER['http']->status(403);
    
    // Если не указано конкретное действие - выводим таблицу пользователей
    if(!$_SERVER['http']->section){
        $users_table->tables = array("a"=>"users");
        $users_table->search();
    
        $column = array();
        $column['title']= 'ID';
        $column['text'] =   '"$row[id]"';
        $column['width']=   '10px';
        $users_table->add_column($column);
        unset($column);
    
        $column = array();
        $column['title']= ' ';
        $column['text'] =   '""';
        $column['icon'] =   '($row["locked"]?"icon-lock":"icon-unlock")';
        $column['url']  =   '($row["locked"]?"/users/unlock/".$row["id"]."/":"/users/lock/".$row["id"]."/")';
        $column['hint'] =   '($row["locked"]?"Разблокировать":"Заблокировать")';
        $column['width']=   '30px';
        $users_table->add_column($column);
        unset($column);


        $column = array();
        $column['title']= 'Пользователь';
        $column['text'] =   '"$row[name]"';
        $column['icon'] =   '"icon-user"';
        $column['url']  =   '"/users/edit/".$row["id"]."/"';
        $column['width']=   '250px';
        $users_table->add_column($column);
        unset($column);

        $column = array();
        $column['title']= 'Ф.И.О.';
        //$column['text'] =   '"$row[fio]"';
        $users_table->add_column($column);
        unset($column);


        $users_table->add_action_button(
            $name       =   'add',     //!< Имя кнопки
            $title      =   'Добавить пользователя',     //!< Текст кнопки 
            $hint       =   'Добавить пользователя',     //!< Всплываюша подсказка
            $icon       =   'icon-plus',     //!< Иконка кнопки
            $url        =   '/users/add/',     //!< URL кнопки
            $onclick    =   ''      //!< Событие на кнопке
        );


        $column['icon'] =   '"icon-pencil"';
        $column['hint'] =   '"Редактировать"';
        $column['url']  =   '"/companies/edit/".$row["id"]."/"';

    
        $this->data['users_table'] = $users_table->fetch();
    }
    
?>
