<?php

    if(!$_SERVER['auth']->is_group(1))$_SERVER['http']->status(403);

    $user_id = $_SERVER['http']->action?$_SERVER['http']->action:$_SERVER['auth']->user['id'];
    
    $_SERVER['db']->search_one("users", array("id"=>$user_id));
    $user_info = $_SERVER['db']->record;
    
    
    $user_form = $_SERVER['wirix']->lib_load("html_form");
    $user_form->id = "edit_user_form";
    $user_form->title = "Редактировать пользователя";
    $user_form->send_button_text = 'Сохранить';
    $user_form->submit_url = "/wirix/acts/users/edit/edit.ajax.php";

    $_SERVER['db']->search(array("a"=>"groups_of_users"),array(),array("user_id"=>$user_id));
    $value = '';
    foreach($_SERVER['db']->rows as $row)$value .= ",".$row['group_id'];
    
    $_SERVER['db']->search(array("a"=>"groups"),array(),array());
    $values = array();
    foreach($_SERVER['db']->rows as $row)$values[] = array("value"=>$row['id'],"title"=>$row['name']);

    // Подключаем поля формы
    include(dirname(__FILE__)."/../user.fields.php");

    // Передаём в форму id пльзователя
    $input  = $_SERVER['wirix']->lib_load("html_input");
    $input->value = $user_id;
    $input->type='hidden';
    $input->name = 'user_id';
    $user_form->add_input($input);
    unset($input);
    $user_form->commit();

    $this->data['edit_user_form'] = $user_form->fetch();
?>
