<?php

    if($_SERVER['auth']->is_auth()){
        $_H->status(302,"/cms/");
        die;
    }
    else{
        $this->data['auth_form'] = $_SERVER['auth']->fetch_login_form();
    }
