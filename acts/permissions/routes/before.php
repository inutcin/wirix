<?php

    $table = $_SERVER['wirix']->lib_load("html_table");
    $table->title="";

    $permissions = $_SERVER['wirix']->lib_load("permissions");
    
    $routes = $permissions->get_routes($_SERVER['http']->get('path'));
    $path = $_SERVER['http']->get('path')?$_SERVER['http']->get('path'):"/";
    
    
    $column = array(
        "title" =>  $path,
        "text"  =>  '"".$row["name"]',
        "url"   =>  '"/permissions/routes/?path=".$row["path"]'
    );
    $table->add_column($column);
    
    // Вычисляем предыдущий маршрут
    $prev_path = $path;
    $tmp = explode("/", $prev_path);
    array_pop($tmp);
    $prev_path = "/".implode("/", $tmp);
    $prev_path = preg_replace("/^\/\/(.*)$/", "/$1", $prev_path);
    
    $table->rows = $routes;
    
    $this->data['return_path'] = htmlspecialchars($prev_path);
    $this->data['path'] = htmlspecialchars($path);
    
    

    $this->data['permissions_table'] = $table->fetch();
