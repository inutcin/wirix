<?php

$table = $_SERVER['wirix']->lib_load('html_datascheme_table');
$table->id = 'locales_table';

$datascheme = $_SERVER['wirix']->init_datascheme('permission');
$datascheme->id = 'permission_datascheme';
$datascheme->set("path", $_SERVER['http']->get('path'));

$table->scheme = $datascheme;

$this->data['groups_table'] = $table->fetch();
$table->commit();

$this->data['return_path'] = htmlspecialchars($_SERVER['http']->get('path'));


