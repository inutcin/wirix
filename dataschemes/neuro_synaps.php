<?php

/**
    Схема данных, описывающая притерии нейроситей
*/
class datascheme_neuro_synaps extends wirix_datascheme_mysql{
    
    function init(){
        
        // Устанавливаем основную таблицу схемы
        $this->table = 'neuro_synapses';
        parent::init();
    }
}   
