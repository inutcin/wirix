<?php

/**
    Схема данных, описывающая разрешения на пути
*/
class datascheme_permission extends wirix_datascheme_mysql{
    
    function init(){
        
        // Устанавливаем основную таблицу схемы
        $this->table = 'permissions';
        parent::init();
        
        $this->set_enabled_fields(array("id", "path","group_id", "access", "order"));
        $this->set_hidden_fields(array("id"));
        $this->sort="`order`";
        $this->order="ASC";

        // Связываем с таблицой языков
        $this->set_join(
            "groups", 
            'group_id', 
            'id', 
            'name'
        );
        
    }


}   
