<?php

/**
    Схема данных, описывающая притерии нейроситей
*/
class datascheme_neuro_reaction extends wirix_datascheme_mysql{
    
    function init(){
        
        // Устанавливаем основную таблицу схемы
        $this->table = 'neuro_reactions';
        parent::init();

        $this->set_enabled_fields(array("id", "net_id", "name"));
        $this->set_hidden_fields(array("id"));

        $this->set_join(
            "neuro_nets",
            "net_id",
            "id",
            "name"
        );        
    }
}   
