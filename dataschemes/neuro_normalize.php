<?php

/**
*/
class datascheme_neuro_normalize extends wirix_datascheme_mysql{
    
    function init(){
        
        // Устанавливаем основную таблицу схемы
        $this->table = 'neuro_normalize';
        parent::init();

        $this->set_enabled_fields(array("id", "net_id", "criteria_id", "from", "to", "level"));
        $this->set_hidden_fields(array("id"));


        $this->set_join(
            "neuro_nets",
            "net_id",
            "id",
            "name"
        );        

        $this->set_join(
            "neuro_criteria",
            "criteria_id",
            "id",
            "name"
        );        


    }
}   
