<?php

/**
    Схема данных, описывающая строки перевода
*/
class datascheme_locale extends wirix_datascheme_mysql{
    
    function init(){
        
        // Устанавливаем основную таблицу схемы
        $this->table = 'locales';
        parent::init();
        
        $this->set_enabled_fields(array("id", "msgid","msgstr", "lang"));
        $this->set_hidden_fields(array("id"));
        

        // Связываем с таблицой языков
        $this->set_join(
            "locale_langs", 
            'lang', 
            'lang', 
            'name',
            array('name','country')
        );
    }


}   
