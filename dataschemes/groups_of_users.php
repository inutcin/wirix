<?php

/**
    Схема данных, описывающая принадлежность пользователей группам
*/
class datascheme_groups_of_users extends wirix_datascheme_mysql{
    
    function init(){
        
        // Устанавливаем основную таблицу схемы
        $this->table = 'groups_of_users';
        parent::init();
        
        $this->set_enabled_fields(array("user_id","group_id"));
        

        // Связываем с таблицой групп
        $this->set_join(
            "groups", 
            'group_id', 
            'id', 
            'name',
            array('name')
        );
        
        // Связываем с таблицой параметров пользователей (надо выбрать ФИО)
        // а это - user_prop_id=2
        $this->set_join(
            "user_prop_values", 
            'user_id', 
            'user_id', 
            'value',
            array('value',"user_prop_id"),
            'user_prop_id=2'
        );
        
        // Устанавливаем кастомные комменты и описания для полей
        $this->set_comment("user_id", "Пользователь");
        $this->set_description("user_id", "Пользователь, принадлежащий группе");

        $this->set_comment("group_id", "Группа");
        $this->set_description("group_id", "Группа, к которой принадлежит пользователь");
    }


}   
