<?php

/**
    Схема данных, описывающая притерии нейроситей
*/
class datascheme_neuro_criteria extends wirix_datascheme_mysql{
    
    function init(){
        
        // Устанавливаем основную таблицу схемы
        $this->table = 'neuro_criteria';
        parent::init();

        $this->set_enabled_fields(array("id", "net_id", "name","enabled"));
        $this->set_hidden_fields(array("id"));

        $this->set_join(
            "neuro_nets",
            "net_id",
            "id",
            "name"
        );        

    }
}   
