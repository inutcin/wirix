<?php

/**
    Схема данных, описывающая принадлежность пользователей группам
*/
class datascheme_locale_langs extends wirix_datascheme_mysql{
    
    function init(){
        
        // Устанавливаем основную таблицу схемы
        $this->table = 'locale_langs';
        parent::init();
        
        $this->set_enabled_fields(array("id", "lang","name", "country"));
        $this->set_hidden_fields(array("id"));
        

    }


}   
