<?php

/**
    Схема данных, описывающая нейросети
*/
class datascheme_neuro_net extends wirix_datascheme_mysql{
    
    function init(){
        
        // Устанавливаем основную таблицу схемы
        $this->table = 'neuro_nets';
        parent::init();
        
        $this->set_enabled_fields(array("name"));
    }
}   
