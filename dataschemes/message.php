<?php

/**
    Схема данных, описывающая сообщения
*/
class datascheme_message extends wirix_datascheme_mysql{
    
    function init(){
        
        // Устанавливаем основную таблицу схемы
        $this->table = 'messages';
        parent::init();
        
        $this->set_enabled_fields(array("id", "from","to", "read_date", "reply_id", "deleted"));
        $this->set_hidden_fields(array("id"));
        $this->sort="`a`.`ctime`";
        $this->order="DESC";
        
    }


}   
