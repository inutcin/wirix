<?php
    include_once(dirname(__FILE__)."/../../wirix.class.php");

    /**
     * Класс обработки файлов fastreport
     */
    class wirix_fastreport extends wirix{
        
        var $DOM = '';
        
        var $tmpl = array(); //!< Шалон формы
        
        var $script = '';
        
        var $resolution_x = 96; //!< Разрешение по горизонтали
        var $resulution_y = 96; //!< Разрешение по вертикали
        var $round_precision = 0; //!< Точность округления
        var $included_fonts = array('Arial','Tahoma');

        var $report_pages = array(); //!< Загруженные страницы отчеты
        var $dataset = array();
        var $grouping = array();
        
        var $cols_coords=array(); // Информации о ячейках макетной таблицы
        var $rows_coords=array(); // Информации о ячейках макетной таблицы
        var $blocks_index = array();
        
        var $cols_width=array(); // Информации о ячейках макетной таблицы
        var $rows_height=array(); // Информации о ячейках макетной таблицы
        
        var $cols = array();
        var $rows = array();
        
        var $grid = false;  // Показывать сетку разметки
        var $PaperHeight = 0;
        var $PaperWidth = 0;
        
        function __construct(){
            parent::__construct(__CLASS__);
        }

        /**
            Построение xls-отчета
        */
        function fetch_xls($pagename=0,$data = array(),$dataset=array(),$output_filename){
            global $_H;
            global $_W;
            $this->create_grid($pagename,$data,$dataset);
            $this->create_cells($pagename,$data,$dataset);
            

            $sheet = 'Лист1';
            
            $excel = $_W->lib_load("excel");
            foreach($this->cols as $col_num=>$col)
                $excel->set_width($sheet, $this->get_col_name($col_num), $col['width']/9);
                
            foreach($this->rows as $row_num=>$row)
                $excel->set_height($sheet, $row_num+1, $row['height']/1.4);


            $borders = array();
            $empty_cells = array();
            foreach($this->cells as $row_num=>$table_row){
                foreach($table_row as $col_num=>$cell){
                    if(isset($cell["item"]["Name"]) && $cell["colspan"]>0 && $cell["rowspan"]>0){
                        $item = $this->report_pages[$pagename]["items"][
                                    $this->cells[$row_num][$col_num]["item"]["block"]
                                ]["items"][
                                    $this->cells[$row_num][$col_num]["item"]["Name"]
                                ]['attrs'];
                                
                        $excel_cell_address = $this->get_col_name($col_num).($row_num+1);
                            
                        $excel->set_data(
                            $sheet,
                            $excel_cell_address,
                            $this->data_substitute($item["Text"],$data,$dataset,"xls")
                        );

                        if(isset($item["src"]) && $item["src"] && isset($item["position"]) && $item["position"]=='table'){
                            $excel->add_image(
                                $sheet,$item["src"],
                                $excel_cell_address,
                                0,
                                0,
                                $item['Width'],
                                $item['Height']
                            );
                        }

                        $excel->add_merge(
                            $sheet,
                            $this->get_col_name($col_num).($row_num+1)
                            .":"
                            .$this->get_col_name($col_num+$cell["colspan"]-1).($row_num+1+$cell["rowspan"]-1)
                        );
                        
                        if(isset($item['HAlign']))
                            $excel->set_halign($sheet,$excel_cell_address,$this->xls_halign($item['HAlign']));

                        if(isset($item['VAlign']))
                            $excel->set_valign($sheet,$excel_cell_address,$this->xls_valign($item['VAlign']));

                        if(isset($item['Color']))
                            $excel->set_fill($sheet,$excel_cell_address,$this->xls_bg_color($item['Color']));

                        if(isset($item['Font.Height']))
                            $excel->set_fontsize($sheet,$excel_cell_address,abs($item['Font.Height'])-4);

                        if(isset($item['Font.Name']))
                            $excel->set_fontface($sheet,$excel_cell_address,-$item['Font.Name']);
                        
                        if(isset($item['Font.Style']) && $item['Font.Style'])
                            $excel->set_fontbold($sheet,$excel_cell_address,true);
                        
                        if(isset($item['Frame.Typ']))
                            $excel->set_borders($sheet,$excel_cell_address,$this->xls_frame($item['Frame.Typ']));
                            
                        $excel->set_wrap($sheet,$excel_cell_address,true);

                        // Дополняем бордеры снизу
                        if(isset($item['Frame.Typ']) && $item['Frame.Typ']&8 && $cell["rowspan"]>1){
                            for($i=$col_num,$c=$col_num+$cell["colspan"];$i<$c;$i++){
                                $cell_address = $this->get_col_name($i).($row_num+$cell["rowspan"]);
                                if(!isset($borders[$cell_address]))$borders[$cell_address] =array();
                                if(!isset($empty_cells[$cell_address]))$empty_cells[$cell_address] ='';
                                $borders[$cell_address]["bottom"] = '000000';
                            }                        
                            
                        }

                        // Дополняем бордеры справа
                        if(isset($item['Frame.Typ']) && $item['Frame.Typ']&2 && $cell["colspan"]>1){
                            for($i=$row_num,$c=$row_num+$cell["rowspan"];$i<$c;$i++){
                                $cell_address = $this->get_col_name($col_num+$cell['colspan']-1).($i+1);
                                if(!isset($borders[$cell_address]))$borders[$cell_address] =array();
                                if(!isset($empty_cells[$cell_address]))$empty_cells[$cell_address] ='';
                                $borders[$cell_address]["right"] = '000000';
                            }                        
                            
                        }

                        /*
                        // Дополняем бордеры сверху
                        if(isset($item['Frame.Typ']) && $item['Frame.Typ']&4 && $cell["rowspan"]>1){
                            for($i=$col_num,$c=$col_num+$cell["colspan"];$i<$c;$i++){
                                $cell_address = $this->get_col_name($i).($row_num+1);
                                if(!isset($borders[$cell_address]))$borders[$cell_address] =array();
                                if(!isset($empty_cells[$cell_address]))$empty_cells[$cell_address] ='';
                                $borders[$cell_address]["top"] = '000000';
                            }                        
                            
                        }


                        // Дополняем бордеры слева
                        if(isset($item['Frame.Typ']) && $item['Frame.Typ']&1 && $cell["colspan"]>1){
                            for($i=$row_num,$c=$row_num+$cell["rowspan"];$i<$c;$i++){
                                $cell_address = $this->get_col_name($col_num).($i);
                                if(!isset($borders[$cell_address]))$borders[$cell_address] =array();
                                if(!isset($empty_cells[$cell_address]))$empty_cells[$cell_address] ='';
                                $borders[$cell_address]["left"] = '000000';
                            }                        
                            
                        }
                        */

                    }
                }
            }

            foreach($borders as $cell_name=>$border){
                if($border){
                    $excel->set_borders($sheet,$cell_name,$border);
                    $excel->set_data($sheet,$cell_name,' ');
                }
                
            }

            $excel->filename = $output_filename;
            $excel->output();
            die;
            return false;
        }
        
        private function get_col_name($col_num){
            $result = '';
            $alphabet = array("A","B","C","D","E","F","G","H","I","J","K","L",
                "M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
            
            $result = $alphabet[$col_num % 26];
            if(floor($col_num/26))
                $result = $alphabet[floor($col_num / 26)-1].$result;
            
            
            return $result;
        }

        
        /**
            Построение html-отчета
        */
        function fetch_html($pagename=0,$data = array(),$dataset=array()){
            $this->create_grid($pagename,$data,$dataset);
            $this->create_cells($pagename,$data,$dataset);
            
            $html = '<style>';
            $html .= "\n".'.pagebreak{page-break-after: always;}';
            $html .= "\n".'body{';
            $html .= "\n".'margin: 0px;';
            $html .= "\n".'}';
            $html .= "\n".'table.reportpage{border-collapse: collapse;';
            $html .= 'margin: 0px;';
            $html .= '}';

            $html .= "\n".'table.reportpage td{border:1px '.($this->grid?"#CCC":"transparent").' dashed; ';
            $html .= '}';
            $html .= "\n".'table.reportpage td img{width:100%;';
            $html .= '}';
            $html .= "\n\n".'.page table.item:hover{background-color: #EFEFEF;}';
            $html .= "\n\n".'table {border-collapse:collapse;}';
            $html .= "\n\n".'.item td{display:table-cell;}';
            foreach($this->included_fonts as $fontname){
                $html.="\n\t".'@font-face{';
                $html.="\n\t\t".'font-family:'.$fontname.';';
                $html.="\n\t\t".'src:url(http://'.$_SERVER["HTTP_HOST"].'/wirix/libs/f/fastreport/fonts/'.$fontname.'.ttf);';
                $html.="\n\t\t".'font-style: normal;';
                $html.="\n\t\t".'font-weight: normal;';
                $html.="\n\t".'}';
            }

            $html .= "\n".'body,html{';
            if(isset($this->report_pages[$pagename]["attrs"]["PaperWidth"]))
                $html .= 'width:'.$this->tofloat($this->report_pages[$pagename]["attrs"]["PaperWidth"]).'mm;';
            //if(isset($this->report_pages[$pagename]["attrs"]["PaperHeight"]))
            //    $html .= 'height:'.$this->tofloat($this->report_pages[$pagename]["attrs"]["PaperHeight"]).'mm;';
            $html .= 
                isset($this->report_pages[$pagename]["attrs"]["FontName"])
                ?
                "font-family: ".$this->report_pages[$pagename]["attrs"]["FontName"].";"
                :
                "font-family: Arial;";
                
            $html .= 
                isset($this->report_pages[$pagename]["attrs"]['Font.Height'])
                ?
                'font-size:'.(abs($this->report_pages[$pagename]["attrs"]['Font.Height'])).';'
                :
                'font-size:10px;';
                
            $html .= '}';
            

            $html .= "\n".'</style>'."\n";
            
            // Шлёпаем картинки не привязанные к таблице
            foreach($this->report_pages[$pagename]["items"] as $blockname=>$block)
                foreach($block["items"] as $itemname=>$item)
                    if(
                        ($item["tagName"]=='TfrxPictureView')
                        && (!isset($item["attrs"]["position"]) || $item["attrs"]["position"]!='table')
                    ){
                        $html .= '<img';
                        if($item["attrs"]['src'])
                            $html.= ' src="'.$item["attrs"]['src'].'"';
                        $html .= ' style="position:absolute;';
                        if($item["attrs"]["Top"])
                            $html .= 'top:'.(
                                $this->tofloat($item["attrs"]["Top"])
                                +
                                $this->tofloat($block["attrs"]["Top"])
                                +
                                $this->tofloat($this->report_pages[$pagename]["attrs"]["TopMargin"])/0.265
                            ).'px;';
                        if($item["attrs"]["Left"])
                            $html .= 'left:'.(
                                $this->tofloat($item["attrs"]["Left"])
                                +
                                $this->tofloat($block["attrs"]["Left"])
                                +
                                $this->tofloat($this->report_pages[$pagename]["attrs"]["LeftMargin"])/0.265
                            ).'px;';
                        $html .= '">';
                    }

            $html .= '<div class="pagebreak" style="';
            $this->PaperHeight = $this->tofloat($this->report_pages[$pagename]["attrs"]["PaperHeight"])/0.264-3;
            $this->PaperWidth = $this->tofloat($this->report_pages[$pagename]["attrs"]["PaperWidth"])/0.264+2;
            $html .= 'height:'.$this->PaperHeight.'px;';
            $html .= 'width:'.$this->PaperWidth.'px;';
            if(isset($this->report_pages[$pagename]["attrs"]["LeftMargin"]))
                $html .= 'padding-left: '.$this->tofloat($this->report_pages[$pagename]["attrs"]["LeftMargin"])/(0.266)/(1).'px;';
            if(isset($this->report_pages[$pagename]["attrs"]["TopMargin"]))
                $html .= 'padding-top: '.$this->tofloat($this->report_pages[$pagename]["attrs"]["TopMargin"])/(0.266)/(1).'px;';
            if(isset($this->report_pages[$pagename]["attrs"]["BottomMargin"]))
                $html .= 'padding-bottom: '.$this->tofloat($this->report_pages[$pagename]["attrs"]["BottomMargin"])/(0.266)/(1).'px;';
            if(isset($this->report_pages[$pagename]["attrs"]["RightMargin"]))
                $html .= 'padding-right: '.$this->tofloat($this->report_pages[$pagename]["attrs"]["RightMargin"])/(0.266)/(1).'px;';
            $html .= '">';
            $html .= '<table class="reportpage" border="0" style="';
            
            $html .= '">';
            for($i=0,$ii=count($this->rows);$i<$ii;$i++){
                $html .= '<tr>';
                $html .= '<th style="height:'.$this->rows[$i]["height"].'px;"></th>';
                for($j=0,$jj=count($this->cols);$j<$jj;$j++){
                    if(
                        1
                        &&
                        (
                            (isset($this->cells[$i][$j]["colspan"]) && $this->cells[$i][$j]["colspan"]==0)
                            ||
                            (isset($this->cells[$i][$j]["rowspan"]) && $this->cells[$i][$j]["rowspan"]==0)
                        )
                    )
                        continue;
                    $html .= '<td';
                    
                    if( 
                        1
                        &&
                        (isset($this->cells[$i][$j]["colspan"]) && $this->cells[$i][$j]["colspan"]>=1)
                        &&
                        (isset($this->cells[$i][$j]["rowspan"]) && $this->cells[$i][$j]["rowspan"]>=1)
                    ){
                        $html .= ' colspan="'.$this->cells[$i][$j]["colspan"].'" rowspan="'.$this->cells[$i][$j]["rowspan"].'"';
                    }

                    $html .= ' style="';
                    
                    if($i==0)$html .= 'width:'.$this->cols[$j]["width"].'px;';

                    if(
                        isset($this->cells[$i][$j]['item'])
                        &&
                        isset(
                            $this->report_pages[$pagename]["items"]
                            [$this->cells[$i][$j]['item']['block']]['items']
                            [$this->cells[$i][$j]['item']['item']]["attrs"]["Text"]
                    )){
                        $item = $this->report_pages[$pagename]["items"]
                            [$this->cells[$i][$j]['item']['block']]['items']
                            [$this->cells[$i][$j]['item']['item']]["attrs"];
                            
                        if(isset($item['Font.Height']))$html .= 'font-size:'.(abs($item['Font.Height'])).';';
                        if(isset($item['Font.Name']))$html .= 'font-family:'.$item['Font.Name'].';';
                        if(isset($item['Font.Style']))$html .= $this->html_font_style($item['Font.Style']);
                        if(isset($item['Color']))$html .= $this->html_bg_color($item['Color']);
                        if(isset($item['HAlign']))$html .= $this->html_halign($item['HAlign']);
                        if(isset($item['VAlign']))$html .= $this->html_valign($item['VAlign']);
                        if(isset($item['Frame.Typ']))$html .= $this->html_frame($item['Frame.Typ']);
                        //echo "<pre>";
                        //print_r($item);
                        //echo "</pre>";

                        if($this->grid)
                            $html .= '" title="'.$this->report_pages[$pagename]["items"]
                                [$this->cells[$i][$j]['item']['block']]['items']
                                [$this->cells[$i][$j]['item']['item']]["attrs"]["Name"].'';
                    }
                    
                    $html .= '" ';
                    $html .= '>';
                    $html .= 
                        isset(
                            $this->cells[$i][$j]['item']['block']
                        )
                        &&
                        isset(
                            $this->report_pages[$pagename]["items"]
                            [$this->cells[$i][$j]['item']['block']]['items']
                            [$this->cells[$i][$j]['item']['item']]["attrs"]["Text"]
                        )
                        ?
                        $this->data_substitute($this->report_pages[$pagename]["items"]
                            [$this->cells[$i][$j]['item']['block']]['items']
                            [$this->cells[$i][$j]['item']['item']]["attrs"]["Text"],
                            $data,
                            $dataset
                        )
                        :
                        ''
                    ;
                    
                    if(
                        isset(
                            $this->cells[$i][$j]['item']['block']
                        )
                        &&
                        (
                        $this->report_pages[$pagename]["items"]
                            [$this->cells[$i][$j]['item']['block']]['items']
                            [$this->cells[$i][$j]['item']['item']]['tagName']=="TfrxPictureView"
                        )
                        &&
                        isset($this->report_pages[$pagename]["items"]
                            [$this->cells[$i][$j]['item']['block']]['items']
                            [$this->cells[$i][$j]['item']['item']]["attrs"]["src"])
                        &&
                        $this->report_pages[$pagename]["items"]
                            [$this->cells[$i][$j]['item']['block']]['items']
                            [$this->cells[$i][$j]['item']['item']]["attrs"]["src"]
                        &&
                        $this->report_pages[$pagename]["items"]
                            [$this->cells[$i][$j]['item']['block']]['items']
                            [$this->cells[$i][$j]['item']['item']]["attrs"]["position"] == 'table'
                    ){
                        $html .= '<img src="'.$this->report_pages[$pagename]["items"]
                            [$this->cells[$i][$j]['item']['block']]['items']
                            [$this->cells[$i][$j]['item']['item']]["attrs"]["src"];
                        $html .= '" style="';
                        if(isset($this->report_pages[$pagename]["items"]
                            [$this->cells[$i][$j]['item']['block']]['items']
                            [$this->cells[$i][$j]['item']['item']]["attrs"]['Width']))
                            $html .= "width:".$this->report_pages[$pagename]["items"]
                            [$this->cells[$i][$j]['item']['block']]['items']
                            [$this->cells[$i][$j]['item']['item']]["attrs"]["Width"]."px;";
                        if(isset($this->report_pages[$pagename]["items"]
                            [$this->cells[$i][$j]['item']['block']]['items']
                            [$this->cells[$i][$j]['item']['item']]["attrs"]['Height']))
                            $html .= "height:".$this->report_pages[$pagename]["items"]
                            [$this->cells[$i][$j]['item']['block']]['items']
                            [$this->cells[$i][$j]['item']['item']]["attrs"]["Height"]."px";
                        $html .= '">';
                    }
                    
                    
                    //$html .= isset($this->cells[$i][$j]["item"]["Name"])?"[".$this->cells[$i][$j]["item"]["Name"]."]":'';
                    //$html .= '<span style="font-size:7px;">'.$this->cols[$j]["width"].'</span>';
                    $html .='</td>';
                }
                $html .= '</tr>';
            }
            $html .= '</table>';
            $html .= '</div>';
            return $html;
        }
        
        
        /*
            Подстановка данных
        */
        function data_substitute($input, $data, $dataset, $format = "html"){
            
            if($format == "html")$input = str_replace("\n","<br/>",$input);
            
            if($format == "xls")$input = str_replace("<b>","",$input);
            if($format == "xls")$input = str_replace("</b>","",$input);
            if($format == "xls")$input = str_replace("<br>","\n",$input);
            if($format == "xls")$input = str_replace("<br/>","\n",$input);
            
            foreach($data as $k=>$v)
                $input = str_replace("$k",$v,$input);
            
            foreach($dataset as $index=>$data)
                foreach($data as $k=>$v){
                    $input = str_replace("$k"."[$index]",$v,$input);
                }

            return $input;
        }
        
        /**
         * Построение ячеек таблицы
        */
        function create_cells($pagename,$data,$dataset){
            $this->cells = array();

            foreach($this->rows as $rownum=>$row){
                if(isset($this->cells[$rownum]))
                    $this->cells[$rownum] = array();
                foreach($this->cols as $colnum=>$col)
                    $this->cells[$rownum][$colnum] = array();
                    
            }

            
            foreach($this->report_pages[$pagename]["items"] as $blockname => $block){
                foreach($block["items"] as $item_name=>$item){
                    if(($item["tagName"]=='TfrxPictureView') && (!isset($item["attrs"]["position"]) || $item["attrs"]["position"]!='table'))
                        continue;
                    if(isset($item["attrs"]["Visible"]) && $item["attrs"]["Visible"]=='False')
                        continue;
                    //if($item_name!='Memo2[0]')continue;
                    $block_top = $block["attrs"]["Top"];
                    $block_left = $block["attrs"]["Left"];

                    $left   = round($item["attrs"]["Left"]+$block_left, $this->round_precision);
                    $top    = round($item["attrs"]["Top"]+$block_top, $this->round_precision);
                    $right  = round($item["attrs"]["Left"]+$item["attrs"]["Width"]+$block_left, $this->round_precision);
                    $bottom = round($item["attrs"]["Top"]+$item["attrs"]["Height"]+$block_top, $this->round_precision);
                    
                    // Находим границы ячеек для элемента
                    for($i=0,$c=count($this->cols);$i<$c;$i++)
                        if(
                            $left>=$this->cols[$i]['start'] 
                            && 
                            $left<($this->cols[$i]['start']+$this->cols[$i]['width'])
                        )
                            break;
                    $col_start = $i;
                    for($i=$col_start,$c=count($this->cols);$i<$c;$i++)
                        if(
                            $right>$this->cols[$i]['start']
                            && 
                            $right<=($this->cols[$i]['start']+$this->cols[$i]['width'])
                        )
                            break;
                    $col_end = $i;
                    
                    for($i=0,$c=count($this->rows);$i<$c;$i++)
                        if(
                            $top>=$this->rows[$i]['start']
                            && 
                            $top<($this->rows[$i]['start']+$this->rows[$i]['height'])
                        )
                            break;
                    $row_start = $i;
                    for($i=$row_start,$c=count($this->rows);$i<$c;$i++)
                        if(
                            $bottom>$this->rows[$i]['start']
                            && 
                            $bottom<=($this->rows[$i]['start']+$this->rows[$i]['height'])
                        )
                            break;
                    $row_end = $i;
                    
                    //заполняем ячеек пространство информацией об элементе
                    for($rownum=$row_start;$rownum<=$row_end;$rownum++)
                        for($colnum=$col_start;$colnum<=$col_end;$colnum++)
                            $this->cells[$rownum][$colnum]["item"] = array(
                                "block" =>  $blockname,
                                "item"  =>  $item_name,
                                "Top"   =>  $item["attrs"]["Top"],
                                "Height"   =>  $item["attrs"]["Height"],
                                "Left"   =>  $item["attrs"]["Left"],
                                "Width"   =>  $item["attrs"]["Width"],
                                "Name"  =>  $item_name
                            );
                    
                }
            }
            
            /**
             * Отмечаем объединённые ячейки по горизонтали
             */
            for($i=0,$ii=count($this->rows);$i<$ii;$i++){
                for($j=0,$jj=count($this->cols);$j<$jj;$j++){
                    if(isset($this->cells[$i][$j]["item"])){
                        $this->cells[$i][$j]["colspan"] = 1;
                        for($k=$j+1;$k<=$jj;$k++){
                            if($k==$jj){
                                $j=$k-1;
                                break;
                            }
                            elseif(
                                isset($this->cells[$i][$k]["item"]["Name"]) 
                                && 
                                $this->cells[$i][$k]["item"]["Name"]==$this->cells[$i][$j]["item"]["Name"]
                            ){
                                $this->cells[$i][$k]["colspan"] = 0;
                                $this->cells[$i][$j]["colspan"]++;
                            }
                            else{
                                $j=$k-1;
                                break;
                            }
                        }
                    }
                }
            }
            
            /**
             * Отмечаем объединённые ячейки по вертикали
             */
            for($j=0,$jj=count($this->cols);$j<$jj;$j++){
                for($i=0,$ii=count($this->rows);$i<$ii;$i++){
                    if(isset($this->cells[$i][$j]["item"])){
                        $this->cells[$i][$j]["rowspan"] = 1;
                        for($k=$i+1;$k<=$ii;$k++){
                            if($k==$ii){
                                $i=$k-1;
                                break;
                            }
                            elseif(
                                isset($this->cells[$k][$j]["item"]["Name"]) 
                                && 
                                $this->cells[$k][$j]["item"]["Name"]==$this->cells[$i][$j]["item"]["Name"]
                            ){
                                $this->cells[$k][$j]["rowspan"] = 0;
                                $this->cells[$i][$j]["rowspan"]++;
                            }
                            else{
                                $i=$k-1;
                                break;
                            }
                        }
                    }
                }
            }
            //echo "<pre>";
            //print_r($this->cells);
            //echo "</pre>";
            //die;
        }
        
        /**
            Построение сетки таблицы
        */
        function create_grid($pagename=0,$data = array(),$dataset=array()){
            $this->cols_from=array(); // Информации о ячейках макетной таблицы
            foreach($this->report_pages[$pagename]["items"] as $blockname => $block){
                foreach($block["items"] as $item_name=>$item){
                    // Убираем невидимые элементы
                    if(isset($item["attrs"]["Visible"]) && $item["attrs"]["Visible"]=='False')
                        continue;
                    // Определяем высоту блока
                    $block_top = round($block["attrs"]["Top"], $this->round_precision);
                    $block_left = round($block["attrs"]["Left"], $this->round_precision);
                    
                    // Намечаем координаты границ столбцов
                    $this->cols_coords[round($item["attrs"]["Left"]+$block_left, $this->round_precision)] = 1;
                    $this->cols_coords[round($item["attrs"]["Left"]+$item["attrs"]["Width"]+$block_left, $this->round_precision)] = 2;
                    // Намечаем координаты границ строк
                    $this->rows_coords[round($item["attrs"]["Top"]+$block_top, $this->round_precision)] = 3;
                    $this->rows_coords[round($item["attrs"]["Top"]+$item["attrs"]["Height"]+$block_top, $this->round_precision)] = 4;
                }
            }
            ksort($this->cols_coords);
            ksort($this->rows_coords);
            /*
            echo "<pre>";
            print_r($this->rows_coords);
            die;
            */

            // Составляем список координатной сетки по строкам
            $n = 0;
            $tmp = array();
            foreach($this->rows_coords as $k=>$v){$tmp[$k] = $n;$n++;}
            $this->rows_coords = $tmp;
            
            // Составляем список координатной сетки по столбцам
            $n = 0;
            $tmp = array();
            foreach($this->cols_coords as $k=>$v){$tmp[$k] = $n;$n++;}
            $this->cols_coords = $tmp;
            unset($tmp);
            
            // Формируем спсок столбцов с координатой начала и шириной
            $this->cols = array();
            $prev = 0;
            $nums = 0;
            foreach($this->cols_coords as $coord=>$num){
                if($coord==0)continue;
                $this->cols[$nums] = array(
                    "start"=>round($prev, $this->round_precision),
                    "width"=>round($coord-$prev, $this->round_precision)
                );
                $prev = $coord;
                $nums++;
            }
            /*
            echo "<pre>";
            print_r($this->cols);
            die;
            */

            $this->rows = array();
            $prev = 0;
            $nums = 0;
            foreach($this->rows_coords as $coord=>$num){
                if($coord==0)continue;
                $this->rows[$nums] = array(
                    "start"=>round($prev, $this->round_precision),
                    "height"=>round($coord-$prev, $this->round_precision)
                );
                $prev = $coord;
                $nums++;
            }
        }
        
        /**
            Загрузка и парсинг файла шаблона отчета
        */
        
        function load($filename,$pagename=0,$data=array(),$dataset=array(),$coeff=0.7){
            if(!file_exists($filename)){
                $this->error = _t('file not found');
                return false;
            }
            $data = file_get_contents($filename);
            $data = str_replace("\n","{break}",$data);
            if(preg_match("# ScriptText.Text=\"(.*?)\"#i",$data,$m)){
                $data = preg_replace("# ScriptText.Text=\"(.*?)\"#i", "",$data);
                $this->script = mb_convert_encoding($m[1],"utf-8","cp1251");
                $this->script = html_entity_decode($this->script);
            }

            $tags_enc = array(
                "# Caption=\"(.*?)\"#i",
                "# DataSetName=\"(.*?)\"#i",
                "# Items.Text=\"(.*?)\"#i",
                "# PrintOptions.Printer=\"(.*?)\"#i"
            );
            
            foreach($tags_enc as $tag)
                if(preg_match_all($tag, $data, $m)){
                    rsort($m[1]);
                    foreach($m[1] as $text)
                        $data = str_replace($text,mb_convert_encoding($text,"utf-8","cp1251"),$data);
                }
            if(preg_match_all("#\[(.*?)\]#",$data,$m)){
                //echo "<pre>";
                //print_r($m);
                //echo "</pre>";
            }
            $data = str_replace("{break}","\n",$data);
            
            $this->DOM = new DOMDocument();
            $this->DOM->loadXML($data);
            $report_pages = $this->DOM->getElementsByTagName("TfrxReportPage");
            
            for($i=0,$c=$report_pages->length;$i<$c;$i++){
                $page = $report_pages->item($i);
                $attrs = $page->attributes;
                
                $tmp = array("attrs"=>array(),"items"=>array());
                for($j=0;$j<$attrs->length;$j++){
                    $tmp["attrs"][$attrs->item($j)->name] = $attrs->item($j)->value;
                    if($attrs->item($j)->name=='Name')
                        $page_name = $attrs->item($j)->value;
                }

                for($j=0;$j<$page->childNodes->length;$j++){
                    $page_child = array("attrs"=>array(),"items"=>array());
                    $attrs = $page->childNodes->item($j)->attributes;
                    if(!$attrs)continue;
                    
                    $page_child["tagName"] = $page->childNodes->item($j)->tagName;
                    for($k=0;$k<$attrs->length;$k++){
                        $page_child["attrs"][$attrs->item($k)->name] = $attrs->item($k)->value;
                        if($attrs->item($k)->name=='Name')
                            $block_name = $attrs->item($k)->value;
                    }
                    
                    
                    for($k=0;$k<$page->childNodes->item($j)->childNodes->length;$k++){
                        $page_child_item = array("attrs"=>array());
                        $attrs = $page->childNodes->item($j)->childNodes->item($k)->attributes;
                        if(!$attrs)continue;
                        
                        $page_child_item["tagName"] = $page->childNodes->item($j)->childNodes->item($k)->tagName;
                        for($l=0;$l<$attrs->length;$l++){
                            $page_child_item["attrs"][$attrs->item($l)->name] = $attrs->item($l)->value;
                            
                            if(
                                $attrs->item($l)->name == 'Left'
                                ||
                                $attrs->item($l)->name == 'Top'
                                ||
                                $attrs->item($l)->name == 'Width'
                                ||
                                $attrs->item($l)->name == 'Height'
                            ){
                                $page_child_item["attrs"][$attrs->item($l)->name] = 
                                    $this->tofloat($attrs->item($l)->value);
                            }
                            
                            if($attrs->item($l)->name=='Name')
                                $item_name = $attrs->item($l)->value;
/*
                            if($attrs->item($l)->name=='Font.Height')
                                $page_child_item["attrs"][$attrs->item($l)->name] += 2;
*/

                        }
                        $page_child["items"][$item_name] = $page_child_item;
                    }
                    
                    $tmp["items"][$block_name] = $page_child;
                }
                
            
                foreach($tmp['items'] as $blockname=>$block){
                    if($block['tagName']!='TfrxGroupHeader')continue;
                    /*
                    echo "<pre>";
                    print_r($block['items']);
                    echo "</pre>";
                    */
                }
            
                $this->report_pages[$page_name] = $tmp;
            }
            
            
            foreach($this->report_pages[$pagename]["items"] as $blockname=>$block){
                // Если тип блока TfrxMasterData
                if($block["tagName"]=='TfrxMasterData'){
                    
                    // Вычисляем общую высоту MasterData - блока
                    $maxheight = 0;
                    foreach($block["items"] as $itemname=>$item)
                        if($item["attrs"]["Top"]+$item["attrs"]["Height"]>$maxheight)
                            $maxheight = $item["attrs"]["Top"]+$item["attrs"]["Height"];
                    
                    // Создаём блоки для MasterData - блока
                    if(1)foreach($dataset as $k=>$data){
                        foreach($block["items"] as $itemname=>$item){
                            $new_itemname = $itemname."[$k]";
                            
                            $item["attrs"]["Name"]  = $item["attrs"]["Name"]."[$k]";
                            $item["attrs"]["Text"]  = $item["attrs"]["Text"]."[$k]";
                            $item["attrs"]["Top"]   += $maxheight*$k;
                            
                            $this->report_pages[$pagename]["items"][$blockname]['items']
                                [$new_itemname] = $item;
                            
                        }
                    }
                    // Для каждого блока типа TfrxFooter сдвигаем коордитаты
                    foreach($this->report_pages[$pagename]["items"] as $bn=>$b)
                        if($b["tagName"] == "TfrxFooter"){
                            $delta = 0;
                            if(count($dataset))
                                $delta = $maxheight*count($dataset) - 1;
                                
                            $this->report_pages[$pagename]["items"][$bn]["attrs"]["Top"] += $delta-$maxheight+1; 
                        }
                        
                    // Убираем лишние блоки
                    foreach($block["items"] as $itemname=>$item)
                        if(!preg_match("/^\[\d+\].*$/",$itemname))
                            unset($this->report_pages[$pagename]["items"][$blockname]['items'][$itemname]);
                    
                }
            }


            foreach($this->report_pages[$pagename]["items"] as $blockname=>$block){
                if($block["tagName"]=='TfrxMasterData'){
                    // Вычисляем верхний край блока Footer
                    $top = 0;
                    foreach($block["items"] as $itemname=>$item)
                        if($item["attrs"]["Top"]>$top)$top = $item["attrs"]["Top"];
                    
                    
                    // Для всех плавающих картинок сдвигаем координаты
                    foreach($this->report_pages[$pagename]["items"] as $bn=>$b)
                        if($b["tagName"] == "TfrxFooter")
                        foreach($b["items"] as $itemname=>$item)
                            if(
                                (
                                $item["tagName"]=='TfrxPictureView' 
                                )
                                && (
                                    !isset($item["attrs"]["position"]) 
                                    || 
                                    $item["attrs"]["position"]!='table'
                                )
                            ){
                                $this->report_pages[$pagename]["items"][$bn]
                                    ["items"][$itemname]["attrs"]["Top"] += 
                                    $top-$maxheight*(count($dataset)-1)+10;
                            }
                }
            }

            
        }
        
        
        private function html_font_style($style){
            $result = '';
            switch($style){
                case '0':
                break;
                case '1':
                    $result = 'font-weight:bold;';
                break;
            }
            return $result;
        }
        
        private function html_halign($halign){
            $result = 'text-align:';
            switch($halign){
                case 'haBlock':
                    $result .= 'justify';
                break;
                case 'haCenter':
                    $result .= 'center';
                break;
                case 'haRight':
                    $result .= 'right';
                break;
                case 'haLeft':
                    $result .= 'left';
                break;
            }
            $result  .= ';';
            return $result;
            
        }

        private function xls_halign($halign){
            $result = str_replace("text-align:","",$this->html_halign($halign));
            $result = str_replace(";","",$result);
            $result = trim($result);
            return $result;
            
        }

        private function html_valign($halign){
            $result = 'vertical-align:';
            switch($halign){
                case 'vaCenter':
                    $result .= 'middle';
                break;
                case 'vaBottom':
                    $result .= 'bottom';
                break;
                case 'vaTop':
                    $result .= 'top';
                break;
            }
            $result  .= ';';
            return $result;
        }
        
        private function xls_valign($valign){
            $result = str_replace("vertical-align:","",$this->html_valign($valign));
            $result = str_replace(";","",$result);
            $result = trim($result);
            return $result;
            
        }

        private function html_bg_color($color){
            if(!$color)return "background-color: transparent;";
            $r = $color&0xFF0000>>16;
            $g = $color&0x00FF00>>8;
            $b = $color&0x0000FF;
            return "background-color: rgb($r,$g,$b);";
        }

        private function xls_bg_color($color){
            if(!$color)return "FFFFFF";
            $r = sprintf("%X", $color&0xFF0000>>16);
            $g = sprintf("%X", $color&0x00FF00>>8);
            $b = sprintf("%X", $color&0x0000FF);
            return "$r$g$b";
        }

        private function html_frame($type){
            $html = '';
            if($type&8)$html .= 'border-bottom: 1px black solid;';
            if($type&2)$html .= 'border-right: 1px black solid;';
            if($type&4)$html .= 'border-top: 1px black solid;';
            if($type&1)$html .= 'border-left: 1px black solid;';
            return $html;
        }

        private function xls_frame($type){
            $borders = array();
            if($type&8)$borders["bottom"]   = '000000';
            if($type&2)$borders["right"]    = '000000';
            if($type&4)$borders["top"]      = '000000';
            if($type&1)$borders["left"]     = '000000';
            return $borders;
        }

        function tofloat($num) {
            $num = str_replace(",",".",$num);
            $num = floatval($num);
            return $num;
        }
    }
