function wirix_fileselect_show(filename,selector_id,callback_dom){
    
    $("body").append(
        '<div class="fileselecter_main" id="'+selector_id+'">'+
            '<div class="title">Выбора файла'+
                '<div class="close" onclick="$(this).parent().parent().remove();">[X]</div>'+
            '</div>'+
            '<div class="path">'+filename+'</div>'+
            '<div class="ls" rel="/"></div>'+
            '<div class="ok">Выбрать</div>' +
            '<div class="ok" onclick="$(this).parent().remove();>Отмена</div>' +
        '</div>'
    );
    
    $('.fileselecter_main .ok').mousedown(function(){
        $(this).addClass("pressed");
    });
    
    $('.fileselecter_main .ok').click(function(){
        callback_dom.val($('.fileselecter_main .selected').attr('rel'));
        $(this).parent().remove();
    });
    
    wirix_fileselecter_load(filename,selector_id,callback_dom);
    return false;
}

function wirix_fileselecter_load(filename,selector_id,callback_dom){
    var settings = '';

    $.get("/wirix/libs/f/fileselecter/settings.ajax.php?id="+selector_id,function(data){
        settings = data;
        $('#'+selector_id+' .ls').first().load(
            "/wirix/libs/f/fileselecter/load.ajax.php?id="+selector_id+"&path="+filename,
            function(){
                $('.fileselecter_main .ls div').click(function(){
                    $('#.fileselecter_main .ls div').removeClass("selected");
                    $(this).addClass("selected");
                });
                $('.fileselecter_main .ls div').dblclick(function(){
                    if($(this).hasClass("folder")){
                        wirix_fileselecter_load($(this).attr("rel"),selector_id)
                    }
                });
                
                $('.fileselecter_main .path').html(filename);

            }


        );
    });
}

