<?php

    include_once(dirname(__FILE__)."/../../wirix.class.php");

    $id = $_H->get("id");
    
    if(!isset($_SESSION['wirix']["wirix_fileselecter"][$id])){
        echo json_encode(array("error"=>$id."is not set"));
        die;
    }
    
    $obj = json_decode(base64_decode($_SESSION['wirix']["wirix_fileselecter"][$id]));
    
    if(is_object($obj)){
        $obj = get_object_vars($obj);
    }
    else{
        echo json_encode(array("error"=>$id."is not object"));
        die;
    }
    
    echo json_encode($obj);
    
    
