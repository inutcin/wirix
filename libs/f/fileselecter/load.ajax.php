<?php


    if(!isset($_GET["path"])){
        echo "Filepath is empty";
        die;
    }

    $path = str_replace("\\","/",$_GET["path"]);
    
    include_once(dirname(__FILE__)."/../../wirix.class.php");

    $id = $_H->get("id");
    
    if(!isset($_SESSION['wirix']["wirix_fileselecter"][$id])){
        echo json_encode(array("error"=>$id."is not set"));
        die;
    }
    
    $settings = json_decode(base64_decode($_SESSION['wirix']["wirix_fileselecter"][$id]));
    
    if(is_object($settings)){
        $settings = get_object_vars($settings);
    }
    else{
        echo json_encode(array("error"=>$id."is not object"));
        die;
    }

    if(!isset($settings['fake_root_path']) || !trim($settings['fake_root_path'])){
        echo "fake_root_path is not defined";
        die;
    }
    else{
        $fake_root_path = $settings['fake_root_path'];
    }

    if(!isset($settings['real_root_path']) || !trim($settings['real_root_path'])){
        echo "real_root_path is not defined";
        die;
    }
    else{
        $real_root_path = $settings['real_root_path'];
    }
    
    
    $pattern = str_replace("","",$fake_root_path);
    $path = preg_replace("#^".$pattern."#i","",$path);
    
    $path = explode("/",$path);
    $file = $path[count($path)-1];
    
    unset($path[count($path)-1]);
    
    $dir = $real_root_path.implode("/",$path);

    $parent_path = $path;
    if(isset($parent_path[count($parent_path)-1]))
        unset($parent_path[count($parent_path)-1]);
        
    $parent_dir = $real_root_path.implode("/",$parent_path);
    
    $dd = opendir($dir);
    $fileobj = $_W->lib_load("file");
    
    echo '<div class="folder" rel="'.get_rel($parent_dir,"").'">';
    echo '<img src="/wirix/libs/f/file/i/icons/folder.png">';
    echo '[назад]';
    echo '</div>';
    
    while($filename = readdir($dd)){
        if($filename=='.' || $filename=='..')continue;
        $mime = $fileobj->get_mime_by_filename($filename);
        echo '<div class="'.
            (is_dir($dir."/".$filename)?"folder":"file").
            ' '.
            ($filename==$file?"selected":"").
            '" rel="'.get_rel($dir,$filename).'">';
        if(is_dir($dir."/".$filename))
            echo '<img src="/wirix/libs/f/file/i/icons/folder.png">';
        else
            echo '<img src="'.$fileobj->get_mime_icon($mime,16).'">';
        echo $filename;
        echo "</div>\n";
    }
    closedir($dd);
    
    
    function get_rel($dir,$filename){
        
        global $fake_root_path;
        global $real_root_path;
        global $settings;
        
        $rel = $dir."/".$filename;
        $rel = preg_replace("#^".$real_root_path."#i","",$rel);
        $rel = $fake_root_path.$rel;
        if(isset($settings["slashes_type"]) && $settings["slashes_type"]=='windows')
            $rel = str_replace("/","\\",$rel);
        if(is_dir($dir."/".$filename))$rel .= '\\';
        
        if(isset($settings["slashes_type"]) && $settings["slashes_type"]=='windows'){
            $rel = "\\".$rel;
            $rel = str_replace("\\\\","\\",$rel);
        }
            
        return $rel;
    }
    
