<?php
    include_once(dirname(__FILE__)."/../../wirix.class.php");

    /**
     * Класс обработки ошибок
     */
    class wirix_file extends wirix{
        var $name       = ''; //!< Имя файла
        var $stat       = array();
        var $upload_dir = '';   //!< КАталог,куда сохраняются файлы
        
        function __construct(){
            parent::__construct();
            $this->upload_dir = $_SERVER['DOCUMENT_ROOT']."/uploads/files";
        }
        
        private function check(){
            if(!$_SERVER['error']->check_file($this->name))
                return false;
            $this->stat();
            return true;
        }

        private function pure_size_format(){
            return $this->stat['size'];
        }
        
        /**
            Перевод размера файла в человекочитаемый формат
        */
        function human_size_format($original_size){
            $unit = $_SERVER['wirix']->lib_load('unit');
            
            $suffixes = array("KiB", "MiB", "GiB", "TiB");
            $last_suffix = 'B';
            $size = $original_size;
            
            foreach($suffixes as $suffix){
                $ssize = $unit->translate($size, $last_suffix, $suffix);
                if($ssize<1)break;
                $size = $ssize;
                $last_suffix = $suffix;
            }
            
            return $size."".$last_suffix;
        }
        
        /**
            Загрузка информации о файле
        */
        private function stat(){
            $this->stat = stat($this->name);
        }
        
        /**
            Загрузка файла
            
            $info - массив из $_FILES, например $_FILES['attach']
            
            Возвращает ID загруженного файла
        */
        function upload($info){
            if(isset($info['error']) && $info['error']){
                $this->error = _t(wirix_error::upload_error($info['error']));
                return false;
            }
            elseif(!isset($info['error'])){
                $this->error = _t("Unknown error");
                return false;
            }
            
            $dirname = $this->upload_dir;
            if(!is_dir($dirname)){
                $this->error = _t("Directory %s not exists", array("%s"=>$dirname));
                return false;
            }
            
            $stat = stat($dirname);
            
            $stat['mode'] = ($stat['mode'] & 0666);
	    /*
            if(sprintf("%o", $stat['mode'])!=666){
                $this->error = _t("Directory has %s mode", array("%s"=>sprintf("%o", $stat['mode'])));
                return false;
            }
	    */
            
            $name       = isset($info['name'])?$info['name']:"unknown";
            $mime       = isset($info['type'])?$info['type']:"text/plain";
            $size       = isset($info['size'])?$info['size']:"0";
            $tmp_name   = $info['tmp_name'];
            $crc        = md5_file($tmp_name);
            
            $ext = explode(".",$name);
            $ext = $ext[count($ext)-1];
            
            $file_id = $_SERVER['db']->insert("files", array(
                "name"  => $name,
                "mime"  => $mime,
                "size"  => $size,
                "crc"   => $crc
            ));
            
            
            $filename = $this->get_filename($file_id).".$ext";
            if(!@copy($tmp_name, $filename)){
                $this->error = _t("Cant store tmp file at  %s", array("%s"=>$filename));
                return false;
            }
            
            @chmod($filename, 0644);
            return $file_id;
        }
        
        /**
            Замена файла
            
            $file_id - ID файла
            $info - массив из $_FILES, например $_FILES['attach']
        */
        function replace($file_id, $info){
            $filename = $this->get_filename($file_id);

            $name       = isset($info['name'])?$info['name']:"unknown";
            $mime       = isset($info['type'])?$info['type']:"text/plain";
            $size       = isset($info['size'])?$info['size']:"0";
            $tmp_name   = $info['tmp_name'];
            $crc        = md5_file($tmp_name);

            if(!@copy($tmp_name, $filename)){
                $this->error = _t("Cant store tmp file at  %s", array("%s"=>$filename));
                return false;
            }
            
            $_SERVER['db']->update(
                "files", 
                array(
                    "name"  => $name,
                    "mime"  => $mime,
                    "size"  => $size,
                    "crc"   => $crc
                ),
                array(
                    "id"=>$file_id
                )
            );
            
            
            @chmod($filename, 0777);
            return $file_id;
        }
        
        /**
            Получение имени файла с путём по ID файла
        */
        function get_filename($file_id){
            $dirname = $this->upload_dir;
            for($i=4;$i>=1;$i--){
                $base = floor(($file_id) / pow(10, $i));
                $dirname .= "/".$base;
                if(!is_dir($dirname)){
                    mkdir($dirname);
                    @chmod($dirname, 0777);
                }
            } 
            $filename = $dirname."/".$file_id;
            return $filename;
        }
        
        /**
            Получение пути к icon по mime-type файла
        */
        function get_mime_icon($mime, $size=16){
            $base_url = "/wirix/libs/f/file/i/mime";
            
            $tmp = explode("/", $mime);
            $icon = "";
            if(isset($tmp[1])){
                $icon = $base_url."/".$tmp[0]."/".$tmp[1]."-icon-$size"."x$size.png";
                $filename = $_SERVER['DOCUMENT_ROOT'].$icon;
                if(!file_exists($filename))$icon = '';
            }
            
            if(!$icon)$icon = $base_url."/unknown-icon-$size"."x$size.png";

            return $icon;
        }
        
        /**
            Получение информации о файле по его ID
        */
        function get_file_info($file_id){
            $_SERVER['db']->search_one("files", array(
                "id"=>intval($file_id)
            ));
            return $_SERVER['db']->record;
        }
        
        /**
            Возвращает дискриптор на открытый файл по ID файла
        */
        function get_file_descriptor($file_id){
            $fd = @fopen($this->get_filename($file_id), "r");
            return $fd;
        }
        
        /**
            Удаление файла по его ID
        */
        function delete($file_id){
            $_SERVER['db']->delete("files", array("id"=>$id));
            unlink($this->get_filename($file_id));
        }
        
        /**
            Получение mime-типа по имени файла
        */
        function get_mime_by_filename($filename){
            $tmp = explode(".", $filename);
            $ext = $tmp[count($tmp)-1];
            $ext = strtolower($ext);
            switch($ext){
                case 'avi':return "video/avi";
                case 'mp4':return "video/mp4";
                case 'mov':return "video/mov";
                case 'mpg':return "video/mpg";
                case 'flv':return "video/x-flv";
                case 'mkv':return "video/mkv";
                case 'mp3':return "audio/mp3";
                case 'mid':return "audio/midi";
                case 'txt':return "text/plain";
                case 'sql':return "text/sql";
                case 'csv':return "text/cvs";
                case 'log':return "text/log";
                case 'jpg':return "image/jpeg";
                case 'jpeg':return "image/jpeg";
                case 'png':return "image/png";
                case 'psd':return "image/psd";
                case 'xpi':return "image/xpi";
                case 'ai':return "image/ai";
                case 'djvu':return "image/djvu";
                case 'gif':return "image/gif";
                case 'doc':return "application/msword";
                case 'docx':return "application/msword";
                case 'pdf':return "application/pdf";
                case 'ppt':return "application/ppt";
                case 'pptx':return "application/pptx";
                case 'xls':return "application/vnd.ms-excel";
                case 'xlsx':return "application/vnd.ms-excel";
            }
        }
        
    }

?>
