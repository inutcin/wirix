<?php

    include_once("../../wirix.class.php");
    if(!$_A->is_group(1)){$_H->status(403);die;}
    
    $path = $_H->get("path");
    if(preg_match("#\.\.#",$path)){
        ?><script>alert('<?php echo _t('Cant remove directory').": LINE ".__LINE__;?>')</script><?php
        die;
    }

    $delete = $_H->get("delete");
    if(preg_match("#\.\.#",$path)){
        ?><script>alert('<?php echo _t('Cant remove directory').": LINE ".__LINE__;?>')</script><?php
        die;
    }


    $filemanager = $_W->lib_load("filemanager");
    $delete_path = $filemanager->root_path.$path."/".$delete;
    if(!@rmdir($delete_path)){
        ?><script>alert('<?php echo _t('Cant remove directory')." $delete_path ".": LINE ".__LINE__;?>')</script><?php
        die;
    }
    else{
    }
?>
<script>
    parent.document.location.reload();
</script>

    
