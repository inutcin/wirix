<?php

    include_once("../../wirix.class.php");
    if(!$_A->is_group(1)){$_H->status(403);die;}
    
    if(preg_match("#.*\.php$#",$_FILES['file']["name"])){
        ?><script>alert('<?php echo _t('Cant load .php file');?>')</script><?php
        die;
    }

    $path = $_H->get("path");
    if(preg_match("#\.\.#",$path)){
        ?><script>alert('<?php echo _t('Cant make directory');?>')</script><?php
        die;
    }

    $dirname = $_H->post("mkdir");
    if(preg_match("#\.\.#",$path)){
        ?><script>alert('<?php echo _t('Cant make directory');?>')</script><?php
        die;
    }


    $filemanager = $_W->lib_load("filemanager");
    $upload_path = $filemanager->root_path.$path."/".$dirname;
    if(!@mkdir($upload_path)){
        ?><script>alert('<?php echo _t('Cant make directory');?>')</script><?php
        die;
    }
    else{
        chmod($upload_path, 0777);
    }
?>
<script>
    parent.document.location.reload();
</script>

    
