<?php
    include_once(dirname(__FILE__)."/../../wirix.class.php");

    $_SERVER["wirix"]->lib_load("file");
    class wirix_filemanager extends wirix_file{
        var $root_path = '';   //!< Реальный корневой путь в ФС
        
        function __construct(){
            parent::__construct();
            if(!$this->root_path)
                $this->root_path = $_SERVER["DOCUMENT_ROOT"]."/uploads/files";
        }
        
        function fetch($object = NULL){
            
            global $_H;
            global $_W;
            
            $file = $_W->lib_load("file");
            
            $html = '';
            
            $path = $_H->get('path');
            $path = $this->root_path.$path;
            if(preg_match("#\.\./#i", $path))$path = $this->root_path;
            if(!is_dir($path))$path = $this->root_path;
            
            $current_path = str_replace($this->root_path,"",$path);
            $parent_path = explode("/",$current_path);
            unset($parent_path[count($parent_path)-1]);
            $parent_path = implode("/",$parent_path);

            $fd = opendir($path);
            if($path!=$this->root_path)$html .= '<div class="folder"><a href="?path='.$parent_path.'">['._t('Back').']</a></div>';
            while($filename=readdir($fd)){
                if($filename=='.' || $filename=='..')continue;
                $full_path = $path."/".$filename;
                $url_path = $current_path."/".$filename;
                $download_path = str_replace($_SERVER["DOCUMENT_ROOT"], "", $full_path);
                if(is_dir($full_path))
                    $html .= '<div class="folder"><a href="?path='.$url_path.
                        '"><img src="/wirix/libs/f/file/i/icons/folder.png">'.
                        $filename.'</a>'.'<a  target="upload" onclick="return confirm(\''._t("delete").'?\');" class="del-button" href="/wirix/libs/f/filemanager/rmdir.ajax.php?path='.
                        $current_path.'&delete='.$filename.'">'.
                        '<i class="icon-trash"></i></a></div>';
                else
                    $html .= '<div class="file"><a target="_blank" href="'.$download_path.'"><img src="'.
                        $file->get_mime_icon($file->get_mime_by_filename($filename),16).'">'.$filename.'</a>'.
                        '<a  target="upload" onclick="return confirm(\''._t("delete").'?\');" class="del-button" href="/wirix/libs/f/filemanager/unlink.ajax.php?path='.
                        $current_path.'&delete='.$filename.'">'.
                        '<i class="icon-trash"></i></a></div>';
            }
            closedir($fd);
            
            $forms = '';
            $forms .= '<form target="upload" enctype="multipart/form-data" method="post" action="/wirix/libs/f/filemanager/mkdir.ajax.php?path='.$current_path.'">';
            $forms .= '<input type="text" name="mkdir" id="mkdir" class="submit-button">';
            $forms .= '<input type="submit" name="file-mkdir" id="file-mkdir" class="btn btn-primary submit-button" value="Создать каталог">';
            $forms .= '</form>';
            $forms .= '<form target="upload" enctype="multipart/form-data" method="post" action="/wirix/libs/f/filemanager/upload.ajax.php?path='.$current_path.'">';
            $forms .= '<input type="file" name="file" id="file-upload">';
            $forms .= '<input type="submit" name="send" value="'._t('Upload').'" class="btn btn-primary submit-button">';
            $forms .= '</form>';
            $html .= '<iframe name="upload" style="display:none;"></iframe>';
            
            return '<div class="filemanager">'.$forms.$html."</div>";
        }
        
    }
