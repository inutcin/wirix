<?php
/**
    Ссылки по теме

    https://oauth.yandex.ru/client/ - страничка API
    http://api.yandex.ru/oauth/doc/dg/tasks/get-oauth-token.xml - получение токена


*/
class wirix_ya_api extends wirix{

    var $app_pass = "";
    var $app_id = "";
    var $token = '';
    var $timeout = 30;
    var $per_page = 1000;
    var $content = '';
    
    private $conters = array();
    
    
    public function __construct() {
        parent::__construct(__CLASS__);
    }    
    
    /**
        Получение списка доступных счетчиков
    */
    function metrica_get_counters(){
        $url = "http://api-metrika.yandex.ru/counters.json?pretty=1&oauth_token=".
            $this->token."&per_page=".$this->per_page;
        $http = $_SERVER['wirix']->lib_load('http');
        $http->url = $url;
        $http->timeout = $this->timeout;
        $http->request();
        $content = $http->content;
        if($http->returned_code==200){
            $data = json_decode($content);
            if(property_exists($data, "counters"))
                foreach($data->counters as $counter){
                    $this->counters[$counter->site] = array(
                        "id"=>$counter->id
                    );
                }
            unset($http);
            return true;
        }
        else{
            $data = json_decode($content);
            $this->error = $data->error;
            unset($http);
            return false;
        }
    }
    
    /**
        Получение посещаемости по определённому счетчику
    */
    function metrica_get_traffic($counter_id, $from_timestamp=0, $to_timestamp=0, $group='day'){
        
        $this->metrica_get_counters();
        
        if(!$from_timestamp)
            $date1 = time()-1*24*60*60;
        else
            $date1 = $from_timestamp;

        if(!$to_timestamp)
            $date2 = $date1+1*24*60*60;
        else
            $date2 = $to_timestamp;

        $date1 = date("Ymd", $date1);
        $date2 = date("Ymd", $date2);
        
        $url = "http://api-metrika.yandex.ru/stat/traffic/summary.json?id=".
            $counter_id."&pretty=1&oauth_token=".$this->token."&per_page=1000".
            "&group=$group&date1=".$date1."&date2=".$date2;
        
        $http = $_SERVER['wirix']->lib_load('http');
        $http->url = $url;
        $http->timeout = $this->timeout;
        $http->request();
	$this->content = $http->content;
        if($http->returned_code==200){
            $content = $http->content;
            $data = json_decode($content);

            $result = array();
            foreach($data->data as $item){
                $result[$item->date] = get_object_vars($item);
            }
            unset($http);
            return $result;
        }
        unset($http);
        return false;
        
    }
    
    
    /**
        Получение сводки переходов по источникам
    */
    function metrica_get_src($counter_id, $from_timestamp=0, $to_timestamp=0, $group='day'){
        
        if(!$from_timestamp)
            $date1 = time()-1*24*60*60;
        else
            $date1 = $from_timestamp;

        if(!$to_timestamp)
            $date2 = $date1+1*24*60*60;
        else
            $date2 = $to_timestamp;

        $date1 = date("Ymd", $date1);
        $date2 = date("Ymd", $date2);
        
        $url = "http://api-metrika.yandex.ru/stat/sources/summary.json?id=".
            $counter_id."&pretty=1&oauth_token=".$this->token."&per_page=1000".
            "&group=$group&date1=".$date1."&date2=".$date2;
            
        $http = $_SERVER['wirix']->lib_load('http');
        $http->url = $url;
        $http->timeout = $this->timeout;
        $http->request();
        if($http->returned_code==200){
            $content = $http->content;
            $data = json_decode($content);
            

            $result = array();
            foreach($data->data as $item){
                $keys = array("id","visits","denial","page_views","name","visit_time", "depth", "visits_delayed");
                foreach($keys as $key)
                $row[$key] = is_array($item->$key)?array_pop($item->$key):$item->$key;
                $result[$row['id']] = $row;
            }
            unset($http);
            return $result;
        }
        unset($http);
        return false;
        
    }
    
    
    /**
        Получение переходов с поисковых систем по определённому счетчику
    */
    function metrica_get_se($counter_id, $from_timestamp=0, $to_timestamp=0, $group='day'){
        
        $this->metrica_get_counters();
        
        if(!$from_timestamp)
            $date1 = time()-1*24*60*60;
        else
            $date1 = $from_timestamp;

        if(!$to_timestamp)
            $date2 = $date1+1*24*60*60;
        else
            $date2 = $to_timestamp;

        $date1 = date("Ymd", $date1);
        $date2 = date("Ymd", $date2);
        
        $url = "http://api-metrika.yandex.ru/stat/sources/search_engines.json?id=".
            $counter_id."&pretty=1&oauth_token=".$this->token."&per_page=1000".
            "&date1=".$date1."&date2=".$date2;
        
        $http = $_SERVER['wirix']->lib_load('http');
        $http->url = $url;
        $http->timeout = $this->timeout;
        $http->request();
        if($http->returned_code==200){
            $content = $http->content;
            $data = json_decode($content);

            $result = array();
            foreach($data->data as $item){
                $keys = array("id","visits","denial","page_views","name","visit_time", "depth", "visits_delayed");
                foreach($keys as $key)
                $row[$key] = is_array($item->$key)?array_pop($item->$key):$item->$key;
                $result[$row['id']] = $row;
            }
            unset($http);
            return $result;
        }
        unset($http);
        return false;
        
    }


    /**
        Получение популярных страниц
    */
    function metrica_get_pop_pages($counter_id, $from_timestamp=0, $to_timestamp=0, $group='url'){
        
        if(!$from_timestamp)
            $date1 = time()-1*24*60*60;
        else
            $date1 = $from_timestamp;

        if(!$to_timestamp)
            $date2 = $date1+1*24*60*60;
        else
            $date2 = $to_timestamp;

        $date1 = date("Ymd", $date1);
        $date2 = date("Ymd", $date2);
        
        $url = "http://api-metrika.yandex.ru/stat/content/popular.json?id=".
            $counter_id."&pretty=1&oauth_token=".$this->token."&per_page=1000".
            "&group=$group&date1=".$date1."&date2=".$date2;
        
        $http = $_SERVER['wirix']->lib_load('http');
        $http->url = $url;
        $http->timeout = $this->timeout;
        $http->request();
        if($http->returned_code==200){
            $content = $http->content;
            $data = json_decode($content);

            $result = array();
            foreach($data->data as $item){
                $keys = array("id","page_views","url","exit","entrance");
                foreach($keys as $key)
                    $row[$key] = is_array($item->$key)?array_pop($item->$key):$item->$key;
                $result[] = $row;
            }
            unset($http);
            return $result;
        }
        unset($http);
        return false;
        
    }



    /**
        Получение статистики по определённому счетчику
    */
    function metrica_get_phrases($counter_id, $from_timestamp=0, $to_timestamp=0){
        
        if(!$from_timestamp)
            $date1 = time()-1*24*60*60;
        else
            $date1 = $from_timestamp;

        if(!$to_timestamp)
            $date2 = $date1+1*24*60*60;
        else
            $date2 = $to_timestamp;

        $date1 = date("Ymd", $date1);
        $date2 = date("Ymd", $date2);
        
        $url = "http://api-metrika.yandex.ru/stat/sources/phrases.json?id=".
            $counter_id."&pretty=1&oauth_token=".$this->token."&date1=".$date1."&date2=".$date2;
        
        $http = $_SERVER['wirix']->lib_load('http');
        $http->url = $url;
        $http->timeout = $this->timeout;
        $http->request();
        if($http->returned_code==200){
            $content = $http->content;
            $data = json_decode($content);
            $result = array();
            foreach($data->data as $item){
                $result[$item->phrase] = get_object_vars($item);
                foreach($result[$item->phrase]['search_engines'] as $k=>$engine){
                    $engine = get_object_vars($engine);
                    unset($engine['se_url']);
                    $result[$item->phrase]['search_engines'][$k] = $engine;
                }
            }
            unset($http);
            return $result;
        }
        unset($http);
        return false;
    }

    
}
