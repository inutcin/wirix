<?php

class wirix_ya_xml extends wirix{

    var $request_url = "";
    var $proxy_host = "";
    var $proxy_port = "";
    var $proxy_user = "";
    var $proxy_pass = "";
    var $raw_content = ''; 
    
    // Ответы на запросы
    var $xml = array();//!< Объект полученного XML
    var $found = 0;//!< Количество найденных документов
    var $groups = array();  //!< Массив объектов найденных документов, каждый элемент - ассоциативный массив вида
    var $docs_on_page = 100;
    /**
            [url] => http://kids.wikimart.ru/child_care/cosmetics/nappy/
            [domain] => kids.wikimart.ru
            [modtime] => 20090321T074123
            [size] => 241101
            [charset] => utf-8
            [mime-type] => text/html
            [saved-copy-url] => http://hghltd.yandex.net/yandbtm?tld=ru&text=%D0%BF%D0%BE%D0%B4%D0%B3%D1%83%D0%B7%D0%BD%D0%B8%D0%BA%D0%B8&url=http%3A%2F%2Fkids.wikimart.ru%2Fchild_care%2Fcosmetics%2Fnappy%2F&fmode=inject&mime=html&l10n=ru&sign=04cef99ef6fbcd7993cf9f572c7733fc&keyno=0
        )
    
    
    */
    
    
    public function __construct() {
        parent::__construct(__CLASS__);
    }    
    
    /**
        Совершание запроса к Yandex XML
    */
    function request($request, $params = array()){

	$default_params = array(
	    /*
	    Поддерживается только для типов поиска «русский» и «турецкий».
	    Идентификатор страны или региона поиска. Определяет правила ранжирования документов. Например, если передать в данном параметре значение «11316» (Новосибирская область), при формировании результатов поиска используется формула, определенная для Новосибирской области.
	    Список идентификаторов часто используемых стран и регионов приведен в приложении.
	    */
	    "lr"		=>	"225",
	    /*
	    Язык уведомлений поискового ответа. Влияет на текст, передаваемый в теге found-docs-human, а также в сообщениях об ошибках.
	    Возможные значения зависят от используемого типа поиска:
		«русский (yandex.ru)» — «ru» (русский), «uk» (украинский), «be» (белорусский), «kk» (казахский). Если не задан, уведомления передаются на русском языке;
		«турецкий (yandex.com.tr)» — поддерживается только значение «tr» (турецкий);
		«мировой (yandex.com)» — поддерживается только значение «en» (английский).
	    */
	    "l10n"		=>	"ru",
	    /*
		Правило сортировки результатов поиска. Возможные значения:
		    «rlv» — по релевантности;
		    «tm» — по времени изменения документа.
		Если параметр не задан, результаты сортируются по релевантности.
		При сортировке по времени изменения параметр может содержать атрибут order — порядок сортировки документов. Возможные значения:
		    «descending» — прямой (от наиболее свежего к наиболее старому). Используется по умолчанию;
		    «ascending» — обратный (от наиболее старого к наиболее свежему).
		Формат: sortby=<тип сортировки>.order%3D<порядок сортировки>. Например, для обратной сортировки по дате необходимо использовать следующую конструкцию: sortby=tm.order%3Dascending.
	    */
	    "sortby"		=>	"rlv",
	    /*
		Правило фильтрации результатов поиска (исключение из результатов поиска документов в соответствии с одним из правил). Возможные значения:
		    «none» — фильтрация отключена. В выдачу включаются любые документы, вне зависимости от содержимого;
		    «moderate» — умеренная фильтрация. Из выдачи исключаются документы, относящиеся к категории «для взрослых», если запрос явно не направлен на поиск подобных ресурсов;
		    «strict» — семейный фильтр. Вне зависимости от поискового запроса из выдачи исключаются документы, относящиеся к категории «для взрослых», а также содержащие ненормативную лексику.
		Если параметр не задан, используется умеренная фильтрация.		
	    */
	    "filter"		=>	"none",
	    /*
		Правило фильтрации результатов поиска (исключение из результатов поиска документов в соответствии с одним из правил). Возможные значения:
		    «none» — фильтрация отключена. В выдачу включаются любые документы, вне зависимости от содержимого;
		    «moderate» — умеренная фильтрация. Из выдачи исключаются документы, относящиеся к категории «для взрослых», если запрос явно не направлен на поиск подобных ресурсов;
		    «strict» — семейный фильтр. Вне зависимости от поискового запроса из выдачи исключаются документы, относящиеся к категории «для взрослых», а также содержащие ненормативную лексику.
		Если параметр не задан, используется умеренная фильтрация.
	    */
	    "maxpassages"	=>	"4",
	    /*
		Правило фильтрации результатов поиска (исключение из результатов поиска документов в соответствии с одним из правил). Возможные значения:
		    «none» — фильтрация отключена. В выдачу включаются любые документы, вне зависимости от содержимого;
		    «moderate» — умеренная фильтрация. Из выдачи исключаются документы, относящиеся к категории «для взрослых», если запрос явно не направлен на поиск подобных ресурсов;
		    «strict» — семейный фильтр. Вне зависимости от поискового запроса из выдачи исключаются документы, относящиеся к категории «для взрослых», а также содержащие ненормативную лексику.
		Если параметр не задан, используется умеренная фильтрация.
	    */
	    "page"		=>	"0"
	);
	foreach($default_params as $k=>$v)
	    if(isset($params[$k]))$default_params[$k]=$params[$k];

	/*
	    query	
	    Текст поискового запроса. При обработке учитываются особенности языка запросов Яндекса (вместо специальных символов необходимо использовать соответствующие экранированные последовательности).
	    На запрос наложены следующие ограничения: максимальная длина запроса — 400 символов; максимальное количество слов — 40.
	*/
	if($request)$default_params["query"] = $request;
        
	foreach($default_params as $k=>$v)$this->request_url.="&".$k."=".urlencode($v);

        $_SERVER['http']->url = $this->request_url;
	if($this->proxy_host){
	    $_SERVER['http']->proxy['host'] = $this->proxy_host;
    	    $_SERVER['http']->proxy['port'] = $this->proxy_port;
    	    $_SERVER['http']->proxy['username'] = $this->proxy_user;
    	    $_SERVER['http']->proxy['password'] = $this->proxy_pass;
	}

	/*
        $request = '<?xml version="1.0" encoding="UTF-8"?>'.	
            '<request>'.
            '<query>'.$request.'</query>'.
                '<groupings>'.
                '<groupby attr="d" mode="deep" groups-on-page="'.$this->docs_on_page.'"  docs-in-group="1" />'.
                '</groupings>'.
            '</request>';
            $_SERVER['http']->post_request = $request;
	*/
    
        
        $_SERVER['http']->request();

        if($_SERVER['http']->returned_code!=200){
            return false;
        }
        
	$this->raw_content = $_SERVER['http']->content;
        $this->xml = simplexml_load_string($this->raw_content);
        $this->found = $this->xml->response->found[0];
        
        $result = is_object($this->xml->response->results->grouping)?get_object_vars($this->xml->response->results->grouping):array();
        if(isset($result['group']))$result = $result['group'];
        $this->groups = array();
        foreach($result as $k=>$res){
            $item = get_object_vars($res->doc);
            $item["position"] = $k+1;
            $this->groups[] = $item;
        }

        return true;
    }

    /**
	Выдача найденных документов в виде json
    */
    function get_docs_as_array(){
	$data = str_replace("\n","", $this->raw_content);


	if(preg_match("/<found priority=\"all\">(\d+)<\/found>/",$data,$m))
	    $this->found = $m[1];
	
	preg_match_all("/<doc\s+.*?>(.*?)<\/doc>/", $data, $matches);
	$docs = array();
	if(!is_array($matches[1]))return array();

	$this->docs_on_page = count($matches[1]);

	$pages_count = $this->docs_on_page?(floor(($this->found-1)/$this->docs_on_page)+1):0;
	$pages_array = array();
	for($i=0;$i<$pages_count;$i++)
	    $pages_array[] = array("title"=>$i+1, "value"=>$i);
	

	if(is_array($matches[1]) && count($matches[1]))foreach($matches[1] as $v){
	    $doc = array();
	    if(preg_match("/<title.*?>(.*?)<\/title>/",$v,$m))$doc["title"] = $m[1];
	    if(preg_match("/<url.*?>(.*?)<\/url>/",$v,$m))$doc["url"] = $m[1];
	    if(preg_match("/<domain.*?>(.*?)<\/domain>/",$v,$m))$doc["domain"] = $m[1];
	    if(preg_match("/<size.*?>(.*?)<\/size>/",$v,$m))$doc["size"] = $m[1];
	    if(preg_match("/<lang.*?>(.*?)<\/lang>/",$v,$m))$doc["lang"] = $m[1];
	    if(preg_match("/<mime-type.*?>(.*?)<\/mime-type>/",$v,$m))$doc["mime_type"] = $m[1];
	    if(preg_match("/<charset.*?>(.*?)<\/charset>/",$v,$m))$doc["charset"] = $m[1];
	    if(preg_match("/<passage>(.*?)<\/passage>/",$v,$m))$doc["passage"] = $m[1];
	    if(preg_match("/<saved-copy-url>(.*?)<\/saved-copy-url>/",$v,$m))$doc["saved_copy_url"] = $m[1];
	    if(preg_match("/<modtime.*?>(\d{4})(\d{2})(\d{2})T.*<\/modtime>/",$v,$m)){
		$doc["modtime"] = $m[1]."-".$m[2]."-".$m[3];
		$doc["modtimeh"]= $m[3].".".$m[2].".".$m[1];
	    }

	    foreach($doc as $key=>$val)$doc[$key] = preg_replace("/<hlword.*?>(.*?)<\/hlword.*?>/",'<span class="hlword">$1</span>',$val);
	    $docs[] = $doc;
	}
	return array("found"=>$this->found, "pages"=>$pages_array, "docs"=>$docs);
    }
    
}
