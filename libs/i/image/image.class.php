<?php
    include_once(dirname(__FILE__)."/../../wirix.class.php");

    /**
     * Класс обработки ошибок
     */
    class wirix_image extends wirix{
        var $filename = '';      //!< Ссылка на изображение
        var $thrumb = '';   //!< Ссылка на уменьшенную иконку
        
        var $thrumb_width   = '80';
        var $thrumb_height  = '100';
        var $thrumb_quality = 1;
        var $mode = 0;      //!< Режимы ресайза 0 - заполнить весь прямоугольник нового изображения, сохранив максимум старого, 1 - списать в прямоугольник нового изображения
        
        /*
            Функция создаёт картинку для предпросмотра
            Берёт из $this->filename и сохраняет в $this->thrumb
        */
        function create_thrumb(
            $force_ext = '' //!< Указываем принудительно расширение файла
        ){
            if($force_ext=='jpg')$force_ext = 'jpeg';
            
            $func = "";
            if($force_ext)$force_ext = $force_ext;
            elseif(preg_match("/^.*\.gif$/i", $this->filename)) $force_ext = 'gif';
            elseif(preg_match("/^.*\.jpg$/i", $this->filename)) $force_ext = 'jpeg';
            elseif(preg_match("/^.*\.jpeg$/i", $this->filename)) $force_ext = 'jpeg';
            elseif(preg_match("/^.*\.png$/i", $this->filename)) $force_ext = 'png';
            
            $func = "imagecreatefrom$force_ext";
        
            $im=$func($this->filename);
            
            // Ошибка создания картинки
            if(!$im){
                @unlink($this->filename);
                return false;
            }
            
            $off_x = 0;
            $off_y = 0;
            if($this->mode==0){
    
                $W = imagesx($im);
                $H = imagesy($im);
                $w = $this->thrumb_width;
                $h = $this->thrumb_height;
                
                $K = $W/$H;
                $k = $w/$h;
                
                if($K>$k){
                    $off_x = ($W-$H*$k)/2;
                    $W = $H*$k;
                }
                else{
                    $off_y = ($H-$W*$k)/2;
                    $H = $W*$k;
                }
                
            }elseif($this->mode==1){
                
                $W = imagesx($im);
                $H = imagesy($im);
                
                $k1=$this->thrumb_width/$W;
                $k2=$this->thrumb_height/$H;
                $k=$k1>$k2?$k2:$k1;
            
                $w=intval($W*$k);
                $h=intval($H*$k);
            }

            $im1=imagecreatetruecolor($w,$h);
            $background = imagecolorallocate($im1, 255, 255, 255);
            imagecolortransparent($im1, $background);
            imagealphablending($im1, false);
            imagesavealpha($im1, true);            
            imagecopyresampled($im1,$im,0,0,$off_x,$off_y,$w,$h,$W,$H);

            
        
        
        
            imagepng($im1,$this->thrumb,$this->thrumb_quality);
            imagedestroy($im);
            imagedestroy($im1);
            return true;
        }
        
        
    }

?>
