<?php
    include_once(dirname(__FILE__)."/../../wirix.class.php");

    /**
     * Класс по переводу единиц измерения
     */
    class wirix_unit extends wirix{
        
        var $tmatrix = array(
            "B"     =>  array(
                "KiB"   =>  0.0009765625,
                "MiB"   =>  9.53674316406e-07,
                "GiB"   =>  9.31322574615e-10,
                "TiB"   =>  9.09494701773e-13
            ),
            "KiB"   =>  array(
                "B"     =>  1024,
                "MiB"   =>  0.0009765625,
                "GiB"   =>  9.53674316406e-07,
                "TiB"   =>  9.31322574615e-10
            ),
            "MiB"   =>  array(
                "B"     =>  1048576,
                "KiB"   =>  1024,
                "GiB"   =>  0.0009765625,
                "TiB"   =>  9.53674316406e-07
            ),
            "GiB"   =>  array(
                "B"     =>  1073741824,
                "KiB"   =>  1048576,
                "MiB"   =>  1024,
                "TiB"   =>  0.0009765625
            ),
            "TiB"   =>  array(
                "B"     =>  1.09951162778e+12,
                "KiB"   =>  1073741824,
                "MiB"   =>  1048576,
                "GiB"   =>  1024
            )
        );
        
        function translate($value, $from, $to, $signs = 2){
            if(!isset($this->tmatrix[$from][$to]))return $value;
            return round($value*$this->tmatrix[$from][$to], $signs);
        }
        
    }

?>
