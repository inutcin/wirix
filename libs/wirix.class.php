<?php
/**
    @mainpage 
    
    \tableofcontents
    
    Введение
    =====================================
    
    Библиотека классов Wirix оптимизирована для быстрого создания web-ориентированных 
    информационных систем, работающих на web-сервере и предоставляющих доступ для 
    пользователей через любой современный web-браузер
    
    @todo закончить главную страницу
    
    
    Полезные ссылки
    =====================================
    
    - Документация Doxygen <http://www.stack.nl/~dimitri/doxygen/manual/markdown.html>

*/

/**
    @page Архитектура структура и идеологи
    @brief Описываются базовые принципы построения библиотеки
    @todo Описать архитектуру
*/


/**
    @page  Использование
    @brief Описываются основные приёмы использования
    @todo Описать примеры использования

*/

/**
    
    @page  Установка
    @brief системные требования, установка и настройка
    
    Системные требования
    =======================================
    
    + php >5.3
        + curl
        + gd
        + 
    + mysql >5.0
    + apache 2
        + mod_rewrite
        
    
    Процесс установки
    =======================================
    
    + Перейти в рабочий каталог сайта (DOCUMENT_ROOT)
    + \code{.pl} $ svn export http://kask-seo.ru:8080/svn/wirix/trunk/install install \endcode
    + \code{.pl} $ chmod 777 install \endcode
    + запустить в браузере url /install/
    + следовать инструкциям
    + удалить каталог /install/ (если его не удалять, установщик по завершению положит туда .htaccess, блокирующий доступ)

    Процесс обновления
    =======================================

    + Перейти в рабочий каталог сайта (DOCUMENT_ROOT)
    + \code{.pl} $ svn export http://kask-seo.ru:8080/svn/wirix/trunk/update update \endcode
    + \code{.pl} $ chmod 777 update \endcode
    + запустить в браузере url /update/
    + следовать инструкциям
    + удалить каталог /update/ (если его не удалять, установщик по завершению положит туда .htaccess, блокирующий доступ)


    Процесс упаковки
    =======================================

    + Перейти в рабочий каталог сайта (DOCUMENT_ROOT)
    + \code{.pl} $ svn export http://kask-seo.ru:8080/svn/wirix/trunk/pack pack \endcode
    + \code{.pl} $ chmod 777 pack \endcode
    + запустить в браузере url /pack/
    + следовать инструкциям
    + удалить каталог /pack/ (если его не удалять, установщик по завершению положит туда .htaccess, блокирующий доступ)

*/


/**
    
    @page   Языковая поддержка
    @todo Языковая поддержка

*/
    date_default_timezone_set("Europe/Moscow");
    define("__DR__", $_SERVER["DOCUMENT_ROOT"]);
    define("__WRX_LIB__", __DR__."/wirix/libs");
    define("__WRX_THEMES__", __DR__."/wirix/themes");
    define("__THEMES__", __DR__."/themes");
    define("__WRX_ACTS__", __DR__."/wirix/acts");
    define("__ACTS__", __DR__."/acts");

    /*
        Замена отсутствующей в php 5.2 функции get_called_class
    */
    if(!function_exists('get_called_class')) {
    function get_called_class() {
        $bt = debug_backtrace();$l = 0;
        do {
            $l++;$lines = file($bt[$l]['file']);$callerLine = $lines[$bt[$l]['line']-1];
            preg_match('/([a-zA-Z0-9\_]+)::'.$bt[$l]['function'].'/',$callerLine,$matches);
           if ($matches[1] == 'self') {
               $line = $bt[$l]['line']-1;
               while ($line > 0 && strpos($lines[$line], 'class') === false)$line--;
               preg_match('/class[\s]+(.+?)[\s]+/si', $lines[$line], $matches);
           }
        }
        while ($matches[1] == 'parent'  && $matches[1]);
        return $matches[1];
      }
    }

    /**
     * Класс обработки ошибок
     */
    class wirix_error{
        
        /**
         * Проверка наличия директории
         */
        function check_dir(
            $dirname,
            $verbose = true,    //!< Выводить сообщение об ошибках в браузер
            $mask = 0444        //!< Маска (по умолчанию права на чтение), прикладывается по поразрядному AND, результат - соответствие правам
        ){
            if(!is_dir($dirname)){
                if($verbose)echo wirix_error::alert("Каталог <b>$dirname</b> не существует");
                return false;
            }
            $dirname = realpath($dirname);

            $stat = stat($dirname);
                
            if($permission = ($stat['mode'] & $mask)){
            }
            else{
                if($verbose)echo wirix_error::alert("Каталог <b>$dirname</b> имеет права доступа <b>".
                    sprintf("%o", $stat['mode'] & 0777)."</b>, должны быть как минимум <b>".
                    sprintf("%03o", $mask)."</b>");
                return false;
            }   
            
            return true;
        }

        /**
         * Проверка наличия файла
         */
        function check_file(
            $filename,
            $verbose = true,    //!< Выводить сообщение об ошибках в браузер
            $mask = 0444        //!< Маска (по умолчанию права на чтение), прикладывается по поразрядному AND, результат - соответствие правам
        ){
            if(!file_exists($filename)){
                if($verbose)echo wirix_error::alert("Файл <b>$filename</b> не существует");
                return false;
            }
            $filename = realpath($filename);

            $stat = stat($filename);
                
            if($permission = ($stat['mode'] & $mask)){
            }
            else{
                if($verbose)echo wirix_error::alert("Файл <b>$filename</b> имеет права доступа <b>".
                    sprintf("%o", $stat['mode'] & 0777)."</b>, должны быть как минимум <b>".
                    sprintf("%03o", $mask)."</b>");
                return false;
            }   
            
            return true;
        }

        /**
         * Проверка наличия класса
         */
        function check_class($classname, $verbose = true){
            if(class_exists($classname))return true;
            if($verbose)echo wirix_error::alert("Класс <b>$classname</b> не существует");
        }

        
        /**
         * Возвращает html-код текста ошибки
         */
        static function alert($text){
            return '<div style="color: red; background-color: #FDD;padding: 8px;font-family:sans-serif;font-size: 12px;border: 1px #FBB solid;-moz-border-radius:8px;-webkit-border-radius:8px;margin: 3px;">'.$text.'</div>';
        }
        
        /**
         * Возвращает html-код текста успеха
         */
        static function success($text){
            return '<div style="color: green; background-color: #DFD;padding: 8px;font-family:sans-serif;font-size: 12px;border: 1px #BFB solid;-moz-border-radius:8px;-webkit-border-radius:8px;margin: 3px;">'.$text.'</div>';
        }
        
        /**
            Текст ошибки загрузки файла
        */
        function upload_error($errno, $lang = 'en'){
            $error_ru = '';
            $error_en = '';
            switch($errno){
                case 1:
                    $error_en = 'The uploaded file exceeds the upload_max_filesize directive in php.ini.';
                    $error_ru = 'Загружаемый файл превышает установленный директивой upload_max_filesize размер в php.ini';
                break;
                case 2:
                    $error_en = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.';
                    $error_ru = 'Загружаемый файл превышает установленный директивой MAX_FILE_SIZE размер в html-форме';
                break;
                case 3:
                    $error_en = 'The uploaded file was only partially uploaded.';
                    $error_ru = 'Файл был загружен лишь частично';
                break;
                case 4:
                    $error_en = 'No file was uploaded.';
                    $error_ru = 'Файл не был загружен';
                break;
                case 6:
                    $error_en = 'Missing a temporary folder.';
                    $error_ru = 'Ошибка временного каталога';
                break;
                case 7:
                    $error_en = 'Failed to write file to disk.';
                    $error_ru = 'Ошибка записи на диск.';
                break;
                case 8:
                    $error_en = 'A PHP extension stopped the file upload. ';
                    $error_ru = 'Загрузка файла была остановлены расширением PHP';
                break;
            }
            
            return $lang=='ru'?$error_ru:$error_en;
        }
    }

    $_SERVER['error'] = new wirix_error();




    /**
        Базовый класс для всех библиотек wirix
    */
    class wirix{
        var $called_class = '';     //!< Имя вызвавшего класса
        
        var $errno = 0;
        var $error = '';
        var $permission_path = '/'; //!< Маршрут назначения прав. Компонент, имеет те же права, что и указанный маршрут
        var $id; //!< Идентификатор
        var $aux_code = ''; //!< Дополнительный код
        
        function __construct($called_class=''){
            return $this->wirix($called_class);
        }

        /**
            Сброс данных объекта в сессию
        */
        function commit(){
            if(!isset($_SESSION['wirix'][$this->called_class]))
                $_SESSION['wirix'][$this->called_class] = array();
            $_SESSION['wirix'][$this->called_class][$this->id] = base64_encode(@json_encode($this, JSON_HEX_TAG|JSON_HEX_AMP|JSON_HEX_APOS|JSON_HEX_QUOT));
//            if($this->id=='test_case_edit_form')echo "<pre>".print_r($_SESSION['wirix'],1)."</pre>";
        }
        
        /**
            Экспорт класса в json
        */
        function export(){
            return serialize($this);
        }
        
        /**
            Импорт класса из json
        */
        function import($data){
            $data = unserialize($data);
            $data = get_object_vars($data);
            foreach($data as $k=>$v)
                $this->$k = $v;
        }
        
        function __destruct(){
            $this->commit();
            return true;
        }
        

        function wirix($called_class=''){
            
            $this->called_class = $called_class?$called_class:get_called_class();

            // Производим проверку прав, если не пытаемся вызвать один из корневых компонентов
            if(
                $called_class
                && $called_class!='wirix_permissions'
                && $called_class!='wirix_auth' 
                && $called_class!='wirix_http' 
                && $called_class!='wirix_session' 
                && $called_class!='wirix_locale' 
                && $called_class!='wirix_db'
            ){
                $permissions = $this->lib_load('permissions');
                $groups = isset($_SERVER['auth']->user['groups_ids'])?$_SERVER['auth']->user['groups_ids']:0;
                if(!$permissions->is_access($this->permission_path, $groups)){
                }
            }
            $called_class_conf = $this->lib_conf_path($this->called_class);
            if($called_class_conf)include($called_class_conf);
        }
        
/*
        function __set($key, $value){
            $_SESSION['wirix'][$this->called_class][$key] = $value;
            $this->$key = $value;
        }
*/        
        /**
            Инициалиация библиотеки: исполнение SQL-скрипта 
        */
        function lib_init(
            $called_class = ''  //!< Указание имени класса для вызова вне контекста объекта
        ){
            if($called_class)$this->called_class = $called_class;
            if(!class_exists('db'))return false;
            $sql_init_filename = $_SERVER['wirix']->lib_sql_init_path($this->called_class);
            $db = new db();
            $db->sql_load_file($sql_init_filename, true);
            unset($db);
            return true;
        } 
        
        /**
            Очистка после установки
        */
        function lib_clear(){
            $db = new db();
            $sql_clear_filename = $_SERVER['wirix']->lib_sql_clear_path($this->called_class);
            $db->sql_load_file($sql_clear_filename);
            unset($db);
            return true;
        }
        
        
        /**
            Загрузка библиотеки: инклюд нужного файла и, при необходимоти, возврат ссылки на объект класса 
            Пример использования
            $auth = $_SERVER['wirix']->lib_load("auth");
            $_SERVER['wirix']->lib_load("auth");
        */
        function lib_load(
            $name, //!< Имя библиотеки
            $return_object = true,  //!< Возвращать ссылку на объект
            $permission_path = ''   //!< Путь для ограничения прав
        ){
            $filename = $this->lib_class_path($name);

            if($permission_path)
                $this->permission_path = $permission_path;
            if(!$_SERVER['error']->check_file($filename))return false;
            
            include_once($filename);
            
            $name = "wirix_".$name;
            
            if(!$_SERVER['error']->check_class($name))return false;
            
            $obj = true;
            if($return_object)$obj = new $name();
            return $obj;
        }
        
        /**
         * Получение полного пути к файлу класса библиотеки
         */
        function lib_class_path($name){
            return dirname(__FILE__)."/".substr($name,0,1)."/".$name."/".$name.".class.php";
        }
        
        /**
         * Получение полного пути к файлу конфига библиотеки
         */
        function lib_conf_path($name){
            $name = str_replace("wirix_","", $name);
            $filename = __DR__."/conf/".$name.".conf.php";
            if(file_exists($filename))return $filename;
            $filename = __DR__."/wirix/conf/".$name.".conf.php";
            if(file_exists($filename))return $filename;
            return false;
        }


        /**
         * Получение полного пути к файлу sql-init
         */
        function lib_sql_init_path($name){
            return dirname(__FILE__)."/".substr($name,0,1)."/".$name."/".$name.".init.sql";
        }

        /**
         * Получение полного пути к файлу sql-clear
         */
        function lib_sql_clear_path($name){
            return dirname(__FILE__)."/".substr($name,0,1)."/".$name."/".$name.".clear.sql";
        }

        
        /**
            Генерация html из элемента
        */
        function fetch($object = null){
            if(is_object($object) && method_exists($object, "fetch"))
                return $object->fetch();
            return '';
        }
        
        function error_rise(){
        }
        
        /**
            Загрузка схемы данных
            
            $scheme_name -имя схемы данных
            $filename - имя файла из которого взять класс схемы данных, если не указан
            то берём из папочки dataschemes
        */
        function init_datascheme($scheme_name, $filename = ''){
            
            $this->lib_load("datascheme_mysql", false);
            if($filename && file_exists($filename)){
            }
            elseif($filename && !file_exists($filename)){
                $this->error = "Отсутствует файл описания <b>$filename</b>схемы данных '$scheme_name'";
            }
            elseif(file_exists($filename = __DR__."/dataschemes/".$scheme_name.".php")){
            }
            elseif(file_exists($filename = __DR__."/wirix/dataschemes/".$scheme_name.".php")){
            }
            else{
                $this->error = "Отсутствует файл описания схемы данных '$scheme_name'";
                return false;
            }
            include_once($filename);

            
            $class_name = 'datascheme_'.$scheme_name;
            if(!class_exists($class_name)){
                $this->error = "Класс <b>$class_name</b> отсутствует";
                return false;
            }

            $obj = new $class_name();
            $obj->init();
            
            return $obj;
            
        }
        
    }

    function _w($msgid, $params = array()){
        echo _t($msgid, $params);
    }
    
    function _t($msgid, $params = array()){
        return $_SERVER['locale']->get($msgid, $params);
    }


$_SERVER['wirix'] = new wirix;
$_SERVER['wirix']->id = 'wirix';
$_W = $_SERVER['wirix'];

$_SERVER['db'] = $_SERVER['wirix']->lib_load('db');
$_SERVER['db']->id = 'db';
$_D = $_SERVER['db'];

$_SERVER['mod_rewrite'] = $_SERVER['wirix']->lib_load('mod_rewrite');
$_MR = $_SERVER['mod_rewrite'];

$_SERVER['http'] = $_SERVER['wirix']->lib_load('http');
$_SERVER['http']->id = 'http';
$_H = $_SERVER['http'];

$_SERVER['wirix']->lib_load('session');

$_SERVER['locale'] = $_SERVER['wirix']->lib_load('locale');
$_L = $_SERVER['locale'];

$_SERVER['locale']->id = 'locale';
$_SERVER['auth'] = $_SERVER['wirix']->lib_load('auth');
$_SERVER['auth']->id = 'auth';
$_A = $_SERVER['auth'];


?>
