<?php
    include_once(dirname(__FILE__)."/../../wirix.class.php");

    /**
     * Класс оплаты
     */
    class wirix_robokassa extends wirix{

		// Url обращения к робокассе
		var $url = "http://test.robokassa.ru/Index.aspx";
        // url для XML-API
        var $xml_url = "http://test.robokassa.ru/Webservice/Service.asmx/";

        // Тестовый режим и передаваемый при нем код статуса
        var $test_mode = true;
        var $stat_code = 100;

		// регистрационная информация (логин, пароль #1)
		var $mrh_login = "demo";
		var $mrh_pass1 = "password_1";
		var $mrh_pass2 = "password_2";
		
		var $email;
		
		// номер заказа
		var $inv_id = 1	;
		
		// описание заказа
		var $inv_desc = "Техническая документация по ROBOKASSA";
		
		// сумма заказа
		var $out_summ = "10.96";
		
		// тип товара
		var $shp_item = 1;
		
		// предлагаемая валюта платежа
		// default payment e-currency
		var $in_curr = "";
		
		// язык
		var $culture = "ru";
		
		// кодировка
		var $encoding = "utf-8";
		
        function __construct(){
            parent::__construct();
        }
        
        /**
        	Вычисление сигнатуры 1
        */
		function calc_crc1(){
			$crc  = md5($this->mrh_login.":".$this->out_summ.":".$this->inv_id.":".$this->mrh_pass1);
			return $crc;
		}   

        /**
        	Вычисление сигнатуры 2
        */
		function calc_crc2(){
			$crc  = md5($this->out_summ.":".$this->inv_id.":".$this->mrh_pass2);
			return $crc;
		}   
		
        /**
        	Вычисление сигнатуры 3
        */
		function calc_crc3(){
			$crc  = md5($this->out_summ.":".$this->inv_id.":".$this->mrh_pass1);
			return $crc;
		}   
        
        /**
        	Вычисление сигнатуры 4
        */
		function calc_crc4(){
			$crc  = md5($this->mrh_login.":".$this->inv_id.":".$this->mrh_pass2);
			return $crc;
		}   
        
		function fetch($object = null){
			// формирование подписи
			// generate signature
			$crc  = $this->calc_crc1();
			return '<form action="'.$this->url.'" method="GET">
<input type="text" name="MrchLogin" value="'.$this->mrh_login.'">			
<input type="text" name="OutSum" value="'.$this->out_summ.'">			
<input type="text" name="InvId" value="'.$this->inv_id.'">			
<input type="text" name="InvDesc" value="'.$this->inv_desc.'">			
<input type="text" name="SignatureValue" value="'.$crc.'">			
<input type="text" name="IncCurrLabel" value="'.$this->in_curr.'">			
<input type="text" name="Email" value="'.$this->email.'">			
<input type="text" name="Culture" value="'.$this->culture.'">			
<input type="submit" name="pay" value="Оплатить"/>
</form>';
		}
		
		/**
			Получениессылки на оплату
		*/
		function get_url(){
			$crc = $this->calc_crc1();
			return $this->url.'?'.
				'MrchLogin='.$this->mrh_login.			
				'&OutSum='.$this->out_summ.			
				'&InvId='.$this->inv_id.
				'&InvDesc='.$this->inv_desc.			
				'&SignatureValue='.$crc.			
				'&IncCurrLabel='.$this->in_curr.			
				'&Email='.$this->email.			
				'&Culture='.$this->culture;
			
		} 

        /**
          Формирование URL для получения XML о состоянии счета от робокассы
        */
        function get_xml_url(){
            $url = $this->xml_url."OpState?MerchantLogin=".$this->mrh_login."&InvoiceID=".$this->inv_id.
              "&Signature=".$this->calc_crc4().($this->test_mode?"&StateCode=".$this->stat_code:"");
            return $url;
        }

        /**
            Получение состояния платежа
        */
        function get_payment_xml_state(){
            $url = $this->get_xml_url();
            $_SERVER['http']->url = $url;
            $_SERVER['http']->request();
            if($_SERVER['http']->returned_code!=200){
                return array("code"=>0);
            }

            $xml = simplexml_load_string($_SERVER['http']->content);
            if(!$xml){
                return array("code"=>1);
            }
            
            $xml = get_object_vars($xml);
            $xml['Result'] = get_object_vars($xml['Result']);
            $xml['State'] = get_object_vars($xml['State']);
            $xml['Info'] = get_object_vars($xml['Info']);

            $result = array();
            $result['code']         = isset($xml['State']['Code'])?$xml['State']['Code']:false;
            $result['sum']          = isset($xml['Info']['IncSum'])?$xml['Info']['IncSum']:false;
            $result['sum_cur']      = isset($xml['Info']['IncCurrLabel'])?$xml['Info']['IncCurrLabel']:false;
            $result['sum_account']  = isset($xml['Info']['IncAccount'])?$xml['Info']['IncAccount']:false;
            $result['income']       = isset($xml['Info']['OutSum'])?$xml['Info']['OutSum']:false;
            $result['income_cur']   = isset($xml['Info']['OutCurrLabel'])?$xml['Info']['OutCurrLabel']:false;

            return $result;
        }

		
		/**
			Проверка уведомления об оплате
			@param $crc - проверяемое значение crc
		*/       
		function check_notify($crc){
            // Если платёж уже принят - то результат проверки контрольной суммы - всегда отрицательный
            /*
            if(
                $_SERVER['db']->search_one(
                    "balance_incomes", 
                    array(), 
                    "`id`=".$this->inv_id." AND `accept_date`!='0000-00-00 00:00:00'"
                )
            )return false;
            */
			return strtolower($crc)==$this->calc_crc2();	
		}
        

		/**
			Проверка успешного перенаправления
			@param $crc - проверяемое значение crc
		*/       
		function check_success($crc){
			return strtolower($crc)==$this->calc_crc3();
		}

    }

	
