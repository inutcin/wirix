<?php

    /**
        Класс для работы с простой однослойной нейросетью с функцией активации типа 
        "линейный порог(гистерезис)"
    */
    
    class wirix_neuro_simple extends wirix{
        
        var $dimension = 1;         //!< Размерность сети (количество нейронов на выходе)
        var $weights = array();     //!< Веса связей
        var $normalize = array();   //!< Таблица нормализации
        
        var $thr_start  = 0;        //!< Старт линейного порога функции активации
        var $thr_finish = 1;        //!< Финиш линейного порога функции активации
        
        var $min_w = 0;         //!< Минимальный вес при генерации случайных весов
        var $max_w = 0.1;       //!< Максимальный вес при генерации случайных весов
        
        var $step = 0.01;       //!< Шаг обучения
        
        function __construct(){
            parent::__construct();
        }


        /**
            Установка интервала нормализации до указанного значения.
            
            @param $id - номер входа, для которого устанавливаем нормализацию
            @param $value - значение, до которого параметру выставляется уровень (<=)
            @param - уровень входа от 0 до 1 (от "нет" до "да")
        */
        function set_normalize_before(
            $id,        //!< Номер входа
            $value,     //!< Сначение параметра
            $level      //!< Уровень 
        ){
            
            if(!isset($this->normalize[$id]))$this->normalize[$id] = array();
            if(!isset($this->normalize[$id]['before']))$this->normalize[$id]['before'] = array();
            $this->normalize[$id]['before'] = array("value"=>"$value", "level" => $level);
        }


        /**
            Установка интервала нормализации после указанного значения.
            
            @param $id - номер входа, для которого устанавливаем нормализацию
            @param $value - значение, после которого параметру выставляется уровень
            @param - уровень входа от 0 до 1 (от "нет" до "да")
        */
        function set_normalize_after(
            $id,        //!< Номер входа
            $value,     //!< Сначение параметра
            $level      //!< Уровень 
        ){
            
            if(!isset($this->normalize[$id]))$this->normalize[$id] = array();
            if(!isset($this->normalize[$id]['after']))$this->normalize[$id]['after'] = array();
            $this->normalize[$id]['after'] = array("value"=>"$value", "level" => $level);
        }

        /**
            Установка интервала нормализации от и до указанных значений.
            
            @param $id - номер входа, для которого устанавливаем нормализацию
            @param $value - значение, после которого параметру выставляется уровень
            @param - уровень входа от 0 до 1 (от "нет" до "да")
        */
        function set_normalize_from_to(
            $id,        //!< Номер входа
            $from,      //!< Сначение параметра от которого (строго больше)
            $to,        //!< Сначение параметра до которого (меньше или равно)
            $level      //!< Уровень 
        ){
            
            if(!isset($this->normalize[$id]))$this->normalize[$id] = array();
            if(!isset($this->normalize[$id]['from_to']))$this->normalize[$id]['from_to'] = array();
            $this->normalize[$id]['from_to'][] = array("from"=>$from, "to"=>$to, "level"=>$level);
        }
        
        /**
            Получение нормализованного значения в интервале от 0 до 1
            для входного параметра с указанным ID по таблице нормализации
            
            @param $id      - ID параметра для которого получаем нормализованное значение
            @param $value   - ненормализованное значение параметра
            
            @return нормализаванное значение параметра или -1 в случае неверного указания ID параметра
        */
        private function get_normalize_value($id, $value){
            if(!isset($this->normalize[$id]))return -1;
            
            if($value <= $this->normalize[$id]['before']['value'])return $this->normalize[$id]['before']['level'];
            foreach($this->normalize[$id]['from_to'] as $info)
                if($value > $info['from'] && $value <= $info['to'])return $info['level'];
            if($value > $this->normalize[$id]['after']['value'])return $this->normalize[$id]['after']['level'];
            return 0.5; // Возвращаем неопределённость
        }

        /**
            Создание нейросети. Веса создаваемых связей генерируются случайно в дипазоне от 
            $this->min_w до $this->max_w
            
            @param $inputs  - Количество входов
            @param $dims    - Количество выходов(размерность)
        */
        function create($inputs, $dims){
            $this->dimension = $dims;
            // Назначаем случайные веса
            for($i=0;$i<$dims;$i++){
                if(!isset($this->weights[$i]))$this->weights[$i] = array();
                for($j=0;$j<$inputs;$j++){
                    $this->weights[$i][$j] = $this->min_w + (($this->max_w - $this->min_w)/10000)*rand(0,10000);
                    $this->inputs[$j] = 0;
                }
            }
        }

        /**
            Вычисление ответа сети на 
            
            @param $vector - массив входных параметров
        */
        function answer($vector){
            $answer = array();
            foreach($vector as $k=>$input)$vector[$k] = $this->get_normalize_value($k, $input);
            
            foreach($this->weights as $output_id=>$inputs){
                $answer[$output_id] = 0;
                foreach($inputs as $input_id=>$input)
                    $answer[$output_id] += $this->weights[$output_id][$input_id]*$vector[$input_id];


                $answer[$output_id] = $this->F($answer[$output_id]);
            }
            return $answer;
        }
        
        
        /**
            Обучение нейросети
            
            @param $learn_inputs   - пример входных параметров
            @param $learn_outputs  - пример правильного выхода
        */
        function learn($learn_inputs = array(), $learn_outputs = array()){
            // Вычисляем реальный выход нейросети
            $real_output = $this->answer($learn_inputs);
            
            // Вычисление градиент между обучающей выборкой и реальным выходом
            $diff = array();
            foreach($learn_outputs as $name=>$output)
                $diff[$name] = $learn_outputs[$name]-$real_output[$name];
                
            // Вычисляем размер шага в направлении антиградиента
            $steps = array();
            foreach($this->weights as $output_id=>$inputs){
                if(!isset($steps[$output_id]))$steps[$output_id] = array();
                foreach($inputs as $input_id=>$input)
                    $steps[$output_id][$input_id] = 
                        $this->step*$learn_inputs[$input_id]*$diff[$output_id];
                
            }

			// Длина градиента
			$ssum = 0;            
            foreach($this->weights as $output_id=>$inputs)
                foreach($inputs as $input_id=>$input)
                	$ssum += abs($steps[$output_id][$input_id]);
            
            
            // Делаем шаг в направлении антиградиента
            foreach($this->weights as $output_id=>$inputs)
                foreach($inputs as $input_id=>$input)
                    $this->weights[$output_id][$input_id] += $steps[$output_id][$input_id];
            
            
			return $ssum;            
            
            /*
            print_r($learn_inputs);
            print_r($learn_outputs);
            print_r($diff);
            echo "<hr/>";


            print_r($real_output);
            print_r($steps);
            print_r($this->weights);
            */
            
             
            
        }
        
        
        /**
            Функция активации нейрона (линейный порог)
        */
        private function F($x){
        	//return $x;
            $result = $x<$this->thr_start?0:$x;
            $result = $result>$this->thr_finish?1:$result;
            
            return $result;
        }
    }

?>
