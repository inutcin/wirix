<?php
    include("../../wirix.class.php");
    
    $neuro_perc = $_SERVER['wirix']->lib_load("neuro_perc");
    
    
    /*****************************************************
    
    Тестовый пример для обучения нейросети без учителя
    по разбиению жратвы на классы

    
    *****************************************************/
    
    //===Устанавливаем объём внутренних слоёв нейросети
    // 2 слоя
    $neuro_perc->set_layers_count(1);
    // по 4 нейрона
    $neuro_perc->set_neurons_in_layer(16);
    
    // 4 входа
    $neuro_perc->set_inputs_count(4);
    // 3 выхода
    $neuro_perc->set_outputs_count(1);
    
    
    // Нормализация белков
    $neuro_perc->set_norm(1,array(
        array("[from"=>-INF,    "to)"=>0.4,     "value"=>0),
        array("[from"=>0.4,     "to)"=>1.6  ,   "value"=>0.1),
        array("[from"=>1.6,     "to)"=>4.0,     "value"=>0.2),
        array("[from"=>4.0,     "to)"=>8.0,     "value"=>0.3),
        array("[from"=>8.0,     "to)"=>15.0,    "value"=>0.4),
        array("[from"=>15.0,    "to)"=>20.0,    "value"=>0.5),
        array("[from"=>20,      "to)"=>25,      "value"=>0.6),
        array("[from"=>25,      "to)"=>30,      "value"=>0.7),
        array("[from"=>30,      "to)"=>35,      "value"=>0.8),
        array("[from"=>35,      "to)"=>45,      "value"=>0.9),
        array("[from"=>45,      "to)"=>INF,     "value"=>1),
    ));

    // Нормализация жиров
    $neuro_perc->set_norm(2,array(
        array("[from"=>-INF,    "to)"=>10,  "value"=>0),
        array("[from"=>10,      "to)"=>20,  "value"=>0.1),
        array("[from"=>20,      "to)"=>30,  "value"=>0.2),
        array("[from"=>30,      "to)"=>40,  "value"=>0.3),
        array("[from"=>40,      "to)"=>50,  "value"=>0.4),
        array("[from"=>50,      "to)"=>60,  "value"=>0.5),
        array("[from"=>60,      "to)"=>70,  "value"=>0.6),
        array("[from"=>70,      "to)"=>80,  "value"=>0.7),
        array("[from"=>80,      "to)"=>90,  "value"=>0.8),
        array("[from"=>90,      "to)"=>100, "value"=>0.9),
        array("[from"=>100,     "to)"=>INF, "value"=>1),
    ));

    // Нормализация углеводов
    $neuro_perc->set_norm(3,array(
        array("[from"=>-INF,    "to)"=>10,  "value"=>0),
        array("[from"=>10,      "to)"=>20,  "value"=>0.1),
        array("[from"=>20,      "to)"=>30,  "value"=>0.2),
        array("[from"=>30,      "to)"=>40,  "value"=>0.3),
        array("[from"=>40,      "to)"=>50,  "value"=>0.4),
        array("[from"=>50,      "to)"=>60,  "value"=>0.5),
        array("[from"=>60,      "to)"=>70,  "value"=>0.6),
        array("[from"=>70,      "to)"=>80,  "value"=>0.7),
        array("[from"=>80,      "to)"=>90,  "value"=>0.8),
        array("[from"=>90,      "to)"=>100, "value"=>0.9),
        array("[from"=>100,     "to)"=>INF, "value"=>1),
    ));

    
    // Нормализация Калорийности
    $neuro_perc->set_norm(4,array(
        array("[from"=>-INF,    "to)"=>80,      "value"=>0),
        array("[from"=>80,      "to)"=>160,     "value"=>0.1),
        array("[from"=>160,     "to)"=>240,     "value"=>0.2),
        array("[from"=>240,     "to)"=>320,     "value"=>0.3),
        array("[from"=>320,     "to)"=>400,     "value"=>0.4),
        array("[from"=>400,     "to)"=>480,     "value"=>0.5),
        array("[from"=>480,     "to)"=>560,     "value"=>0.6),
        array("[from"=>560,     "to)"=>640,     "value"=>0.7),
        array("[from"=>640,     "to)"=>720,     "value"=>0.8),
        array("[from"=>720,     "to)"=>800,     "value"=>0.9),
        array("[from"=>800,     "to)"=>INF,     "value"=>1),
    ));

    // Нормализация переваривания
    /*
    $neuro_perc->set_norm(5,array(
        array("[from"=>-INF,    "to)"=>0.4,     "value"=>0),
        array("[from"=>0.4,     "to)"=>0.8,     "value"=>0.1),
        array("[from"=>0.8,     "to)"=>1.2,     "value"=>0.2),
        array("[from"=>1.2,     "to)"=>1.6,     "value"=>0.3),
        array("[from"=>1.6,     "to)"=>2.0,     "value"=>0.4),
        array("[from"=>2.0,     "to)"=>2.4,     "value"=>0.5),
        array("[from"=>2.4,     "to)"=>2.8,     "value"=>0.6),
        array("[from"=>2.8,     "to)"=>3.2,     "value"=>0.7),
        array("[from"=>3.2,     "to)"=>3.6,     "value"=>0.8),
        array("[from"=>3.6,     "to)"=>4,       "value"=>0.9),
        array("[from"=>4.0,     "to)"=>INF,     "value"=>1),
    ));
    */


    /** ********************************************************
        Зададим обучающие примеры
        
    **********************************************************/
    $inputs = array();
    $data = file("food.csv");
    foreach($data as $line){
        if(!$line = trim($line))continue;
        $line = str_replace(",",".",$line);
        $tmp = explode("\t", $line);
        $flag = 0;
        foreach($tmp as $k=>$v){
            if(trim($tmp[$k])=='')$flag = 1;
            $tmp[$k] = trim($tmp[$k])?trim($tmp[$k]):0;
        }
        $row = array();
        for($i=1;$i<=4;$i++)$row[] = $tmp[$i];
        if(!$flag)$inputs[$tmp[0]] = $row;
    }
    

    // Вводим примеры в нейросеть
    foreach($inputs as $k=>$v){$neuro_perc->add_example($v,array());}

    $neuro_perc->create();
    
    // Обучим нейросеть
    $neuro_perc->learn_wt($iterations = 1000 , $step=0.3);
    
    // Выведем итоговое разделение по классам

    echo '<table border="1" style="border-collapse: collapse;">';
    echo '<tr><th></th><th>Качество</th></tr>';
    $min = INF;
    $max = -INF;
    foreach($inputs as $i=>$v_row){
        echo "<tr>";
        $data = array();
        foreach($v_row as $a=>$b)$data[$a+1] = $b;
        $neuro_perc->set_inputs($data);
        $outputs_row = $neuro_perc->calc();
        echo "<td>".$i."</td>";
        foreach($outputs_row as $k=>$v)echo "<td>".number_format($v,5,',',' ')."</td>";
        if($v<$min)$min = $v;
        if($v>$max)$max = $v;
        echo "</tr>";
    }
    echo "</table>";
    
    $intervals = 30;
    $step = ($max-$min)/$intervals;
    $gistogramm = array();
    $classes = array();
    $mo = array();
    
    // Распределяем продукты по интервалам
    for($i=0;$i<$intervals-1;$i++){
        $from   = $min+$i*$step;
        $to     = $min+($i+1)*$step;
        $index  = "[".number_format($from,5,',',' ').":".number_format($to,5,',',' ').")";
        $gistogramm[$index] = 0;
        $mo[$index] = ($to+$from)/2.0;
        $classes[$index] = array();
        foreach($inputs as $name=>$v_row){
            $data = array();
            foreach($v_row as $a=>$b)$data[$a+1] = $b;
            $neuro_perc->set_inputs($data);
            $outputs_row = $neuro_perc->calc();
            foreach($outputs_row as $k=>$v){}
            if($v>=$from && $v<$to){
                $gistogramm[$index]++;
                $classes[$index][$name] = $v;
            }
        }
    }
    
    // Находим пиковые интервалы
    $indexes = array();
    foreach($gistogramm as $index=>$count)$indexes[] = $index;
    $peaks = array();
    foreach($indexes as $i=>$index){
        if($i==0 && $gistogramm[$indexes[$i+1]]<$gistogramm[$indexes[$i]]){
            $peaks[] = $mo[$index];
        }
        elseif($i==count($indexes)-1 && $gistogramm[$indexes[$i-1]]<$gistogramm[$indexes[$i]]){
            $peaks[] = $mo[$index];
        }
        elseif(
            $i>0 
            &&
            $i<count($indexes)-1
            &&
            $gistogramm[$indexes[$i-1]]<$gistogramm[$indexes[$i]]
            &&
            $gistogramm[$indexes[$i+1]]<$gistogramm[$indexes[$i]]
        ){
            $peaks[] = $mo[$index];
        }
    }
   
    
    // Ставим каждому продукту расстояние от ядра класса
    $classes = array();
    foreach($peaks as $i=>$center){
        $classes[$i] = array();
        
        foreach($inputs as $name=>$v_row){
            $data = array();
            foreach($v_row as $a=>$b)$data[$a+1] = $b;
            $neuro_perc->set_inputs($data);
            $outputs_row = $neuro_perc->calc();
            foreach($outputs_row as $k=>$v){}
            $classes[$i][$name] = abs($center-$v);
        }
        asort($classes[$i]);
    }
    
    // Исключаем из классов продукты с самым большим расстоянием
    foreach($classes[0] as $name=>$radius){
        if(!isset($classes[0][$name]))continue;
        $min_radius = INF;
        $min_index = -1;
        for($i=0;$i<count($classes);$i++)
            if($classes[$i][$name]<$min_radius){$min_radius = $classes[$i][$name]; $min_index = $i;}
        // Очищаем все, ктоме минимального
        for($i=0;$i<count($classes);$i++)
            if($i!=$min_index && isset($classes[$i][$name]))unset($classes[$i][$name]);
    }
    
    $content_title = array("белки","жиры","углеводы","калории");
    foreach($classes as $class_num=>$class){
        echo "<h3>Категория ".($class_num+1)."</h3>";
        echo "<ol>";
        foreach($class as $name=>$v){
            $content = ' (';
            foreach($inputs[$name] as $k=>$v){
                $content .= $content_title[$k]."=".$inputs[$name][$k]."; ";
            }
            $content .= ')';
            echo "<li>".$name." ".$content."</li>";
        }
        echo "</ol>";
    }
    
    echo "<pre>";
    print_r($gistogramm);
    echo "</pre>";
    
    echo "<pre>";
    print_r($peaks);
    echo "</pre>";
