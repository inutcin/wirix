<?php

    /**
        Многослойный перцептрон
    */
    class wirix_neuro_perc extends wirix{
        
        var $layers_count = 1;                  //!< Количество слоёв
        var $neurons_in_layer = 1;              //!< Нейронов в слое
        var $w  = array(                            //!< Массив связей
            "input"=>array(),                       // Связи входного слоя
            "internal"=>array()                     // ПРочие связи(включая и выходной слой)
        );                                          
        var $neurons_positions = array();       //!< Положения нейронов по слоям
        var $neurons = array();                 //!< Нейроны
        var $inputs_count = 1;                  //!< Количество входов
        var $outputs_count = 1;                 //!< Количество выходов

        var $inputs = array();                  //!< Входы
        var $outputs = array();                 //!< Выходы
        
        var $norm = array();                    // Массив нормализации. Каждый элемент array("[from"=>-INF,"to)"=>INF,"value"=>0.5)
        
        var $step = 1;                          //!< Шаг обучения
        var $examples = array();                //!< Примеры для обучения
        
        function __construct(){
            parent::__construct(__CLASS__);
        }
        
        
        /**
            Добавление примера для обучения
            @params  $inputs - массив ненормализованные значений входов с индексом от 0
            @params  $outputs - нормализованный массив выходов с индексом от 0
        */
        function add_example($inputs, $outputs){
            $this->examples[] = array(
                "inputs"    =>  $inputs,
                "outputs"   =>  $outputs
            );
        }
        
        /**
            Очистка обучающих примеров
        */
        function clear_example(){
            $this->examples = array();
        }
        
        /**
            Установка массива нормализации
            @params $input_num - номер входа
            @params $norm Массив нормализации. Каждый элемент array("[from"=>-INF,"to)"=>INF,"value"=>0.5)
        */
        function set_norm($input_num, $norm){
            if(!isset($this->norm[$input_num]))
            $this->norm[$input_num] = $norm;
        }
        
        /**
            Установка значения входов
            @params - массив входов. Индекс начинается с 1
        */
        function set_inputs($inputs){$this->inputs = $inputs;}

        /**
            Установка значения выходов. Контрольных значений для обучения
            @params - массив выходов. Индекс начинается с 1
        */
        function set_outputs($outputs){$this->outputs = $outputs;}
        
        /**
            Задание количества внутренних слоёв
            @param $count - количество слоёв
         */
        function set_layers_count($count){$this->layers_count = $count;}
        
        /**
            Задание количества нейронов в 1-м внутреннем слое
            @param $count - количество нейронов в 1-м внутреннем слое
        */
        function set_neurons_in_layer($count){$this->neurons_in_layer = $count;}

        /**
            Задание количества нейронов входного слоя
            @param $count - количество нейронов входного слоя
         */
        function set_inputs_count($count){$this->inputs_count = $count;}

        /**
            Задание количества нейронов выходного слоя
            @param $count - количество нейронов выходного слоя
         */
        function set_outputs_count($count){$this->outputs_count = $count;}
        
        /**
            Создание(инициация) нейросети по установленным параметрам
         */
        function create(){
            $n=0;
            
            // Формируем входы
            for($i=1;$i<=$this->inputs_count;$i++)$this->inputs[$i] = 0;
            
            // Формируем нейроны внутренних слоёв
            for($i=0;$i<$this->layers_count;$i++)
                for($j=0;$j<$this->neurons_in_layer;$j++){
                    if(!isset($this->neurons_positions[$i]))$this->neurons_positions[$i] = array();
                    $n++;
                    $this->neurons_positions[$i][$j] = $n;
                    $this->neurons[$n] = array("out"=>0,"layer"=>$i,"position"=>$n);
                }
            
            // Формируем нейроны выходного слоя
            for($j=0;$j<$this->outputs_count;$j++){
                if(!isset($this->neurons_positions[$i]))$this->neurons_positions[$i] = array();
                $n++;
                $this->neurons_positions[$i][$j] = $n;
                $this->neurons[$n] = array("out"=>0,"layer"=>$i,"value"=>0);
            }
            
            // Формируем связи нейронов 0-го слоя со входами
            $w=0;
            foreach($this->neurons_positions[0] as $positions)
                foreach($this->inputs as $i=>$v){
                    $w++;
                    if(!isset($this->w['input'][$i]))$this->w['input'][$i] = array();
                    $this->w['input'][$i][$positions] = -0.001+rand(0,2000000)/100000000;
                }
            
            // Формируем связи нейронов внутренних слоёв
            for($i=1;$i<$this->layers_count;$i++){
                foreach($this->neurons_positions[$i] as $n_pos_to){
                    foreach($this->neurons_positions[$i-1] as $n_pos_from){
                        if(!isset($this->w['internal'][$n_pos_from]))
                            $this->w['internal'][$n_pos_from] = array();
                        $this->w['internal'][$n_pos_from][$n_pos_to] = -0.001+rand(0,2000000)/100000000;
                    }
                }
            }
            
            // Формируем связи нейронов выходногослоя
            foreach($this->neurons_positions[$this->layers_count] as $n_pos_to){
                foreach($this->neurons_positions[$this->layers_count-1] as $n_pos_from){
                    if(!isset($this->w['internal'][$n_pos_from]))
                        $this->w['internal'][$n_pos_from] = array();
                    $this->w['internal'][$n_pos_from][$n_pos_to] = -0.001+rand(0,2000000)/100000000;
                }
            }
        
        }
        
        /**
            Вычисление выхода нейросети
            @returns массив ответа нейросети
        */
        function calc(){
            $inputs = array();
            foreach($this->inputs as $num=>$val){
                $inputs[$num] = $this->get_norm_value($num, $val);
            }

            // Вычисляем выход 0-го слоя нейросети
            foreach($this->neurons_positions[0] as $pos){
                $sum = 0;
                foreach($this->inputs as $in=>$val){
                    $sum += 
                            $inputs[$in]
                            *
                            $this->w['input']
                            [$in]
                            [$pos];
                }
                $this->neurons[$pos]['value'] = $this->s($sum);
            }

            // Вычисляем выходы нейронов промежуточных слоёв и выходного
            for($i=1;$i<=$this->layers_count;$i++){
                foreach($this->neurons_positions[$i] as $pos_to){
                    $sum = 0;
                    foreach($this->neurons_positions[$i-1] as $pos_from){
                        $sum += $this->w['internal']
                            [$pos_from]
                            [$pos_to]*$this->neurons[$pos_from]['value'];
                    }
                    $this->neurons[$pos_to]['value'] = $this->s($sum);
                }
            }
            
            $return = array();
            foreach($this->neurons_positions[$this->layers_count] as $pos)
                $return[] = $this->neurons[$pos]['value'];
            return $return;
        }
        
        /**
            Получение нормализованного значения для входа
            @params $input_num - номер входа (начиная от 1)
            @params $value - ненормализованное значение
            
            @returns - нормализованное значение
        */
        function get_norm_value($input_num, $value){
            if($value=='')return 0.5;
            if(!isset($this->norm[$input_num]))return 0.5;
            foreach($this->norm[$input_num] as $norm){
                if($norm["[from"]<=$value && $norm["to)"]>$value)return $norm["value"];
            }
            // Если нет значения нормализации выдаём неопределённое
            return 0.5;
        }
        
        
        /**
            Один шаг обучения нейросети без учителя по установленным 
            функциями set_inputs значениям входов
        */
        function learn_step_wt(){
            $delta = array("input"=>array(),"internal"=>array()); // Массив ошибок
            $sum_delta = 0;
            // Вычисляем выход нейросети
            $this->calc();
            
            // Определяем победителя входного слоя
            $neurons = array();
            foreach($this->w['input'] as $in=>$row){
                foreach($row as $out=>$w){
                    if(!isset($neurons[$out]))$neurons[$out] = array();
                    $neurons[$out][$in] = $w;
                }
            }
            
            // Считаем расстояния
            $min = INF;
            $min_ind = 0;
            foreach($neurons as $out=>$row){
                $sum = 0;
                foreach($row as $in=>$w){
                    $sum += pow($w-$this->inputs[$in], 2);
                }
                if($sum<$min){$min = $sum;$min_ind=$out;}
            }
            
            foreach($this->w['input'] as $inp_num=>$row){
                $in     =   $this->wallf($this->get_norm_value($inp_num, $this->inputs[$inp_num]));  
                $out    =   $this->wallf($this->neurons[$min_ind]['value']);
                $delta  = ($out*$in)*$this->step;
                $this->w['input'][$inp_num][$min_ind] 
                    -= $delta;            
            }
            
            // Корректируем остальные связи
            
            // Проходим слой за слоем
            
            for($in_layer=0;$in_layer<count($this->neurons_positions)-1;$in_layer++){
                $neurons = array();
                foreach($this->neurons_positions[$in_layer] as $in_id){
                    foreach($this->neurons_positions[$in_layer+1] as $out_id){
                        if(!isset($neurons[$out_id]))$neurons[$out_id] = array();
                        $neurons[$out_id][$in_id] = $this->w['internal'][$in_id][$out_id];
                    }
                }
                // Вычисляем минимальное расстояние
                $min = INF;
                $min_ind = 0;
                foreach($neurons as $out=>$row){
                    $sum = 0;
                    foreach($row as $in=>$w){
                        $sum += pow($w-$this->neurons[$in]['value'], 2);
                    }
                    if($sum<$min){$min = $sum;$min_ind=$out;}
                }
                foreach($this->neurons_positions[$in_layer] as $inp_num){
                    $in     =   $this->wallf($this->neurons[$inp_num]['value']);  
                    $out    =   $this->wallf($this->neurons[$min_ind]['value']);
                    $delta  = ($out*$in)*$this->step;
                    $this->w['internal']
                    [$inp_num]
                    [$min_ind] 
                        -= $delta;            
                }

                
            }
            
            return $sum_delta;
        }
        
        /**
            Обучение без учителя
        */
        function learn_wt($iterations=100, $step=1){
            $this->step = $step;
            $return = INF;
            // Повторяем $iterations раз все примеры для обучения
            for($i=0;$i<$iterations;$i++){
                foreach($this->examples as $example){
                    // Приводим значения входов и выходов к пригодному для шага обучения виду
                    $inputs = array();foreach($example['inputs'] as $k=>$v)$inputs[$k+1] = $v;
                    // Устанавливаем значения входов и контрольные значения выходов
                    $this->set_inputs($inputs);
                    // Производим один шаг обучения
                    $return = $this->learn_step_wt();
                }
            }
            return $return;
        }
        
        
        /**
            Один шаг обучения нейросети по установленным функциями set_inputs и set_outputs
            значениям входов и выходов
        */
        function learn_step(){
            $delta = array(); // Массив ошибок
            // Вычисляем выход нейросети
            $this->calc();
            // Вычисляем ошибки выходного слоя
            foreach($this->neurons_positions[$this->layers_count] as $k=>$pos){
                $delta[$pos] = 
                    -$this->ds($this->neurons[$pos]['value'])
                    *
                    ($this->outputs[$k+1] - $this->neurons[$pos]['value']);
            }
            // Вычисляем ошибки внутренних слоёв
            for($i=$this->layers_count-1;$i>=0;$i--){
                foreach($this->neurons_positions[$i] as $pos_from){
                    $d = 0;
                    foreach($this->neurons_positions[$i+1] as $pos_to){
                        $d +=   $this->ds($this->neurons[$pos_to]['value'])
                                *
                                $delta[$pos_to]
                                *
                                $this->w['internal'][$pos_from][$pos_to];
                    }
                    $delta[$pos_from] = $d;
                }
            }
            
            // Правим веса входов
            foreach($this->w['input'] as $inp_num=>$row){
                foreach($row as $n_num=>$w){
                    $this->w['input'][$inp_num][$n_num] 
                        -=  (
                            $this->step
                            *
                            $delta[$n_num]
                            *
                            $this->ds($this->neurons[$n_num]['value'])
                            *
                            $this->inputs[$inp_num]
                            );
                            
                }
            }
            
            // Правим остальные веса
            foreach($this->w['internal'] as $inp_num=>$row){
                foreach($row as $out_num=>$w){
                    $this->w['internal'][$inp_num][$out_num] 
                        -=  $this->step
                            *
                            $delta[$out_num]
                            *
                            $this->ds($this->neurons[$out_num]['value'])
                            *
                            $this->neurons[$inp_num]['value'];
                            
                }
            }
        }
        
        /**
            Обучение нейросети на примерах
            @param $iterations - количество итераций обучения
            @param $step - величина шага обучения
        */
        function learn($iterations=100, $step=1){
            $this->step = $step;
            // Повторяем $iterations раз все примеры для обучения
            for($i=0;$i<$iterations;$i++)
                foreach($this->examples as $example){
                    // Приводим значения входов и выходов к пригодному для шага обучения виду
                    $inputs = array();foreach($example['inputs'] as $k=>$v)$inputs[$k+1] = $v;
                    $outputs = array();foreach($example['outputs'] as $k=>$v)$outputs[$k+1] = $v;
                    // Устанавливаем значения входов и контрольные значения выходов
                    $this->set_inputs($inputs);
                    $this->set_outputs($outputs);
                    // Производим один шаг обучения
                    $this->learn_step();
                }
        }
        
        /**
            Активационная функция - сигмоид
        */
        function s($x){
            return 1/(1+exp(-$x));
        }
        
        /**
            Производная активационной функции
        */
        function ds($x){
            //return $this->s($x)*(1-$this->s($x));
            return $x*(1-$x);
        }
        
        /**
            Пороговая функция
        */
        function wallf($x){
            return $x>0.5?1:0;
        }
        
    }
