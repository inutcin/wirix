<?php
    include("../../wirix.class.php");
    
    $neuro_perc = $_SERVER['wirix']->lib_load("neuro_perc");
    
    
    /*****************************************************
    
    Тестовый пример для обучения нейросети без учителя
    по разбиению автомобилей на классы

    
    Итого имеем нейросеть с 4 входами и 3 выходами.
    *****************************************************/
    
    //===Устанавливаем объём внутренних слоёв нейросети
    // 2 слоя
    $neuro_perc->set_layers_count(1);
    // по 4 нейрона
    $neuro_perc->set_neurons_in_layer(6);
    
    // 4 входа
    $neuro_perc->set_inputs_count(4);
    // 3 выхода
    $neuro_perc->set_outputs_count(1);
    
    
    /***********************************************************
    Используем следующую таблицу нормазизации признаков
    
    Нет             - 0
    Скорее нет      - 0.25
    Ни нет ни да    - 0.5
    Скорее да       - 0.75
    Да              - 1
    
                                        0       0.25    0.50    0.75    1
    -----------------------------------------------------------------------
    1)Большая масса автомобиля(т.)      0.5     1       2       5       >5    
    2)Большая мощность двигателя(л.с.)  20      50      100     200     >200      
    3)Большое количество пассажиров(чел)2       5       10      20      >20  
    4)Большая грузоподъёмность(т.)      1       2       3       4       >4
    
    *****************************************************/
    
    $neuro_perc->set_norm(1,array(
        array("[from"=>-INF,    "to)"=>0.5,     "value"=>0),
        array("[from"=>0.5,     "to)"=>1,       "value"=>0.25),
        array("[from"=>1,       "to)"=>2,       "value"=>0.5),
        array("[from"=>2,       "to)"=>5,       "value"=>0.75),
        array("[from"=>5,       "to)"=>INF,     "value"=>1),
    ));

    $neuro_perc->set_norm(2,array(
        array("[from"=>-INF,    "to)"=>20,      "value"=>0),
        array("[from"=>20,      "to)"=>50,      "value"=>0.25),
        array("[from"=>50,      "to)"=>100,     "value"=>0.5),
        array("[from"=>100,     "to)"=>200,     "value"=>0.75),
        array("[from"=>200,     "to)"=>INF,     "value"=>1),
    ));
    
    $neuro_perc->set_norm(3,array(
        array("[from"=>-INF,    "to)"=>2,       "value"=>0),
        array("[from"=>2,       "to)"=>5,       "value"=>0.25),
        array("[from"=>5,       "to)"=>10,      "value"=>0.5),
        array("[from"=>10,      "to)"=>20,      "value"=>0.75),
        array("[from"=>20,      "to)"=>INF,     "value"=>1),
    ));

    $neuro_perc->set_norm(4,array(
        array("[from"=>-INF,    "to)"=>1,       "value"=>0),
        array("[from"=>1,       "to)"=>2,       "value"=>0.25),
        array("[from"=>2,       "to)"=>3,       "value"=>0.5),
        array("[from"=>3,       "to)"=>4,       "value"=>0.75),
        array("[from"=>4,       "to)"=>INF,     "value"=>1),
    ));


    /** ********************************************************
        Зададим обучающие примеры
        
    **********************************************************/
    $inputs = array();
    
    //                                 Масса(т)Двигатель(л.с.)Пассажиры Груз (т.)
    //['Ока']
    $inputs[0]              =   array(  0.645,      33,         4,      0.34);
    //['Газель пас.']
    $inputs[1]              =   array(  2.880,      152,        14,     1.5);
    //['Калина'] 
    $inputs[2]              =   array(  1.110,      88,         5,      0.475);
    //['Камаз']
    $inputs[3]              =   array(  10.400,     260,        3,      6);
    //['Газель груз']
    $inputs[4]              =   array(  2.880,      152,        3,      1.5);
    //['ПАЗ4234']
    $inputs[5]              =   array(  9.895,      122,        30,     6.2);
    //['ЗИЛ-5301']
    $inputs[6]              =   array(  3.695,      136,        3,      3);
    //['Патриот']
    $inputs[7]              =   array(  2.170,      116,        5,      0.6);
    //['ЛиАЗ-6213']
    $inputs[8]              =   array(  27.895,     278,        150,    11);


    $outputs = array();
    //                                  Легковая    Пассажир.   Грузовая.
    //['Ока']
    $outputs[0]             =   array(       1,        0,        0    );
    //['Газель пас.']
    $outputs[1]             =   array(       0,        1,        0    );
    //['Калина'] 
    $outputs[2]             =   array(       1,        0,        0    );
    //['Камаз']
    $outputs[3]             =   array(       0,        0,        1    );
    //['Газель груз']
    $outputs[4]             =   array(       0,        0,        1    );
    //['ПАЗ4234']
    $outputs[5]             =   array(       0,        1,        0    );
    //['ЗИЛ-5301']
    $outputs[6]             =   array(       0,        0,        1    );
    //['Патриот']
    $outputs[7]             =   array(       1,        0,        0    );
    //['ЛиАЗ-6213']
    $outputs[8]             =   array(       0,        1,        0    );

    //['Белаз']
    $inputs[9]              =   array(  74,         1050,       1,      90 );
    //['БТР80']
    $inputs[10]             =   array(  13.6,       260,        10,     3 );
    //['Урал мото']
    $inputs[11]             =   array(  0.3,        40,         3,      0.25 );
    //['Повозка']
    $inputs[12]             =   array(  0.2,        1,          4,      0.2);
    //['Велосипед']
    $inputs[13]             =   array(  0.02,       0.3,         1,     0.1);
    //['Трамвай']
    $inputs[14]             =   array(  18.4,       245,        168,    12);


    // Вводим примеры в нейросеть
    foreach($inputs as $k=>$v){$neuro_perc->add_example($v,array());}

    $neuro_perc->create();
    
    // Обучим нейросеть
    $neuro_perc->learn_wt($iterations = 1000 , $step=1);
    
    // Выведем итоговое разделение по классам

    $in_titles = array("Ока\t","Газель пас","Калина\t",
    "Камаз\t","Газель груз","ПАЗ4234","ЗИЛ-5301","Патриот","ЛиАЗ-6213",'Белаз','БТР80','Урал мото','Повозка','Велосипед','Трамвай');
    
    echo '<table border="1" style="border-collapse: collapse;">';
    echo '<tr><th></th><th>Качество</th></tr>';
    foreach($inputs as $i=>$v_row){
        echo "<tr>";
        $data = array();
        foreach($v_row as $a=>$b)$data[$a+1] = $b;
        $neuro_perc->set_inputs($data);
        $outputs_row = $neuro_perc->calc();
        echo "<td>".$in_titles[$i]."</td>";
        foreach($outputs_row as $k=>$v)echo "<td>".number_format($v,5,',',' ')."</td>";
        echo "</tr>";
    }
    echo "</table>";
    
    
    echo "<h1>Семантические расстояния</h1>";
    echo "<table border='1' style='border-collapse: collapse;'>";
    echo "<tr><th></th><th>Положение</th>";
    foreach($inputs as $i=>$v)echo "<th>".$in_titles[$i]."</th>";
    echo "</tr>";
    foreach($inputs as $i=>$v_row){
        $data = array();
        foreach($v_row as $a=>$b)$data[$a+1] = $b;
        $neuro_perc->set_inputs($data);
        $outputs_row = $neuro_perc->calc();
        echo "<tr>";
        echo "<th>".$in_titles[$i]."</th><th>".number_format($outputs_row[0],4,',',' ')."</th>";
        foreach($inputs as $j=>$v_col){
            $data = array();
            foreach($v_col as $a=>$b)$data[$a+1] = $b;
            $neuro_perc->set_inputs($data);
            $outputs_col = $neuro_perc->calc();
            echo "<td style='text-align: right; padding: 0px 5px;'>".number_format(abs($outputs_row[0]-$outputs_col[0]),4,',',' ')."</td>";
        }
        echo "</tr>";
    }
    echo "</table>";

    
