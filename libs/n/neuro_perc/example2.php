<?php
    include("../../wirix.class.php");
    
    $neuro_perc = $_SERVER['wirix']->lib_load("neuro_perc");
    
    
    
    //===Устанавливаем объём внутренних слоёв нейросети
    // 2 слоя
    $neuro_perc->set_layers_count(2);
    // по 4 нейрона
    $neuro_perc->set_neurons_in_layer(25);
    
    // 25 входов
    $neuro_perc->set_inputs_count(25);
    // 10 выходов
    $neuro_perc->set_outputs_count(10);
    
    

    /** ********************************************************
        Зададим обучающие примеры на пиксельной матрице

        0,0,0,0,0,
        0,0,0,0,0,
        0,0,0,0,0,
        0,0,0,0,0,
        0,0,0,0,0,
        
    **********************************************************/
    include("digits_examples.php");


    // Вводим примеры в нейросеть
    foreach($inputs as $k=>$v){$neuro_perc->add_example($v,$outputs[$k]);}

    $neuro_perc->create();
    
    // Обучим нейросеть
    $neuro_perc->learn($iterations = 10000 , $step=1);
    
    $out_titles = array(0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9);
    $in_titles = array(0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9);

    /*
    $out_titles = array(0,1,2,3,4,5,6,7,8,9);
    $in_titles = array(0,1,2,3,4,5,6,7,8,9);
    */
    
    echo "<pre>";
    foreach($inputs as $k=>$v){
        $data = array();
        foreach($v as $a=>$b)$data[$a+1] = $b;
        $neuro_perc->set_inputs($data);
        $outputs = $neuro_perc->calc();
        echo $in_titles[$k].":\t";
        foreach($outputs as $a=>$b)
            echo $out_titles[$a]."(".round(100*$b,2)."%)\t";
        echo "\n";
    }
    
    /******************************************************************
    
        Даём не существовавшие в выборке примеры
    
    
        0,0,0,0,0,
        0,0,0,0,0,
        0,0,0,0,0,
        0,0,0,0,0,
        0,0,0,0,0,
    
    
    ******************************************************************/
    $inputs = array();
    $inputs[0] = array(
        0,0,1,1,0,
        0,1,0,0,1,
        0,1,0,0,1,
        0,1,0,0,1,
        0,0,1,1,0,
    );
    $inputs[1] = array(
        0,0,0,0,1,
        0,0,0,0,1,
        0,0,0,1,0,
        0,0,0,1,0,
        0,0,0,1,0,
    );
    $inputs[2] = array(
        0,1,1,1,0,
        1,0,0,0,1,
        0,0,1,1,0,
        0,1,0,0,0,
        0,1,1,1,1,
    );
    $inputs[3] = array(
        0,1,1,1,0,
        1,0,0,0,1,
        0,0,1,1,0,
        1,0,0,0,1,
        0,1,1,1,0,
    );
    $inputs[4] = array(
        1,0,0,0,1,
        0,1,0,0,1,
        0,0,1,1,1,
        0,0,0,0,1,
        0,0,0,0,1,
    );
    $inputs[5] = array(
        1,1,1,1,0,
        1,0,0,0,0,
        1,1,1,0,0,
        0,0,0,1,0,
        1,1,1,0,0,
    );

    $inputs[6] = array(
        0,1,1,1,0,
        1,0,0,0,0,
        1,1,1,1,0,
        1,0,0,0,1,
        0,1,1,1,0,
    );

    $inputs[7] = array(
        1,1,1,1,1,
        0,0,0,1,0,
        0,0,1,0,0,
        0,0,1,0,0,
        0,0,1,0,0,
    );

    $inputs[8] = array(
        0,1,1,1,1,
        1,0,0,0,1,
        0,1,1,1,0,
        1,0,0,0,1,
        1,1,1,1,0,
    );


    $inputs[9] = array(
        0,1,1,1,0,
        1,0,0,0,1,
        0,1,1,1,1,
        0,0,0,0,1,
        0,1,1,1,0,
    );


    echo htmlspecialchars("<table border='1'>");
    echo htmlspecialchars('<tr><th rowspan="2">Что подавали</th><th colspan="'.count($out_titles).'">Как опозналось(%)</th></tr>');
    echo htmlspecialchars('<tr>');
    foreach($out_titles as $title)echo htmlspecialchars('<th>'.$title.'</th>');
    echo htmlspecialchars('</tr>');
    foreach($inputs as $k=>$v){
        echo htmlspecialchars('<tr><th>'.$k.'</th>');
        $data = array();
        foreach($v as $a=>$b)$data[$a+1] = $b;
        $neuro_perc->set_inputs($data);
        $outputs = $neuro_perc->calc();
        foreach($outputs as $a=>$b){
            $c = sprintf("%x",(1-$b)*8);
            echo htmlspecialchars('<td style="background-color: #'.$c.$c.$c.'">'.round(100*$b,2)."</td>");
        }
        echo htmlspecialchars('</tr>');
    }
    echo htmlspecialchars("</table>");



    echo "</pre>";
    
