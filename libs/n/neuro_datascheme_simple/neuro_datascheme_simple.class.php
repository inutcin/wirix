<?php

    /**
        Класс для работы с простой однослойной нейросетью, параметры которой берутся из БД
    */
    
    $_SERVER['wirix']->lib_load('neuro_simple');
    class wirix_neuro_datascheme_simple extends wirix_neuro_simple{
        var $name;                  //!< Имя нейросети
        var $examples = array();    //!< Массив обучающих примеров
        var $db_normalize = array();   //!< Массив нормализаций
        var $criteria = array();    //!< Массив критериев
        var $reactions = array();   //!< Массив критериев
        var $synapses = array();    //!< Массив Синапсов
        
        private $net_scheme;                    //!< Схема нейросети
        private $criteria_scheme;               //!< Схема критериев
        private $reactions_scheme;              //!< Схема реакций
        private $synapses_scheme;               //!< Схема синапсов
        private $examples_scheme;               //!< Схема примеров
        private $example_criteria_scheme;       //!< Схема критериев для примеров
        private $example_reactions_scheme;      //!< Схема реакций для примеров
        private $normalize_scheme;       //!< Схема синапсов
        
        function __construct(){
            if(!$this->net_scheme = $_SERVER['wirix']->init_datascheme("neuro_net"))
                print($_SERVER['wirix']->error);
            if(!$this->criteria_scheme = $_SERVER['wirix']->init_datascheme("neuro_criteria"));
                print($_SERVER['wirix']->error);
            if(!$this->reactions_scheme = $_SERVER['wirix']->init_datascheme("neuro_reaction"));
                print($_SERVER['wirix']->error);
            if(!$this->synapses_scheme = $_SERVER['wirix']->init_datascheme("neuro_synaps"));
                print($_SERVER['wirix']->error);
            if(!$this->examples_scheme = $_SERVER['wirix']->init_datascheme("neuro_example"));
                print($_SERVER['wirix']->error);
            if(!$this->example_criteria_scheme = $_SERVER['wirix']->init_datascheme("neuro_example_criteria"));
                print($_SERVER['wirix']->error);
            if(!$this->example_reactions_scheme = $_SERVER['wirix']->init_datascheme("neuro_example_reaction"));
                print($_SERVER['wirix']->error);
            if(!$this->normalize_scheme = $_SERVER['wirix']->init_datascheme("neuro_normalize"));
                print($_SERVER['wirix']->error);
                
            $this->net_scheme->per_page                 = 0;
            $this->criteria_scheme->per_page            = 0;
            $this->reactions_scheme->per_page           = 0;
            $this->synapses_scheme->per_page            = 0;
            $this->examples_scheme->per_page            = 0;
            $this->example_criteria_scheme->per_page    = 0;
            $this->example_reactions_scheme->per_page   = 0;
            $this->normalize_scheme->per_page           = 0;
            
        }


        /**
            Добавить обучающий пример
            @param $criteria - массив обучающих примеров
            array(
                array("id"=>$criteria_id, "value"=>$value),
                array("id"=>$criteria_id, "value"=>$value),
                array("id"=>$criteria_id, "value"=>$value),
            )
            $reactions - массив правильных ответов
            array(
                array("id"=>$reaction_id, "value"=>$value),
                array("id"=>$reaction_id, "value"=>$value),
                array("id"=>$reaction_id, "value"=>$value),
            )
        */
        function add_example($name, $criteria, $reactions){
            $this->examples[] = array("id"=>0, "name"=>$name, "criteria"=>$criteria, "reactions"=>$reactions);
        }
        

        /**
            Поиск номера синапса по домеру критерия и реакции
            -1 - если не найдено
        */
        function synaps_search($criteria_id, $reaction_id){
            foreach($this->synapses as $sid=>$synaps)
                if(
                    $synaps['criteria_id']==$criteria_id && 
                    $synaps['reaction_id']==$reaction_id
                )return $sid;
            return -1;
        }

        /**
            Поиск номера критерия по его ID
            -1 - если не найдено
        */
        function criteria_search($criteria_id){
            foreach($this->criteria as $cid=>$crit)
                if($crit['id']==$criteria_id)return $cid;
            return -1;
        }


        /**
            Создание отсутствующих синапсов
        */
        function create_absent_synapses(){
            foreach($this->criteria as $cid=>$criteria)
                foreach($this->reactions as $rid=>$reaction)
                    if($this->synaps_search($cid, $rid)<0){
                        $rand = 0.0001 + 0.00001*rand(0,1000);
                        $this->synapses[] = array(
                            "id"            =>  0,
                            "criteria_id"   =>  $cid,
                            "reaction_id"   =>  $rid,
                            "weight"        =>  $rand  
                        );
                        if(!isset($this->weights[$rid]))$this->weights[$rid] = array();
                        $this->weights[$rid][$cid] = $rand;
                                            
                    }
            return true;
        }
        
        /**
            Добавление критерия
        */
        function add_criteria($name){
            $this->criteria[] = array("name"=>$name, "id"=>0);
        }

        /**
            Добавление реакции
        */
        function add_reaction($name){
            $this->reactions[] = array("name"=>$name, "id"=>0);
        }

        
        /**
            Загрузка нейросети из БД
        */
        function load(){
            if(!intval($this->id)){
                $this->error = 'Не указан ID нейросети';
                return false;
            }
            
            // Загружаем имя нейросети
            $this->net_scheme->clear();
            $this->net_scheme->set("id", $this->id);
            $this->net_scheme->find("first");
            $this->name = $this->net_scheme->get('name');
            
            // Загружаем критерии нейросети
            $this->criteria_scheme->clear();
            $this->criteria_scheme->set("net_id", $this->id);
            $criteries = $this->criteria_scheme->find("all");
            foreach($criteries as $criteria)$this->criteria[] = array(
                "id"    =>  $criteria['id'],
                "name"  =>  $criteria['name'],
                "enabled"=> $criteria['enabled']
            );
                
            // Загружаем реакции нейросети
            $this->reactions_scheme->clear();
            $this->reactions_scheme->set("net_id", $this->id);
            $reactions = $this->reactions_scheme->find("all");
            $this->dimension = count($reactions);
            foreach($reactions as $reaction)$this->reactions[] = array(
                "id"    =>  $reaction['id'],
                "name"  =>  $reaction['name']
            );
            
            // Загружаем синапсы нейросети
            $this->synapses_scheme->clear();
            $this->synapses_scheme->set("net_id", $this->id);
            $synapses = $this->synapses_scheme->find("all");
            foreach($synapses as $synaps){
                    $this->synapses[] = array(
                    "id"   =>  $synaps['id'],
                    "criteria_id"   =>  $synaps['criteria_id'],
                    "reaction_id"   =>  $synaps['reaction_id'],
                    "weight"        =>  $synaps['weight']
                );
                $this->weights[$synaps['reaction_id']][$synaps['criteria_id']] = $synaps['weight'];
            }
            
            // Загружаем примеры
            $this->examples_scheme->clear();
            $this->examples_scheme->set("net_id", $this->id);
            $examples = $this->examples_scheme->find("all");
            foreach($examples as $example){
                
                $criteria = array();
                $this->example_criteria_scheme->clear();
                $this->example_criteria_scheme->set("example_id", $example['id']);
                $example_criteria = $this->example_criteria_scheme->find("all");
                foreach($example_criteria as $item)
                    $criteria[] = array("id"=>$item['criteria_id'],"value"=>$item['level']);
                
                $reactions = array();
                $this->example_reactions_scheme->clear();
                $this->example_reactions_scheme->set("example_id", $example['id']);
                $example_reactions = $this->example_reactions_scheme->find("all");
                foreach($example_reactions as $item)
                    $reactions[] = array("id"=>$item['reaction_id'],"value"=>$item['level']);
                
                
                $this->examples[] = array(
                    "id"=>$example['id'],"name"=>$example['name'], 
                    "criteria"=>$criteria, "reactions"=>$reactions
                );
            }
            
            // Загружаем нормализацию
            $this->normalize_scheme->clear();
            $this->normalize_scheme->set("net_id", $this->id);
            $normalize = $this->normalize_scheme->find("all");
            foreach($normalize as $norm){
                if(!isset($this->db_normalize[$norm['criteria_id']]))$this->normalize[$norm['criteria_id']] = array();
                $this->db_normalize[$norm['criteria_id']][] = array(
                    "id"=>$norm['id'], "from"=>$norm['from'], "to"=>$norm['to'], "level"=>$norm['level']
                );
                
                if($norm['from']==-1){
                    $this->set_normalize_before($this->criteria_search($norm['criteria_id']),$norm['to'],$norm['level']/10);
                }
                elseif($norm['to']==-1){
                    $this->set_normalize_after($this->criteria_search($norm['criteria_id']),$norm['from'],$norm['level']/10);
                }
                else{
                    $this->set_normalize_from_to($this->criteria_search($norm['criteria_id']),$norm['from'],$norm['to'],$norm['level']/10);
                }
            }
            
        }
        
        /**
            Сохранение нейросети в базе данных
        */
        function save(){
            if(!intval($this->id)){
                $this->error = 'Не указан ID нейросети';
                return false;
            }
            
            // Сохраняем критерии
            foreach($this->criteria as $criteria){
                $this->criteria_scheme->clear();                
                $this->criteria_scheme->set(array(
                    "id"=>$criteria['id'], 
                    "net_id"=>$this->id, 
                    "name"=>$criteria['name'],
                    "enabled"=>$criteria['enabled']
                ));
                if($criteria['id'])
                    $this->criteria_scheme->save();
                else
                    $this->criteria_scheme->add();
            }

            // Сохраняем реакции
            foreach($this->reactions as $reaction){
                $this->reactions_scheme->clear();                
                $this->reactions_scheme->set(array(
                    "id"=>$reaction['id'], 
                    "net_id"=>$this->id, 
                    "name"=>$reaction['name']
                ));
                if($reaction['id'])
                    $this->reactions_scheme->save();
                else
                    $this->reactions_scheme->add();
            }

            // Сохраняем синапсы
            foreach($this->synapses as $synaps){
                $this->synapses_scheme->clear();                
                $this->synapses_scheme->set($saves = array(
                    "id"=>$synaps['id'], 
                    "net_id"=>$this->id, 
                    "criteria_id"=>$synaps['criteria_id'], 
                    "reaction_id"=>$synaps['reaction_id'], 
                    "weight"=>$this->weights[$synaps['reaction_id']][$synaps['criteria_id']]
                ));
                if($synaps['id'])
                    $this->synapses_scheme->save();
                else
                    $this->synapses_scheme->add();
            }
            
        }
        
        
        /**
            Обучение нейросети на примерах из БД
            
            @param $learn_inputs   - для совместимости с родительским классом
            @param $learn_outputs  - для совместимости с родительским классом
        */
        function learn($learn_inputs = array(), $learn_outputs = array()){
        	$sum = 0;
        	foreach($this->examples as $example){
        		$criterias = array();$reactions = array();
        		foreach($example['criteria'] as $criteria)$criterias[] = $criteria['value'];
        		foreach($example['reactions'] as $reaction)$reactions[] = $reaction['value'];
	        	$sum = parent::learn($criterias, $reactions);
	        	unset($criterias); unset($reactions);
        	}
        	return $sum;
        }
        
        
    }

?>

