-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Янв 18 2013 г., 14:22
-- Версия сервера: 5.5.28
-- Версия PHP: 5.4.4-10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- База данных: `projects_00010`
--

-- --------------------------------------------------------

--
-- Структура таблицы `neuro_criteria`
--

DROP TABLE IF EXISTS `neuro_criteria`;
CREATE TABLE IF NOT EXISTS `neuro_criteria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `net_id` int(11) unsigned NOT NULL COMMENT 'Нейросеть критерия',
  `name` char(128) NOT NULL COMMENT 'Название критерия',
  PRIMARY KEY (`id`),
  KEY `ctime` (`ctime`),
  KEY `mtime` (`mtime`),
  KEY `net_id` (`net_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Критерии(входы нейросети)' AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `neuro_criteria`
--

INSERT INTO `neuro_criteria` (`id`, `ctime`, `mtime`, `net_id`, `name`) VALUES
(1, '2013-01-09 11:19:03', '2013-01-11 15:17:00', 1, 'На сайте есть sitemap.xml'),
(2, '2013-01-09 14:18:13', '2013-01-11 15:17:00', 1, 'На сайте есть файл robots.txt');

-- --------------------------------------------------------

--
-- Структура таблицы `neuro_examples`
--

DROP TABLE IF EXISTS `neuro_examples`;
CREATE TABLE IF NOT EXISTS `neuro_examples` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `net_id` int(11) unsigned NOT NULL COMMENT 'Нейросеть реакции',
  `name` char(128) NOT NULL COMMENT 'Название примера',
  PRIMARY KEY (`id`),
  KEY `ctime` (`ctime`),
  KEY `mtime` (`mtime`),
  KEY `net_id` (`net_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Примеры для обучения нейросети' AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `neuro_examples`
--

INSERT INTO `neuro_examples` (`id`, `ctime`, `mtime`, `net_id`, `name`) VALUES
(1, '2013-01-10 12:29:13', '0000-00-00 00:00:00', 1, 'Пример 1');

-- --------------------------------------------------------

--
-- Структура таблицы `neuro_example_criteria`
--

DROP TABLE IF EXISTS `neuro_example_criteria`;
CREATE TABLE IF NOT EXISTS `neuro_example_criteria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `example_id` int(11) unsigned NOT NULL COMMENT 'Пример, для которого сигналы',
  `criteria_id` int(11) unsigned NOT NULL COMMENT 'Критерий сигнала',
  `level` float(12,5) NOT NULL DEFAULT '0.00100' COMMENT 'Уровень входного сигнала',
  PRIMARY KEY (`id`),
  KEY `ctime` (`ctime`),
  KEY `mtime` (`mtime`),
  KEY `net_id` (`example_id`),
  KEY `criteria_id` (`criteria_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Образцовые критерии примера для обучения' AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `neuro_example_criteria`
--

INSERT INTO `neuro_example_criteria` (`id`, `ctime`, `mtime`, `example_id`, `criteria_id`, `level`) VALUES
(1, '2013-01-10 12:34:22', '0000-00-00 00:00:00', 1, 1, 1.00000),
(2, '2013-01-10 12:34:22', '0000-00-00 00:00:00', 1, 2, 0.50000);

-- --------------------------------------------------------

--
-- Структура таблицы `neuro_example_reactions`
--

DROP TABLE IF EXISTS `neuro_example_reactions`;
CREATE TABLE IF NOT EXISTS `neuro_example_reactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `example_id` int(11) unsigned NOT NULL COMMENT 'Пример, для которого сигналы',
  `reaction_id` int(11) unsigned NOT NULL COMMENT 'Реакция примера',
  `level` float(6,5) NOT NULL DEFAULT '0.00100' COMMENT 'Уровень реакции',
  PRIMARY KEY (`id`),
  KEY `ctime` (`ctime`),
  KEY `mtime` (`mtime`),
  KEY `net_id` (`example_id`),
  KEY `criteria_id` (`reaction_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Образцовые реакции примера для обучения' AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `neuro_example_reactions`
--

INSERT INTO `neuro_example_reactions` (`id`, `ctime`, `mtime`, `example_id`, `reaction_id`, `level`) VALUES
(1, '2013-01-10 12:34:52', '0000-00-00 00:00:00', 1, 1, 0.10000);

-- --------------------------------------------------------

--
-- Структура таблицы `neuro_nets`
--

DROP TABLE IF EXISTS `neuro_nets`;
CREATE TABLE IF NOT EXISTS `neuro_nets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `name` char(128) NOT NULL COMMENT 'Название нейросети',
  PRIMARY KEY (`id`),
  KEY `ctime` (`ctime`),
  KEY `mtime` (`mtime`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Доступные нейросети' AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `neuro_nets`
--

INSERT INTO `neuro_nets` (`id`, `ctime`, `mtime`, `name`) VALUES
(1, '2013-01-09 11:00:12', '0000-00-00 00:00:00', 'Прогноз по возможности продвижения сайта');

-- --------------------------------------------------------

--
-- Структура таблицы `neuro_normalize`
--

DROP TABLE IF EXISTS `neuro_normalize`;
CREATE TABLE IF NOT EXISTS `neuro_normalize` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `net_id` int(11) unsigned NOT NULL COMMENT 'Нейросеть',
  `criteria_id` int(11) unsigned NOT NULL COMMENT 'ID критерия',
  `from` char(32) NOT NULL COMMENT 'Минимум (больше)',
  `to` char(32) NOT NULL COMMENT 'Максимум (меньше или равно)',
  `level` enum('0','1','2','3','4','5','6','7','8','9','10') NOT NULL DEFAULT '5' COMMENT 'Уровень',
  PRIMARY KEY (`id`),
  KEY `ctime` (`ctime`),
  KEY `mtime` (`mtime`),
  KEY `criteria_id` (`criteria_id`),
  KEY `net_id` (`net_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Нормализация критериев' AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `neuro_normalize`
--

INSERT INTO `neuro_normalize` (`id`, `ctime`, `mtime`, `net_id`, `criteria_id`, `from`, `to`, `level`) VALUES
(1, '2013-01-09 04:52:28', '0000-00-00 00:00:00', 1, 1, '0', '0.5', '5'),
(2, '2013-01-11 14:47:08', '0000-00-00 00:00:00', 1, 1, '0.5', '1', '10'),
(3, '2013-01-11 14:47:24', '0000-00-00 00:00:00', 1, 1, '-1', '0', '0'),
(4, '2013-01-11 14:48:17', '0000-00-00 00:00:00', 1, 1, '1', '-1', '10');

-- --------------------------------------------------------

--
-- Структура таблицы `neuro_reactions`
--

DROP TABLE IF EXISTS `neuro_reactions`;
CREATE TABLE IF NOT EXISTS `neuro_reactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `net_id` int(11) unsigned NOT NULL COMMENT 'Нейросеть реакции',
  `name` char(128) NOT NULL COMMENT 'Название реакции',
  PRIMARY KEY (`id`),
  KEY `ctime` (`ctime`),
  KEY `mtime` (`mtime`),
  KEY `net_id` (`net_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Доступные нейросети' AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `neuro_reactions`
--

INSERT INTO `neuro_reactions` (`id`, `ctime`, `mtime`, `net_id`, `name`) VALUES
(1, '2013-01-09 12:01:08', '2013-01-11 15:17:00', 1, 'Эффективный звонок'),
(2, '2013-01-09 14:25:47', '2013-01-11 15:17:00', 1, 'Вторичный звонок'),
(3, '2013-01-09 14:25:47', '2013-01-11 15:17:00', 1, 'Сайт бесперспективен');

-- --------------------------------------------------------

--
-- Структура таблицы `neuro_synapses`
--

DROP TABLE IF EXISTS `neuro_synapses`;
CREATE TABLE IF NOT EXISTS `neuro_synapses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `net_id` int(11) unsigned NOT NULL COMMENT 'Нейросеть реакции',
  `criteria_id` int(11) unsigned NOT NULL COMMENT 'Критерий синапса',
  `reaction_id` int(11) unsigned NOT NULL COMMENT 'Реакция синапса',
  `weight` float(6,5) NOT NULL DEFAULT '0.00100' COMMENT 'Вес синапса',
  PRIMARY KEY (`id`),
  KEY `ctime` (`ctime`),
  KEY `mtime` (`mtime`),
  KEY `net_id` (`net_id`),
  KEY `criteria_id` (`criteria_id`),
  KEY `reaction_id` (`reaction_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Доступные нейросети' AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `neuro_synapses`
--

INSERT INTO `neuro_synapses` (`id`, `ctime`, `mtime`, `net_id`, `criteria_id`, `reaction_id`, `weight`) VALUES
(1, '2013-01-09 14:31:36', '2013-01-11 15:17:00', 1, 0, 0, 0.00059),
(2, '2013-01-09 14:31:36', '2013-01-11 15:17:00', 1, 0, 1, 0.00501),
(3, '2013-01-09 14:31:36', '2013-01-11 15:17:00', 1, 0, 2, 0.00201),
(4, '2013-01-09 14:31:36', '2013-01-11 15:17:00', 1, 1, 0, 0.00911),
(5, '2013-01-09 14:31:36', '2013-01-11 15:17:00', 1, 1, 1, 0.00155),
(6, '2013-01-09 14:31:36', '2013-01-11 15:17:00', 1, 1, 2, 0.00984);
