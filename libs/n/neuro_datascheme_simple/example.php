<?php
    include($_SERVER['DOCUMENT_ROOT']."/wirix/libs/wirix.class.php");
    $neuro = $_SERVER['wirix']->lib_load("neuro_datascheme_simple");
    
    // Назначаем ID нейросети
    $neuro->id = 1;
    // Загрузка нейросети
    $neuro->load();
    // Добавляем новый критерий
//    $neuro->add_criteria("На сайте есть файл robots.txt");
    // Добавляем новую реакцию
//    $neuro->add_reaction("Вторичный звонок");
    // Добавляем новую реакцию
//    $neuro->add_reaction("Сайт бесперспективен");
    // Создаём несуществующие синапсы
    $neuro->create_absent_synapses();
    
    // Добавляем обучающий пример
    /*
    $criteria = array();
    $criteria[] = array("id"=>1,"value"=>1);
    $criteria[] = array("id"=>2,"value"=>0);
    $reactions = array();
    $reactions[] = array("id"=>1,"value"=>1);
    $name = 'Пример 1';
    $neuro->add_example($name, $criteria, $reactions);
    */
    
    // Сохраняем нейросеть
    $neuro->save();
    
    echo "<pre>";
    print_r($neuro);
    echo "</pre>";
        
?>
