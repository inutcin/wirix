<?php

    /**
        Обфускатор кода
        
        $obfuscator = $_SERVER['wirix']->lib_load('obfuscator');
        $obfuscator->input = __DR__."/wirix/libs/wirix.class.php";
        $obfuscator->output = __DR__."/wirix/libs/wirix1.class.php";
        $obfuscator->create_php();
        
    */
    class wirix_obfuscator extends wirix{
        
        var $input  = '';       //!< Входной файл
        var $output = '';       //!< Выходной файл
        
        var $erase_breaks = true;   //!< Удаление переводов строк
        
        var $terms          = array();      //!< Набор нетерминалов(терминов)
        var $enable_terms   = array("^","space","function","block","string1","string2","variable","mcomment","scomment");//!< Массив активных терминов
        var $tree           = array();      //!< Дерево граммаического разбора
        var $reserved_words = array(
            "include","include_once","require","require_once",
            "if","do","elseif","for","foreach","while","until","list","array",
            "switch");
        
        
        function __construct(){

            //===== Задаём набор терминов
            $func_call = "(\->|[\.\+\-\*\=\/\n\)\;]){1,1}[\s\n]*[\w\d\_]+[\s\n]*\(";
            
            // Пространство
            $this->terms['space'] = array(
                "^"=>"asasdasdasdasd", 
                // Список терминов, которые могут быдь включены в термин и шаблон, с которого он начинается

                "function"=>"function[\s\n]+[\w\d\_]+[\s\n]*\(.*?\)[\s\n]*\{",
                "func_call"=>$func_call,

                "class"=>"class[\s\n]+.*\{",
                "block"=>"\{", // Шаблон начала термина, который может быть включен как подтермин
                "string1"  =>"'",
                "string2"  =>'"',
                "variable" =>"\\$",
                "mcomment" =>"\\/\\*",
                "scomment" =>"\\/\\/",
            );

            // Блок кода
            $this->terms['block'] = array(
                "^"=>"\}", // шаблон, которым завершается термин
                "func_call"=>$func_call,
                "string1"  =>"'",
                "string2"  =>'"',
                "block"=>"\{"
            );
            
            // Функция
            $this->terms['function'] = array(
                "^"=>"\}",
                "func_call"=>$func_call,
                "block"=>"\{",
                "string1"  =>"'",
                "string2"  =>'"',
                "mcomment" =>"\\/\\*",
                "scomment" =>"\\/\\/",
            );

            // Функция
            $this->terms['func_call'] = array(
                "^"=>"\)",
                "func_call"=>$func_call,
                "string1"  =>"'",
                "string2"  =>'"',
                "mcomment" =>"\\/\\*",
                "scomment" =>"\\/\\/",
            );

            
            // Строка 1
            $this->terms['string1'] = array(
                "^"=>"'"
            );

            // Строка 2
            $this->terms['string2'] = array(
                "^"=>'"'
            );
            
            // Переменная
            $this->terms['variable'] = array(
                "^"=>'[^\w\d\_]'
            );
            
            // Переменная
            $this->terms['mcomment'] = array(
                "^"=>"\\*\\/"
            );

            // Переменная
            $this->terms['scomment'] = array(
                "^"=>"\n"
            );

            // class
            $this->terms['class'] = array(
                "^"=>"\}",
                "function"=>"function[\s\n]+[\w\d\_]+[\s\n]*\(.*?\)[\s\n]*\{",
            );
            
        }   

        /**
            Построение дерева грамматического разбора
            не думаю, что потом смогу разобраться в этом коде
            
            Дерево там: - во!
            
            
        */
        function build_grammar_tree(
            $source = '', //!< Если дан исходник для разбора - парсим его, иначе - парсим $this->input - файл
            $debug = 0
        ){
            // Дерево
            $this->tree = array();
            
            // счётчик числа узлов дерева
            $max_tree_pointer = 1;
            // Счетчик текущего положения указателя
            $tree_pointer = 1;
            // Текущий термин
            $term = "space";
            // Инициализируем дерево
            $this->tree[$tree_pointer] = array("parent"=>0,"term"=>$term);
            
            // Текущая подстрока на обработке
            $current = '';
            // Если не задан исходник для разбора - берём имя файла из $this->input
            if(!$source)$source = file_get_contents($this->input);
            // Читаем источник посимвольно
            while($tree_pointer && strlen($source)){
                // Определяем имя текущего термина
                $term = $this->tree[$tree_pointer]["term"];

                // Берём следующий символ
                $c = substr($source, 0,1);
                $source = substr($source, 1,strlen($source)-1);
                
                // Складываем очередной прочтённый символ в строку
                $current .= $c;
                // Игнорируем для разбора переводы каретки, пробелы и табы
                if($c==' ' || $c=="\r" || $c=="\t")continue;
                // Получаем возможные варианты перехода к терминам
                $terms = $this->terms[$term];
                
                // Для начала проверим не пора ли покинуть текущий термин и вернуться к родительскому
                $liter = $terms["^"];
                // Если в конце текущей строки есть подстрока- терминал выхода из термина
                if(preg_match("/(".$liter.")$/sm", $current, $m)){
                    // Костыль: не закрываем строки, если они кончаются на \' илм \"
                    if($term=='string1' && preg_match("/\\\\'$/sm", $current)){
                    }
                    elseif($term=='string2' && preg_match("/\\\\\"$/sm", $current)){
                    }
                    else{
                        // Оставляем набранную строку в текущем узле
                        $this->tree[$tree_pointer][] = substr($current, 0, strlen($current)-strlen($m[1]));
                        // Сохраняем каким терминалом закончился текущий термин
                        $this->tree[$tree_pointer][] = array($this->tree[$tree_pointer]["parent"]=>$m[1]);
                        // Возвращаем указатель текущего положения в дереве родительскому элементу
                        $tree_pointer = $this->tree[$tree_pointer]["parent"];
                        // Сбрасываем строку разбора
                        $current = '';
                        continue;
                    }
                }
                unset($terms['^']);
                
                // Перебираем в текущем термины варианты возможного перехода(имена терминов и регекспы)
                foreach($terms as $next_term=>$liter){
                    // Если термин не в списке разрешенных
                    if(array_search($next_term, $this->enable_terms)===false)continue;
                    // Шаблон начинаэщего терминала
                    $pattern = "/(".$liter.")$/ms";
                    // Вылавливаем начало термина(внутри строк - не работают)
                    if($term!="string2" && $term!="string1" && preg_match($pattern, $current, $m)){
                        // Увеличиваем счётчик узлов дерева
                        $max_tree_pointer++;
                        // Задаём номер следующего(порождённого) узла
                        $next_tree_pointer = $max_tree_pointer;
                        
                        // Оставляем набранную строку в текущем узле
                        $this->tree[$tree_pointer][] = substr($current, 0, strlen($current)-strlen($m[1]));
                        // Сохраняем каким терминалом начался дочерний термин
                        $this->tree[$tree_pointer][] = array($next_tree_pointer=>$m[1]);
                        
                        // Создаём узел дочернего термина
                        $this->tree[$next_tree_pointer] = array("parent"=>$tree_pointer,"term"=>$next_term);
                        // Ставим указатель текущего положения на вновь созданный узел
                        $tree_pointer = $next_tree_pointer;
                        // Назначаем текущим термином термин созданного дочернео узла
                        $term = $next_term;
                        // Сбрасываем текущую строку
                        $current = '';
                    }
                    
                }
            }
            // Закрываем дерево разбора
            $this->tree[$tree_pointer][] = array(0=>$current);
            
            // В режиме отладки вывести дерево
            if($debug)print_r($this->tree);
        }
        
        /**
            Вычищаем код от комментариев
        */
        function erase_comments(){
            foreach($this->tree as $node_id=>$item){
                if(!isset($item['term']))continue;
                if($item['term']!='scomment' && $item['term']!='mcomment')continue;
                $parent_id = $item['parent'];
                $this->tree[$node_id][0] = '';
                $this->tree[$node_id][1][$parent_id] = '';
                $parent_node = $this->tree[$parent_id];
                foreach($parent_node as $k=>$node)
                    if(is_array($node) && isset($node[$node_id]))
                        $this->tree[$parent_id][$k][$node_id] = '';
            }
        }
        
        
        /**
            Выделение из строк переменных и массивов, то есть разбиение строк вида
            
            "ssDFSFsF $variable ASASxx" на "ssDFSFsF ".$variable." ASASxx"
        */
        function exclude_vars_string(){
            foreach($this->tree as $node_id=>$node){
                if(!isset($node['term']) || $node['term']!='string2')continue;
                $this->tree[$node_id][0] = preg_replace("/(\\\\$\w[\w\d\_]*\[?[\w\d\_]*\]?)/", "\".$1.\"", $this->tree[$node_id][0]); 
            }
        }
        
        
        /**
            Замена строк на base64-последовательности
        */
        function base64_string(){
            foreach($this->tree as $node_id=>$node){
                if(!isset($node['term']))continue;
                if($node['term']!='string2' && $node['term']!='string1')continue;
                if(!isset($node['parent']))continue;
                print_r($node);
                $parent_id = $node['parent'];
                /*
                foreach($this->tree[$parent_id] as $id=>$n)
                    if(is_array($n) && isset($n[$node_id]))
                        $this->tree[$parent_id][$node_id] = '';
                */
                if($this->tree[$parent_id]['term']=='space')
                    $this->tree[$node_id][0] = "base64_decode('" . base64_encode($this->tree[$node_id][0]) . "')"; 
            }
        }
        
        /**
            Обработка грамматического дерева для обфускации
            То есть восстановление исходника из грамматического дерева
            или части дерева начиная от указанного узла
        */
        function process_grammar_tree(
            $node_id = 1 //!< Обрабатываемый узел. Если не указан - обрабатывает от корня
        ){
            $text = '';
            foreach($this->tree[$node_id] as $key=>$item){
                if($key=='0'){$text .= $item; continue;}
                if($key=='parent' || $key=='term')continue;
                if(!is_array($item)){
                    $text .= $item;
                }
                else{
                    foreach($item as $next_node_id=>$letter);
                    $text .= $letter;
                    if($this->tree[$node_id]['parent']!=$next_node_id)
                        $text .= $this->process_grammar_tree($next_node_id);
                }    
            }
            
            return $text;
        }
        
        
        /**
            Проведение процесса обфускации
        */
        function process(){
            // Строим грамматическое дерево для строк и камментов
            $this->enable_terms = array("^","string1","string2","scomment","mcomment");
            $this->build_grammar_tree();
            // Убираем камменты
            $this->erase_comments();
            // Выделяем в строках переменные
            $this->exclude_vars_string();

            $source = $this->process_grammar_tree();

            $this->enable_terms = array("^","string1","string2","function","block","class");
            $this->build_grammar_tree($source);
            
            // Преобразуем строки в base64
            foreach($this->tree as $node_id=>$node){
                // Только для строк
                if($node['term']!='string1' && $node['term']!='string2')continue;
                // Только для строк у которых есть родительский node
                if(!isset($this->tree[$node['parent']]['term']))continue;
                $parent_id = $node['parent'];
                // ТОлько для строк, у которых родительский node  = function, block, space
                if(
                    $this->tree[$parent_id]['term']!=='function'
                    &&
                    $this->tree[$parent_id]['term']!=='block'
                    &&
                    $this->tree[$parent_id]['term']!=='space'
                )continue;
                
                $this->tree[$node_id][0] = base64_encode($this->tree[$node_id][0]);
                // Закрывающая кавычка
                $this->tree[$node_id][1][$node['parent']] = 
                    preg_replace("/^([\"'])$/", "$1)",$node[1][$node['parent']]);
                // Открывающая кавычка
                foreach($this->tree[$parent_id] as $i=>$n){
                    if(isset($n[$node_id]))$this->tree[$parent_id][$i][$node_id] = 
                        preg_replace("/^([\"'])$/", "base64_decode($1", $n[$node_id]);
                }
            }
            $source = $this->process_grammar_tree();
            
            
            // Перестраиваем грам.дерево для выделения функций
            
            $this->enable_terms = array("^","string1","string2", "func_call");
            $this->build_grammar_tree($source);
            $dict = array();
            foreach($this->tree as $node_id=>$node){
                if(!isset($node['term']) || $node['term']!="func_call")continue;
                if(!isset($node['parent']))continue;
                $parent_id = $node['parent'];
                foreach($this->tree[$parent_id] as $k=>$n){
                    if(!is_array($n))continue;
                    foreach($n as $t1=>$t2);
                    if(!$t1 || $t2=="'" || $t2=="\"")continue;
                    $t2 = preg_replace("/^[^\w\d\_]*([\w\d\_]+?)[\n\s]*\(.*$/ms", "$1", $t2);
                    if(array_search($t2, $this->reserved_words)!==false)continue;
                    if(!isset($dict[$t2]))$dict[$t2] = '$_SERVER["f'.count($dict)."\"]";
                    $this->tree[$parent_id][$k][$t1] = str_replace($t2,$dict[$t2],$this->tree[$parent_id][$k][$t1]);
                }
                break;
            }
            
            // Создаём словарь функций
            $func_dict = "";
            foreach($dict as $k=>$v)$func_dict .= $v." = '$k';\n";

            $source = $this->process_grammar_tree();
            // Крепим словарь к исходнику
            $source = "<?php eval(base64_decode('".base64_encode($func_dict)."')); ?>".$source;
            // Удаляем переводы строк и лишние табы
            if($this->erase_breaks){
                $source = str_replace("\n","{break} ", $source);
                $source = preg_replace("/\{break\}\s+/","{break} ", $source);
                $source = preg_replace("/([\,\.;\:])\s*\{break\}\s+/","$1{break}", $source);
                $source = str_replace("{break}","", $source);
            }
            
            return $source;
            
        }
        
    }
?>
