<?php
    include_once(dirname(__FILE__)."/../../wirix.class.php");
    $_SERVER['wirix']->lib_load('html_table', false);
    
/**
\class wirix_html_db_table
\brief формирование html-таблицы, связанной с SQL-запросом. C фильтрацией, сортировкой и навигацией
    
    
Пример использования
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.php}
    $table = $_SERVER['wirix']->lib_load('html_db_table');
    $table->title = 'Прайс-лист';

    $column = array();
    $column['title'] = 'ID';
    $column['text'] =   '"$row[id]"';
    $column['width'] =   '10px';
    $table->add_column($column);
    unset($column);

    $column = array();
    $column['title'] = 'Производитель';
    $column['text'] =   '"$row[producer]"';
    $column['width'] =   '10px';
    $table->add_column($column);
    unset($column);

    $column = array();
    $column['title'] = 'Наименование';
    $column['text'] =   '"$row[articul]"';
    $column['width'] =   '10px';
    $table->add_column($column);
    unset($column);

    $column = array();
    $column['title'] = 'Цена';
    $column['align'] = 'right';
    $column['text'] =   '"$row[price]"';
    $column['width'] =   '10px';
    $table->add_column($column);
    unset($column);


    $table->id = 'price';
    
    $table->sorting = array(
        "4"=>"price"
    )
    
    $table->rows_per_page = 15;
    $table->tables  = array("a"=>$table_name);
    $query = $_SERVER['http']->get($table->id."_query");
    $query = mysqli_real_escape_string($_SERVER['db']->link, $query);
    $table->cond   = "
        `a`.`producer` LIKE '%".$query."%' OR
        `a`.`articul` LIKE '%".$query."%' OR
        `a`.`price` LIKE '%".$query."%'
    ";
    $table->search();
    
    
    echo $table->fetch();
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/
    class wirix_html_db_table extends wirix_html_table{

        var $pages = array();       //!< Массив страниц
        var $rows_per_page = 30;    //!< Строк на страницу
        var $tables = array();      //!< Массив таблиц для выборки
        var $join_fields = array(); //!< Массив связывания таблиц
        var $search = array();      //!< Массив фильтрации
        var $cond = "1";            //!< Условия на выборку
        var $sort = "";             //!< Сортировка результата, например `a`.`id` ASC
        var $fields_array = array();//!< Массив выводимых полей
        var $group_by = '';         //!< Группировка, например `a`.`id`
        
        var $action_buttons = array();  //!< Массив кнопок панели
        
        var $show_search_form = true;   //!< Показывать форму поиска
        var $search_string = '';        //!< Строка запроса
        var $sorting = array();         //!< Сортировка столбцов. 
                                        //!< Содержит имена столбцов из вывода SELECT для сортировки/
                                        //!< НАчинается с индекса 1
        var $filter = array();          //!< Фильтрация по столбцам
                                        //!< Содержит имена столбцов из вывода SELECT для сортировки
                                        //!< Начинается с индекса 1
        var $filter_select = array();   
        var $filter_row = array();   
        var $pagination = true;         //!< Выводить пагинацию
        
        var $avg = array();
/**
\var $avg
\brief Определение строки усреднения данных по столбцам

Каждый элемент массива - ключ массива строки результата по которому среднее и формат number_format, например 

    array(
        "1"=>array(                     // Номер столбца
            "key"=>"value1",            // Имя поля, которое усредняем
            "format"=>array(2,","," ")  // Формат вывода array(знаков после запятой, знак разделителя дробной части, знак разделителя тысяч)
        ),
        "2"=>array(
            "key"=>"value2",
            "format"=>array(2,","," ")
        )
    )
*/
        var $sum = array();             //!< Суммирующая строка.
        
        var $show_avg_rows = array(); //!< Показывать усредняющие строки array("top","bottom")
        var $show_sum_rows = array(); //!< Показывать суммирующие строки array("top","bottom")
        
        var $avg_row = array();
        var $sum_row = array();
        
        var $js_reload_callback = ''; //!< JS-код, исполняемый, когда очередная страница таблицы загружена
        
        function __construct(){
            if($this->called_class)
                $this->called_class = __CLASS__;
            parent::__construct();
            // Добавляем строку с чекбоксом
            $column = array();
            $column['width']    = '16px';
            $this->add_column($column);
        }
        
        function __destruct(){
        }
        
        /**
            \brief Добавление кнопки на панель действий
        */
        function add_action_button(
            $name,                  //!< Имя кнопки
            $title      =   '',     //!< Текст кнопки 
            $hint       =   '',     //!< Всплываюша подсказка
            $icon       =   '',     //!< Иконка кнопки
            $url        =   '',     //!< URL кнопки
            $onclick    =   ''      //!< Событие на кнопке
        ){
            $this->action_buttons[$name] = array(
                "title"         => $title,
                "hint"          => $hint,
                "icon"          => $icon,
                "url"           => $url,
                "onclick"       => $onclick
            );
        }
        
        
/**
\brief Выборка из БД данных для подстановки в таблицу. 

Пример использования.

    $table->tables      = array("a"=>"users","b"=>"groups");
    $table->join_fields = array("`a`.`master_gid`=`b`.`id`"=>"LEFT");
    $table->search      = array("`a`.`locked`"=>"checked");
    $table->search();
    
*/
        function search($limit = -1, $offset = -1){
            // Ограничение на вывод результатов
            $limit  = $limit!=-1?$limit:intval($_SERVER['http']->get($this->id.'_rows_per_page', $this->rows_per_page));    
            // Смещенние начальной позиции результатов
            $offset = $offset!=-1?$offset:intval($_SERVER['http']->get($this->id.'_offset', 0));          

            if(!$this->sort)
            $this->sort = 
                mysqli_real_escape_string(
                    $_SERVER['db']->link, 
                    $_SERVER['http']->get($this->id."_sort_field",'`a`.`id`')
                ).
                " ".
                mysqli_real_escape_string(
                    $_SERVER['db']->link, 
                    $_SERVER['http']->get($this->id."_sort_order", 'DESC')
                );

            // Составляем селекты без фильтров
            $cond = str_replace("\n","{break}",$this->cond);
            $cond = preg_replace("/\/\*\+filter\*\/.*?\/\*\-filter\*\//","",$cond);
            $cond = str_replace("{break}","\n",$cond);
            foreach($this->filter as $k=>$filed){
                $_SERVER['db']->search(
                    $this->tables,$this->join_fields,
                    $this->search,
                    $cond,
                    "$filed ASC",0,0,
                    array($filed => "value"),
                    $filed
                );
                $this->filter_select[$k] = $_SERVER['db']->rows;
            }


            if($this->filter){

                $this->cond = str_replace("\n","{break}",$this->cond);
                $this->cond = preg_replace("/\/\*\+filter\*\/.*?\/\*\-filter\*\//","",$this->cond);
                $this->cond = str_replace("{break}","\n",$this->cond);

                // Словарь полей, чтобы не повторяться
                $voc = array();
                foreach($this->filter as $k=>$field)$voc[$field] = 0;
                foreach($this->filter as $k=>$field){
                    $voc[$field]++;
                    if($voc[$field]>1)continue;
                    $field_name = $this->id."_select_".$k;
                    $field_value = $_SERVER['http']->get($field_name,'-');
                    if($field_value!='-'){
                        if($field_value){
                            $this->cond .= "/*+filter*/ AND ".$field."='".mysqli_real_escape_string(
                                $_SERVER['db']->link,$field_value
                            )."'";
                            $this->cond .= "/*-filter*/";
                        }
                        else{
                            $this->cond .= "/*+filter*/ AND ".$field." IS NULL";
                            $this->cond .= "/*-filter*/";
                        }
                    }
                }
            }
            
            $_SERVER['db']->search(
                $this->tables,
                $this->join_fields,
                $this->search,
                $this->cond,
                $this->sort,
                $limit,
                $offset,
                $this->fields_array,
                $this->group_by
            );
            $this->rows = $_SERVER['db']->rows;
            $this->pages = $_SERVER['db']->pages;

            if($this->filter_select){
                $row = array();
                $k=0;
                $order = $_SERVER['http']->get($this->id."_sort_order");

                // Чистим путь от дерьма
                foreach($this->filter_select as $l=>$m)$base = $_SERVER['http']->base_url(array($this->id."_select_$l",$this->id."_sort_order"));
                // Навешиваем в каждый столбец селект если надо
                $colnum=1;
                foreach($this->columns as $colval){
                    $this->filter_row[$colnum] = '';
                    if(isset($this->filter_select[$colnum])){
                        $this->filter_row[$colnum] = '<select id="'.$this->id.'_select_'.$colnum.'" onchange="return  html_db_table_reload(\''.
                            preg_replace("/^.*\?(.*?)$/",
                                "/wirix/libs/h/html_db_table/reload.ajax.php?$1".
                                "&id=".$this->id.
                                "&class=".$this->called_class,
                                $base
                            );
                        foreach($this->filter_select as $l=>$m)
                            $this->filter_row[$colnum] .= 
                                '&'.$this->id."_select_$l='".'+$(\'#'.$this->id.'_select_'.$l.'\').val()+'."'";

                        $this->filter_row[$colnum] .= 
                            '&'.$this->id."_sort_field".'='.$this->sorting[$colnum].
                            '&'.$this->id."_sort_order".'='.$order.'\',\''.
                            $this->id.'\',\''.$this->js_reload_callback.'\');">';

                        $field_name = $this->id."_select_".$colnum;
                            
                        $this->filter_row[$colnum] .= '<option value="-">All</option>';
                        foreach($this->filter_select[$colnum] as $r)
                            $this->filter_row[$colnum] .= '<option '.
                                ($_SERVER['http']->get($field_name)==$r['value']?'selected':'').
                                '>'.$r['value'].'</option>';
                            
                        $this->filter_row[$colnum] .= '</select>';
                    }
                    $colnum++;
                }
                
            }

            $groupping_oper = array("count", "sum", "avg", "min", "max", "VARIANCE", "STD", "BIT_OR", "BIT_AND");
            
            // Если задана усредняющая строка
            if($this->rows && $this->avg){
                $avg_fields_array = array();
                //print_r($this->fields_array);
                foreach($this->avg as $avg_detail){
                    if(!$exp = array_keys($this->fields_array, $avg_detail['key']))continue;
                    $exp = implode("",$exp);
                    // Удаляем прочие групповые операции
                    foreach($groupping_oper as $oper)
                        $exp = preg_replace("/$oper\s*\(\s*(.*?)\s*\)\s*/i","$1", $exp);
                    $avg_fields_array["AVG(".$exp.")"] = $avg_detail['key'];
                }
                $_SERVER['db']->search(
                    $this->tables,
                    $this->join_fields,
                    $this->search,
                    $this->cond,
                    $this->sort,
                    1,
                    0,
                    $avg_fields_array
                );
                $this->avg_row = $_SERVER['db']->rows[0];
            }

            // Если задана суммирующая строка
            if($this->rows && $this->sum){
                $sum_fields_array = array();
                foreach($this->sum as $sum_detail){
                    if(!$exp = array_keys($this->fields_array, $sum_detail['key']))continue;
                    $exp = implode("",$exp);
                    // Удаляем прочие групповые операции
                    foreach($groupping_oper as $oper)
                        $exp = preg_replace("/$oper\s*\(\s*(.*?)\s*\)\s*/i","$1", $exp);
                    $sum_fields_array["SUM(".$exp.")"] = $sum_detail['key'];
                }
                $_SERVER['db']->search(
                    $this->tables,
                    $this->join_fields,
                    $this->search,
                    $this->cond,
                    $this->sort,
                    1,
                    0,
                    $sum_fields_array
                );
                $this->sum_row = $_SERVER['db']->rows[0];
            }
            
        }
        
/**
\brief генерация html-кода таблицы

\returns html-код таблицы
*/
        function fetch($object = null){

            if($this->sorting)foreach($this->columns as $k=>$column){
                if(!isset($column['title']))continue;
                if(!isset($this->sorting[$k]))continue;

                $order = $_SERVER['http']->get($this->id."_sort_order");
                $order = ($order=="DESC"?'ASC':'DESC');
                
                $base = $_SERVER['http']->base_url(array($this->id."_sort_field",$this->id."_sort_order"));
                $this->columns[$k]['title'] = '<a href="#" onclick="return  html_db_table_reload(\''.
                    preg_replace(
                        "/^.*\?(.*?)$/",
                        "/wirix/libs/h/html_db_table/reload.ajax.php?$1&id=".$this->id."&class=".$this->called_class,
                        $base
                    ).
                    '&'.$this->id."_sort_field".'='.$this->sorting[$k].
                    '&'.$this->id."_sort_order".'='.$order.'\',\''.
                    $this->id.'\',\''.$this->js_reload_callback.'\');">'.
                    strip_tags($column['title'],"select option input div span img").
                    '</a>';
            }
            
            // Добавляем строку с чекбоксом если есть кнопки действия
            if(0 && $this->action_buttons){
                $this->columns[0]['title']    = '<input type="checkbox" name="chkbx_all" id="'.
                    $this->id.'_chkbx_all" onclick="html_db_table_check_all(\''.
                    $this->id.'_chkbx_all\', \''.
                    $this->id.'_table_chckbx\');">';
                $this->columns[0]['text']     = '"<input type=\"checkbox\" name=\"".$row["id"]."\" id=\"'.
                    $this->id.'_chkbx_".$row["id"]."\" class=\"'.
                    $this->id.'_table_chckbx\">"';
            }else{
                unset($this->columns[0]);
            }

            $html = '';
            $html .= $this->fetch_action_form();
            $html .= parent::fetch();
            $html .= '<script language="javascript" src="/wirix/libs/h/html_db_table/js/scripts.js" type="text/javascript"></script>';
            $this->commit();
            
            return $html;
        }
        
/**
    \brief Генерация html-кода формы поиска
    
    \returns html-код формы поиска
*/
        function fetch_action_form(){
            $html   =   '<div class="btn-toolbar" id="'.$this->id.'_btn-toolbar">';
            
            if($this->pagination)$html   .= $this->fetch_pages();
            $html   .=  '<div class="btn-group">';
            
            foreach($this->action_buttons as $name=>$but){
                $html   .=  '<a class="btn" title="'.
                    $but['hint'].'" href="'.
                    $but['url'].'" onclick="'.
                    $but['onclick'].'">';
                $html   .=  '<i class="'.$but['icon'].'"></i>'.$but['title'];
                $html   .=  '</a>';
            }
            
            if($this->show_search_form){
                $html   .=  '<span class="btn search">';
                $html   .=  '<input type="text" name="'.
                    $this->id.'_query" id="'.
                    $this->id.'_query" value="'.
                    (   
                        $this->search_string
                        ?
                        $this->search_string
                        :
                        $_SERVER['http']->get($this->id.'_query')
                    )
                    .
                    '" onkeyup="return html_db_table_search_press(\''.$this->id.'_query\',\''.
                    str_replace("?".$_SERVER['QUERY_STRING'],"",$_SERVER['REQUEST_URI']).'\');">';
                $html   .=  '</span>';
    
                $html   .=  '<a class="btn" title="Искать" onclick="return html_db_table_search(\''.
                    $this->id.'_query\',\''.
                    str_replace("?".$_SERVER['QUERY_STRING'],"",$_SERVER['REQUEST_URI']).'\',\''.
                    $_SERVER['QUERY_STRING'].'\');">';
                $html   .=  '<i class="icon-search"></i>';
                $html   .=  '</a>';
            }


            $html   .=  '</div>';
            $html   .=  '</div>';
            return $html;
        }


/**
    \brief Генерация HTML-кода страничной навигации. Для генерации используется атрибут $this->pages = array("смещение"=>"номер страницы")
    
    \returns HTML-код страничной навигации
*/
        function fetch_pages(){
            
            if(count($this->pages)<2)return '';
            
            $offset_key = $this->id."_offset";
            $rows_per_page_key = $this->id."_rows_on_page";
            $base_url = preg_replace(
                "/^.*\?(.*?)$/",
                "/wirix/libs/h/html_db_table/reload.ajax.php?$1&id=".$this->id."&class=".$this->called_class,
                $_SERVER['http']->base_url(array($offset_key,$rows_per_page_key))
            );
            
            
            $html = '<div class="pagination">';
            $html .= '<ul>';
            foreach($this->pages as $offset => $page_num){
                $html .= '<li '.($offset==$_SERVER['http']->get($offset_key)?'class="active"':"").
                    '><a href="#" onclick="return  html_db_table_reload(\''.$base_url.'&'.$offset_key.'='.$offset.'&'.$rows_per_page_key.'='.intval($_SERVER['http']->get($rows_per_page_key, $this->rows_per_page)).'\',\''.$this->id.'\',\''.$this->js_reload_callback.'\');">'.$page_num.'</a></li>';
            }
            $html .= '</ul>';
            $html .= '</div>';
            return $html;
        }

        
    }
?>
