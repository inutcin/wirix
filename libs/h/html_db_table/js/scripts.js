function html_db_table_check_all(master_check, check_class){
    if($('#'+master_check).attr('checked')=='checked')
        $('.'+check_class).attr('checked','checked');
    else
        $('.'+check_class).removeAttr('checked');
}

/**
    Групповое применение накликанного, отсылыется списком на action/список
    classname - имя класса чекбоксов
*/
function html_db_table_act_selected(classname, action){
    var inputs = document.getElementsByClassName(classname);
    var id = 0;
    var list = '0';
    for(i=0,c=inputs.length;i<c;i++){
        id = inputs.item(i).id;
        item_id = inputs.item(i).name;
        if($('#'+id).attr('checked'))list += ','+item_id;
    }
    if(list=='0'){alert('Ничего не выбрано');return false;}
    if(!confirm('Уверены?'))return false;
    document.location.href = action+'/'+list;
    return false;
}

/**
    Направление на страницу поиска
*/
function html_db_table_search(input_id, base_url, query_string){
    var query = input_id+'='+$('#'+input_id).val();
    document.location.href = base_url+(query_string==''?'?'+query:'?'+query_string+'&'+query)+document.location.hash;
}

/**
    Контроль нажатия клавиш в поле ввода поиска и перенаправление на страницу 
    поиска при нажатии Enter
*/
function html_db_table_search_press(input_id, base_url){
    var kk = window.event.keyCode;
    if(kk==13)html_db_table_search(input_id, base_url);
}

/**
    
*/
function html_db_table_reload(url,id,callback){
    
    var parentdiv = $('#'+id).parents()[0];
    $('#'+id).addClass('disabled');
    $('#'+id+'_btn-toolbar').addClass('disabled');
    $('#'+id).addClass('loading');
    $.get(url, function(data){
        var div = document.createElement('div');
        div.id = "id";
        div.className = 'html_table html_db_table';
        div.innerHTML = data;
        $('#'+id).remove();
        $('#'+id+'_btn-toolbar').remove();
        parentdiv.appendChild(div);
        eval(callback);
    });
    return false;
}
