function wirix_html_modal_show(id, title, url, callback){
    //$('#'+id).modal('show');
    $('body').append('<div class="modal-backdrop fade in"></div>');
    $('.modal-backdrop').addClass('in');
    $('#'+id).addClass('in');
    $('#'+id).removeClass('hide');
    $('#'+id+' .close').click(function(){
        wirix_html_modal_hide(id);
    });
    $('#'+id).focus();
    
    $('#'+id).keydown(function(data){
        if(data.which==27)wirix_html_modal_hide(id);
    })
    
    $('#'+id+'_title').html(title);
    $('#'+id+' .modal-body').html('<div style="text-align: center;"><img src="/wirix/libs/h/html/i/progress/001.gif"></div>');
    if(url!='')$('#'+id+' .modal-body').load(url,callback);
    return false;
}

function wirix_html_modal_hide(id){
    $('#'+id).addClass('in');
    $('#'+id).addClass('hide');
    $('#'+id).keydown(function(){})
    $('.modal-backdrop').remove();
    $('#ui-datepicker-div').remove();
}
