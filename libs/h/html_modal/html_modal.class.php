<?php
    include_once(dirname(__FILE__)."/../../wirix.class.php");
    
    /**
        @class wirix_html_modal
        @brief Генерация модального окна
        @ref wirix_html_modal

Объект данного класса автоматически создаётся при создании объекта класса @ca wirix_html 
и подгружает js-функцию генерации модального окна. Пример использования.
@code{.js}

// Вызов модального окна
wirix_html_modal_show(
    id,     // ID окна (не знаете про что это - используйте myModal)
    title,  // строка-заголовок модального окна
    url,    // Страница, которыя будет подгружена Ajax-ом (методом GET) в основную область окна
    callback// Js-функция, которая будет выполнена при усешном открытии окна
);

// Скрытие модального окна
wirix_html_modal_hide(
    id // ID окна (не знаете про что это - используйте myModal)
);
@endcode

    */
    $_SERVER['wirix']->lib_load("html_menu", false);
    class wirix_html_modal extends wirix{
        
        var $title = 'Заголовок модального окна';  //!< Заголовок панели навигации
        var $body_text = ''; //!< Текст тела окна
        var $ok_button_text = '';   //!< Текст ОК-кнопки
        var $cancel_button_text = '';   //!< Текст ОТМЕНА-кнопки
        
        function __construct(){
            parent::__construct(__CLASS__);
            $this->id = 'myModal';
            $this->body_text = 'Текст диалогового окна';
            $this->ok_button_text       = 'Применить';
            $this->cancel_button_text   = 'Отмена';
        }
        
        function fetch($object=null){
            $html = '
<!-- Modal -->
<div id="'.$this->id.'" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="'.$this->id.'_title">'.$this->title.'</h3>
  </div>
  <div class="modal-body">
    '.$this->body_text.'
  </div>
</div>
';
            return $html;
        }
        
    }
?>
