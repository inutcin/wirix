function setCookie(name, value, options) {
  options = options || {};
  var expires = options.expires;
  if (typeof expires == "number" && expires) {
    var d = new Date();
    d.setTime(d.getTime() + expires*1000);
    expires = options.expires = d;
  }
  if (expires && expires.toUTCString) {
    options.expires = expires.toUTCString();
  }
  value = encodeURIComponent(value);
  var updatedCookie = name + "=" + value;
  for(var propName in options) {
    updatedCookie += "; " + propName;
    var propValue = options[propName];   
    if (propValue !== true) {
      updatedCookie += "=" + propValue;
     }
  }
  document.cookie = updatedCookie;
}

function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}


/**
    Вызов формы правки прав на строку
    id = id записи в таблице, если 0, то берутся накликанные id из чекбоксов с 
    классом classname. У этих чекбоксов id забит в аттрибуте "name"
*/
function html_show_perm_modal(table, id, url, classname){
    
    if(id ==0){
        var inputs = document.getElementsByClassName(classname);
        var ids = 0;
        for(i=0,c=inputs.length;i<c;i++){
            ids = inputs.item(i).id;
            item_id = inputs.item(i).name;
            if($('#'+ids).attr('checked'))
                id += ','+item_id;
        }
    }
    
    id = ''+id+'';

    if(id==0){alert('Ничего не выбрано');return false;}
    id = id.replace("0,", "");
    
    html_bind_close_modal();
    
    $('#wirix_html_modal_title').html('Изменение прав записи(ей) №'+id+' таблицы '+table);
    $('#wirix_html_modal_body').load('/wirix/libs/d/db/show_perm.ajax.php?table='+table+'&id='+id);
    
    $('#wirix_html_modal_save').click(function(){
        $.post(
            '/wirix/libs/d/db/save_perm.ajax.php',
            {
                r0:$('#r0').attr('checked'),
                r1:$('#r1').attr('checked'),
                r2:$('#r2').attr('checked'),
                w0:$('#w0').attr('checked'),
                w1:$('#w1').attr('checked'),
                w2:$('#w2').attr('checked'),
                d0:$('#d0').attr('checked'),
                d1:$('#d1').attr('checked'),
                d2:$('#d2').attr('checked'),
                uid:$('#perm_uid').val(),
                gid:$('#perm_gid').val(),
                ids:$('#perm_records_id').val(),
                table:$('#perm_table').val()
            },
            function(data){
                var answ = eval("("+data+")");
                if(answ.error!=''){alert(answ.error);return false;}
                $('#wirix_html_modal').modal('hide');
                if(url!='')document.location.href = url;
            }
        )
    });
    $('#wirix_html_modal').modal('show');
}

function html_bind_close_modal(){
    $('#wirix_html_modal_close').click(function(){
        $('#wirix_html_modal').modal('hide');$('#wirix_html_modal').css('display','none');
    });
}
