<?php
    include_once(dirname(__FILE__)."/../../wirix.class.php");
    
    class wirix_html extends wirix{
        
        var $doctype = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        var $css_files  = array();   //!< Массив подключаемых файлов стилей
        var $js_scripts = array();   //!< Массив подключаемых файлов js-скриптов
        var $encoding = 'utf-8';     //!< Кодировка
        var $use_bootstrap = true;
        var $use_jquery = true;
        var $title = '';
        var $description = '';
        var $keywords = '';
        
        function __construct(){
            parent::__construct();
        }
        
        function page_head(){
/*             $_SERVER['http']->content_type($mime_type = 'text/html', $this->encoding);
            $base_path = $_SERVER['http']->get_base_path(realpath(dirname(__FILE__)));
            
            ob_start();
            echo $this->doctype."\n";
            echo "<html>\n";
            echo "<head>\n";
            echo '<meta http-equiv="Content-Type" content="text/html; charset='.$this->encoding.'" />';
            echo '<title>'.$this->title.'</title>';
            echo '<meta name="description" content="'.$this->description.'">';
            echo '<meta name="keywords" content="'.$this->keywords.'">';
            
            if($this->use_jquery){
                echo '<script src="/wirix/libs/h/html/js/jquery.min.js" type="text/javascript"></script>';
                echo '<script src="/wirix/libs/h/html/js/jquery-ui.js"></script>';
            }
            if($this->use_bootstrap){
                echo '<script language="javascript" src="/wirix/libs/h/html/bootstrap/js/bootstrap.js" type="text/javascript"></script>';
                echo '<script language="javascript" src="/wirix/libs/h/html/bootstrap/js/bootstrap-popover.js" type="text/javascript"></script>';
                echo '<link rel="stylesheet" href="/wirix/libs/h/html/bootstrap/css/bootstrap.css" type="text/css"/>';
            }
            
            // Получаем список всех подключенных файлов и ищем в их каталогах файл css/style.css
            $included_files = get_included_files();
            foreach($included_files as $file){
                $css_file = dirname($file).'/css/style.css';
                if($css_file==$_SERVER['DOCUMENT_ROOT'].'/css/style.css')continue;
                if(file_exists($css_file))
                    echo '<link rel="stylesheet" href="'.preg_replace("/^.*?\/wirix\/(.*)$/","/wirix/$1",$css_file).'" type="text/css"/>'."\n";
            }
            foreach($this->css_files as $css)echo '<link rel="stylesheet" href="'.$css.'" type="text/css"/>'."\n";
            foreach($this->js_scripts as $js)echo '<script type="text/javascript" language="javascript" src="'.$js.'"></script>'."\n";
            echo '<link rel="shortcut icon" href="/favicon.png" type="image/x-icon"/>';
            echo "</head>";
            echo "<body>";
            $modal = $_SERVER['wirix']->lib_load('html_modal');
            echo $modal->fetch();
            $html = ob_get_contents();
            ob_end_clean();
            $_SERVER['http']->content_type();
            return $html; */
        }
        
        function page_footer(){
            ob_start();
            // Получаем список всех подключенных файлов и ищем в их каталогах файл js/scripts.js
            $included_files = get_included_files();
            foreach($included_files as $file){
                $js_file = dirname($file).'/js/scripts.js';
                if($js_file==$_SERVER['DOCUMENT_ROOT'].'/js/scripts.js')continue;
                if(file_exists($js_file))
                    echo '<script language="javascript" src="'.preg_replace("/^.*?\/wirix\/(.*)$/","/wirix/$1",$js_file).'" type="text/javascript"/></script>'."\n";
            }

?>
</body>
</html>
<?php
            $html = ob_get_contents();
            ob_end_clean();
            return $html;
        }
        
    }
    
?>
