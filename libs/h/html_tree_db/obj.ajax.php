<?php
    //include($_SERVER['DOCUMENT_ROOT']."/wirix/libs/wirix.class.php");
    include(dirname(__FILE__)."/../../wirix.class.php");
    
    $tree = $_W->lib_load("html_tree_db");
    
    $wirix_id = $_H->get('wirix_id');
    
    if(!isset($_SESSION['wirix']['wirix_html_tree_db'][$wirix_id])){
        echo "No session data";
        die;
    }
    
    $data = json_decode(base64_decode($_SESSION['wirix']['wirix_html_tree_db'][$wirix_id]));
    
    $table = $_W->lib_load('html_tree_db');
    
    $data = get_object_vars($data);
    foreach($data as $k=>$v)$table->$k = is_object($v)?get_object_vars($v):$v;
