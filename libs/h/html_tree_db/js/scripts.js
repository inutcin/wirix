function wirix_html_tree_db_context_menu(){
    
    
    $(".wirix_html_tree_db .item .title").bind("contextmenu", function(data){
        var obj = $('#'+this.id);
        var cat_id = obj.attr('rel');
        var wirix_id = wirix_html_tree_db_get_wirix_id(obj);
        
        $('#'+wirix_id+" > .context-menu").css('display', 'block');
        $('#'+wirix_id+" > .context-menu").css("top", event.clientY-80);
        $('#'+wirix_id+" > .context-menu").css("left", event.clientX-30);
        
        $('#'+wirix_id+" > .context-menu .item").attr("rel", cat_id);
        
        $('#'+wirix_id+" > .context-menu").mouseleave(function(data){
            $('#'+wirix_id+" > .context-menu").css('display','none');
        });
        
        $('#'+wirix_id+" > .context-menu .item").unbind('click');
        $('#'+wirix_id+" > .context-menu .item").click(function(data){
            wirix_html_tree_db_act($(this).attr("act"), $(this).attr("rel"), wirix_html_tree_db_get_wirix_id($(this)));
        });
        
        return false;
    });
    
    $(".wirix_html_tree_db .item .title").unbind('dblclick');
    $(".wirix_html_tree_db .item .title").dblclick(function(){wirix_html_tree_db_edit(this);});
    $(".wirix_html_tree_db .item .title").unbind('click');
    $(".wirix_html_tree_db .item .title").click(function(){wirix_html_tree_db_load(this);});

    $(".wirix_html_tree_db .title").draggable({
        revert : true, 
        revertDuration: 200
    });
    
    $(".wirix_html_tree_db .title").droppable({
        drop: function(data, ui){
            parent_id = $(this).attr("rel");
            id = ui.draggable.attr('rel');
            
            if(id!=parent_id)
                wirix_html_tree_db_move(id, parent_id, wirix_html_tree_db_get_wirix_id($(this)))

        }
    });    
}
wirix_html_tree_db_context_menu();


function wirix_html_tree_db_act(act, cat_id, wirix_id){
    switch(act){
        case 'up':
            wirix_html_tree_db_sort(cat_id, wirix_id, -1);
        break
        case 'down':
            wirix_html_tree_db_sort(cat_id, wirix_id, +1);
        break
        case 'add':
            wirix_html_tree_db_add(cat_id, wirix_id);
        break
        case 'save':
            wirix_html_tree_db_save(cat_id, wirix_id);
        break
        case 'delete':
            wirix_html_tree_db_delete(cat_id, wirix_id);
        break
    }
}

function wirix_html_tree_db_move(id, target_id, wirix_id){
    
    $.get(
        '/wirix/libs/h/html_tree_db/move.ajax.php?parent_id='
        +target_id  +   '&wirix_id='
        +wirix_id   +   '&id=' + id,
        function(data){
            $('#'+wirix_id+' #item_'+id).remove();
            wirix_html_tree_db_load($('#'+wirix_id+' #item_'+target_id), true);
        }
    );
}

function wirix_html_tree_db_add(cat_id, wirix_id){
 
    var div_id = wirix_id+'_childs_'+cat_id;
    var div = $('#'+div_id);
    
    div.prepend(
        '<div class="item">'
            +'<div class="expand nochild"></div>'
            +'<div class="title">'
                +'<input type="text" value="" placeholder="Новый раздел"/>'
            +'</div>'
        +'</div>'
    );
    
    $('#'+wirix_id+" > .context-menu").css('display','none');
    $('#'+wirix_id+' #item_'+cat_id+' .childs input').first().focus();
    $('#'+wirix_id+' #item_'+cat_id+' .childs input').first().keydown(function(data){
        
        if(data.which==13){
            $('#'+div_id+' .title').prepend('<img class="loading" src="/wirix/libs/h/html_form/i/progressbar.gif"/>');
            $.post(
                '/wirix/libs/h/html_tree_db/add.ajax.php?parent_id='
                    +cat_id+'&wirix_id='
                    +wirix_id,
                {value:$('#'+div_id+' input').val()},
                function(data){
                    $('#'+wirix_id+' #item_'+cat_id+' .childs input').remove();
                    $('#'+wirix_id+' #item_'+cat_id+' .childs').html(data);
                    wirix_html_tree_db_context_menu();
                }
            );
        }
    });
    
    $('#'+div_id+' input').blur(function(data){
        $('#'+div_id+' .title').prepend('<img class="loading" src="/wirix/libs/h/html_form/i/progressbar.gif"/>');
        $.post(
            '/wirix/libs/h/html_tree_db/add.ajax.php?parent_id='
                +cat_id+'&wirix_id='
                +wirix_id,
            {value:$('#'+div_id+' input').val()},
            function(data){
                $('#'+wirix_id+' #item_'+cat_id+' .childs input').remove();
                $('#'+wirix_id+' #item_'+cat_id+' .childs').html(data);
                wirix_html_tree_db_context_menu();
            }
        );
    });
    
}


function wirix_html_tree_db_edit(obj){
    $(obj).append(
        '<input type="text" value="'+$('#'+$(obj).attr('id')+' > a').html()+'"/>'
    );
    
    $('#'+$(obj).attr('id')+' > a').css("display","none");
    $('#'+$(obj).attr('id')+' > input').focus();
    
    $('#'+$(obj).attr('id')+' > input').keydown(function(data){
        if(data.which==13){
            $('#'+$(obj).attr('id')+' > a').html($('#'+$(obj).attr('id')+' > input').val());
            $('#'+$(obj).attr('id')+' > input').remove();
            $('#'+$(obj).attr('id')+' > a').css("display","inline");
            $('#'+$(obj).attr('id')).append('<img class="loading" src="/wirix/libs/h/html_form/i/progressbar.gif"/>');
            $.post(
                '/wirix/libs/h/html_tree_db/edit.ajax.php?'
                    +'id='+$(obj).attr('rel')
                    +'&wirix_id='+wirix_html_tree_db_get_wirix_id($(obj)),
                {value:$('#'+$(obj).attr('id')+' > a').html()},
                function(){
                    $('#'+$(obj).attr('id')+' img.loading').remove();
                }
            );
        }        
    });
    
    $('#'+$(obj).attr('id')+' > input').blur(function(){
        $('#'+$(obj).attr('id')+' > a').html($('#'+$(obj).attr('id')+' > input').val());
        $('#'+$(obj).attr('id')+' > input').remove();
        $('#'+$(obj).attr('id')+' > a').css("display","inline");
        $('#'+$(obj).attr('id')).append('<img class="loading" src="/wirix/libs/h/html_form/i/progressbar.gif"/>');
        $.post(
            '/wirix/libs/h/html_tree_db/edit.ajax.php?'
                +'id='+$(obj).attr('rel')
                +'&wirix_id='+wirix_html_tree_db_get_wirix_id($(obj)),
            {value:$('#'+$(obj).attr('id')+' > a').html()},
            function(){
                $('#'+$(obj).attr('id')+' img.loading').remove();
            }
        );
    });
}

function wirix_html_tree_db_delete(cat_id, wirix_id){
    $('#'+wirix_id+" > .context-menu").css('display','none');
    if(!confirm('Точно удалить?'))return false;
    $("#"+wirix_id+' #item_'+cat_id+' div.title').append('<img src="/wirix/libs/h/html_form/i/progressbar.gif"/>');
    $.get(
        '/wirix/libs/h/html_tree_db/delete.ajax.php?'
            +'id='+cat_id
            +'&wirix_id='+wirix_id,
        function(){
            $("#"+wirix_id+' #item_'+cat_id).remove();
        }
    );
    
}


function wirix_html_tree_db_sort(cat_id, wirix_id, order){
    $('#'+wirix_id+" > .context-menu").css('display','none');
    var div_id = wirix_id+'_childs_'+cat_id;
    var parent_div = $('#'+div_id).parent().parent();
    $.get(
        "/wirix/libs/h/html_tree_db/sort.ajax.php?wirix_id="+wirix_id+"&cat_id="+cat_id+'&order='+order, function(data){
            wirix_html_tree_db_load(parent_div, true);
        }
    );
    
    
}


function wirix_html_tree_db_load(obj, reload){
    
    var wirix_id = wirix_html_tree_db_get_wirix_id($(obj));
    var cat_id = $(obj).attr('rel');
    
    var classname = $(obj).attr('class');
    var div = $('#'+wirix_id+' #item_'+cat_id+' .childs').first();
    
    var but = $('#'+$(obj.parentNode).attr('id')+' .expand').first();
    
    if(div.html()=='' || reload){
        but.removeClass('plus');
        but.addClass('minus');
        div.prepend('<img style="position:absolute;" src="/wirix/libs/h/html_form/i/progressbar.gif"/>');
        $.get("/wirix/libs/h/html_tree_db/load.ajax.php?wirix_id="+wirix_id+"&cat_id="+cat_id, function(data){
            div.html(data);
            wirix_html_tree_db_context_menu();
        });
    }
    else{
        but.removeClass('minus');
        but.addClass('plus');
        div.html('');
        wirix_html_tree_db_context_menu();
    }
    
}


function wirix_html_tree_db_get_wirix_id(jquery_obj){
    var wirix_id = '';
    var p = jquery_obj.parent();
    
    while(p = p.parent()){
        if(p.attr("class")=='wirix_html_tree_db'){
            wirix_id = p.attr("id");
            break;
        }
    }
    return wirix_id;
}
