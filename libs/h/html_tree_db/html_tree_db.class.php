<?php

    /**
        Генерация дерева с подгружающимися разделами из таблицы базы данных
        у таблицы должно быть 
            - поле parent_id, указывающее на родительский элемент 
            - поле order_by, указывающий порядок следования элементов.
            - поле show('Y' или 'N') указывающее показывать ли раздел
            - поле name, как имя раздела
        
        Пример использования
        
    */

    class wirix_html_tree_db extends wirix{
        
        var $table  = '';   //!< Таблица, в которой хранится дерево
        var $title  = '';   //!< Заголовок дерева (имя корневого элемента)
        var $cond   = 1;    //!< Условие выборки для элементов дерева
        var $onclick = '';  //!< Шаблон события onclick элементов дерева, например 'return show($id);'
        var $url = '';      //!< Шаблон ссылки элементов дерева, например '/cms/structure/$id/'
        var $active_id = 0; //!< ID активного элемента
        var $insert_default_values = array(
            "show"      =>  "Y",
            "order_by"  =>  "0"
        );
        var $edit_tree_condition = '1';
        var $add_callback = ""; //!< Код, который через eval выполняется после вставки элемента ($id - ID вставленного элемента, $value = его имя, $parent_id - ID элемента, куда переносится)
        var $delete_callback = ""; //!< Код, который через eval выполняется после удаления элемента ($id - ID вставленного элемента)
        var $save_callback = ""; //!< Код, который через eval выполняется после правки элемента ($id - ID вставленного элемента, $value = его имя)
        var $move_callback = ""; //!< Код, который через eval выполняется после переноса элемента ($id - ID переносимого элемента, $parent_id - ID элемента, куда переносится)
        
        function __construct(){
            parent::__construct(__CLASS__);
        }


        function fetch_tree(
            $target_id,
            $current_id=0
        ){
            global $_D;
            // Выстраиваем цепочку до корня
            $parent_id = $target_id;
            $path = array();
            $path[] = $parent_id;
            while($_D->search_one($this->table, array(), "`id`=$parent_id AND `show`='Y' AND ".$this->cond)){
                $parent_id = $_D->record['parent_id'];
                $path[$parent_id] = $_D->record;
                if($current_id==$parent_id)break;
            }
            
            $html = '';
            if(!$current_id)$html .= $this->fetch_item_header($current_id, true);
            $_D->search(
                array("a"=>$this->table),array(),array(),
                "`parent_id`=$current_id AND `show`='Y' AND ".$this->cond,
                "`order_by` ASC",0,0,
                array("id"=>"id")
            );
            
            $rows = $_D->rows;
            foreach($rows as $row){
                $html .= $this->fetch_item_header(
                    $row['id'], 
                    isset($path[$row['id']]) || $row['id']==$target_id?true:false, 
                    $row['id']==$target_id?'active':''
                );
                if(isset($path[$row['id']]))
                    $html .= $this->fetch_tree($target_id, $row['id']);
                
                if($row['id']==$target_id)
                    $html .= $this->fetch_childs_items($row['id']);
                    
                $html .= $this->fetch_item_footer();
            }
            
            if(!$current_id)$html .= $this->fetch_item_footer();
            
            return $html;
        }
        
        /**
            Генерация заголовка элемента дерева
        */
        function fetch_item_header(
            $id,
            $expanded = false,
            $class = '' // Цепочка каталогов, которые надо раскрыть
        ){
            global $_D;
            
            $id = intval($id);
            
            $html = '<div class="item" rel="'.$id.'" id="item_'.$id.'">';
            
            $title = _t('Tree root');
            if(!$id){
                $title = $this->title;
            }
            elseif($_D->search_one($this->table, array(), "`id`=$id AND `show`='Y' AND ".$this->cond)){
                if(isset($_D->record['name']))$title = $_D->record['name'];
            }
            
            // Определяем количество детей у раздела
            if(
                $_D->search_one(
                    $this->table,array(),
                    "`parent_id`=$id AND `show`='Y' AND ".$this->cond,
                    "count(`id`) as `childs`"
                )
            )$childs = $_D->record['childs'];
            
            $expand_class = "nochilds";
            
            if($childs && $expanded)
                $expand_class = 'minus';
            elseif($childs && !$expanded)
                $expand_class = 'plus';
                
            $html .='<div class="expand '.$expand_class.'" id="'.$this->id.'_expand_'.$id.'" rel="'.$id.'"></div>'; 
            $html .='<div class="title '.$class.'" id="'.$this->id.'_title_'.$id.'" rel="'.$id.'" onclick="';
            eval('$html .= "'.$this->onclick.'";');
            $html .= '"><a href="';
            eval('$html .= "'.$this->url.'";');
            $html .= '">'.$title.'</a></div>'; 
            $html .='<div class="childs" rel="'.$id.'" id="'.$this->id.'_childs_'.$id.'">';

            return $html;
        }

        function fetch_childs_items($parent_id){

            global $_D;
            
            $parent_id = intval($parent_id);
            
            $_D->search(
                array("a"=>$this->table),array(),array(),
                "`parent_id`=$parent_id AND `show`='Y' AND ".$this->cond,
                "`order_by` ASC",0,0,
                array(
                    "id"=>"id"
                )
            );
            $childs = $_D->rows;
            
            $html = '';
            foreach($childs as $child){
                $html .= $this->fetch_item_header($child['id']);
                $html .= $this->fetch_item_footer();
            }
            
            return $html;
        }

        /**
            Генерация хвостовика элемента дерева
        */
        function fetch_item_footer(){
            $html ='</div>'; 
            $html .='</div>'; 
            return $html;
        }

        
        function add(
            $parent_id, 
            $value
        ){
            global $_D;
            $fields = $this->insert_default_values;
            $fields['parent_id']    = intval($parent_id);
            $fields['name']         = $value;
            $id = $_D->insert($this->table, $fields);
            if($this->add_callback)
                eval($this->add_callback);
            return $id;
        }

        function delete($id){
            global $_D;
            $_D->record = array();
            if($_D->search_one($this->table,array("id"=>$id)))
                $_D->delete(
                    $this->table, 
                    array(),
                    "`id`=$id AND ".$this->edit_tree_condition
                );
            $record = $_D->record;
            if($this->delete_callback)
                eval($this->delete_callback);
            return true;
        }

        function save($id, $value){
            global $_D;
            $_D->record = array();
            if($_D->search_one($this->table,array("id"=>$id)))
                $_D->update(
                    $this->table, 
                    array("name"=>$_D->link->real_escape_string($value)), 
                    array(),
                    "`id`=".intval($id)." AND ".$this->edit_tree_condition
                );
            $record = $_D->record;
            if($this->save_callback)
                eval($this->save_callback);
            return true;
        }

        function move($id, $parent_id){
            global $_D;
            $_D->record = array();
            if($_D->search_one($this->table,array("id"=>$id)))
                $_D->update($this->table, 
                    array("parent_id"=>intval($parent_id)), 
                    array(),    
                    "`id`=".intval($id)." AND ".$this->edit_tree_condition
                );
            $record = $_D->record;
            if($this->move_callback)
                eval($this->move_callback);
            return true;
        }

        function sorting($id, $order){
            global $_D;
            
            // Оперделяем ID родителя элемента
            
            if(!$_D->search_one($this->table,array(), "`id`=$id AND ".$this->edit_tree_condition))return false;
            $parent_id = $_D->record['parent_id'];
            
            // Получаем список всех элементов этого родителя и выстраиваем
            $_D->search(
                array("a"=>$this->table),array(), array(),
                "`parent_id`=$parent_id AND ".$this->edit_tree_condition,
                "`order_by` ASC"
            );
            $items = $_D->rows;
            
            $result = array();
            $next = 0;
            foreach($items as $item){
                if($next){
                    $next = 0;
                    continue;
                }
                if($item['id']==$id && $order == -1 && isset($result[count($result)-1])){
                    $result[count($result)] = $result[count($result)-1];
                    $result[count($result)-2] = $item;
                }
                elseif($item['id']==$id && $order == 1 && isset($items[count($result)+1])){
                    $result[count($result)] = $items[count($result)+1];
                    $result[count($result)] = $item;
                    $next = 1;
                }
                else{
                    $result[count($result)] = $item;
                }
            }
            // Пересортировываем в БД
            foreach($result as $order_by=>$item)
                $_D->update(
                    $this->table, 
                    array("order_by"=>$order_by), 
                    array("id"=>$item['id'])
                );
            

            return true;
        }
        
        function fetch($object=null){
            
            $this->commit();
            
            $html = '<div class="'.__CLASS__.'" id="'.$this->id.'">';
            $html .= '<div class="context-menu">';
            $html .= '<div class="item" act="up" rel="0"><i class="icon-arrow-up"></i>Выше</div>';
            $html .= '<div class="item" act="down" rel="0"><i class="icon-arrow-down"></i>Ниже</div>';
            $html .= '<div class="item" act="add" rel="0"><i class="icon-plus"></i>Добавить</div>';
            $html .= '<div class="item" act="delete" rel="0"><i class="icon-trash"></i>Удалить</div>';
            $html .= '</div>';
            $html .= $this->fetch_tree($this->active_id);
            $html .= $this->fetch_item_footer();
            $html .= '</div>';
            return $html;
        }
        
        
    }
