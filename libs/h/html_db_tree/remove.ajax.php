<?php
    /**
        Перемещение элементов дерева
    */
    include_once("../../../../wirix/libs/wirix.class.php");

    $id = intval($_SERVER['http']->get('id'));      // ID элемента, который перемещаем
    $parent_id = intval($_SERVER['http']->get('parent_id'));    // Куда перемещаем
    $wirix_id = $_SERVER['http']->get('wirix_id');  // ID wirix-элемента, чтобы найти его в сессии
    
    if(!isset($_SESSION['wirix']['wirix_html_db_tree'][$wirix_id]))die('No session data');
    
    
    if(!$parent_id || !$id)die('ID failed');
    
    // Получаем данные формы из сессии
    $data = get_object_vars(json_decode(
	base64_decode($_SESSION['wirix']['wirix_html_db_tree'][$wirix_id])
    ));
    
    $table_name = $data['scheme']->table;

    $_SERVER['db']->update($table_name, array("parent_id"=>$parent_id), array("id"=>$id));

    print_r($data);
    die;
    // Узнаё родителя этого элемента
    $_SERVER['db']->search_one($table_name,array("id"=>$id));
    $item = $_SERVER['db']->record;

    // ОПределяемся с дополнительными условиями
    $search = array();
    $data['search'] = get_object_vars($data['search']);
    foreach($data['search'] as $k=>$v){
	$search["`a`.`$k`"] = $v;
    }
    
    // Выбираем все разделы того же уровня, что и пересортируемый
    $search["`a`.`parent_id`"] = $item['parent_id'];
    $_SERVER['db']->search(
        array("a"=>$table_name),
        array(),
        $search,
        "",
        "`a`.`order_by` ASC",0,0
    );
    $sections = $_SERVER['db']->rows;

    // Перенумеровываем
    foreach($sections as $k=>$v)$sections[$k]['order_by'] = $k;
    
    // Меняем местами пересортируемый с соседним, в зависимости от того вверх надо или вниз
    foreach($sections as $k=>$v)if($sections[$k]['id']==$id){
        switch($act){
            case 'up':
                $sections[$k-1]['order_by'] = $k;
                $sections[$k]['order_by'] = $k-1;
            break;
            case 'down':
                $sections[$k+1]['order_by'] = $k;
                $sections[$k]['order_by'] = $k+1;
            break;
        }
    }
        
    foreach($sections as $section){
        $id = $section['id'];
        unset($section['id']);
        $_SERVER['db']->update($table_name, $section, array("id"=>$id));
    }
    
