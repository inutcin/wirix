<?php
    include_once("../../../../wirix/libs/wirix.class.php");
    $id = intval($_SERVER['http']->get('id'));
    $wirix_id = $_SERVER['http']->get('wirix_id');
    
    $answer = array("error"=>"", "parent_id"=>0);
    
    if(!isset($_SESSION['wirix']['wirix_html_db_tree'][$wirix_id]))die('No session data');
    
    
    // Получаем данные формы из сессии
    $data = get_object_vars(json_decode(
	base64_decode($_SESSION['wirix']['wirix_html_db_tree'][$wirix_id])
    ));
    
    $scheme = $_SERVER['wirix']->init_datascheme(str_replace("datascheme_", "", $data['scheme']->called_class));
    $scheme->set_condition($data['cond']);
    $data['search'] = get_object_vars($data['search']);
    
    foreach($data['search'] as $k=>$v)$scheme->set($k, $v);



    $scheme->clear();
    $scheme->set('parent_id', $id);
    $items = $scheme->find('all');

    if($items){
        $answer['error'] = _t('Cant delete item included child subitems');
        echo json_encode($answer);
        die;
    }

    $scheme->clear();
    $scheme->set('id', $id);
    $item = $scheme->find('first');
    $parent_id = $item['parent_id'];
    
    $answer['parent_id'] = $parent_id;
    $scheme->delete();
    echo json_encode($answer);
    die;
