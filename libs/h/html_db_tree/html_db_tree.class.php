<?php
    include_once(dirname(__FILE__)."/../../wirix.class.php");
    
    /**
        Генерация дерева с подгружающимися разделами из таблицы базы данных
        у таблицы должно быть 
            - поле name
            - поле parent_id, указывающее на родительский элемент 
            - поле order_by, указывающий порядок следования элементов.
            - поле show('Y' или 'N') указывающее показывать ли раздел
        
    */
    class wirix_html_db_tree extends wirix{
        
        var $scheme = '';
        var $active_id = 0;       //!< ID 
        var $cond = "";     //!< Кастомные условия выборки элементов дерева
        var $current_id = 0;    //!< ID текущего корня дерева
        var $title_field = 'name';  //!< Имя поля таблицы, служашее заголовком элементов
        var $url_pattern = '';      //!< Выражение, использующееся для генерации ссылки
        var $onclick_pattern = '';      //!< Выражение, использующееся для генерации обработчика элемента дерева
        var $editable = false;          //!< Редактируемость структуры
        var $search = array();
        
        function __construct(){
            parent::__construct(__CLASS__);
        }
        
        function fetch($object=null){
            $html = '';
            $html .= '<div class="'.__CLASS__.'" id="'.$this->id.'">';
            if($this->editable){
                $html .= '<div style="margin-bottom: 10px;">';
                $html .= '<a href="#" id="'.$this->id.'_add_link"><i class="icon-plus"></i>'._t('Add New Subcategory').'</a>';
                $html .= '</div>';
            }
            $html .= '<div id="'.__CLASS__.'_'.$this->id.'_items_'.$this->current_id.'">'; 
            $html .= '</div>'; 
            $html .= '</div>';
            $html .= '
<script>
    $("body").ready(function(){
        html_db_tree_load("'.
            $this->id.'", '.
            $this->current_id.', '.
            $this->active_id.');
        $("#'.$this->id.'_add_link").click(function(){
            html_db_tree_add("'.$this->id.'", 0);
        });
    });
</script>';
            $this->commit();
            return $html;
        }
        
    }
?>
