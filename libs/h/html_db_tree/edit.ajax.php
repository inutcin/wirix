<?php
    include_once("../../../../wirix/libs/wirix.class.php");
    $item_id = intval($_SERVER['http']->get('item_id'));
    $wirix_id = $_SERVER['http']->get('wirix_id');
    
    if(!isset($_SESSION['wirix']['wirix_html_db_tree'][$wirix_id]))die('No session data');
    
    
    // Получаем данные формы из сессии
    $data = get_object_vars(json_decode(
	base64_decode($_SESSION['wirix']['wirix_html_db_tree'][$wirix_id])
    ));
    
    $scheme = $_SERVER['wirix']->init_datascheme(str_replace("datascheme_", "", $data['scheme']->called_class));
    $scheme->id = 'edit_section_scheme';
    $scheme->set_condition($data['cond']);
    $data['search'] = get_object_vars($data['search']);
    
    foreach($data['search'] as $k=>$v)$scheme->set($k, $v);
    $scheme->set('id', $item_id);
    $scheme->find('first');
    
    /*
    echo "<pre>";
    print_r($scheme);
    echo "</pre>";
    */
    $scheme->fields['parent_id']['join_schema']->title = 'Родительский элемент';
    array_push($scheme->hidden_fields, 'parent_id');
    
    //$scheme->search = 
    
    $form = $_SERVER['wirix']->lib_load('html_form_datascheme');
    $form->id = 'section_edit_form';
    $form->primary_key = $item_id;
    $form->scheme = $scheme;
    $form->datascheme_class = $data['scheme']->called_class;
    $form->datascheme_id = $data['scheme']->id;
    $form->method = 'save';
    $form->success_redirect = '#';
    $form->use_back_button = false;
    //$scheme_name = str_replace("datascheme_", "", $data['scheme']->called_class);
    echo $form->fetch();
?>
<script>
    $('#html_form_<?php echo $form->id;?>_submit').click(function(){
        $('#form_<?php echo $form->id;?>').submit();
        setTimeout(function(){
            html_db_tree_load('<?php echo $wirix_id;?>', <?php echo $form->scheme->get('parent_id');?>);
            wirix_html_modal_hide('myModal');
        }, 2000);
        return false;
    });
</script>
