<?php
    include_once("../../../../wirix/libs/wirix.class.php");
    header("Cache-Control: no-cache");
    header("Pragma: no-cache");

    $item_id = intval($_SERVER['http']->get('item_id'));

    $active_id = intval($_SERVER['http']->get('active_id'));
    $wirix_id = $_SERVER['http']->get('wirix_id');
    
    
    if(!isset($_SESSION['wirix']['wirix_html_db_tree'][$wirix_id]))die('No session data');
    
    // Получаем данные формы из сессии
    $data = get_object_vars(json_decode(
	base64_decode($_SESSION['wirix']['wirix_html_db_tree'][$wirix_id])
    ));
    
    $search = array("`a`.`show`"=>"Y");
    foreach($data['search'] as $k=>$v)if(trim($v))$search["`a`."."`$k`"] = $v;
    
    //if($item_id)$search['`a`.`parent_id`'] = $item_id;
    $search['`a`.`parent_id`'] = $item_id;
    
//    print_r($search);
    
    $_SERVER['db']->search(
        array("a"=>$data['scheme']->table, "b"=>$data['scheme']->table),
        array("`a`.`parent_id`=`b`.`id` AND `b`.`show`='Y'"=>"LEFT"),
        $search,
        "",
        "`a`.`order_by` ASC",
        0,
        0,
        array(
            "`a`.`id`"=>"id",
            "`a`.`parent_id`"=>"parent_id",
            "`a`.`order_by`"=>"order_by",
            "`a`.`name`"=>"title",
            "count(`b`.`id`)"=>"subsections"
        ),
        "`a`.`id`"
    );
    $sections = $_SERVER['db']->rows;
    
    $active_id = intval($_SERVER['http']->get('active_id'));

    // Строим путь до активного элемента
    $count = 0;
    $cid = array();
    $_SERVER['db']->search_one($data['scheme']->table, array("id"=>$active_id));
    while($_SERVER['db']->record['id']!=$item_id && $_SERVER['db']->record['id']){
        $cid[] =  $_SERVER['db']->record['id'];
        $count++;
        $_SERVER['db']->search_one($data['scheme']->table, array("id"=>$_SERVER['db']->record['parent_id']));
        // Защита от зацикла
        if($count>100)break;
    }
    $path_len = count($cid);
    $next_id = array_pop($cid);
    
    foreach($sections as $section){
?>
<div class="wirix-tree-item<?php echo ($next_id==$section['id'] && $path_len==1)?" active":"";?>" 
    id="<?php echo $data['id']?>_<?php echo $section['id']?>"
>
    <a href="<?php 
        if(isset($data['url_pattern']) && $data['url_pattern'])
            eval($exec_url ="echo $data[url_pattern];")
    ?>" <?php if(isset($data['onclick_pattern']) && $data['onclick_pattern'])
            eval($exec_clk ="echo 'onclick=\"$data[onclick_pattern]\"';")
    ?>>
        <?php echo $section['title'];?>
    </a>
    <?php if($data['editable']){?>
    <?php }?>
    <i id="<?php echo $section['id']?>" title="Развернуть" class="icon-folder-close<?php echo ($next_id==$section['id'] && $path_len==1)?" icon-white":"";?> folder" onclick="html_db_tree_expand('<?php echo $data['id']?>', <?php echo $section['id'];?>, 0);"></i>
</div>
<div class="wirix-tree-subitems" style="display: none;" id="wirix_html_db_tree_<?php 
    echo $data['id']?>_items_<?php echo $section['id']?>"></div>
<?        
    }
    
    echo '<div style="display:none;"></div>';
?>
<?php if($active_id){?>
<script>
    <?php if($next_id!=''){?>
    html_db_tree_load('<?php echo $data['id']?>', <?php echo $next_id;?>, <?php echo $active_id;?>);
    <?php }?>
</script>
<?php }?>

<?php if($data['editable']){?>
<script>
    var start_id_element = null;
    
    $(".wirix-tree-item").bind ("contextmenu", function(data){
        var id = data.target.id;
        html_db_tree_show_menu(data, id, '<?php echo $data['id']?>', <?php echo $item_id?>);
        return false;
    });

    $(".wirix-tree-item").draggable({
        revert : true, 
        revertDuration: 200
    });

    $(".wirix-tree-item").droppable({
        drop: function(data, ui){
            var re = RegExp('<?php echo $data['id']?>_');
            parent_id = this.id;
            id = ui.draggable.attr('id');
            parent_id = parent_id.replace(re,"");
            id = id.replace(re,"");
            if(id!=parent_id)
                html_db_tree_move(
                    '<?php echo $data['id']?>', 
                    id, 
                    parent_id
                );
        }
    });
</script>
<?php }?>
