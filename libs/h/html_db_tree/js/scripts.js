function html_db_tree_get_dom_id(wirix_id, item_id){
    return 'wirix_html_db_tree_'+wirix_id+'_items_'+item_id;
}


function html_db_tree_expand(wirix_id, item_id, active_id){
    var id = html_db_tree_get_dom_id(wirix_id, item_id);
    if($('#'+id).css('display')=='block'){
        $('#'+id).css('display', 'none');
        $('#'+wirix_id+'_'+item_id+' .icon-folder-close').removeClass('icon-folder-open');
    }
    else{
        $('#'+id).css('display', 'block');
        $('#'+wirix_id+'_'+item_id+' .icon-folder-close').addClass('icon-folder-open');
        html_db_tree_load(wirix_id, item_id, active_id);
    }
}


function html_db_tree_load(wirix_id, item_id, active_id){
    var id = html_db_tree_get_dom_id(wirix_id, item_id);
    
    $('#'+wirix_id+'_'+item_id+' .icon-folder-close').addClass('icon-folder-open');
    
    $('#'+id).css('display', 'block');
    $('#'+id).prepend('<img style="position: absolute;" src="/wirix/libs/h/html/i/progress/001.gif">');
    $('#'+id).load(
        '/wirix/libs/h/html_db_tree/load.ajax.php?wirix_id='
            +wirix_id+
        '&item_id='
            +item_id+
        '&active_id='
            +active_id,
        function(){
            $('.wirix-tree-item-tools .icon-plus').attr('title', 'Добавить дочерний элемент элемент дерева');
            $('.wirix-tree-item-tools .icon-arrow-up').attr('title', 'Выше');
            $('.wirix-tree-item-tools .icon-arrow-down').attr('title', 'Ниже');
            $('.wirix-tree-item-tools .icon-pencil').attr('title', 'Редактировать элемент дерева');
            $('.wirix-tree-item-tools .icon-remove').attr('title', 'Удалить элемент дерева');
        }
    );

}

function html_db_tree_add(wirix_id, item_id){
    var id = html_db_tree_get_dom_id(wirix_id, item_id);
    wirix_html_modal_show('myModal','Добавление элемента дерева','/wirix/libs/h/html_db_tree/add.ajax.php?wirix_id='
            +wirix_id+
        '&item_id='
            +item_id
    );
}

function html_db_tree_delete(wirix_id, id, item_id){
    if(!confirm('Точно удалить?'))return false;
    $.get(
        '/wirix/libs/h/html_db_tree/delete.ajax.php?wirix_id='
            +wirix_id+
        '&id='
            +id
        ,
        function(data){
            var answer = JSON.parse(data);
            if(answer.error!=''){
                alert(answer.error);
                return false;
            }
            $('.wirix_html_db_tree_popup').remove();
            html_db_tree_load(wirix_id, answer.parent_id, answer.parent_id);
        }
    );
}


function html_db_tree_edit(wirix_id, item_id){
    var id = html_db_tree_get_dom_id(wirix_id, item_id);
    wirix_html_modal_show('myModal','Редактирование элемента дерева','/wirix/libs/h/html_db_tree/edit.ajax.php?wirix_id='
            +wirix_id+
        '&item_id='
            +item_id
    );
}


function html_db_tree_resort(type, wirix_id, item_id, id){
    $('.wirix_html_db_tree_popup').html('<img style="position: absolute;" src="/wirix/libs/h/html/i/progress/001.gif"><br/>');
    $.get(
        '/wirix/libs/h/html_db_tree/resort.ajax.php?wirix_id='
            +wirix_id+
        '&id='
            +id+
        '&act='
            +type
        ,
        function(){
            html_db_tree_load(wirix_id, item_id);
        }
    );
}

function html_db_tree_move(wirix_id, id, parent_id){
    $.get(
        '/wirix/libs/h/html_db_tree/remove.ajax.php?wirix_id='
            +wirix_id+
        '&id='
            +id+
        '&parent_id='
            +parent_id
        ,
        function(){
            html_db_tree_load(wirix_id, 0, id);
        }
    );
}


function html_db_tree_hide(wirix_id, item_id){
    var id = html_db_tree_get_dom_id(wirix_id, item_id);
    $('#'+id).html('');
    $('#'+id).css('display','none')
    $('#'+wirix_id+'_'+item_id+' .icon-folder-close').removeClass('icon-folder-open');
    return false;
}


function html_db_tree_show_menu(event, id, wirix_id, item_id){
    var re = new RegExp(wirix_id+'_');
    var id = id.replace(re, "");
    $('#'+wirix_id+'_'+id).append(
        '<div class="wirix_html_db_tree_popup">'
            +'<div onclick="html_db_tree_resort(\'up\', \''+wirix_id
                +'\', '+item_id+', '+id+');"><i class="icon-arrow-up"></i>Вверх</div>'
            +'<div onclick="html_db_tree_resort(\'down\', \''+wirix_id
                +'\', '+item_id+', '+id+');"><i class="icon-arrow-down"></i>Вниз</div>'
            +'<div onclick="html_db_tree_add(\''+wirix_id+'\', '+
                id+');"><i class="icon-plus"></i>Добавить подраздел</div>'
            +'<div onclick="html_db_tree_edit(\''+wirix_id+'\', '+id+');"><i class="icon-pencil"></i>Переименовать</div>'
            +'<div onclick="return html_db_tree_delete(\''+wirix_id+'\', '+id+', '+item_id+');"><i class="icon-remove"></i>Удалить</div>'
        +'</div>'
    );
    $('.wirix_html_db_tree_popup').css("top", event.clientY-10);
    $('.wirix_html_db_tree_popup').css("left", event.clientX-30);
    $('.wirix_html_db_tree_popup').mouseleave(function(data){
        $('.wirix_html_db_tree_popup').remove();
    });
}


