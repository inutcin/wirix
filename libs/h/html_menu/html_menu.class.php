<?php
    include_once(dirname(__FILE__)."/../../wirix.class.php");
    
    /**
        \class wirix_html_menu
        \brief Генерация 2-х уровнего меню меню
        \ref wirix_html_menu

\code{.pl}
$menu = $_SERVER['wirix']->lib_load("html_menu");
$menu->id = 'second_menu';
$menu->type = 'nav nav-pills';
$menu->items = array (
    array(
        "text"=>"Тематики",   
        "url"=>"/site_hunter.php?sec=themes", 
        "onclick"   => "return confirm('wat?');",
        "class"=>(
            $_SERVER['http']->get('sec')=='themes' || $_SERVER['http']->get('sec')=='new_theme'
            ?"active dropdown":"dropdown"
        ),
        "childs"=>array(
            array(
                "text"=>"Список тематик",   
                "url"=>"/site_hunter.php?sec=themes",    
                "icon"=>"icon-th-list"
            ),
            array(
                "text"=>"Новая тематика",   
                "url"=>"/site_hunter.php?sec=new_theme",    
                "icon"=>"icon-file"
            )
        )
    ),
    array(
        "text"=>"Сайты",      
        "url"=>"/site_hunter.php?sec=sites"
    )
);

$menu->fetch();
\endcode
    */
    class wirix_html_menu extends wirix{
        
        var $items = array();           //!< Массив элементов меню
        var $type = "nav nav-pills";    //!< Типы меню  "nav nav-pills"/"nav nav-pills"
        var $id = '';                   //!< ID меню
        
        function __construct(){
            parent::__construct();
        }
        
        /**
            Генерация поля
        */
        function fetch($object = null){
            ob_start();
?>            

<ul class="<?php echo $this->type?>" id="<?php echo $this->id;?>">
    <?php foreach($this->items as $k=>$item){?>
    <li class="<?php echo $_SERVER['http']->arrays($item, "class","dropdown")?>">
        <a id="<?php echo $this->id;?>_item_<?php echo $k?>" class="dropdown-toggle" href="<?php echo $_SERVER['http']->arrays($item, "url", "#")?>"
        <?php if($_SERVER['http']->arrays($item, "onclick")){?>onclick="<?php echo $_SERVER['http']->arrays($item, "onclick", "true")?>"<?php }?>
        >
            <?php if($_SERVER['http']->arrays($item, "icon")){?><i class="<?php echo $_SERVER['http']->arrays($item, "icon");?>"></i><?php }?>
            <?php echo $item['text'] ?>
            <?php if($_SERVER['http']->arrays($item, "childs")){?>
            <b class="caret"></b>
            <?php }?>
        </a>
        <?php if($_SERVER['http']->arrays($item, "childs")){?>
        <ul class="dropdown-menu">
            <?php foreach($item['childs'] as $child){?>
            <a href="<?php echo $_SERVER['http']->arrays($child, "url","#");?>" class="<?php echo $_SERVER['http']->arrays($child, "class", "");?>">
                <?php if($_SERVER['http']->arrays($child, "icon")){?><i class="<?php echo $_SERVER['http']->arrays($child, "icon");?>"></i><?php }?>            
                <?php echo $_SERVER['http']->arrays($child, "text", "ссылка");?>
            </a>
            <?php if($_SERVER['http']->arrays($child, "separator")){?><div class="separator">&#160;</div><?php }?>
            <?php }?>
        </ul>
        <?php }?>
    </li>
    <?php }?>
</ul>
<?php
            $html = ob_get_contents();
            ob_end_clean();
            return $html;
        }
    }
?>
