<?php
    include_once(dirname(__FILE__)."/../../wirix.class.php");
    
    /**
        Генерация хлебных крошек
    */
    class wirix_html_breadcrumbs extends wirix{
        
        var $crumbs = array();
        
        function __construct(){
            parent::__construct();
        }
        
        /**
            Добавляем очередную крошку
        */
        function add($text, $url = ''){
            if(!$url)$url = $_SERVER['REQUEST_URI'];
            $link = $_SERVER['wirix']->lib_load("html_href");
            $link->text = $text;
            $link->title = $text;
            $link->url = $url;
            $this->crumbs[] = $link;
        }
        
        /**
            Генерация поля
        */
        function fetch($object = null){
            ob_start();
            echo '<ul class="breadcrumb">';
            for($i=0,$c=count($this->crumbs);$i<$c;$i++){
                echo "<li>".$this->crumbs[$i]->fetch().($i<($c-1)?' <span class="delimiter">&raquo;</span> ':"")."</li>";
            }
            echo "</ul>";
            $html = ob_get_contents();
            ob_end_clean();
            return $html;
        }
        
        /**
            Воозвращает последний элемент-ссылку
        */
        function last(){
            if($this->crumbs)
                return $this->crumbs[count($this->crumbs)-1];
            return false;
        }
        
    }
?>
