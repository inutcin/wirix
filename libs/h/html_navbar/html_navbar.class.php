<?php
    include_once(dirname(__FILE__)."/../../wirix.class.php");
    
    /**
        Генерация панели навигации

        $menu = $_SERVER['wirix']->lib_load('main_navbar');
        $menu->fetch();
    */
    $_SERVER['wirix']->lib_load("html_menu", false);
    class wirix_html_navbar extends wirix_html_menu{
        
        var $title = 'Заголовок панели навигации';  //!< Заголовок панели навигации
        var $auth_enable = false;
        
        function __construct(){
            parent::__construct();
        }
        
        function fetch($object=null){
            $html = '';
$html .= '
<div class="navbar">
  <div class="navbar-inner">
    <div class="container">
    <a class="brand" href="/">'.$this->title.'</a>

'.parent::fetch();


    if($this->auth_enable){
        $ava_src = '/images/users_photos/'.($_SERVER['auth']->is_auth()?$_SERVER['auth']->user['id']:"0")."_tiny.png";
        $filename = $_SERVER['DOCUMENT_ROOT'].$ava_src;
        $html.='<div class="auth">';
        if($_SERVER['auth']->is_auth())$html.='<img src="'.(file_exists($filename)?$ava_src:'/wirix/libs/a/auth/i/unknown-user50.png').'" class="ava">';
        $_SERVER['auth']->id = 'auth_form';
        if(!$_SERVER['auth']->is_auth()){
            $html .= $_SERVER['auth']->fetch_login_form();
        }
        else{
            $html .= $_SERVER['auth']->fetch_exit_form();
        }
        $_SERVER['auth']->commit();
        $html .= '</div>'; 
    }


    $html .= '
    </div>
  </div>
</div>
';
            return $html;
        }
        
    }
?>
