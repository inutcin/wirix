<?php

    class wirix_html_form_datascheme extends wirix_html_form{
        
        var $scheme             = null;     //!< Cвязанная схема данных
        var $datascheme_id      = null;     //!< ID связанной схемы данных
        var $method = 'add';                //!< Метод обработки (save, delete или add)
        var $primary_key        = 0;        //!< ID элемента для редактировани
        var $success_redirect   = '';       //!< Редирект пр успешной отработке формы
        
        protected $types = array( //!< Соответствие типов БД типам инпутов
            "enum"      => "select",
            "text"      => "textarea",
            "longtext"  => "textarea",
            "timestamp" => "date",
            "date"      => "date",
            "datetime"  => "date",
            "tinyint"   => "checkbox"
        );
        
        function __construct(){
            //wirix::__construct(__CLASS__);
            $this->use_back_button = true;
            $this->back_button_text =  _t('Cancel');
            return $this->wirix_html_form_datascheme();
        }
        
        function wirix_html_form_datascheme(){
            $this->called_class = __CLASS__;
        }
        
        function fetch($object = null){
            // Без прямо указанного метода никуде не едем
            if(!$this->method){
                $this->error = "Не указан метод работы с формой html_form_datascheme";
                return false;
            }
            // Выбираем обработчик по методу
            $this->submit_url = "/wirix/libs/h/html_form_datascheme/act.ajax.php";
            
            // Инициализируем объект схемы данных
            $this->scheme->id = $this->datascheme_id;
            
            // Получаем данные во все поля при редактировании
            if($this->primary_key && ($this->method=='save' || $this->method=='delete' || $this->method=='view')){
                $this->scheme->clear();
                $this->scheme->set($this->scheme->primary_key,$this->primary_key);
                if(!$this->scheme->find('first')){
                    echo wirix_error::alert($this->scheme->error);
                }
            }
            
            // Если ID формы не указан - даём одномеёёный со схемой данных и методом
            if(!$this->id)$this->id = $datascheme."_".$this->method."_form";
            
            // Формируем список полей из доступных
            foreach($this->scheme->enabled_fields as $field_name){
                $field = $this->scheme->fields[$field_name];
                
                // Определяем тип поля
                $type = '';
                if(array_search($field_name, $this->scheme->hidden_fields)!==false)$type = 'hidden';
                
                // Получаем тип поля из таблицы соответствий
                if(!$type)
                    if(isset($this->types[$this->scheme->fields[$field_name]['type']]))
                        $type = $this->types[$this->scheme->fields[$field_name]['type']];
                
                // Если тип поля не определём - будет текстовое
                if(!$type)$type = 'text';

                // Для удаления - только тип none
                if(($this->method=='delete'  || $this->method=='view')  && $type!='hidden')$type = 'none';
                
                $input  = $_SERVER['wirix']->lib_load("html_input");
                $input->type = $type;
                $input->name = $field_name;
                $input->value = $field['value']!=''?$field['value']:$field['default'];
                $input->regexp = "/".$this->scheme->regexps[$field_name]['regexp']."/";
                $input->title = $field['comment'];
                $input->comment = $field['description'];
                
                switch($type){
                    case 'hidden':
                    break;
                    case 'select':
                        $values = array();
                        foreach($field['values'] as $value)$values[] = array("title"=>$value, "value"=>$value);
                        $input->values = $values;
                    break;
                    default:
                        
                        // Для поля со связанной таблицей её название
                        if(isset($this->scheme->fields[$field_name]['join_schema'])){
                            $input->title = $this->scheme->fields[$field_name]['comment'];
                            $input->regexp = "/.+/";
                            
                            if($type!='none')$input->type = "suggest";
                            $input->value = $field['value']?$field['value']:$field['default'];
                            $join_sheme = $this->scheme->fields[$field_name]['join_schema'];
                            $join_sheme->clear();
                            $join_sheme->set($join_sheme->primary_key, $field['value']);
                            if($field['value'])$join_sheme->find('first');
                            
                            if(!$input->value)
                                $input->suggest_value = '';
                            else
                                $input->suggest_value = $join_sheme->fields[$join_sheme->title_field]['value'];

                            $search = "";
                            foreach($join_sheme ->fields as $fname => $fieldd)
                                if($fieldd['value'])$search .= "&search[$fname]=".$fieldd['value'];

                            
                            $input->suggest_url = 
                                "/wirix/libs/h/html_form_datascheme/suggest.ajax.php".
                                "?table=".$this->scheme->fields[$field_name]['join_schema']->table.
                                "&primary_key=".$this->scheme->fields[$field_name]['join_schema']->primary_key.
                                "&title_field=".$this->scheme->fields[$field_name]['join_schema']->title_field.
                                $search.
                                "&condition=".$join_sheme->condition;
                            
                            //Исключение специально для типа none
                            if($type=='none')$input->value = $input->suggest_value;
                                
                        }
                        
                    break;
                }

                $this->add_input($input);
                unset($input);
            }

            $title = $this->scheme->title.". ";
            switch($this->method){
                case 'add':
                    $title .= _t('Add');
                    $this->send_button_text = _t('Add');
                break;
                case 'save':
                    $title .= _t('Edit');
                    $this->send_button_text = _t('Edit');
                break;
                case 'delete':
                    $title .= _t('Delete');
                    $this->send_button_text = _t('Delete');
                break;
                case 'delete':
                    $title .= _t('View');
                    $this->send_button_text = _t('View');
                break;
            }
            // Выставляем заголовок из схемы данных, если не указан свой
            if(!$this->title || $this->title=='Форма')$this->title = $title;

            $this->commit();
            return parent::fetch();
        }
        
    }
