<?php
    include_once("../../wirix.class.php");
    /**
        Скрипт, предоставляющий json-данные для автозаполнения поля из
        нужной таблицы.
        Принимает GET-данные
        table - имя таблицы
        primary_id - поле таблицы, из которого будут браться ID автозаполнения
        title_field - имя поля, из которого будут браться заголовки автодополнения
    */

    if(!isset($_GET['search']))$_GET['search'] = array();
    $search = $_GET['search'];
    $cond = $_SERVER['http']->get('condition');
    $scond = '';
    if(is_array($search))foreach($search as $k=>$v){
        $k = $_SERVER['db']->link->real_escape_string($k);
        $v = $_SERVER['db']->link->real_escape_string($v);
        $scond .= " AND `a`.`$k`='$v'";
    }
    
    $_SERVER['db']->search(
        array("a"=>$_SERVER['http']->get('table')),
        array(),
        array(),
        "`a`.`".
            $_SERVER['db']->link->real_escape_string($_SERVER['http']->get('title_field')).
            "` LIKE '%".
            $_SERVER['db']->link->real_escape_string($_SERVER['http']->get('q'))."%'
            $scond AND $cond",
        "",
        10,
        0,
        array(
            "`a`.`".$_SERVER['db']->link->real_escape_string($_SERVER['http']->get('primary_key'))."`"=>"id",
            "`a`.`".$_SERVER['db']->link->real_escape_string($_SERVER['http']->get('title_field'))."`"=>"value",
        )
    );
    
    header("Content-type: application/json");
    echo json_encode($_SERVER['db']->rows);
