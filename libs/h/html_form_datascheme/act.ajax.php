<?php

    include_once("../../wirix.class.php");
    $answer = array();
    $answer['redirect'] = '';
    $answer['error'] = '';
    
    $form_id = $_SERVER['http']->post('form_id');
    if(!$form_id)
        $answer['error'] = _t('Form ID is empty');
    
    
    if(!$answer['error'] && !isset($_SESSION['wirix']['wirix_html_form_datascheme'][$form_id]))
        $answer['error'] = _t('No data about form ID %form_id in session', array("%form_id"=>$form_id));
    
    $form = @$_SESSION['wirix']['wirix_html_form_datascheme'][$form_id];
    $form = json_decode(
        base64_decode($form)
    );
    
    $method = '';
    if(!$answer['error'] && !$form->method)
        $answer['error'] = _t('From processing method is empty');
    elseif(!$answer['error'])
        $method = $form->method;


    if(!$answer['error'] && $form->method!='add' && !$form->primary_key)
        $answer['error'] = _t('Primary key value is empty');
    
    if(!$answer['error'] && !($method=='add' || $method=='save' || $method=='delete'))
        $answer['error'] = _t('Unknown form processing method');
    
    //
    if(!$answer['error'] && $form->success_redirect)
        $answer['redirect'] = $form->success_redirect;
    elseif(!$answer['error'])
        $answer['redirect'] = $_SERVER['HTTP_REFERER'];
        
    if(!$answer['error']){
        // Формируем массив полей
        $inputs = $form->inputs;
        $fields = array();
        foreach($inputs as $input){
            if($input->name=='id')continue;
            $fields[$input->name] = $_SERVER['http']->post($input->name);
        }
        
        // Инициализируем схему данных
        $class = str_replace("datascheme_", "", $form->scheme->called_class);
        $scheme = $_SERVER['wirix']->init_datascheme($class);
        
        // Устанавливаем значения полей
        $scheme->set($fields);
        switch($method){
            case 'add':
            break;
            case 'save':
                $scheme->set($scheme->primary_key, $form->primary_key);
            break;
            case 'delete':
                $scheme->set($scheme->primary_key, $form->primary_key);
            break;
            case 'view':
                $scheme->set($scheme->primary_key, $form->primary_key);
            break;
        }
        if(!$scheme->$method())
            $answer['error'] = $scheme->error;
        else
            $answer['success'] = _t('Success!');
        
    }
    
    
    echo json_encode($answer);
    
