<?php
    include_once(dirname(__FILE__)."/../../wirix.class.php");
    
    /**
        @class wirix_http
        @ref wirix_http
        @brief Метод для работы с http-протоколом
    */
    class wirix_http extends wirix{
        
        var $module = '';   //!< Первый каталог адреса
        var $section = '';  //!< Второй каталог адреса
        var $action = '';   //!< Третий каталог адреса
        var $item = '';     //!< Четвёртый каталог адреса

        
        var $url = '';      //!< Адрес, на который направляем запрос
        var $timeout = 10;  //!< Таймаут соединения
        
        var $post_params = array(); //!< Параметры POST - запроса в виде массива  (сипользуется, если не задана строка $this->post_request)
        var $post_request = '';     //!< Параметры POST-запроса в виде строки par0=val0&par1=val1
        
        
        var $cookiefile = ''; //!< используемый файл куков. Используется как для вывода, так и для получения.
        /*
            @var $proxy
            @brief параметры прокси-соединения, в виде массива вида
             
            Используются в виде
            
            \code{.js}
            $proxy - 
                array(
                    "host"=>"yandex.ru",
                    "port"=> "3128",
                    "username"=>"JohnDoe",
                    "password"=> "foobar"
                )
            \endcode
            
            Если не указан или пуст - соединяемся напрямую, если логин или пароль не указаны, соединяемся без авторизации
        */
        var $proxy = array();
        var $http_auth = ''; //!< Данные для http-авторизации username:password
        var $useragent = "User-Agent: Mozilla/5.0 (X11; U; Linux i686; ru; rv:1.9.1.16) Gecko/20111108 AlexaToolbar/alxf-2.14 Iceweasel/3.5.16 (like Firefox/3.5.16)"; //!< USeragent
        var $outfile = '';          //!< Файл для сохранения контента, если он не указан - контент сохраняется в переменной $this->content
        var $out_headers_file = ''; //!< Файл для сохранения заголовков ответа, если он не указан, заголовки сохраняются в переменной $this->returned_headers
        var $in_headers_file = '';  //!< Файл, для сохранения зололовков запроса
        var $ssl_verify = false;    //!< Удостовериться в отсутствии SSL-ошибок, при использовании https 
        var $referer = '';          //!< Отправляемый REFERER
        var $follow_location = 1;   //!< Следовать редиректам
        var $content = "";          //!< В данную переменную складывается полученное содержимое 
        var $returned_headers = array();//!< Возвращаемые заголовки
        var $returned_code = 200;   //!< Возвращаемый код ответа
        var $error_code = 0;        //!< Код ошибки
        var $headers = array();     //!< Отправляемые вручную заголовки
        var $cookie = '';           //!< Отправляемые запросом куки в формате "name=value; name=value; ..."
        var $only_headers = false;  //!< Получить только заголовки ответа (метод HEAD)
        
        
        function __construct(){
            // Установка referer
            if(!isset($_SERVER['HTTP_REFERER']))
                $this->referer = "http://".$_SERVER['HTTP_HOST'];
            else
                $this->referer = $_SERVER['HTTP_REFERER'];
            
            $tmp = preg_split("/[\/\?]/", $_SERVER['REQUEST_URI']);
            
            $this->module     = $this->arrays($tmp,1);
            $this->section    = $this->arrays($tmp,2);
            $this->action     = $this->arrays($tmp,3);
            $this->item       = $this->arrays($tmp,4);
            $this->subitem    = $this->arrays($tmp,5);
            $this->subsubitem    = $this->arrays($tmp,6);

            parent::__construct();
        }
        
        
        /**
            @fn get_post_clean
            @brief Очистка от тегов GET и POST
             
            Каждый элемент массива GET и POST подвергается обработке strip_tags
        */
        function get_post_clean(){
            $_SERVER['http']->get_clean();
            $_SERVER['http']->post_clean();
        }

        /**
            @fn get_post_escape
            @brief Зачистка GET и POST от SQL-инъекций
             
            Каждый элемент массива GET и POST подвергается обработке addslashes
        */
        function get_post_escape(){
            $_SERVER['http']->get_escape();
            $_SERVER['http']->post_escape();
        }

        function get_clean(){foreach($_GET as $k=>$v)    $_GET[$k] = strip_tags($v,"");}
        function get_escape(){foreach($_GET as $k=>$v)    $_GET[$k]   = addslashes ($v);}
        function post_clean(){foreach($_POST as $k=>$v)    $_POST[$k] = is_scalar($v)?strip_tags($v,""):"";}
        function post_escape(){foreach($_POST as $k=>$v)    $_POST[$k]   = addslashes ($v);}
        
        /**
            @fn request
            @brief выполнение http-запроса, согласно установленным параметрам
            
            @returns Возвращает код 0,1 в зависимости от результата $this->http_code
        */
        function request(){
            $ch = curl_init();
            if(
                isset($this->proxy['username']) && $this->proxy['username'] && 
                isset($this->proxy['password']) && $this->proxy['password']
            )curl_setopt ($ch, CURLOPT_PROXYUSERPWD, 
                $this->proxy['username'].":".$this->proxy['password']);
            
            if(isset($this->proxy['host']) && $this->proxy['host'] && 
                isset($this->proxy['port']) && $this->proxy['port']){
                curl_setopt ($ch, CURLOPT_PROXY, $this->proxy['host'].":".$this->proxy['port']);
                curl_setopt ($ch, CURLOPT_HTTPPROXYTUNNEL, $this->proxy['host'].":".$this->proxy['port']);
            }

            /*
             * Опция для правильной работы с прокси. Нашли два решения
             */
            curl_setopt($ch, CURLOPT_HTTPHEADER,array("Expect:"));
            // curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);

            
            curl_setopt ($ch, CURLOPT_URL, $this->url);
            curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);

            if($this->only_headers){
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'HEAD');
                curl_setopt($ch, CURLOPT_NOBODY, true );
            }
            elseif($this->post_request || $this->post_params){
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($ch, CURLOPT_NOBODY, false );
            }
            else{
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
                curl_setopt($ch, CURLOPT_NOBODY, false );
            }

            if($this->post_request){
                curl_setopt ($ch, CURLOPT_POST, 1);
                curl_setopt ($ch, CURLOPT_POSTFIELDS, $this->post_request);
            }
            elseif($this->post_params){
                curl_setopt ($ch, CURLOPT_POST, 1);
                curl_setopt ($ch, CURLOPT_POSTFIELDS, $this->post_params);
            }

            if($this->cookiefile){
                curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookiefile);
                curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookiefile);
            }

            if($this->cookie){
                curl_setopt($ch, CURLOPT_COOKIE, $this->cookie);
            }

            if($this->http_auth){
                curl_setopt ($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
                curl_setopt ($ch, CURLOPT_USERPWD, $this->http_auth);             
            }
            
            if($this->headers){
                curl_setopt ($ch, CURLOPT_HTTPHEADER, $this->headers);
            }

            if($this->outfile){
                $fd = fopen($this->outfile, "w");
                curl_setopt ($ch, CURLOPT_FILE, $fd);
            }

            if($this->out_headers_file){
                $hd = fopen($this->out_headers_file, "w");
                curl_setopt ($ch, CURLOPT_WRITEHEADER, $hd);
            }

            if($this->in_headers_file){
                curl_setopt ($ch, CURLINFO_HEADER_OUT, true);
            }
                
            curl_setopt ($ch, CURLOPT_HEADER, 0);

            curl_setopt ($ch, CURLOPT_REFERER, $this->referer);
            curl_setopt ($ch, CURLOPT_USERAGENT, $this->useragent);
            curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, $this->ssl_verify);
            curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, $this->ssl_verify);
            curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, $this->follow_location);
            curl_setopt ($ch, CURLOPT_TIMEOUT, $this->timeout);
            
            $result = curl_exec ($ch);
            if($this->in_headers_file){
                file_put_contents($this->in_headers_file, curl_getinfo($ch, CURLINFO_HEADER_OUT));
            }
            $this->returned_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
    
            $this->content = $result;
            $this->returned_headers = array();
            

            if($this->outfile && isset($fd) && $fd)fclose($fd);
            if($this->out_headers_file && isset($hd) && $hd)fclose($hd);

            if($this->returned_code)return 1;

            return 0;
        }
        
        /**
            @brief Отправка заголовка content-type
            
        */
        function content_type(
            $mime_type = 'text/html',   //!< Mime-тип (по умолчанию text/html)
            $charset = 'utf-8'          //!< Кодировка (по умолчанию - utf-8)
        ){
            header("Content-Type: $mime_type; charset=$charset");
        }

        /**
            @fn status
            @brief Формирование заголовка со статусом
            
         */
        function status(
            $code = '200',          //!< Код возврата
            $url = '/',             //!< Куда редиректим (для 301 и 302 кодов)
            $message_type = '',     //!< Тип сообщения (error/message)
            $message = ''           //!< Текст сообщения
        ){
            $uri = '';
            if($message_type && $message_type=='error'){
                $uri = $_SERVER['http']->add_to_get("error_status", $message);
            }
            elseif($message_type && $message_type=='message'){
                $uri = $_SERVER['http']->add_to_get("ok_status", $message);
            }
                
            
            switch($code){
                case '200':
                    header("HTTP/1.0 200 OK");
                break;
                case '301':
                    header("HTTP/1.0 301 Moved Permanently");
                    header("Location: $url$uri");
                    die;
                break;
                case '302':
                    header("HTTP/1.0 302 Moved Temporarily");
                    header("Location: $url$uri");
                    die;
                break;
                case '404':
                    header("HTTP/1.0 404 Not Found");
                break;
                case '403':
                    header("HTTP/1.1 403 Access denied");
                break;
            }
        }
        
        /*
         * Добавление в GET-строку еще параметра
         * Возвращает результирующую GET-строку, начинающуюся с ?
         */
        function add_to_get($key, $value){
            if(!count($_GET))return "?".urlencode($key)."=".urlencode($value);
            $result = "?";
            foreach($_GET as $k=>$v)if($k!=$key)$result .= "&".urlencode($key)."=".urlencode($value);
            return $result."&".urlencode($key)."=".urlencode($value);
        }
        
        /**
         * Обёртка массива $_GET
         */
        function get($key, $default_value = ''){
            return htmlspecialchars(
                $_SERVER['http']->arrays($_GET, $key, $default_value),
                ENT_NOQUOTES
            );
        }

        /**
         * Обёртка массива $_POST
         */
        function post($key, $default_value = ''){
            return htmlspecialchars(
                $_SERVER['http']->arrays($_POST, $key, $default_value),
                ENT_NOQUOTES
            );
        }

        /**
         * Обёртка массива $_COOKIE
         */
        function cookie($key, $default_value = ''){
            return htmlspecialchars(
                $_SERVER['http']->arrays($_COOKIE, $key, $default_value),
                ENT_NOQUOTES
            );
        }

        /**
         * Обёртка массива $_SESSION
         */
        function session($key, $default_value = ''){
            return $_SERVER['http']->arrays($_SESSION, $key, $default_value);
        }
        
        function arrays($array, $key, $default_value = ''){
            if(!isset($array[$key]))return $default_value;
            return $array[$key];
        }
        
        
        /**
            @fn get_domain
            @brief получение из url имени домена

            @returns имя домена
        */
        function get_domain($url){
            if(!preg_match("/^\s*(https?:\/\/)?(.+?)\/.*\s*$/i", $url, $m))return '';
            $domain = preg_replace("/^www\.(.*)$/i", "$1", $m[2]);
            return $domain;
        }
        
        /**
            Вычленение базового url без указанных в списке параметров GET
            Бывает полезно при пагинации
        */
        function base_url($get_list = array()){
            list($base_url) = explode("?", $_SERVER['REQUEST_URI']);
            $base_url .= "?";
            foreach($_GET as $k=>$v){
                if(array_search($k,$get_list)!==false)continue;
                $base_url .= urlencode($k)."=".urlencode($v)."&";
            }
            return $base_url;
        }
        
        /**
            @brief Посылает заголовок имени загружаемого файла
        */
        function attach(
            $filename //!< Имя файла
        ){
            header("Content-Disposition:attachment; filename=\"$filename\"");
        }
        
        /**
            @brief Формирование http-заголовка Last-Modified из unix-timestamp
            
        */
        function last_modified(
            $timestamp = 0 //!< Время, в формате unix timestamp, если не указан - используется текущее серверное время
        ){
            if(!$timestamp)$timestamp = time();
            if(!$timestamp){
                $lastmod = gmdate("D, d M Y H:i:s", $timestamp)." GMT";
                header("Last-Modified: $lastmod");
            }
        }
        
        /**
            Функция возвращает базовый URL пути файла
            
            @returns из полного пути до файла в файловой системе отрезает DOCUMENT_ROOT
        */
        function get_base_path($dirname){
            return str_replace($_SERVER['DOCUMENT_ROOT'], "", $dirname);
        }
        
        /*
            Транслитерация кириллицы в кодировке UTF-8
            
            @returns транслитерация строки
        */
        function translit(
            $string //!< Стока в кодировке UTF-8
        ){
            
            $a1 = "A B C D E F G H I J K L M N O P Q R S T U V W X Y Z a b c d e f g h i j k l m n o p q r s t u v w x y z а б в г д е ё ж з и й к л м н о п р с т у ф х ц ч ш щ ъ ы ь э ю я 0 1 2 3 4 5 6 7 8 9 . , - _ :";
            $a2 = "A B C D E F G H I J K L M N O P Q R S T U V W X Y Z a b c d e f g h i j k l m n o p q r s t u v w x y z А Б В Г Д Е Ё Ж З И Й К Л М Н О П Р С Т У Ф Х Ц Ч Ш Щ Ъ Ы Ь Э Ю Я 0 1 2 3 4 5 6 7 8 9 . , - _ :";
            $b  = "a b c d e f g h i j k l m n o p q r s t u v w x y z a b c d e f g h i j k l m n o p q r s t u v w x y z a b v g d e yo zh z i j k l m n o p r s t u f h c ch sh shh  y  e yu ya 0 1 2 3 4 5 6 7 8 9 - - - - -";

            $a1 = explode(" ", $a1);
            $a2 = explode(" ", $a2);
            $b = explode(" ", $b);
            
            $alphabet = array();
            foreach($a1 as $k=>$v)$alphabet[$a1[$k]] = $b[$k];
            foreach($a2 as $k=>$v)$alphabet[$a2[$k]] = $b[$k];
            
            $string = str_replace(" ", "-", $string);
            $result = '';
            for($i=0,$c=mb_strlen($string, "utf-8");$i<$c;$i++){
                $s = mb_substr($string,$i,1,"utf-8");
                $result .= 
                    isset($alphabet[$s])
                    ?
                    $alphabet[$s]
                    :
                    "";
            }
            $result = preg_replace("#[\-]{2,}#i", "-", $result);
            $result = preg_replace("#[\-]$#i", "", $result);
                        
            return $result;
        }
        
        function from_punycode($punycode){
    	    require_once('idna_convert.class.php');
    	    $idn = new idna_convert(array('idn_version'=>2008));
    	    $domain = $idn->decode($punycode);
    	    unset($idn);
    	    return $domain;
        }
        
    }
    
?>
