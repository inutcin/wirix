<?php
    include_once(dirname(__FILE__)."/../../wirix.class.php");
    
    /**
        Генерация панели навигации

        $menu = $_SERVER['wirix']->lib_load('main_navbar');
        $menu->fetch();
    */
    $_SERVER['wirix']->lib_load("html_menu", false);
    class wirix_html_toolbar extends wirix_html_menu{
        
        
        function __construct(){
            parent::__construct();
        }
        
        function fetch($object=null){
            $html = '';
$html .= '
<div class="'.$this->type.'">
  <div class="btn-group">
';
    foreach($this->items as $item)
        $html .= '<a href="'.(isset($item['url'])?$item['url']:"").'" class="btn" title="'.(isset($item['hint'])?$item['hint']:"").'"><i class="'.(isset($item['class'])?$item['class']:"").'"></i>'.(isset($item['text'])?$item['text']:"").'</a>';

    $html .= '
  </div>
</div>
';
            return $html;
        }
        
    }
?>
