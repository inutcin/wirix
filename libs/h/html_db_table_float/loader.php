<?php
    /**
        одгрузка таблицы
    */
    include_once(dirname(__FILE__)."/../../wirix.class.php");
    
    $_W->lib_load('dates');
    
    $id = $_SERVER['http']->get('id');
    $class = $_SERVER['http']->get('class')?$_SERVER['http']->get('class'):'wirix_html_db_table';

    $obj = json_decode(base64_decode($_SESSION['wirix'][$class][$id]));
    
    $tables = is_object($obj->tables)?get_object_vars($obj->tables):$obj->tables;
    $tables_join = is_object($obj->join_fields)?get_object_vars($obj->join_fields):$obj->join_fields;
    $search = is_object($obj->search)?get_object_vars($obj->search):$obj->search;
    $conds = is_object($obj->cond)?get_object_vars($obj->cond):$obj->cond;
    $order = $obj->sort;
    $fields = is_object($obj->fields_array)?get_object_vars($obj->fields_array):$obj->fields_array;
    $groupby = $obj->group_by;
    $columns = is_object($obj->columns)?get_object_vars($obj->columns):$obj->columns;
    
    $_D->search(
        $tables,
        $tables_join,
        $search,
        $conds,
        $order,
        100000,
        0,
        $fields,
        $groupby
    );
    
    $rows = array();
    foreach($_D->rows as $k=>$row){
        $row_item = array();
        foreach($columns as $c=>$column){
            if($c==0)continue;
            $text = '';
            if(isset($column->text))eval('$text='.$column->text.';');
            $rel = '';
            if(isset($column->rel))eval('$rel='.$column->rel.';');
            $class = '';
            if(isset($column->class))eval('$class='.$column->class.';');
            $row_item[] = array(
                "text"  =>  htmlspecialchars($text),
                "rel"   =>  $rel,
                "class" =>  $class,
            );
        }
        $rows[] = $row_item;
    }


    echo json_encode(array(
        "total"=>count($rows),
        "rows_per_page"=>$obj->rows_per_page,
        "columns"=>$columns,
        "rows"=>$rows,
    ));
    
