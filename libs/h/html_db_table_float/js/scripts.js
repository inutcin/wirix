function wirix_html_db_table_float_show_rows(id,data){
    $("#"+id+"_body tr").remove();
    var count = 0;
    for(i=data.start;i<=data.end;i++){
        $("#"+id+"_body").append('<tr num="'+i+'"></tr>');
        for(j in data.rows[i]){
            $("#"+id+"_body tr").last().append('<td'+
                (data.rows[i][j].rel?' rel="'+data.rows[i][j].rel+'"':"")+
                (data.rows[i][j].class?' class="'+data.rows[i][j].class+'"':"")+
                '><input type="text" value="'+
                data.rows[i][j].text+'"></td>');
        }
        
        count++;
    }

    // Высота скролбара
    $("#"+id+" .scroll").height($("#"+id+" table").height()-2);

    // Высота кнопки скрола
    scroll_height=$("#"+id+" .scroll").first().height();
    if(data.rows_per_page>data.total)data.rows_per_page = data.total;
    scroll_height = scroll_height*(data.rows_per_page/data.total);
    if(scroll_height<20)scroll_height = 16;
    $("#"+id+" .scroll div.picker").css('height',scroll_height);
    
    // Расстояние сверху от кнопки скрола до верхушки лотка скрола
    var scroll_top = $("#"+id+" .scroll").first().height()-scroll_height-2;
    scroll_top = scroll_top*((data.start)/(data.total-data.rows_per_page));
    // Если общее число записей меньше страницы - перемещаем в начало и прячем
    if(parseInt(data.total)<=parseInt(data.rows_per_page)){
        scroll_top=0;
        $("#"+id+" .scroll").hide();
    }
    else{
        $("#"+id+" .scroll").show();
    }
    // Если до конца страница или меньше - перемещаем в конец
    if(parseInt(data.total)-parseInt(data.start)<=parseInt(data.rows_per_page))scroll_top=$("#"+id+" .scroll").first().height()-scroll_height-2;
    $("#"+id+" .scroll div.picker").css('top',scroll_top);
    
    // Событие "клик по строке"
    $('#'+id+' input').unbind("click");
    $('#'+id+' input').click(function(){
        wirix_html_db_table_float_set_focus(id,$(this),data);
    });
    
    $('#'+id+' input').unbind("dblclick");
    // Событие "двойной клик по строке"
        $('#'+id+' input').dblclick(function(){
        eval(data.on_active);
    });
    
    eval(data.on_show);
}


function wirix_html_db_table_float_set_focus(id,obj,data){
    
    obj.parent().parent().parent().find("tr").removeClass("tr-selected");
    obj.parent().parent().parent().find("td").removeClass("tr-selected");
    obj.parent().parent().addClass("tr-selected");
    obj.parent().parent().find("td").addClass("tr-selected");

    $('#'+id+' input').unbind("keydown");
    obj.keydown(function(event){
        if(event.which==40){
            wirix_html_db_table_float_row_poinder_move(id,data,+1);
            return false;
        }
        else if(event.which==38){
            wirix_html_db_table_float_row_poinder_move(id,data,-1);
            return false;
        }
        else if(event.which==34){
            wirix_html_db_table_float_row_poinder_move(id,data,data.rows_per_page);
            return false;
        }
        else if(event.which==33){
            wirix_html_db_table_float_row_poinder_move(id,data,-data.rows_per_page);
            return false;
        }
        else if(event.which==13){
            eval(data.on_active);
            return false;
        }
        else{
            return true;
        }
    });
    data.current = obj.parent().parent().attr("num");
    obj.focus();
    eval(data.on_select);
    eval('data_'+id+'=data;');
}

function wirix_html_db_table_float_scroll(id,data){
    var scroll_height=$("#"+id+" .scroll div.picker").first().height();
    // Расстояние сверху от кнопки скрола до верхушки лотка скрола
    scroll_height = $("#"+id+" .scroll").height()-scroll_height-2;
    var picker_top = $("#"+id+" .scroll div.picker").css("top");
    var picker_ratio = parseInt(picker_top)/parseInt(scroll_height);
    data.start = parseInt((data.total-data.rows_per_page)*picker_ratio);
    data.end = parseInt(data.start)+parseInt(data.rows_per_page)-1;
    wirix_html_db_table_float_show_rows(id,data);
    eval('data_'+id+'=data;');
}

/**
    Перемещение курсора по таблице на указанное число позиций
*/
function wirix_html_db_table_float_row_poinder_move(
    id,         //!< JS-id таблицы
    data,       //!< Массив даных
    direction   //!< НАправление пролистывания и число позиций
){
    
    // Если текущая позиция выбрана - перемещаем её на указанное число
    if(data.current!=-1)
        data.current = parseInt(data.current)+parseInt(direction);
    
    // Если позиция после установки вышла за нижнюю границу таблицы - вернуть в 0
    if(data.current<=0){
        data.current = 0;
    }
    // Если позиция после установки вышла за верхнюю границу таблицы - вернуть в последнюю строку
    else if(data.current>=data.total-1){
        data.current = data.total-1;
    }
    
    // Если позиция после установки вышла за границу окна просмотра - сместить окно на величину смещения
    if(data.current<data.start || data.current>data.end){
        //Если окно просмотра после смещения выйдет за границы таблицы - не смещать
            data.end=parseInt(data.end)+parseInt(direction);
            data.start=parseInt(data.start)+parseInt(direction);
    }
    
    if(data.current>=data.total-1){
        data.end=data.total-1;
        data.start=data.total-data.rows_per_page;
    }

    if(data.current<=0){
        data.end=data.rows_per_page-1;
        data.start=0;
    }

    
    wirix_html_db_table_float_show_rows(id,data);
    wirix_html_db_table_float_set_focus(
        id,
        $('#'+id+' tr[num='+data.current+']').find('input').first(),
        data
    );
}

function htmlspecialchars_decode(string, quote_style) {
  //       discuss at: http://phpjs.org/functions/htmlspecialchars_decode/
  //      original by: Mirek Slugen
  //      improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  //      bugfixed by: Mateusz "loonquawl" Zalega
  //      bugfixed by: Onno Marsman
  //      bugfixed by: Brett Zamir (http://brett-zamir.me)
  //      bugfixed by: Brett Zamir (http://brett-zamir.me)
  //         input by: ReverseSyntax
  //         input by: Slawomir Kaniecki
  //         input by: Scott Cariss
  //         input by: Francois
  //         input by: Ratheous
  //         input by: Mailfaker (http://www.weedem.fr/)
  //       revised by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // reimplemented by: Brett Zamir (http://brett-zamir.me)
  //        example 1: htmlspecialchars_decode("<p>this -&gt; &quot;</p>", 'ENT_NOQUOTES');
  //        returns 1: '<p>this -> &quot;</p>'
  //        example 2: htmlspecialchars_decode("&amp;quot;");
  //        returns 2: '&quot;'

  var optTemp = 0,
    i = 0,
    noquotes = false;
  if (typeof quote_style === 'undefined') {
    quote_style = 2;
  }
  string = string.toString()
    .replace(/&lt;/g, '<')
    .replace(/&gt;/g, '>');
  var OPTS = {
    'ENT_NOQUOTES': 0,
    'ENT_HTML_QUOTE_SINGLE': 1,
    'ENT_HTML_QUOTE_DOUBLE': 2,
    'ENT_COMPAT': 2,
    'ENT_QUOTES': 3,
    'ENT_IGNORE': 4
  };
  if (quote_style === 0) {
    noquotes = true;
  }
  if (typeof quote_style !== 'number') { // Allow for a single string or an array of string flags
    quote_style = [].concat(quote_style);
    for (i = 0; i < quote_style.length; i++) {
      // Resolve string input to bitwise e.g. 'PATHINFO_EXTENSION' becomes 4
      if (OPTS[quote_style[i]] === 0) {
        noquotes = true;
      } else if (OPTS[quote_style[i]]) {
        optTemp = optTemp | OPTS[quote_style[i]];
      }
    }
    quote_style = optTemp;
  }
  if (quote_style & OPTS.ENT_HTML_QUOTE_SINGLE) {
    string = string.replace(/&#0*39;/g, "'"); // PHP doesn't currently escape if more than one 0, but it should
    // string = string.replace(/&apos;|&#x0*27;/g, "'"); // This would also be useful here, but not a part of PHP
  }
  if (!noquotes) {
    string = string.replace(/&quot;/g, '"');
  }
  // Put this in last place to avoid escape being double-decoded
  string = string.replace(/&amp;/g, '&');

  return string;
}
