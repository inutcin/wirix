<?php
    include_once(dirname(__FILE__)."/../../wirix.class.php");
    $_SERVER['wirix']->lib_load('html_db_table', false);
    
/**
\class wirix_html_db_table
\brief формирование html-таблицы, связанной с SQL-запросом. C фильтрацией, сортировкой и навигацией
    
    
Пример использования
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.php}
    $table = $_SERVER['wirix']->lib_load('html_db_table');
    $table->title = 'Прайс-лист';

    $column = array();
    $column['title'] = 'ID';
    $column['text'] =   '"$row[id]"';
    $column['width'] =   '10px';
    $table->add_column($column);
    unset($column);

    $column = array();
    $column['title'] = 'Производитель';
    $column['text'] =   '"$row[producer]"';
    $column['width'] =   '10px';
    $table->add_column($column);
    unset($column);

    $column = array();
    $column['title'] = 'Наименование';
    $column['text'] =   '"$row[articul]"';
    $column['width'] =   '10px';
    $table->add_column($column);
    unset($column);

    $column = array();
    $column['title'] = 'Цена';
    $column['align'] = 'right';
    $column['text'] =   '"$row[price]"';
    $column['width'] =   '10px';
    $table->add_column($column);
    unset($column);


    $table->id = 'price';
    
    $table->sorting = array(
        "4"=>"price"
    )
    
    $table->rows_per_page = 15;
    $table->tables  = array("a"=>$table_name);
    $query = $_SERVER['http']->get($table->id."_query");
    $query = mysqli_real_escape_string($_SERVER['db']->link, $query);
    $table->cond   = "
        `a`.`producer` LIKE '%".$query."%' OR
        `a`.`articul` LIKE '%".$query."%' OR
        `a`.`price` LIKE '%".$query."%'
    ";
    $table->search();
    
    
    echo $table->fetch();
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/
    class wirix_html_db_table_float extends wirix_html_db_table{
        
        var $start_row = 0;
        var $end_row = 0;
        var $current_row = 0;
        var $on_active = ""; //!< js-код, который исполняется при активации строки (Enter или DblClick)
        var $on_select = ""; //!< js-код, который исполняется при выборе строки
        var $on_show = ""; //!< js-код, который исполняется после показа таблицы

        function __construct(){
            if($this->called_class)
                $this->called_class = __CLASS__;
            parent::__construct(__CLASS__);
        }
        
        function __destruct(){
        }
        
        function fetch(){
            $html = '';
            if($this->show_search_form)$html .= $this->fetch_search_form();
            $html .= $this->fetch_code();
            $html .= $this->fetch_js();
            return $html;
        }
        
        private function fetch_code(){
            $html = '<div class="'.__CLASS__.'" id="'.$this->id.'">';
            $html .= '<div class="scroll"><div class="picker"></div></div>';
            $html .= '<table><tbody class="'.__CLASS__.'_head">';
            $html .= '<tr>';
            foreach($this->columns as $cnum=>$column){
                if($cnum==0)continue;
                $html .= '<th';
                if(isset($column["width"]) && $column["width"])$html .= ' width="'.$column["width"].'"';
                $html .= '>';
                if(isset($column["title"]))$html .= $column["title"];
                $html .= '</th>';
            }
            $html .= '</tr>';
            if(count($this->filter_select)){
                $html .= '<tr>';
                for($i=1;$i<count($this->columns);$i++){
                    if(!isset($this->filter_select[$i])){
                        $html .= '<th';
                        if(isset($this->columns[$i]["width"]) && $this->columns[$i]["width"])$html .= ' width="'.$this->columns[$i]["width"].'"';
                        $html .= '>';
                        $html .= ' ';
                        $html .= '</th>';
                        continue;
                    }
                    $html .= '<th';
                    if(isset($this->columns[$i]["width"]) && $this->columns[$i]["width"])$html .= ' width="'.$this->columns[$i]["width"].'"';
                    $html .= '>';
                    $html .= '<select rel="'.$i.'" ';
                    if(isset($this->columns[$i]["width"]) && $this->columns[$i]["width"])$html .= ' style="width:'.$this->columns[$i]["width"].';"';
                    $html .= '>';
                    $html .= '<option rel="0">...</option>';
                    foreach($this->filter_select[$i] as $num=>$option){
                        $html .= '<option rel="'.($num+1).'">'.htmlspecialchars($option["value"]).'</option>';
                    }
                    $html .= '</select>';
                    $html .= '</th>';
                }
                $html .= '</tr>';
            }
            
            $html .= '<tbody id="'.$this->id.'_body">';
            $html .= '<tr><td colspan="'.count($this->columns).'" class="progress"></td></tr>';
            $html .= '</tbody>';
            $html .= '</table>';
            $html .= '</div>';
//            echo "<pre>";
//            print_r($this);
//            echo "</pre>";
            return $html;
        }

        private function fetch_search_form(){
            global $_H;
            $html = '<form class="'.__CLASS__.'_search_form">
                <input class="query" name="'.$this->id.'_query" 
                type="text" value="'.$_H->get($this->id.'_query').'" placeholder="Поиск">
            </form>';
            return $html;
        }

        private function fetch_js(){
            $this->commit();
            time_nanosleep(0, 100000000);
            $html = '<script>
    // создаём структуру для хранения строк
    var data_'.$this->id.' = {
        "start":'.$this->start_row.',
        "end":'.($this->start_row+$this->rows_per_page-1).',
        "current":-1
    };
$(document).ready(function(){
';
    $html .= '
    $("#'.$this->id.' select").change(function(){
        var rel = $(this).find(\'option:selected\').attr("rel");
        var c=$("#'.$this->id.' select").length;
        if(rel==0){
            data_'.$this->id.'.rows = data_'.$this->id.'.restore;
            data_'.$this->id.'.rows_per_page = data_'.$this->id.'.restore_per_page;
        }
        else{
            data_'.$this->id.'.rows = new Array();
            count = 0;
            for(i in data_'.$this->id.'.restore){
                flag = false;
                
                for(j=0;j<c;j++){
                    if(
                        $($("#'.$this->id.' select")[j]).find("option:selected").attr("rel")!=0
                        &&
                        htmlspecialchars_decode(
                            data_'.$this->id.'.restore[i][
                                $($("#'.$this->id.' select")[j]).attr("rel")-1
                            ].text
                        )
                        !=
                        $($("#'.$this->id.' select")[j]).val()
                    ){
                        flag=true;
                        break;
                    }
                }
                if(!flag){
                    data_'.$this->id.'.rows[count] = data_'.$this->id.'.restore[i];
                    count++;
                }
            }
            data_'.$this->id.'.start = 0;
            data_'.$this->id.'.end = data_'.$this->id.'.rows_per_page-1;
            data_'.$this->id.'.current = -1;
            data_'.$this->id.'.total = data_'.$this->id.'.rows.length;
        }
        wirix_html_db_table_float_show_rows("'.$this->id.'",data_'.$this->id.');
    });

        $.ajax({
            url: \'/wirix/libs/h/html_db_table_float/loader.php?class='.__CLASS__.'&id='.$this->id.'\',
            data: {},
            type: \'GET\',
            dataType: \'JSON\',
            success: function(data){
                data_'.$this->id.'["rows"] = data.rows;
                data_'.$this->id.'["restore"] = data.rows;
                data_'.$this->id.'["restore_per_page"] = data.rows_per_page;
                data_'.$this->id.'["total"] = data.total;
                data_'.$this->id.'["rows_per_page"] = data.rows_per_page;
                data_'.$this->id.'["on_active"] = "'.$this->on_active.'";
                data_'.$this->id.'["on_select"] = "'.$this->on_select.'";
                data_'.$this->id.'["on_show"] = "'.$this->on_show.'";
                '.__CLASS__.'_show_rows(\''.$this->id.'\',data_'.$this->id.');
                $("#'.$this->id.' .scroll .picker").draggable({
                    axis: "y",
                    containment: "parent",
                    start: function(){
                    },
                    stop: function(){
                    }
                });
                
                $("#'.$this->id.' .scroll").droppable({
                    drop: function(){
                        wirix_html_db_table_float_scroll("'.$this->id.'",data_'.$this->id.');
                    },
                    over: function(){
                        
                    }
                });
            }
        });
    
});

</script>';
            return $html;
        }
        
    }
?>
