<?php
    //перенести в submit_ajax
    if($_H->post('send') && is_array($_POST["regexp_fields"]) && is_array($_POST["title_fields"])){
        $inputs = $_POST["regexp_fields"];
        foreach($inputs as $input_name=>$input_value){
            $regexp = base64_decode($input_value);
            if(!preg_match($regexp, $_SERVER['http']->post($input_name))){
                $answer['error'] = 'Ошибка в поле '.(isset($_POST["title_fields"][$input_name])?"&laquo;".$_POST["title_fields"][$input_name]."&raquo;":"-");
                break;
            }
        }
    }
    elseif(!is_array($_POST["regexp_fields"]) || !is_array($_POST["title_fields"])){
        echo "regexps is absent";
        die;
    }
?>
