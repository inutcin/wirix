<?php
    /**
        Формирование js-скрипта для контроля полей формы
    */
//    include_once("../../../../wirix/libs/wirix.class.php");
//    $_SERVER['http']->content_type("text/javascript");
 
/* 
    echo "<pre>";
    print_r($_SESSION);
    echo "</pre>";
    die;
*/
    
    if(!isset($_SESSION['wirix'][$_SERVER['http']->get('class')][$_SERVER['http']->get('id')]))die('No session data');
    
    // Получаем данные формы из сессии
    $html_form = get_object_vars(json_decode(
	base64_decode($_SESSION['wirix'][$_SERVER['http']->get('class')][$_SERVER['http']->get('id')])
    ));
    foreach($html_form['inputs'] as $input_data){
        $input_data = get_object_vars($input_data);
        if($input_data['type']=='hidden')continue;
        $input = $_SERVER['wirix']->lib_load('html_input');
        foreach($input_data as $k=>$v)
            $input->$k=$v;
        echo $input->fetch_js();
        unset($input);
    }
    // Функция проверки полей формы
    
    echo "\n\nfunction form_".$html_form['id']."_chck(){
";
    echo "\tvar re = new Array(".count($html_form['inputs']).");\n";
    echo "\tvar defaults = new Array(".count($html_form['inputs']).");\n";
    echo "\tvar inptypes = new Array(".count($html_form['inputs']).");\n";
    echo "\tvar ids = new Array(".count($html_form['inputs']).");\n\n";
    foreach($html_form['inputs'] as $k=>$input_data){
        $input_data = get_object_vars($input_data);
        echo "\tids[$k] = '".$input_data['id']."';\n";
        if($input_data['regexp']){
            echo "\tre[$k] = ".$input_data['regexp'].";\n";
        }else
        {
            echo "\tre[$k] = /^.*$/;\n";
        }
        echo "\tdefaults[$k] = '".$input_data['default']."';\n";
        echo "\tinptypes[$k] = '".$input_data['type']."';\n";
    }
    echo "\n\tvar flag = true;\n";
    echo "\tfor(i=0;i<".count($html_form['inputs']).";i++) {\n".
         "\t\tif (inptypes[i] != 'hidden' && (!re[i].test($('#'+ids[i]).val())\n".
         "\t\t\t|| (defaults[i] != '' && $('#'+ids[i]).val() == defaults[i]))) {\n".
         "\t\t\tflag = flag & false;\n".
         "\t\t\tif($('#cg_'+ids[i]).attr('class')!='control-group error')$('#cg_'+ids[i]).attr('class', 'control-group error');\n".
         "\t\t} else {\n".
         "\t\t\tif($('#cg_'+ids[i]).attr('class')!='control-group success')$('#cg_'+ids[i]).attr('class', 'control-group success');\n".
         "\t\t}\n".
         "\t}\n";
    echo "$('#html_form_".$_SERVER['http']->get('id')."_submit').attr('class',flag?($('#html_form_".$_SERVER['http']->get('id')."_submit').attr('class')=='submit submit_loaded'?'submit submit_loaded':'submit btn-primary'):'submit btn');\n";    
    echo "document.getElementById('html_form_".$_SERVER['http']->get('id')."_submit').disabled = !flag;";    
    echo "
}
";

    echo "\nsetInterval(function(){form_".$html_form['id']."_chck();},200);\n";
    
    if($html_form['use_ajax']){
    
?>
$('#form_<?php echo $_SERVER['http']->get('id')?>').submit(function(data){
    $('#html_form_<?php echo $_SERVER['http']->get('id');?>_submit').attr('class','submit btn-primary submit_loaded');
    $.post(
        '<?php echo $html_form['submit_url'];?>',
        {
            "send": 1,
            "form_id":$('#<?php echo $_SERVER['http']->get('id');?>_form_id').val()
<?php foreach($html_form['inputs'] as $input_data){
$input_data = get_object_vars($input_data);?>
            ,"<?php echo $input_data['name']?>":$('#<?php echo $input_data['id']?>').val()
<?php }?>,
            "shown_fields": {<?php
$output = "";
foreach ($html_form['inputs'] as $v) {
    $v = get_object_vars($v);
    if ($v['type'] == 'hidden') {
        $shown = 'false';
    } else {
        $shown = 'true';
    }
    $output .= "'".$v['name']."': '".$shown."', ";
}
echo rtrim($output, ', ');
            ?>},

            "regexp_fields": {<?php
$output = "";
foreach ($html_form['inputs'] as $v) {
    $v = get_object_vars($v);
    $output .= "'".$v['name']."': '".base64_encode($v['regexp'])."', ";
}
echo rtrim($output, ', ');
            ?>},

            "title_fields": {<?php
$output = "";
foreach ($html_form['inputs'] as $v) {
    $v = get_object_vars($v);
    $output .= "'".$v['name']."': '".$v['title']."', ";
}
echo rtrim($output, ', ');
            ?>}
            
        },
        function(data){
            $('#html_form_<?php echo $_SERVER['http']->get('id');?>_submit').attr('class','submit btn-primary');
            $('#html_form_<?php echo $_SERVER['http']->get('id');?>_submit').val('<?php echo $html_form['send_button_text'];?>');
            try{
                var answer = JSON.parse(data);
                if(answer.error){
                    $('#<?php echo $html_form['id']?> .alert-bar').css('display','table-row');
                    $('#<?php echo $html_form['id']?>_alert_error').html(answer.error);
                    $('#<?php echo $html_form['id']?>_alert_error').fadeIn();
                }
                if(answer.success){
                    $('#<?php echo $html_form['id']?> .success-bar').css('display','table-row');
                    $('#<?php echo $html_form['id']?>_alert_success').html(answer.success);
                    $('#<?php echo $html_form['id']?>_alert_success').fadeIn();
                }
                if(!answer.error && answer.redirect && answer.redirect!='#'){
                    if(document.location.href == 'http://<?php echo $_SERVER["HTTP_HOST"];?>'+answer.redirect){
                        document.location.reload();
                    }
                    else{
                        document.location.href = answer.redirect;
                    }
                }
                setTimeout(function(){
                    $('#<?php echo $html_form['id']?>_alert_success').fadeOut();
                }, 2000);
            }
            catch(e){
                $('#<?php echo $html_form['id']?> .alert-bar').css('display','table-row');
                $('#<?php echo $html_form['id']?>_alert_error').html(data);
                $('#<?php echo $html_form['id']?>_alert_error').fadeIn();
            }
        }
    );
    return false;
});
<?php }else{ ?>
$('#form_<?php echo $_SERVER['http']->get('id')?>').submit(function(data){
    $('#html_form_<?php echo $_SERVER['http']->get('id');?>_submit').attr('class','submit submit_loaded');
});
<?php }?>

<?php foreach($html_form['groups']->start as $k=>$item){?>
    <?php if($item->expanded){?>
        $('#group_title_<?php echo $html_form['id'];?>_<?php echo $k?> i').attr('class','icon-minus');
        $('#group_title_<?php echo $html_form['id'];?>_<?php echo $k?>').click(function(){
            group_hide('<?php echo $html_form['id'];?>',<?php echo $k?>);
        });
        $('#grouped_<?php echo $html_form['id'];?>_<?php echo $k?>').show();
    <?php }else{?>
        $('#group_title_<?php echo $html_form['id'];?>_<?php echo $k?> i').attr('class','icon-plus');
        $('#group_title_<?php echo $html_form['id'];?>_<?php echo $k?>').click(function(){
            group_show('<?php echo $html_form['id'];?>',<?php echo $k?>);
        });
        $('#grouped_<?php echo $html_form['id'];?>_<?php echo $k?>').hide();
    <?php }?>
<?php }?>

function group_show(id,k){
    $('#group_title_'+id+'_'+k+' i').attr('title','Свернуть');
    $('#grouped_'+id+'_'+k+'').show();
    $('#group_title_'+id+'_'+k+'').click(function(){
        group_hide(id,k);
    });
    $('#group_title_'+id+'_'+k+' i').attr('class','icon-minus');
}


function group_hide(id,k){
    $('#group_title_'+id+'_'+k+' i').attr('title','Развернуть');
    $('#grouped_'+id+'_'+k+'').hide();
    $('#group_title_'+id+'_'+k+'').click(function(){
        group_show(id,k);
    });
    $('#group_title_'+id+'_'+k+' i').attr('class','icon-plus');
}

    
