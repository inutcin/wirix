<?php
    include_once(dirname(__FILE__)."/../../wirix.class.php");
    
/**
\class wirix_html_form
\brief Генерация html-формы


Пример использования
=============================================


\code{.py}
    //Создаём объект класса wirix_html_form
    $_SERVER['auth']        = $_SERVER['wirix']->lib_load("html_form"); 
    // Даём ему уникальной ID для работы JavaScript-валидатора
    $_SERVER['auth']->id    = 'authform';   
    // Даём заголовок формы
    $_SERVER['auth']->title = 'Авторизация';

    // Создаём объект класса wirix_html_input (поле формы)
    $input  = $_SERVER['wirix']->lib_load("html_input");    
    // Назначаем имя поля 
    $input->name = 'username';                              
    // Назначаем выводимое название поля
    $input->title = 'Имя пользователя';                     
    // Задаём регулярное выражение для валидации значения поля
    $input->regexp = "/^[\d\w]{4,32}$/";                    
    // Задаём значение по умолчанию
    $input->default = 'Введите имя пользователя';           
    // Задаём всплывающий комментарий для поля
    $input->comment = 'Имя пользователя должно содержать от 4 до 32 символов латиницы, цифры, тире, подчеркивание и точку.';
    // Добавляем поле формы в форму
    $_SERVER['auth']->add_input($input);    
    // Чистим память
    unset($input);      

    // Выводим форму
    echo $_SERVER['auth']->fetch(); 
\endcode



Обработка запроса с формы
========================================

Запрос с формы полылается либо асинхронным запросом(ajax), либо обычным 
запросом в созданный невидимый iframe. Это определяется значением атрибута
@code$this->use_ajax.@endcode


Ajax-обработка (не подходит для приправки полей типа file)
----------------------------------------

Обработчик должен вернуть JSON результат вида

@code
{
    "redirect":"url, куда страницу редиректнет после успешного принятия формы, не срабатывает, если есть ошибка",
    "error":"текст ошибки, если таковая имеется",
    "success":"текст"
}
@endcode

Заготовка для кода

@code
<?php
include_once($_SERVER["DOCUMENT_ROOT"]."/wirix/libs/wirix.class.php");
if(!$_SERVER['auth']->is_auth()){$_SERVER['http']->status(403);die;}
// Инициализируем массив ответа
$answer = array();
$answer['redirect'] = '';
$answer['error'] = '';
$answer['success'] = '';
// Проверка полей формы на соответствие регулярному выражению
include($_SERVER["DOCUMENT_ROOT"]."/wirix/libs/h/html_form/check.ajax.php");

echo json_encode($answer);
@endcode


iframe-обработка
----------------------------------------


@code
<?php include_once("../../wirix.class.php");?>
<script src="/wirix/libs/h/html/js/jquery.min.js"></script>
<script>
parent.$('#html_form_<?php echo $_SERVER['http']->post('form_id');?>_submit').attr('class','submit btn-primary');

parent.$('#<?php echo $_SERVER['http']->post('form_id');?> .alert-bar').css('display', 'table-row');
parent.$('#<?php echo $_SERVER['http']->post('form_id');?> .success-bar').css('display', 'table-row');

parent.$('#<?php echo $_SERVER['http']->post('form_id');?>_alert_error').html('Неудачно');
parent.$('#<?php echo $_SERVER['http']->post('form_id');?>_alert_error').fadeIn();
parent.$('#<?php echo $_SERVER['http']->post('form_id');?>_alert_success').html('Успешно');
parent.$('#<?php echo $_SERVER['http']->post('form_id');?>_alert_success').fadeIn();

setTimeout(
    function(){
        parent.$('#<?php echo $_SERVER['http']->post('form_id');?>_alert_error').fadeOut();
    },
    3000
);

</script>@endcode


*/
    class wirix_html_form extends wirix{
        
        var $inputs = array();  //!< Массив полей
        var $groups = array("start"=>array(),"end"=>array(),"expanded"=>1);  //!< Группы полей
        
        var $title = 'Форма';       //!< Имя - заголовок формы
        var $table_css_class = '';  //!< CSS-класс таблицы
        var $id = '';               //!< ID формы
        var $send_button_text =  '';//!< Текст кнопки отправки формы
        var $back_button_text =  '';   //!< Текст кнопки возврата
        var $expanded = true;   //!< Кномка сокрытия формы
        var $icons = array();   //!< Иконки(ссылки) внизу формы
        var $links = array();   //!< Ссылки внизу формы
        
        var $submit_url = '/wirix/libs/h/html_form/submit_ajax.php';       //!< Url, на которой отправляется запрос с формы
        
        var $use_ajax = true;       //!< Отправка запроса через ajax ()
        var $use_back_button = true;//!< Выводить кнопку "назад/отмена"
        var $method = "POST";       //!< Метод отправки запроса GET/POST
        
        var $db_action = '';        //!< Действие, производимое со связанной таблицей(insert, update)
        var $db_table = '';         //!< Таблица, с которой взаимодействует форма
        var $db_id = 0;             //!< ID элемента таблицы БД, с которым связана форма
        
        
        function __construct(){
            parent::__construct($this->called_class?$this->called_class:__CLASS__);

            $this->title            =  _t('Form');
            $this->send_button_text =  _t('Submit');
            $this->back_button_text =  _t('Cancel');

        }
        
        /**
            Добавление ссылки к форме
        */
        function add_link(
            $link_obj       //!< Объект класса @ca wirix_html_href
        ){
            $this->links[] = $link_obj;
            return $this->links[count($this->links)-1];
        }

        
        /**
            Объединение полей в группу
        */
        function add_group(
            $start_input_num,   //!< Номер начального поля группы
            $end_input_num,     //!< Номер конечного поля группы
            $title,             //!< Заголовок группы
            $expanded = true    //!< Развёрнутая группа
        ){
            if(!isset($this->groups['start']))$this->groups['start'] = array();
            if(!isset($this->groups['end']))$this->groups['end'] = array();
            if(!isset($this->groups['start'][$start_input_num]))$this->groups['start'][$start_input_num] = array();
            if(!isset($this->groups['end'][$end_input_num]))$this->groups['end'][$end_input_num] = array();
            $this->groups['start'][$start_input_num] = array( 
                "start" =>  $start_input_num,
                "end"   =>  $end_input_num,
                "title" =>  $title,
                "expanded" =>  $expanded
            );
        }


        /**
            Добавление иконки к форме
        */
        function add_icon(
            $link_obj   //!< Объект класса @ca wirix_html_href
        ){
            $this->icons[] = $link_obj;
            return $this->icons[count($this->icons)-1];
        }
        
        
        /**
            Генерация формы
        */
        function fetch($object = null){
            $this->commit();
            ob_start();
            // Запоминаем форму в сессии
            /*
            unset($_SESSION['wirix']['html_form'][$id = $this->id]);
            if(!isset($_SESSION['wirix']['html_form']))$_SESSION['wirix']['html_form'] = array();
            $_SESSION['wirix']['html_form'][$id = $this->id] = json_encode($this);
            */
            ?>
            <div class="html_form" id="<?php echo $this->id?>">
            <iframe name="iframe_form_<?php echo $this->id?>" style="display: none;"></iframe>
            <form method="<?php echo $this->method;?>" <?php if(!$this->use_ajax){?>target="iframe_form_<?php echo $this->id?>"<?php }?> id="form_<?php echo 
            $this->id?>" enctype="multipart/form-data" <?php if(!$this->use_ajax){?>action="<?php echo $this->submit_url;?>"<?php }?>>
                <input type="hidden" name="form_id" id="<?php echo $this->id?>_form_id" value="<?php echo $this->id?>" />
                <?php if(trim($this->title)){?>
                    <div class="html_form-title alert alert-info">
                        <?php echo $this->title;?>
                    </div>
                <?php }?>
                <div class="html_form-body">
                    <table class="<?php echo $this->table_css_class;?>">
                        <tbody>
                        <?php $grouped=-1;foreach($this->inputs as $k=>$input){
                            if($input->type=='hidden'){
                                echo '<input type="hidden" id="'.$input->id.'" name="'.$input->name.'" value="'.($input->value!=''?$input->value:$input->default).'"/>';
                                continue;
                            }
                        ?>
                        
                        <?php if(isset($this->groups['start'][$k])){$grouped = $k;?>
                        <tr><td colspan="2" class="group-suffix">&#160;</td></tr>
                        <tr>
                            <td colspan="2" class="group-top"><div class="title" id="group_title_<?php echo 
                            $this->id;?>_<?php echo $grouped;?>"><i></i><?php echo 
                            $this->groups['start'][$k]['title'];?></div></td>
                        </tr>
                        <tbody class="form_grouped" id="grouped_<?php echo $this->id;?>_<?php echo $grouped;?>">
                        <?php }?>
                        
                        <?php if($input->type=='textarea'){?>
                            <tr>
                                <th id="<?php echo $this->id?>_key_<?php echo $k;?>" class="key" <?php if($input->comment){
                                    ?>data-content="<?php echo $input->comment;?>"<?php }?>>
                                    <?php echo $input->title; ?> 
                                </th>
                                <td class="val <?php echo $input->type;?>">&#160;</td>
                            </tr>
                            <tr id="cg_<?php echo $input->id?>" class="control-group error">
                                <td colspan="2" class="val <?php echo $input->type;?>  <?php if($grouped>-1){
                                    ?>grouped-right<?php }?>"><?php echo $input->fetch();?></td>
                            </tr>
                        <?php }elseif($input->type=='radio'){?>
                            <tr>
                                <th id="<?php echo $this->id?>_key_<?php echo $k;?>" class="key" <?php if($input->comment){
                                    ?>data-content="<?php echo $input->comment;?>"<?php }?>>
                                    <?php echo $input->title; ?> 
                                </th>
                                <td class="val <?php echo $input->type;?> ">&#160;</td>
                            </tr>
                            <tr id="cg_<?php echo $input->id?>" class="control-group error">
                                <td colspan="2" class="val <?php echo $input->type;?>"><?php echo $input->fetch();?></td>
                            </tr>
                        <?php }else{?>
                            <tr id="cg_<?php echo $input->id?>" class="control-group error">
                                <th id="<?php echo $this->id?>_key_<?php echo $k;?>" class="key" <?php if($input->comment){
                                    ?>data-content="<?php echo $input->comment;?>"<?php }?>>
                                    <?php echo $input->title; ?> 
                                </th>
                                <td class="val <?php echo $input->type;?>"><?php echo $input->fetch();?></td>
                            </tr>
                        <?php }?>
                        <?php if(isset($this->groups['end'][$k])){$grouped = $k;?>
                        </tbody>
                        <tr><td></td></tr>
                        <tbody>
                        <?php }?>
                        
                        <?php }?>
                        <tr class="alert-bar">
                            <td colspan="2"><div id="<?php echo $this->id?>_alert_error" class="alert alert-error" style="display: none;"></div></td>
                        </tr>
                        <tr class="success-bar">
                            <td colspan="2"><div id="<?php echo $this->id?>_alert_success" class="alert alert-success" style="display: none;"></div></td>
                        </tr>
                        <?php if($this->send_button_text){?>
                        <tr class="submit-bar">
                            <td id="<?php echo $this->id?>_links" class="icons">
                                <?php if($this->icons){?>
                                    <?php foreach($this->icons as $link){?>
                                        <?php echo $link->fetch();?>
                                    <?php }?>
                                <?php }?>
                            </td>
                            <td> 
                                <input type="submit" class="submit btn-primary" value="<?php echo $this->send_button_text;?>" id="html_form_<?php echo $this->id;?>_submit"/>
                                <?php if($this->use_back_button){?>
                                    <input type="button" class="submit btn" value="<?php 
                                    echo $this->back_button_text;?>" id="html_form_<?php 
                                    echo $this->id;?>_cancel" style="margin-right: 5px;"/>
                                <?php }?>
                            </td>
                        </tr>
                        <?php }?>
                        <?php if($this->links){?>
                        <tr class="links-bar">
                            <td colspan="2" id="<?php echo $this->id?>_links" class="links">
                            <?php foreach($this->links as $link){?>
                            <?php echo $link->fetch();?>
                            <?php }?>
                            </td>
                        </tr>
                        <?php }?>
                    </table>
                </div>
            </form>
            </div>
            <script language="javascript" type="text/javascript">
            <?php foreach($this->inputs as $k=>$input){
                if($input->type=='hidden' || $input->comment=='')continue;
                ?>
                $('#<?php echo $this->id?>_key_<?php echo $k;?>').popover({title:''});
            <?php }?>
            </script>
            
            <script language="JavaScript" >
            <?php
                $_GET['class']=$this->called_class;
                $_GET['id']=$this->id;
                include("js.php");            
            ?>
            </script>
<?php
            $html = ob_get_contents();
            ob_end_clean();
            return $html;
        }
        
        /**
            Генерация input-элемента
        */
        function generate_input($input_obj=''){
            $html = '<input type="text" name="field" value=""/>';
            return $html;
        }
        
        /**
            Добавление поля к форме
        */
        function add_input(
            $input_obj  //!< Объект класса \ca wirix_html_input
        ){
            $this->inputs[] = $input_obj;
            $this->inputs[count($this->inputs)-1]->id = $this->id."_field".count($this->inputs);
            return true;
        }
        
        /**
            @brief Проверка POST-полей формы
        
            Проверка POST-полей формы. Данные для проверки берутся из массовов
            $POST["title_fields"] и $POST["regexp_fields"]
            
            @returns Возвращает пустой массив если ошибок не найдено или массив вида
            @code
            array("field_name1"=>"field_title1","field_name2"=>"field_title2",..) 
            @endcode
            с полями не соответствующими регекспам
        */
        function fields_error(){
            
            $regexp_fields = 
                isset($_POST["regexp_fields"]) && is_array($_POST["regexp_fields"])
                ?
                $_POST["regexp_fields"]
                :
                array()
                ;
                
            foreach($regexp_fields as $k=>$v)$regexp_fields[$k] = base64_decode($v);    
            
            $error_fields = array();
            foreach($regexp_fields as $k=>$v)
                if(
                    isset($_POST["title_fields"][$k]) && 
                    isset($_POST[$k]) && 
                    !preg_match($v,$_POST[$k])
                )$error_fields[$k] = $_POST["title_fields"][$k];
            
            return $error_fields;
        }
        
    }
?>
