<?php

include_once("../../wirix.class.php");
$answer = array();
$answer['redirect'] = '';
$answer['error'] = '';


$form = get_object_vars(json_decode(
    base64_decode(
    $_SESSION['wirix'][
        $_SERVER['http']->post('class')
    ][
        $_SERVER['http']->post('form_id')
    ]
    )
));
$fields = $_POST;

unset($fields['form_id']);
unset($fields['send']);

// Заменяем дату на ISO-формат
$date_obj = $_SERVER['wirix']->lib_load('dates');

foreach ($fields as $k => $v) {
    foreach ($form['inputs'] as $input) {
        if ($input->type == 'date') {
            $fields[$k] = $date_obj->translate($v, 3, 1);
            break;
        }
    }
}
unset($date_obj);

if (isset($form['db_action']) && $form['db_action'] == 'insert' && $form['db_table']) {
//    if ($_SERVER['db']->search_one(
//            $form['db_table'], //!< Таблица из которой получаем запись
//            array('name') //!< Фильтр поиска "поле"=>"значение"
//    )) {
//        
//    }
    if ($_SERVER['db']->insert($form['db_table'], $fields)) {
        $answer['redirect'] = $form['db_redirect'];
        $answer['success'] = "Запись добавлена";
    } else {
        $answer['error'] = $_SERVER['db']->error;
    }
} elseif (isset($form['db_action']) && $form['db_action'] == 'update' && $form['db_table'] && $form['db_id']) {

//    print_r($fields);
//    print_r($_POST);
//    print_r($form);
//
//    die;
    if ($_SERVER['db']->update($form['db_table'], $fields, array("id" => $form['db_id']))) {
        $answer['redirect'] = $form['db_redirect'];
        $answer['success'] = "Запись добавлена";
    } else {
        $answer['error'] = $_SERVER['db']->error;
    }
} else {
    $answer['error'] = 'Форма не была отправлена. Попробуйте позже';
}

echo json_encode($answer);
?>
