<?php include_once("../../wirix.class.php");?>
<script src="/wirix/libs/h/html/js/jquery.min.js"></script>
<script>
parent.$('#html_form_<?php echo $_SERVER['http']->post('form_id');?>_submit').attr('class','submit btn-primary');

parent.$('#<?php echo $_SERVER['http']->post('form_id');?> .alert-bar').css('display', 'table-row');
parent.$('#<?php echo $_SERVER['http']->post('form_id');?> .success-bar').css('display', 'table-row');

parent.$('#<?php echo $_SERVER['http']->post('form_id');?>_alert_error').html('Неудачно');
parent.$('#<?php echo $_SERVER['http']->post('form_id');?>_alert_error').fadeIn();
parent.$('#<?php echo $_SERVER['http']->post('form_id');?>_alert_success').html('Успешно');
parent.$('#<?php echo $_SERVER['http']->post('form_id');?>_alert_success').fadeIn();

setTimeout(
    function(){
        parent.$('#<?php echo $_SERVER['http']->post('form_id');?>_alert_error').fadeOut();
    },
    3000
);

</script>

