<?php
    include_once(dirname(__FILE__)."/../../wirix.class.php");
    
    /**
        @class wirix_html_href
        Генерация html-ссылки(с иконкой)
        @ref wirix_html_href 
    */
    class wirix_html_href extends wirix{
        
        var $url = "#";     
        var $icon = "/wirix/libs/h/html_href/i/icon_default.png";
        var $text = "Ссылка";
        var $title = "Подсказка к ссылке";
        
        function __construct(){
            parent::__construct();
        }
        
        /**
            Генерация поля
        */
        function fetch($object = null){
            ob_start();
            echo '<a href="'.$this->url.'" '.
                ($this->title?'title="'.$this->title.'" ':'').'>'.
                ($this->icon?'<img src="'.$this->icon.'" class="a-icon"/>':'').
                $this->text.'</a>';
            $html = ob_get_contents();
            ob_end_clean();
            return $html;
        }
    }
?>
