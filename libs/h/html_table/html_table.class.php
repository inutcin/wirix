<?php
    include_once(dirname(__FILE__)."/../../wirix.class.php");
    
    /**
        Генерация html-кода таблицы
    */
    class wirix_html_table extends wirix{
        
        var $css_class = 'table table-bordered';           //!< css-класс таблицы
        var $rows = array();                //!< Массив строк таблицы
        var $columns = array();             //!< Массив описаний столбцов
        var $title = 'Заголовок таблицы';   //!< Заголовок таблицы
        var $pages = array();               //!< Список страниц
        var $id = '';
        var $numered_rows = false;
        
        function __construct(){
            parent::__construct();
        }
        
        function __destruct(){
        }
        
        /**
            Добавление колонки
            
            @param $column - массив параметров колонки вида array("параметр"=>"значение")
                Список параметров
                "colspan"   - Количество объединённых ячеек
                "width"     - Ширина столбца
                "title" - текст заголовка столбца
                "text"  - шаблон текста ячеек
                "hint"  - Всплывающий текст
                "rel"  - Дополнительная информация в теге rel=""
                "url"   - шаблон url ячеек
                "target"   - аттрибут target ссылки
                "align"   - выравнивание в ячейке
                "class" - css-класс ячейки
                "style" - дополнительный стиль ячейки
                "icon" - icon-css-класс (иконка)
                "onclick" - onclick функция
                "istotal" - Сумма по столбцу
                шаблоны пишутся в php вида <?php echo $row['col_name'];?>
        */
        function add_column(
            $column = array()
        ){
            if(!$column)return false;
            $this->columns[] = $column;
        }
        
        
        /**
            Генерация html-кода
        */
        function fetch($object = null){
            
            eval($this->aux_code);
            
           $html = '
        <div class="'.$this->called_class.'" id="'.$this->id.'">
            <table class="'.$this->css_class.'">
            ';
            
            $html .= ($this->title?'<tr><td class="alert alert-info" colspan="'.(count($this->columns)+1).'">'.$this->title.'</td></tr>':'');

            // Смотрим, есть ли хоть один столбец с суммой
            $istotal = false;
            foreach($this->columns as $k=>$column)if(isset($column['istotal']) && $column['istotal']){$istotal = 1; break;}

            $c = 0;
            foreach($this->columns as $k=>$column)if($this->columns[$k]['title']){$c=1;break;}

            // Выводим заголовок, если есть хоть один столбец с заголовком
            if($c){
                $html .= '<tr>';
                if($this->numered_rows)$html .= '<th style="width: 30px;">&#8470;</th>';
                foreach($this->columns as $k=>$column){
                    if(isset($this->columns[$k]['istotal']))$this->columns[$k]['istotal'] = 0;
                    $html .= '<th '.
                        (isset($column['width'])
                            ?
                        'width="'.$column['width'].'"'
                            :
                        ' ').
                        (isset($column['colspan'])
                            ?
                        'colspan="'.$column['colspan'].'"'
                            :
                        ' ').
                    '>'.(isset($column['title'])?$column['title']:'').'</th>';
                }
                $html .= '</tr>';
            }
            
            if(
                isset($this->rows[0])
                && property_exists($this,"show_avg_rows") 
                && property_exists($this,"show_avg_rows")
                && $this->show_avg_rows
            ){
                $avg_tr = '<tr class="avg">';
                $avg_tr .= '<th '.(property_exists($this,"numered_rows") && $this->numered_rows?' colspan="2"':'').'>'._t('Avarage').'</th>';
                $ss = 0;
                foreach($this->rows[0] as $k=>$v){
                    $ss++;
                    if($ss==1)continue;
                    $avg_tr .= '<th>';
                    
                    $row[$k] = isset($this->avg_row[$k])
                    ?
                        (
                            isset($this->avg[$ss]['format'])
                            ?
                            number_format($this->avg_row[$k], $this->avg[$ss]['format'][0], $this->avg[$ss]['format'][1], $this->avg[$ss]['format'][2])
                            :
                            $this->avg_row[$k]
                        )
                    :
                        "";

                    if(isset($this->columns[$ss]['text']) && $this->columns[$ss]['text'])
                        @eval('$avg_tr .= '.$this->columns[$ss]['text'].";");
                    else
                        $avg_tr .= '';
                        
                    $avg_tr .= '</th>';

                }
                $avg_tr .= '</tr>';
            }
            

            if(
                isset($this->rows[0])
                && property_exists($this,"show_sum_rows") 
                && property_exists($this,"show_sum_rows")
                && $this->show_sum_rows
            ){
                $sum_tr = '<tr class="total">';
                $sum_tr .= '<th '.(property_exists($this,"numered_rows") && $this->numered_rows?' colspan="2"':'').'>'._t('Total').'</th>';
                $ss = 0;
                
                
                foreach($this->rows[0] as $k=>$v){
                    $ss++;
                    if($ss<3)continue;

                    $row[$k] = isset($this->sum_row[$k])
                    ?
                        (
                            isset($this->sum[$ss]['format'])
                            ?
                            number_format($this->sum_row[$k], $this->sum[$ss]['format'][0], $this->sum[$ss]['format'][1], $this->sum[$ss]['format'][2])
                            :
                            $this->sum_row[$k]
                        )
                    :
                        "";
                        
                    $sum_tr .= '<th>';
                    $sum_tr .= $row[$k];
                    $sum_tr .= '</th>';
                }
                $sum_tr .= '</tr>';
            }

            
            if(property_exists($this, "show_sum_rows") && array_search("top", $this->show_sum_rows)!==false)
                $html .=  $sum_tr ;
                
            if(property_exists($this, "show_avg_rows") && array_search("top", $this->show_avg_rows)!==false)
                $html .=  $avg_tr ;
            
            if(property_exists($this, "filter") && $this->filter){
                $html .= '<tr>';
                foreach($this->columns as $k=>$column){
                    $html .= "<th>".$this->filter_row[$k]."</th>";
                }
                $html .= '</tr>';
            }
            
            foreach($this->rows as $rownum=>$row){
                
                foreach($row as $k=>$v)if(is_scalar($v))$row[$k] = html_entity_decode($v);
                
                $html .= '<tr>';
                if($this->numered_rows)
                    $html .= '<td class="integer">'.($rownum+1).'</td>';
                    
                foreach($this->columns as $k=>$column){
                    if(isset($this->columns[$k]['istotal'])){
                        @eval('$this->columns[$k]["istotal"] += '.$column['text'].";");
                    }
                    
                    
                    $html .= '<td ';
                    
                    if(isset($column['class']) && $column['class'])
                        @eval('$html .= \' class="\'.'.$column['class'].'.\'"\';');

                    if(isset($column['style']) && $column['style'])
                        @eval('$html .= \' style="\'.'.$column['style'].'.\'"\';');

                    if(isset($column['hint']) && $column['hint'])
                        @eval('$html .= \' title="\'.'.$column['hint'].'.\'"\';');

                    if(isset($column['rel']) && $column['rel'])
                        @eval('$html .= \' rel="\'.'.$column['rel'].'.\'"\';');

                    $html .= ' '.
                        (isset($column['align'])
                            ?
                        'style="text-align:'.$column['align'].';"'
                            :
                        ' ').
                    '>';
                    
                    
                    if(isset($column['url']) && $column['url'])
                        @eval('$html .= '.
                        '\'<a href="\'.'.$column['url'].'.\'"'.
                            (
                                isset($column['onclick']) && $column['onclick']
                            ?
                                ' onclick="\'.'.$column['onclick'].'.\'"'
                            :
                                " "
                            ).
                            
                            (
                                isset($column['target']) && $column['target']
                            ?
                                ' target="\'.'.$column['target'].'.\'"'
                            :
                                " "
                            ).
                            
                        ' >\''.";");

                    if(isset($column['icon']) && $column['icon'])
                        @eval('$html .= \'<i class="\'.'.$column['icon'].'.\'"></i>\';');
                        
                    if(isset($column['img']) && $column['img'])
                        @eval('$html .= '.'\'<img src="\'.'.$column['img'].'.\'"/>\''.";");
                    
                    if(isset($column['text']) && $column['text'])
                        eval('$html .= '.$column['text'].";");
                    else
                        $html .= '';

                    if(isset($column['url']) && $column['url'])
                        $html .= '</a>';
                        
                    $html .= '</td>';
                }
                $html .= '</tr>';
            }
            $html .= '</tr>';

            if($istotal){
                $html .= '<tr>';
                foreach($this->columns as $column){
                    $html .= '<th style="text-align:right;">'.(isset($column['istotal'])?$column['istotal']:'').'</th>';
                }
                $html .= '</tr>';
            }

            if(property_exists($this, "show_avg_rows") && array_search("bottom", $this->show_avg_rows)!==false)
                $html .=  $avg_tr ;

            if(property_exists($this, "show_sum_rows") && array_search("bottom", $this->show_sum_rows)!==false)
                $html .=  $sum_tr ;

            $html .= '</table></div>';
            return $html;
        }
        
    }
?>
