$('#<?php echo $this->id?>').focus(function(){
    if($('#<?php echo $this->id?>').val()=='<?php echo $this->default;?>')
        $('#<?php echo $this->id?>').val('');
});
$('#<?php echo $this->id?>').focusout(function(){
    if($('#<?php echo $this->id?>').val()=='')
        $('#<?php echo $this->id?>').val('<?php echo $this->default;?>');
});


<?php if($this->wyswyg){?>
    $('#<?php echo $this->id?>').cleditor({width: '<?php echo $this->width?>', 'height': '<?php echo $this->height?>'});
<?php }?>
