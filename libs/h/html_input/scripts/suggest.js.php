
$('#<?php echo $this->id?>_suggest').autoSuggest(
    "<?php echo $this->suggest_url;?>",
    {
        startText: "<?php echo $this->suggest_value;?>", 
        minChars: 0, 
        emptyText: "Элемент не найден", 
        selectionLimit: "1", 
        limitText: "Можно выбрать только один элемент", 
        start: function(data){return data; },
        resultClick: function(data){
            $("#<?php echo $this->id?>").val(data.attributes.id);
//            $("#input.html_input_suggest").css("background-color", "#"+data.attributes.image);
        }
    }
);
