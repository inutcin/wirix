
function add_work_type(id, val){
    re = new RegExp('\,'+val+' ','g');
    if(re.test($('#'+id).val())){
        alert('Этот элемент уде выбран');
        return false;
    }
    if(val=='-')return false;
    $('#'+id).val($('#'+id).val()+','+val+' ');
    $('#'+id).after('<div class="input-set-item" id="input-set-item-'+id+'-'+val+
        '" title="Удалить" onclick="delete_work_type(this,'+val+',\''+id+
        '\')">'+$('#'+id+'_select :selected').html()+'<i class="icon-trash"></i></div>');
}

function delete_work_type(obj, val, id){
    var ids=$('#'+id).val();
    re = new RegExp('\,'+val+'','g');
    ids = ids.replace(re, "");
    $('#'+id).val(ids);
    $('#'+obj.id).remove();
}
