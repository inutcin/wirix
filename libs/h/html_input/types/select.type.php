<?php
    $this->values   = array_merge(array(array("value"=>"-", "title"=>"--"._t('Not selected')."--")), $this->values);
?>

<select name="<?php echo $this->name;?>" id="<?php echo $this->id?>" class="<?php echo $this->css_class;?>" <?php if($this->readonly){?>disabled<?php }?>>
<?php foreach($this->values as $row){?>
    <option <?php if(isset($row['value'])){?>value="<?php echo $row['value']?>"<?php }?> <?php if($this->value==$row['value'] || $this->default==$row['value']){?>selected<?php }?>>
        <?php echo $row['title']?>
    </option>
<?php }?>
</select>
