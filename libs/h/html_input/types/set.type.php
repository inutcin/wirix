<?php
    $this->values   = array_merge(array(array("value"=>"-", "title"=>"--нет--")), $this->values);
?>

<select class="<?php echo $this->css_class;?>" id="<?php echo $this->id?>_select" onchange="add_work_type('<?php echo $this->id?>', $('#<?php echo $this->id?>_select').val());">
<?php 
    $titles = array();
    foreach($this->values as $row){
    $titles[$row['value']] = $row['title'];
    ?>
    <option value="<?php echo $row['value']?>" <?php if($this->value==$row['value'] || $this->default==$row['value']){?>selected<?php }?>>
        <?php echo $row['title']?>
    </option>
<?php }?>
</select>
<input name="<?php echo $this->name;?>" id="<?php echo $this->id?>" value="<?php echo $this->value;?>" type="hidden"/>

<?php 
    $vals = explode(",", $this->value);
    foreach($vals as $v){
    if(!$v = trim($v))continue;
    if(!isset($titles[$v]))continue;
?>
<div class="input-set-item" id="input-set-item-<?php 
echo $this->id?>-<?php 
echo $v?>" 
title="Удалить" onclick="delete_work_type(this,<?php 
echo $v?>,'<?php 
echo $this->id?>')"><?php 
echo $titles[$v];?><i class="icon-trash"></i></div>
<?php }?>

