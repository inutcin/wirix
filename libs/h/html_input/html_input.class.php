<?php
    include_once(dirname(__FILE__)."/../../wirix.class.php");
    
    /**
        \class wirix_html_input 
        \ref wirix_html_input "Класс wirix_html_input"
        \brief Генерация поля html-формы
        
        

        
        Типы полей
        ========================================
        
        text (простое текстовое поле)
        ------------------------------------------
        
        \code
        $input  = $_SERVER['wirix']->lib_load("html_input");                    // Создаём объект класса wirix_html_input (поле формы)
        $input->name = 'contract_id';                                           // Назначаем имя поля 
        $input->type = 'text';                                                  // Назначаем тип поля 
        $input->title = 'проект в dotproject';                                  // Назначаем выводимое название поля
        $input->regexp = "/^\d+$/";                                             // Задаём регулярное выражение для валидации значения поля
        $input->placeholder = 'ID проета';                                      // Задаём placeholder
        $input->comment = 'Введите ID проекта в dotproject';                    // Задаём всплывающий комментарий для поля
        $add_form->add_input($input);                                           // Добавляем поле формы в форму
        unset($input);                                                          // Чистим память
        \endcode    
        
        
        hidden (скрытое поле)
        ------------------------------------------
        \code
        $input  = $_SERVER['wirix']->lib_load("html_input");                    // Создаём объект класса wirix_html_input (поле формы)
        $input->name = 'table_name';                                            // Назначаем имя поля 
        $input->type = 'hidden';                                                // Назначаем тип поля 
        $input->regexp = "/^[\w\d\_]+$/";                                       // Задаём регулярное выражение для валидации значения поля
        $input->value = $_H->action;                                            // Задаём значение поля
        $add_form->add_input($input);                                           // Добавляем поле формы в форму
        unset($input);                                                          // Чистим память
        \endcode    
        
        
        set (набор значений)
        ------------------------------------------
        \code
        $input  = $_W->lib_load("html_input");                                  // Создаём объект класса wirix_html_input (поле формы)
        $input->name = 'users_white';                                           // Назначаем имя поля 
        $input->type = 'set';                                                   // Назначаем тип поля 
        $input->title = 'Разрешить пользователям';                              // Назначаем выводимое название поля
        $input->regexp = "/^(\s*\,\d+\s*){0,}$/";                               // Задаём регулярное выражение для валидации значения поля
        $input->placeholder = 'Разрешить пользователям';                        // Задаём placeholder
        $_D->search(                                                            // Получаем значения списка выбора из БД
            array("a"=>"users","b"=>"user_prop_values"),                        // Таблицы из которых выбираем и связи между ними
            array("`a`.`id`=`b`.`user_id` AND `b`.`user_prop_id`=2"=>"LEFT"),                                                            
            array(),"",                                                         // Условия выборки (простое и сложное)         
            "`b`.`value` ASC",                                                  // Порядок вывода результата
            0,0,                                                                // Ограничение вывода и стартовая строка
            array(                                                              // Выводимые поля
                "CONCAT(`b`.`value`,' (',`a`.`name`,')')"   =>  "title",
                "`a`.`id`"                                  =>  "value"
            )
        ); 
        $input->values = $_D->rows;                                             // Назначаем результат запроса списку значений поля
        // Получаем существующие значения
        $_D->search(array("a"=>"access_users"),array(),array("`a`.`section_id`"=>$section_id,"type"=>"allow"));
        $value = "";
        foreach($_D->rows as $row)$value .= ','.$row["user_id"];
        $input->value = $value;
        $form->add_input($input);                                               // Добавляем поле формы в форму
        unset($input);                                                          // Чистим память
        \endcode    



        date (поле выбора даты)
        ------------------------------------------
        
        \code
        $input  = $_SERVER['wirix']->lib_load("html_input");                    // Создаём объект класса wirix_html_input (поле формы)
        $input->name = 'from_date';                                             // Назначаем имя поля 
        $input->type = 'date';                                                  // Назначаем тип поля 
        $input->title = 'Дата начала периода ответственности';                  // Назначаем выводимое название поля
        $input->regexp = "/^\d+\-\d+\-\d+\ \d+:\d+:\d+$/";                      // Задаём регулярное выражение для валидации значения поля
        $input->placeholder = 'Дата начала периода ответственности';            // Задаём placeholder
        $input->comment = 'Введите дату начала периода ответственности';        // Задаём всплывающий комментарий для поля
        $add_form->add_input($input);                                           // Добавляем поле формы в форму
        unset($input);                                                          // Чистим память
        \endcode    
        
        
        suggest(автодополнение)
        -----------------------------------------
        
        Формирование поля
        
        \code
        $input  = $_SERVER['wirix']->lib_load("html_input");                    // Создаём объект класса wirix_html_input (поле формы)
        $input->name = 'contract_id';                                           // Назначаем имя поля 
        $input->type = 'suggest';                                               // Назначаем тип поля 
        $input->title = 'Клиент,номер договора, url';                           // Назначаем выводимое название поля
        $input->regexp = "/^\d+$/";                                             // Задаём регулярное выражение для валидации значения поля
        $input->placeholder = 'Клиент,номер договора, url';                     // Задаём placeholder
        $input->comment = 'Введите имя клиента, номер договора или url сайта';  // Задаём всплывающий комментарий для поля
        $input->suggest_url = '/acts/owners/add/name_contract_url.ajax.php';    // URL ajax-запроса, возвращающего список для автодополнения
        $input->suggest_value = '';                                             // Значение видимого поля автозаполнения по умолчанию
        $add_form->add_input($input);                                           // Добавляем поле формы в форму
        unset($input);                                                          // Чистим память
        \endcode    
            
        Ajax-скрипт списка автодополнения
        
        \code
        <?php
        include $_SERVER["DOCUMENT_ROOT"]."/wirix/libs/wirix.class.php";    // Подключаем WIRIX
        $request = $_D->link->real_escape_string($_H->get('q'));            // Получаем из адреса подстроку для поиска по БД
        // Поиск бо БД (SELECT)
        $_D->search(
            array("a"=>"sql_contracts","b"=>"sql_clients"),                 // Из каких таблиц
            array("`a`.`client_id`=`b`.`id`"=>"LEFT"),                      // Как таблицы связаны
            array(),                                                        // Простое условие поиска (не используется,  если задано сложное условие)
            "`a`.`num` LIKE '%$request%' 
            OR `a`.`url` LIKE '%$request%' 
            OR `b`.`name` LIKE '%$request%'",                               // Сложное условие для поиска
            "`b`.`name` ASC, `a`.`num` ASC, `a`.`url`",                     // Порядок вывода результата
            20,                                                             // Выводить строк максимум
            0,                                                              // Выводить, начиная с этой строки
            array(                                          // Список столбцов для вывода "выражение"=>"имя столбца"
                "CONCAT(`b`.`name`,', ',`a`.`num`,', ',`a`.`url`)"      =>"value",
                "`a`.`id`"                                              =>"id",
                "CONCAT('/uploads/companies/icons/',`b`.`id`,'.png')"   =>"image"
            )
        );
        $result = $_D->rows;                                                // В случае успеха тут зранится массив выводимый строк
        echo json_encode($result);                                          // Формируем json для скармливания
        \endcode
        
        select select (список выбора)
        ----------------------------------------
        
        \code
        $input  = $_SERVER['wirix']->lib_load("html_input");                    // Создаём объект класса wirix_html_input (поле формы)
        $input->name = 'manager_id';                                            // Назначаем имя поля 
        $input->type = 'select';                                                // Назначаем тип поля 
        $input->title = 'Ответственный менеджер';                               // Назначаем выводимое название поля
        $input->regexp = "/^\d+$/";                                             // Задаём регулярное выражение для валидации значения поля
        $input->placeholder = 'Ответственный менеджер';                         // Задаём placeholder
        $_D->search(                                                            // Получаем значения списка выбора из БД
            array("a"=>"sql_managers"),array(),                                 // Таблицы из которых выбираем и связи между ними
            array(),"",                                                         // Условия выборки (простое и сложное)         
            "`a`.`name` ASC",                                                   // Порядок вывода результата
            0,0,                                                                // Ограничение вывода и стартовая строка
            array(                                                              // Выводимые поля
                "`a`.`name`"    =>  "title",
                "`a`.`id`"      =>  "value"
            )
        ); 
        $input->values = $_D->rows;                                             // Назначаем результат запроса списку значений поля
        $add_form->add_input($input);                                           // Добавляем поле формы в форму
        unset($input);                                                          // Чистим память
        \endcode    
            


        
    */
    class wirix_html_input extends wirix{
        
        var $type = 'text';     //!< Тип поля
        var $title = 'Поле';    //!< Заголовок поля
        var $name =  'field';   //!< Имя поля
        var $default = '';       //!< Значение по умолчанию
        var $id = '';           //!< ID элемента
        var $regexp = '';       //!< Регулярное выражение для проверки значения
        var $comment = '';  //!< Комментарий для ввода праильного значения
        var $value = '';    //!< Значение поля
        var $db_id = 0;     //!< ID в БД (для привязки)
        var $values = array();  //!< Список значений для полей многзначного выбора
        var $css_class = '';    //!< CSS класс
        var $suggest_url = "";  //!< Урл, по которому происходит вызов автодополнения массив, массивов вида {id:"id",value:"value",image:"image"}
        var $suggest_value = 'Значение';    //!< Значение поля автозаполнения по умолчанию
        var $wyswyg = true;     //!< Использовать визуальный редактор
        var $placeholder = 'Введите значение';  
        var $maxlength = 0;//!< Ограничение на длину
        var $height = '300px';//!< Высота элемента ()
        var $width = '100%';//!< Ширина элемента ()
        
        var $unique =true;  //!< только уникальные значения
        var $readonly = false;
        
        function __construct(){
            parent::__construct();
        }
        
        /**
            Генерация поля
        */
        function fetch($object = null){
            ob_start();
            $this->value = htmlentities($this->value, ENT_COMPAT | ENT_HTML401, 'UTF-8');
            $type_filename = dirname(__FILE__)."/types/".$this->type.".type.php";
            if(file_exists($type_filename))
                include($type_filename);
            ?>
            <?php
            $html = ob_get_contents();
            ob_end_clean();
            return $html;
        }
        
        /**
            Генерация сопутствующего JS
        */
        function fetch_js(){
            ob_start();
            $type_filename = dirname(__FILE__)."/scripts/".$this->type.".js.php";
            if(file_exists($type_filename))
                include($type_filename);
            ?>
            <?php
            $html = ob_get_contents();
            ob_end_clean();
            return $html;
        }
        
    }
?>
