<?php
    include($_SERVER['DOCUMENT_ROOT']."/wirix/libs/wirix.class.php");
    header("Cache-Control: no-cache");
    header("Pragma: no-cache");

    $wirix_id = $_SERVER['http']->get('id');
    $wirix_class = $_SERVER['http']->get('class');
    
    // Получаем данные формы из сессии
    $data = get_object_vars(json_decode(
	base64_decode($_SESSION['wirix'][$wirix_class][$wirix_id])
    ));
    
    $data = $data['scheme'];

    $form = $_SERVER['wirix']->lib_load("html_form_datascheme");
    $form->datascheme_id  = $wirix_id;
    $form->scheme = $_SERVER['wirix']->init_datascheme(str_replace("datascheme_", "", $data->called_class));
    
    //$data['fields'] = get_object_vars($data['fields']);
    foreach($data->fields as $fname=>$f)
        if($f->value!='')
            $form->scheme->set($fname, $f->value);
    
    
    $form->id = $form->datascheme_id."_add_form"; $form->method = 'add';
    $form->use_back_button = false; $form->send_button_text =
    'Добавить';
    
    echo $form->fetch();

