<?php
    include_once(dirname(__FILE__)."/../../wirix.class.php");
    
    $_SERVER['wirix']->lib_load('html_db_table', false);
    class wirix_html_datascheme_table extends wirix_html_db_table{
        
        var $scheme = null;                 //!< Объект схемы данных
        var $use_add_button = true;         //!< Использовать кнопку добавления
        var $use_edit_button = true;        //!< Использовать кнопку редактирования
        var $use_delete_button = true;      //!< Использовать кнопку удаления
        var $modal_window_name = '';        //!< Имя модального окна
        
        var $enter_button_url = null;       //!< Ссылка на кнопки входа каждого элемента
        var $enter_button_onclick = null;   //!< Событие на кнопке входа каждого элемента
        var $edit_button_url = null;       //!< Ссылка на кнопки входа каждого элемента
        var $edit_button_onclick = null;   //!< Событие на кнопке входа каждого элемента
        var $delete_button_url = null;       //!< Ссылка на кнопки входа каждого элемента
        var $delete_button_onclick = null;   //!< Событие на кнопке входа каждого элемента
        
        function __construct(){
            $this->called_class = __CLASS__;
            parent::__construct();
            $this->modal_window_name = $this->id."_modal";
            
        }
        
        
        function fetch($object = null){
            $this->commit();
            if($this->use_add_button)
                $this->add_action_button(
                    $name       = 'add',               //!< Имя кнопки
                    $title      = _t('Add'),     //!< Текст кнопки 
                    $hint       = _t('Add'),     //!< Всплываюша подсказка
                    $icon       = 'icon-plus',     //!< Иконка кнопки
                    $url        = '#',     //!< URL кнопки
                    $onclick    = 'return wirix_html_modal_show(\''.$this->modal_window_name.
                        '\', \'&#160;\', '.
                        '\'/wirix/libs/h/html_datascheme_table/add.ajax.php?'.
                            'class='.get_class($this).
                            '&id='.$this->id.'\');"'      //!< Событие на кнопке
                );

            if($this->title=='Заголовок таблицы')$this->title = '';

            foreach($this->scheme->enabled_fields as $field_name){
                if(array_search($field_name, $this->scheme->hidden_fields)!==false)
                    continue;
                $field = $this->scheme->fields[$field_name];
                
                $column = array();
                $column['title'] = $field['comment'];
                switch($field['type']){
                    case 'timestamp':
                        $column['text'] =  'wirix_dates::translate($row["'.$field_name.'"],2,1)';
                    break;
                    case 'datetime':
                        $column['text'] =  'wirix_dates::translate($row["'.$field_name.'"],5,1)';
                    break;
                    case 'date':
                        $column['text'] =  'wirix_dates::translate($row["'.$field_name.'"],5,1)';
                    break;
                    default:
                        $column['text'] =  '"$row['.$field_name.']"';
                    break;
                }
                $column['class'] =  '"'.$field['type'].'"';
                $this->add_column($column);
                unset($column);
            }

            // Ссылка на вход в элемент на каждой строке
            if($this->enter_button_url){
                $column = array();
                $column['title'] = ' ';
                $column['width'] = '16px';
                $column['text'] =  '""';
                $column['hint'] =  '"'._t('Preview').'"';
                $column['icon'] =  '"icon-eye-open"';
                $column['text'] =  '""';
                $column['url'] =  $this->enter_button_url;;
                $column['class'] =  '"action"';
                $column['onclick'] =  $this->enter_button_onclick;
                $this->add_column($column);
                unset($column);
            }
        
            // Ссылка на редактирование в элементе на каждой строке
            if($this->use_edit_button){
                $column = array();
                $column['title'] = ' ';
                $column['width'] = '16px';
                $column['text'] =  '""';
                $column['hint'] =  '"'._t('Edit').'"';
                $column['icon'] =  '"icon-pencil"';
                $column['text'] =  '""';
                $column['class'] =  '"action"';
                $column['url'] =  $this->edit_button_url?$this->edit_button_url:'"#"';
                $column['onclick'] =  $this->edit_button_onclick?$this->edit_button_onclick:'"return wirix_html_modal_show(\'myModal\', \'&#160;\', \'/wirix/libs/h/html_datascheme_table/edit.ajax.php?element_id=$row[id]&class='.get_class($this->scheme).'&scheme_id='.$this->scheme->id.'\');"';
                $this->add_column($column);
                unset($column);
            }
        
            // Ссылка на удаление в элементе на каждой строке
            if($this->use_delete_button){
                $column = array();
                $column['title'] = ' ';
                $column['width'] = '16px';
                $column['text'] =  '""';
                $column['hint'] =  '"'._t('Delete').'"';
                $column['icon'] =  '"icon-trash"';
                $column['text'] =  '""';
                $column['url'] =  $this->delete_button_url?$this->edit_button_url:'"#"';;
                $column['class'] =  '"action"';
                $column['onclick'] =  $this->delete_button_onclick?$this->delete_button_onclick:'"return wirix_html_modal_show(\'myModal\', \'&#160;\', \'/wirix/libs/h/html_datascheme_table/delete.ajax.php?element_id=$row[id]&class='.get_class($this->scheme).'&scheme_id='.$this->scheme->id.'\');"';
                $this->add_column($column);
                unset($column);
            }

            $this->tables = array("a"=>$this->scheme->table);
            // Подбираем связанные таблицы и связные поля
            $i = 1;
            foreach($this->scheme->fields as $field_name => $item)
                if(isset($this->scheme->fields[$field_name]['join_schema'])){
                    $this->tables[$this->scheme->alphabet[$i]] = 
                        $this->scheme->fields[$field_name]['join_schema']->table;
                    $this->join_fields[
                        "`a`.`$field_name`=`".$this->scheme->alphabet[$i]."`.`".$this->scheme->fields[$field_name]['join_schema']->primary_key.
                        "` AND ".
                            (
                                $this->scheme->fields[$field_name]['join_schema']->condition
                                ?
                                $this->scheme->fields[$field_name]['join_schema']->condition
                                :
                                "1"
                            )
                    ] = "LEFT";
                    $this->fields_array["`".$this->scheme->alphabet[$i]."`.`".$this->scheme->fields[$field_name]['join_schema']->title_field."`"] = $field_name;
                    $i++;
                }
                else{
                    $this->fields_array["`a`.`$field_name`"] = $field_name;
                }
            
            $this->search = array();
            // Назначаем условие выборки
            foreach($this->scheme->fields as $field => $item)
                if(trim($item['value']))
                    $this->search["`a`.`".$field."`"] = $item['value'];

            if($query = $_SERVER['http']->get($this->id."_query")){
                $this->cond =  '(0';
                foreach($this->scheme->fields as $field_name=>$field)
                    if(array_search($field_name, $this->scheme->enabled_fields)!==false)
                        if(!isset($this->scheme->fields[$field_name]['join_schema']))
                            $this->cond .= " OR `a`.`$field_name` LIKE '%".
                                $_SERVER['db']->link->real_escape_string($query)."%' ";
                $this->cond .=  ')';
            }
            if($this->scheme->condition)
                $this->cond = " ".$this->scheme->condition;
            $this->sort = "`a`.".$this->scheme->sort." ".$this->scheme->order;
            $this->group_by = $this->scheme->group_by;
            $this->search();
            
            $modal = $_SERVER['wirix']->lib_load("html_modal");
            $modal->id = $this->modal_window_name;
            
            return parent::fetch().$modal->fetch();
        }
        
    }
?>
