<?php
    include($_SERVER['DOCUMENT_ROOT']."/wirix/libs/wirix.class.php");
    header("Cache-Control: no-cache");
    header("Pragma: no-cache");

    // Создаём объект формы
    $form = $_SERVER['wirix']->lib_load("html_form_datascheme");
    // Получаем ID схемы данных и передаём в форму
    $form->datascheme_id    = $_SERVER['http']->get('scheme_id');
    $form->scheme = $_SERVER['wirix']->init_datascheme(str_replace("datascheme_", "", $_SERVER['http']->get('class')));
    // Назначаем форме ID
    $form->id = $form->datascheme_id."_form";
    // Назначаем метод обработки(редактирование)
    $form->method = 'save';
    // Не выводим кнопку отмены
    $form->use_back_button = false;
    // Передаём в форму ID элемента из $_GET
    $form->primary_key = $_SERVER['http']->get('element_id');
    $form->send_button_text = 'Редактировать';
    
    echo $form->fetch();
    $form->commit();
