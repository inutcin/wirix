<?php
    include($_SERVER['DOCUMENT_ROOT']."/wirix/libs/wirix.class.php");
    header("Cache-Control: no-cache");
    header("Pragma: no-cache");

    $wirix_id = $_SERVER['http']->get('scheme_id');
    $wirix_class = $_SERVER['http']->get('class');

    // Получаем данные формы из сессии
    $data = get_object_vars(json_decode(
	base64_decode($_SESSION['wirix'][$wirix_class][$wirix_id])
    ));


    // Создаём объект формы
    $form = $_SERVER['wirix']->lib_load("html_form_datascheme");

    // Получаем ID схемы данных и передаём в форму
    $form->datascheme_id    = $wirix_id;
    // Назначаем форме ID
    $form->id = $form->datascheme_id."_form";
    // Назначаем метод обработки(редактирование)
    $form->method = 'delete';
    
    $form->scheme = $_SERVER['wirix']->init_datascheme(str_replace("datascheme_", "", $wirix_class));
    
    $data['fields'] = get_object_vars($data['fields']);
    foreach($data['fields'] as $fname=>$f)
        if($f->value!='')
            $form->scheme->set($fname, $f->value);
    
    // Не выводим кнопку отмены
    $form->use_back_button = false;
    // Передаём в форму ID элемента из $_GET
    $form->primary_key = $_SERVER['http']->get('element_id');
    $form->send_button_text = 'Удалить';
    
    echo $form->fetch();
    $form->commit();

