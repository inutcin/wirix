<?php
    include_once(dirname(__FILE__)."/../../wirix.class.php");


    /**
     * Класс генерации капчи, оставляет после себя в $_SESSION['wirix']['captcha'] текст капчи
     * Пример:
     *      $captcha = $_SERVER['wirix']->lib_load("captcha", true);
     *      $_SERVER['wirix']->lib_load("http");
     *      $captcha->show($_SERVER['wirix']->get('method','numeric'));
     */
    class wirix_captcha extends wirix{
        
        var $font_filename = '';
        var $width = 180;
        var $height = 30;
        var $noise_factor = 10;       //!< Количество элементов шума
        var $captcha = '';
        
        function __construct(){
            parent::__construct();
            $this->font_filename = dirname(__FILE__)."/fonts/FreeSans.ttf";
            if(!isset($_SESSION['wirix']['captcha']))$_SESSION['wirix']['captcha'] = array();
        }
        
        /**
         * Генерация капчи
         */
        function show($type = "arithmetc"){
            if(!trim($type))$type = "arithmetc";
            $type = str_replace("..","",$type);
            $method_filename = dirname(__FILE__)."/methods/$type.inc.php";
            if(!file_exists($method_filename))
                $content = file_get_contents(dirname(__FILE__)."/i/not_supported.png");
            else{
                ob_start();
                include($method_filename);
                // Зашумляем капчу
                for($i=0;$i<$this->noise_factor;$i++){
                    $x0 = round(rand(0,$this->width));
                    $x1 = round(rand(0,$this->width));
                    $y0 = round(rand(0,$this->height));
                    $y1 = round(rand(0,$this->height));
                    $color = imagecolorallocatealpha(
                        $src,rand(150,255),rand(150,255),rand(150,255),rand(20,40)
                    );
                    imageline($src, $x0,$y0,$x1,$y1,$color);
                }
                // Если матюкнулся на ошибки - выводить изображени-заглушку
                if(ob_get_length()){
                    $content = file_get_contents(dirname(__FILE__)."/i/error.png");
                }
                else{
                    imagepng($src); 
                    $content = ob_get_contents();
                }
                ob_end_clean();
            }
            
            $_SERVER['wirix']->lib_load("http");
            $_SERVER['http']->content_type("image/png");
            echo $content;
            return true;
        }
        
    }

?>
