<?php

$width = $this->width;               //Ширина изображения
$height = $this->height;               //Высота изображения
$font_size = $width/20;            //Размер шрифта
$let_amount = 4;            //Количество символов, которые нужно набрать
$fon_let_amount = 30;       //Количество символов на фоне
$font = $this->font_filename;   //Путь к шрифту
  
//набор символов
$letters = array("a","b","c","d","e","f","g");     
  
$src = imagecreatetruecolor($width,$height);    //создаем изображение              
$fon = imagecolorallocate($src,255,255,255);    //создаем фон
imagefill($src,0,0,$fon);                       //заливаем изображение фоном
  
for($i=0;$i < $fon_let_amount;$i++)          //добавляем на фон буковки
{
    //случайный цвет
    $color = imagecolorallocatealpha($src,rand(0,255),rand(0,255),rand(0,255),100);
    //случайный символ
    $letter = $letters[rand(0,sizeof($letters)-1)];
    //случайный размер                             
    $size = rand($font_size-2,$font_size+2);                                           
    imagettftext($src,$size,rand(0,45),
        rand($width*0.1,$width-$width*0.1),
        rand($height*0.2,$height),$color,$font,$letter);
}

$b1 = round(rand(0,9));
$a1 = round(rand(0,9));

$b2 = round(rand(0,9));
$a2 = round(rand(0,9));

$c  = round(rand(0,9));
  
$letters = array($a1,"/",$b1," ","+"," ","0",",",$c," ","*"," ", $a2,"/", $b2);
for($i=0;$i < count($letters);$i++)      //то же самое для основных букв
{
    $x = ($i+1)*$font_size + rand(1,5)+5;
    $y = (($height*2)/3) + rand(0,5)-$font_size/4;                           
    put_symbol($x, $y, $letters[$i], $src, $font_size,$font);
}

/*
    $x = ($i+1)*$font_size + rand(1,5)+5;
    $y = (($height*2)/3) + rand(0,5)-15;                           
    put_symbol($x, $y, $a1, $src, $font_size,$font);

    $x = ($i+1)*$font_size + rand(1,5)+5;
    $y = (($height*2)/3) + rand(0,5)+0;                           
    put_symbol($x, $y, "&#8212;",$src, $font_size,$font);

    $x = ($i+1)*$font_size + rand(1,5)+10;
    $y = (($height*2)/3) + rand(0,5)+20;                           
    put_symbol($x, $y, $b1,$src, $font_size,$font);

    $x = ($i+1)*$font_size + rand(1,5)+55;
    $y = (($height*2)/3) + rand(0,5);                           
    put_symbol($x, $y, "+",$src, $font_size,$font);

    $x = ($i+1)*$font_size + rand(1,5)+75;
    $y = (($height*2)/3) + rand(0,5);                           
    put_symbol($x, $y, "0,".$c,$src, $font_size,$font);

    $x = ($i+1)*$font_size + rand(1,5)+125;
    $y = (($height*2)/3) + rand(0,5)-5;                           
    put_symbol($x, $y, "*",$src, $font_size,$font);


    $x = ($i+1)*$font_size + rand(1,5)+155;
    $y = (($height*2)/3) + rand(0,5)-15;                           
    put_symbol($x, $y, $a2,$src, $font_size,$font);

    $x = ($i+1)*$font_size + rand(1,5)+155;
    $y = (($height*2)/3) + rand(0,5)+0;                           
    put_symbol($x, $y, "&#8212;",$src, $font_size,$font);

    $x = ($i+1)*$font_size + rand(1,5)+160;
    $y = (($height*2)/3) + rand(0,5)+20;                           
    put_symbol($x, $y, $b2,$src,$font_size,$font);
*/

    $answer = round(($a2/$b2) * ($c/10) + ($a1/$b1), 2);
    $_SESSION['wirix']['captcha']['captcha'] = $answer;

function put_symbol($x, $y, $letter, $src,$font_size,$font){
    $colors = array("90","100","110","120","130","140","150"); 
    $color = imagecolorallocatealpha($src,$colors[rand(0,sizeof($colors)-1)],
        $colors[rand(0,sizeof($colors)-1)],
        $colors[rand(0,sizeof($colors)-1)],rand(20,40));
    $size = rand($font_size*2-2,$font_size*2+2);
    imagettftext($src,$size,rand(0,15),$x,$y,$color,$font,$letter);
}

  
//$cod = implode("",$cod);                    //переводим код в строку
?>
