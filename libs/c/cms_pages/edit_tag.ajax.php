<?php

include_once("../../wirix.class.php");
$answer = array();
$answer['redirect'] = '';
$answer['error'] = '';


$form = get_object_vars(json_decode(
    base64_decode(
    $_SESSION['wirix'][
        'wirix_html_form'
    ][
        $_SERVER['http']->post('form_id')
    ]
    )
));

$ins_fields = array(
    "name"=>$_H->post("name"),
    "parent_id"=>intval($_H->post("parent_id"))
);

$id = intval($_H->get('id'));

if($page_id = $_D->update("tags",$ins_fields,array("id"=>$id))){
    $answer['redirect'] = "/cms/tags/".intval($_H->post("parent_id"));
    $answer['success'] = "Запись отредактирована";
}
else{
    $answer['redirect'] = "/cms/tags/".intval($_H->post("parent_id"));
    $answer['success'] = "Запись не изменена";
}

echo json_encode($answer);
?>
