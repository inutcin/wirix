<?php

include_once("../../wirix.class.php");
$answer = array();
$answer['redirect'] = '';
$answer['error'] = '';
$upload_dir = $_SERVER['DOCUMENT_ROOT']."/uploads/pages/";

$form = get_object_vars(json_decode(
    base64_decode(
    $_SESSION['wirix'][
        'wirix_html_form'
    ][
        $_SERVER['http']->post('form_id')
    ]
    )
));

$ins_fields = array(
    "name"=>$_H->post("name"),
    "h1"=>$_H->post("h1"),
    "text"=>$_H->post("text"),
    "anons"=>$_H->post("anons"),
    "publish_from"=>$_H->post("publish_from"),
    "order"=>$_H->post("order"),
    "publish_to"=>$_H->post("publish_to")
);

$id = intval($_H->get('id'));

///////////////// Imaze upload ////////////////////
            if(!isset($_FILES['icon']) || $_FILES['icon']['error'] || !$_FILES['icon']['tmp_name']){
                $answer['error'] = _t('Cant load image');
            }
            
            $image = $_W->lib_load('image');
            
            if(!$answer['error'] && !copy($_FILES['icon']['tmp_name'], $image->filename = $upload_dir.$_FILES['icon']['name'])){
                $answer['error'] = _t('Cant copy temp file image');
            }
            
            
            $image->thrumb = $upload_dir.'page_pic_'.$id.'.png';
            $image->thrumb_width = 32;
            $image->thrumb_height = 32;
            $image->mode = 0;
            if(!$answer['error'] && !$image->create_thrumb()){
                $answer['error'] = _t('Cant resize image');
            }
            @chmod($image->thrumb, 0777);

            $image->thrumb = $upload_dir.'page_tiny_'.$id.'.png';
            $image->thrumb_width = 64;
            $image->thrumb_height = 64;
            $image->mode = 0;
            if(!$answer['error'] && !$image->create_thrumb()){
                $answer['error'] = _t('Cant resize image');
            }
            @chmod($image->thrumb, 0777);

            $image->thrumb = $upload_dir.'page_small_'.$id.'.png';
            $image->thrumb_width = 128;
            $image->thrumb_height = 128;
            $image->mode = 0;
            if(!$answer['error'] && !$image->create_thrumb()){
                $answer['error'] = _t('Cant resize image');
            }
            @chmod($image->thrumb, 0777);

            $image->thrumb = $upload_dir.'page_med_'.$id.'.png';
            $image->thrumb_width = 256;
            $image->thrumb_height = 256;
            $image->mode = 1;
            if(!$answer['error'] && !$image->create_thrumb()){
                $answer['error'] = _t('Cant resize image');
            }
            @chmod($image->thrumb, 0777);

            $image->thrumb = $upload_dir.'page_big_'.$id.'.png';
            $image->thrumb_width = 512;
            $image->thrumb_height = 512;
            $image->mode = 1;
            if(!$answer['error'] && !$image->create_thrumb()){
                $answer['error'] = _t('Cant resize image');
            }
            @chmod($image->thrumb, 0777);

            $image->thrumb = $upload_dir.'page_strong_'.$id.'.png';
            $image->thrumb_width = 768;
            $image->thrumb_height = 768;
            $image->mode = 1;
            if(!$answer['error'] && !$image->create_thrumb()){
                $answer['error'] = _t('Cant resize image');
            }
            @chmod($image->thrumb, 0777);

            $image->thrumb = $upload_dir.'page_huge_'.$id.'.png';
            $image->thrumb_width = 1024;
            $image->thrumb_height = 1024;
            $image->mode = 1;
            if(!$answer['error'] && !$image->create_thrumb()){
                $answer['error'] = _t('Cant resize image');
            }
            @chmod($image->thrumb, 0777);

            @chmod($image->filename, 0777);
            @unlink($image->filename);
            $answer['error']='';
        
///////////////// Imaze upload ////////////////////


////////// Теги
$tags = explode(",", $_H->post("tags"));
$_D->delete("page_tags",array("page_id"=>$id),"",0);
$tag_id = 0;
foreach($tags as $tag){
    if(!trim($tag))continue;
    $_D->insert("page_tags",array("page_id"=>$id,"tag_id"=>$tag));
    $tag_id = $tag;
}



// Чистим неактуальные SEO-правила
$_D->sql_query("DELETE FROM seo WHERE auto =  'Y' AND request_uri NOT IN (
SELECT  `from_url` 
FROM  `mod_rewrite` 
WHERE 1
)", "CHANGE");


$cms_pages = $_W->lib_load("cms_pages");
$chpu = $_H->post("chpu")?$_D->link->real_escape_string($_H->post("chpu")):$cms_pages->get_tag_chpu($tag_id).$_H->translit($_H->post("name"))."/";
$real_url = "/pages/view/$id/";
$url_cond = array("to_url"=>$real_url);
$pins_fields = array(
    "from_url"=>$chpu,
    "to_url"=>$real_url,
    "status"=>200,
    "regex"=>"",
);
if($_D->search_one("mod_rewrite",$url_cond))
    $_D->update("mod_rewrite",$pins_fields,$url_cond);
else
    $_D->insert("mod_rewrite",$pins_fields,$url_cond);


$url_cond = array("request_uri"=>$chpu);
$pins_fields = array(
    "title"=>htmlspecialchars($_H->post("title")),
    "description"=>htmlspecialchars($_H->post("description")),
    "keywords"=>htmlspecialchars($_H->post("keywords")),
    "request_uri"=>$chpu
);
if($_D->search_one("seo",$url_cond)){
    if($_D->search_one("seo",array("id"=>$_D->record["id"],"auto"=>"Y")))
        $_D->update("seo",$pins_fields,$url_cond);
}
else{
    $_D->insert("seo",$pins_fields);
}

if($page_id = $_D->update("pages",$ins_fields,array("id"=>$id))){
    $answer['redirect'] = $_H->post('back_url')?$_H->post('back_url'):"/cms/";
    $answer['success'] = "Запись отредактирована";
}
else{
    $answer['redirect'] = $_H->post('back_url')?$_H->post('back_url'):"/cms/";
    $answer['success'] = "Запись не изменена";
}

?>
<script src="/wirix/libs/h/html/js/jquery.min.js"></script>
<script>
parent.$('#html_form_<?php echo $_SERVER['http']->post('form_id');?>_submit').attr('class','submit btn-primary');

<?php if($answer['error']){ ?>
    parent.$('#<?php echo $_SERVER['http']->post('form_id');?> .alert-bar').css('display', 'table-row');
<?php }else{ ?>
    parent.$('#<?php echo $_SERVER['http']->post('form_id');?> .success-bar').css('display', 'table-row');
<?php } ?>

<?php if($answer['error']){ ?>
    parent.$('#<?php echo $_SERVER['http']->post('form_id');?>_alert_error').html('Неудачно:<?php echo $answer['error'];?>');
    parent.$('#<?php echo $_SERVER['http']->post('form_id');?>_alert_error').fadeIn();
<?php }else{ ?>
    parent.$('#<?php echo $_SERVER['http']->post('form_id');?>_alert_success').html('Успешно');
    parent.$('#<?php echo $_SERVER['http']->post('form_id');?>_alert_success').fadeIn();
    parent.document.location.href='<?php echo $answer['redirect']; ?>';
<?php } ?>

setTimeout(
    function(){
        parent.$('#<?php echo $_SERVER['http']->post('form_id');?>_alert_error').fadeOut();
    },
    3000
);

</script>
