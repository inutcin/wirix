<?php
    include_once(dirname(__FILE__)."/../../wirix.class.php");
    
    class wirix_cms_pages extends wirix{
        
        var $icon_size = 50;
        var $records_on_page = 10;
        
        function __construct() {
            parent::__construct(__CLASS__);
        }       


        /**
            Получение списка статей по ID тега
        */
        function get_pages_by_tag($tag_ids, $offset=0, $sort='`order`', $order='ASC'){
            global $_D;
            
            $_D->search(
                array("a"=>"pages","b"=>"page_tags","c"=>"mod_rewrite"),
                array("`a`.`id`=`b`.`page_id`"=>"LEFT","CONCAT('/pages/view/',`a`.`id`,'/')=`c`.`to_url`"=>"LEFT"),
                array(),
                "`b`.`id` IS NOT NULL AND `b`.`tag_id` IN ($tag_ids)","$sort $order, `a`.`ctime` DESC",$this->records_on_page, $offset,
                array(
                    "`a`.`id`"=>"id",
                    "`a`.`name`"=>"name",
                    "`a`.`anons`"=>"anons",
                    "`a`.`text`"=>"text",
                    "`a`.`publish_from`"=>"ptime",
                    "DATE_FORMAT(`a`.`publish_from`,'%d.%m.%Y %H:%i:%s')"=>"htime",
                    "DATE_FORMAT(`a`.`publish_from`,'%d.%m.%Y')"=>"hdate",
                    "`c`.`from_url`"=>"chpu"
                )
                
            );
            
            $result = $_D->rows;
            
            foreach($result as $k=>$row){
                $_D->search(
                    array("a"=>"page_tags","b"=>"tags"),
                    array("`a`.`tag_id`=`b`.`id`"=>"LEFT"),
                    array("`a`.`page_id`"=>$row['id'])
                );
                $result[$k]['tags'] = $_D->rows;
            }
            
            
            
            return $result;
        }

        /**
            Удаление тега
        */
        function delete_tag($id){
            global $_D;
            
            if(!$_D->search_one("tags",array("id"=>$id))){
                return false;
            }
            
            $parent_id = $_D->record['parent_id'];
            $_D->delete("tags",array("id"=>$id));
            
            return $parent_id;
        }

        /**
         * Получение html-кода формы добавления страницы
         */
        function fetch_edit_tag($id){
            global $_W;
            global $_D;
            
            if(!$_D->search_one("tags",array("id"=>$id))){
                return '';
            }
            
            $tag = $_D->record;
            
            $form = $_W->lib_load("html_form");
            $form->id = $this->id."_tag_edit_form";
            $form->title = _t('Edit tag');
            $form->use_back_button = false;
            $form->send_button_text = _t('Edit');
            
            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'name';
            $input->title = _t('Tag name');
            $input->value= $tag['name'];
            $input->regexp = "/^.+$/";
            $input->placeholder = 'Введите название страницы';
            $form->add_input($input);
            unset($input);

            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'parent_id';
            $input->type = 'select';
            $input->title = _t('Parent');
            $input->regexp = "/^.+$/";
            $input->value= $tag['parent_id'];
            $_D->search(array("a"=>"tags"),array(),array(),"","`a`.`name` ASC",0,0,
                array("`a`.`id`"=>"value","`a`.`name`"=>"title"));
            $input->values = array(array("title"=>_t('Root'),"value"=>0));
            $input->values = array_merge($input->values, $_D->rows);
            $input->placeholder = 'Введите название страницы';
            $form->add_input($input);
            unset($input);

            $form->submit_url = "/wirix/libs/c/cms_pages/edit_tag.ajax.php?id=".$id;

            return $form->fetch();
        }


        /**
         * Получение html-кода формы добавления страницы
         */
        function fetch_add_tag($parent_id){
            global $_W;
            global $_D;
            
            $form = $_W->lib_load("html_form");
            $form->id = $this->id."_tag_add_form";
            $form->title = _t('Add tag');
            $form->use_back_button = false;
            $form->send_button_text = _t('Add');
            
            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'name';
            $input->title = _t('Tag name');
            $input->regexp = "/^.+$/";
            $input->placeholder = _t('Type tag name');
            $form->add_input($input);
            unset($input);

            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'parent_id';
            $input->type = 'select';
            $input->title = _t('Parent');
            $input->regexp = "/^.+$/";
            $input->value = $parent_id;
            $_D->search(array("a"=>"tags"),array(),array(),"","`a`.`name` ASC",0,0,
                array("`a`.`id`"=>"value","`a`.`name`"=>"title"));
            $input->values = array(array("title"=>_t('Root'),"value"=>0));
            $input->values = array_merge($input->values, $_D->rows);
            $form->add_input($input);
            unset($input);

            $form->submit_url = "/wirix/libs/c/cms_pages/add_tag.ajax.php";
            
            return $form->fetch();
        }


        /**
         * Получение html-кода таблицы тэгов 
         */
        function fetch_tags_table($tag_id){
            global $_W;

            $table = $_W->lib_load("html_db_table");
            $table->id = $this->id."_tags_list_table";
            
            
            $path = $this->get_tags_path($tag_id);
            
            $r = array();
            foreach($path as $k=>$p)
                $r[] = 
                    (count($path)>$k+1?'<a href="/cms/tags/'.$p['id'].'">':'<b>').
                    $p["name"].
                    (count($path)>$k+1?'</a>':'</b>');
            
            $table->title = implode("&#160;/&#160;",$r);
            
            $table->title .= '&#160;&#160;&raquo;&#160;<a href="/cms/tags/add/'.(isset($p['id'])?$p['id']:"").'">'._t('Add').'</a>';
            
            $table->show_search_form = false;
            
            $table->tables = array("a"=>"tags");
            $column = array();
            $column['title'] = _t('Name');
            $column['text'] =   '$row["name"]';
            $column['url'] =   '"/cms/tags/".$row["id"]."/"';
            $table->add_column($column);
            unset($column);

            $table->search = array(
                "`a`.`parent_id`"=>intval($tag_id)
            );

            $table->fields_array = array(
                "`a`.`name`"=>"name",
                "`a`.`id`"=>"id"
            );

            $column = array();
            $column['title'] = _t('Edit');
            $column['url'] =   '"/cms/tags/edit/".$row["id"]."/"';
            $column['icon'] =   '"icon-pencil"';
            $column['width'] =   '10px';
            $column['align'] =   'center';
            $table->add_column($column);
            unset($column);

            $column = array();
            $column['title'] = _t('Delete');
            $column['url'] =   '"/cms/tags/delete/".$row["id"]."/"';
            $column['icon'] =   '"icon-trash"';
            $column['width'] =   '10px';
            $column['align'] =   'center';
            $column['onclick'] =   '"return confirm(\'Точно удалить?\')"';
            $table->add_column($column);
            unset($column);


            $table->search();

            return $table->fetch();
        }
        
        /**
            Получение в массиве пути к тегу
        */
        function get_tags_path($tag_id, $max_depth = 10){
            global $_D;
            
            $result = array(
            );
            
            if(!$_D->search_one("tags",array("id"=>$tag_id))){
                return array();
            }

            $current = $_D->record;
            $result[] = $current;
            
            $depth = 0;
            while($depth<$max_depth && isset($current["parent_id"]) && $current["parent_id"]){
                if($_D->search_one("tags",array("id"=>$current['parent_id'])))
                    $result[] = $current = $_D->record;
                $depth++;
            }

            $result[] = array("id"=>0,"name"=>_t('Root'),"parent_id"=>0);

            $result = array_reverse($result);

            return $result;
        }
        
        /**
         * Получение html-кода таблицы страниц 
         */
        function fetch_pages_table($tag_id=0){
            global $_W;
            
            $table = $_W->lib_load("html_db_table");
            $table->id = $this->id."_pages_list_table";
            $table->title = 'Список элементов раздела';
            $table->show_search_form = false;
            
            $table->tables = array("a"=>"pages","b"=>"page_tags");
            $table->join_fields = array("`a`.`id`=`b`.`page_id`"=>"LEFT");
            if($tag_id)$table->cond = "`b`.`id` IS NOT NULL AND `b`.`tag_id`=$tag_id";
            $table->group_by = '`a`.`id`';
            $table->sort = "`order`";
            $table->order = "`ASC`";
            

            $column = array();
            $column['title'] = 'Картинка';
            $column['text'] =   '"<div style=\'width:32px;height:32px;background-image:url(/uploads/pages/page_pic_".$row["id"].".png)\'></div>"';
            $column['width'] =   '10px';
            $table->add_column($column);
            unset($column);

            $column = array();
            $column['title'] = 'Имя';
            $column['text'] =   '$row["name"]';
            $table->add_column($column);
            unset($column);

            $table->fields_array = array(
                "`a`.`name`"=>"name",
                "`a`.`id`"=>"id",
                "DATE_FORMAT(`a`.`publish_from`,'%d.%m.%Y')" =>"publish_from",
                "DATE_FORMAT(`a`.`publish_to`,'%d.%m.%Y')" =>"publish_to"
            );
            $column = array();
            $column['title'] = 'Начало публикации';
            $column['text'] =   '$row["publish_from"]';
            $column['width'] =   '10px';
            $table->add_column($column);
            unset($column);

            $column = array();
            $column['title'] = 'Конец публикации';
            $column['text'] =   '$row["publish_to"]';
            $column['width'] =   '10px';
            $table->add_column($column);
            unset($column);

            $column = array();
            $column['title'] = 'Правка';
            $column['url'] =   '"/cms/pages/edit/".$row["id"]."/?back_url='.urlencode($_SERVER["REQUEST_URI"]).'#pages"';
            $column['icon'] =   '"icon-pencil"';
            $column['width'] =   '10px';
            $column['align'] =   'center';
            $table->add_column($column);
            unset($column);

            $column = array();
            $column['title'] = 'Удаление';
            $column['url'] =   '"/cms/pages/delete/".$row["id"]."/?back_url='.urlencode($_SERVER["REQUEST_URI"]).'#pages"';
            $column['icon'] =   '"icon-trash"';
            $column['width'] =   '10px';
            $column['align'] =   'center';
            $column['onclick'] =   '"return confirm(\'Точно удалить?\')"';
            $table->add_column($column);
            unset($column);

            
            $table->search();
            
            return $table->fetch();
        }
        
        /**
         * Получение html-кода формы добавления страницы
         */
        function fetch_add_page(
            $tag_id //!< Раздел, в котором размещаем по умолчанию
        ){
            global $_W;
            global $_H;
            
            $form = $_W->lib_load("html_form");
            $form->id = $this->id."_pages_add_form";
            $form->title = "Добавление страницы";
            $form->use_back_button = false;
            $form->use_ajax = true;
            $form->send_button_text = _t('Add');
            
            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'name';
            $input->title = "Название страницы";
            $input->regexp = "/^.+$/";
            $input->placeholder = 'Введите название страницы';
            $form->add_input($input);
            unset($input);

            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'publish_from';
            $input->type = 'date';
            $input->title = "Начало публикации";
            $input->placeholder = _t('Publish start date');
            $input->value = date("Y-m-d H:i:s");
            $form->add_input($input);
            unset($input);

            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'publish_to';
            $input->type = 'date';
            $input->title = "Конец публикации";
            $input->placeholder = _t('Publish end date');
            $input->value = date("Y-m-d H:i:s",time()+20*365*24*60*60);
            $form->add_input($input);
            unset($input);

            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'anons';
            $input->type = 'textarea';
            $input->height = '100px';
            $input->wyswyg = false;
            $input->regexp = "/^.{0,255}$/m";
            $input->title = 'Анонс страницы';
            $input->placeholder = 'Анонс страницы';
            $form->add_input($input);
            unset($input);
            
            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'text';
            $input->regexp = "/^.*$/m";
            $input->type = 'textarea';
            $input->title = 'Текст страницы';
            $input->placeholder = 'Текст страницы';
            $form->add_input($input);
            unset($input);

            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'tag_id';
            $input->regexp = "/^\d+$/";
            $input->type = "hidden";
            $input->value = intval($tag_id);
            $input->placeholder = 'Введите название страницы';
            $form->add_input($input);
            unset($input);


            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'back_url';
            $input->regexp = "/^.*$/";
            $input->type = "hidden";
            $input->value = $_H->get("back_url");
            $form->add_input($input);
            unset($input);

            $form->submit_url = "/wirix/libs/c/cms_pages/add_page.ajax.php";
            $form->send_button_text = "Далее &raquo;";
            
            return $form->fetch();
        }


        /**
         * Получение html-кода формы редактирования страницы
         */
        function fetch_edit_page($id = 0){
            global $_W;
            global $_D;
            global $_H;
            
            if(!$_D->search_one("pages",array("id"=>$id)))return '';
            $page = $_D->record;
            
            // Ищем ЧПУ
            $page["chpu"] = $this->get_page_chpu($page["id"]);
            
            
            $form = $_W->lib_load("html_form");
            $form->id = $this->id."_pages_edit_form";
            $form->title = "Редактирование страницы";
            $form->use_back_button = false;
            $form->use_ajax = false;
            $form->send_button_text = 'Сохранить';
            
            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'name';
            $input->title = 'Название страницы';
            $input->regexp = "/^.+$/";
            $input->placeholder = 'Введите название страницы';
            $input->value = $page["name"];
            $form->add_input($input);
            unset($input);

            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'icon';
            $input->title = 'Картинка';
            $input->type = "file";
            $input->regexp = "/^.*$/";
            $input->value = $page["name"];
            $form->add_input($input);
            unset($input);


            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'text';
            $input->type = 'textarea';
            $input->title = 'Текст страницы';
            $input->placeholder = 'Текст страницы';
            $input->value = html_entity_decode ($page["text"]);
            $input->regexp = "/^.*$/m";
            $form->add_input($input);
            unset($input);

            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'order';
            $input->type = 'text';
            $input->title = 'Порядок вывода';
            $input->placeholder = 'Текст страницы';
            $input->value = html_entity_decode ($page["order"]);
            $input->regexp = "/^\d{1,5}$/m";
            $form->add_input($input);
            unset($input);
            
            
            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'tags';
            $input->title = 'Разделы';
            $input->type = "set";
            $input->regexp = "/^.*$/";
            $_D->search(array("a"=>"tags"),array(),array(),"","`a`.`name` ASC",0,0,array(
                "`a`.`name`"=>"title",
                "`a`.`id`"  =>"value"
            ));
            $tags = $_D->rows;
            
            foreach($tags as $k=>$v){
                $tmp = $this->get_tags_path($v['value']);
                $pp = array();
                foreach($tmp as $t)$pp[] = $t['name'];
                unset($pp[0]);
                $tags[$k]['title'] = implode("/",$pp);
            }
            
            $input->values = $tags;
            $input->value = "0";
            $_D->search(array("a"=>"page_tags"),array(),array("`a`.`page_id`"=>$id));
            foreach($_D->rows as $row)$input->value .= ','.$row['tag_id'];
            $tag_id = $row['tag_id'];
            
            $form->add_input($input);
            unset($input);
            
            
            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'publish_from';
            $input->type = 'date';
            $input->title = 'Начало публикации';
            $input->placeholder = _t('Publish start date');
            $input->value = $page["publish_from"];
            $form->add_input($input);
            unset($input);

            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'publish_to';
            $input->type = 'date';
            $input->title = 'Конец публикации';
            $input->placeholder = _t('Publish end date');
            $input->value = $page["publish_to"];
            $form->add_input($input);
            unset($input);

            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'anons';
            $input->type = 'textarea';
            $input->title = 'Анонс страницы';
            $input->height = '100px';
            $input->placeholder = 'Анонс страницы   ';
            $input->wyswyg = false;
            $input->value = html_entity_decode ($page["anons"]);
            $input->regexp = "/^.*$/m";
            $form->add_input($input);
            unset($input);

            
            $input  = $_SERVER['wirix']->lib_load("html_input");    
            $input->name = 'chpu';                              
            $input->title = 'ЧПУ страницы<br/>(включая начальный "/")';                     
            $input->regexp = "/^\/[\d\w\-\/\.]*$/";                    
            $input->placeholder = 'ЧПУ';           
            $input->comment = '';
            $input->value=$page["chpu"]
                ?
                $page["chpu"]
                :
                $page["chpu"] = $this->get_tag_chpu($tag_id).$_H->translit($page["name"])."/"; 
            $form->add_input($input);    
            unset($input);      
            
            
            $page["seo"] = $this->get_chpu_seo($page["chpu"]);
            
            $input  = $_SERVER['wirix']->lib_load("html_input");    
            $input->name = 'h1';                              
            $input->title = 'H1';                     
            $input->regexp = "/^.{0,255}$/";                    
            $input->placeholder = '';           
            $input->comment = '';
            $input->value=$page["h1"]?$page["h1"]:$page["name"];
            $form->add_input($input);    
            unset($input);      

            $input  = $_SERVER['wirix']->lib_load("html_input");    
            $input->name = 'title';                              
            $input->title = 'Title';                     
            $input->regexp = "/^.{0,255}$/";                    
            $input->placeholder = '';           
            $input->comment = '';
            $input->value=$page["seo"]["title"]?$page["seo"]["title"]:$page["name"];
            $form->add_input($input);    
            unset($input);      


            $input  = $_SERVER['wirix']->lib_load("html_input");    
            $input->name = 'description';                              
            $input->title = 'Description';                     
            $input->regexp = "/^.{0,255}$/";                    
            $input->placeholder = '';           
            $input->comment = '';
            $input->value=$page["seo"]["description"]?$page["seo"]["description"]:htmlspecialchars($page["name"]);
            $form->add_input($input);    
            unset($input);      
            
            $input  = $_SERVER['wirix']->lib_load("html_input");    
            $input->name = 'keywords';                              
            $input->title = 'Keywords';                     
            $input->regexp = "/^.{0,255}$/";                    
            $input->placeholder = '';           
            $input->comment = '';
            $input->value=$page["seo"]["keywords"]?$page["seo"]["keywords"]:htmlspecialchars($page["name"]);
            $form->add_input($input);    
            unset($input);      
            
            
            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'back_url';
            $input->regexp = "/^.*$/";
            $input->type = "hidden";
            $input->value = $_H->get("back_url");
            $form->add_input($input);
            unset($input);

            $form->add_group(3,7,"Дополнительно",false);
            $form->add_group(8,12,"SEO",false);
            $form->submit_url = "/wirix/libs/c/cms_pages/edit_page.ajax.php?id=".$page["id"];
            
            return $form->fetch();
        }


        /**
         * Получение страницы по ID
         */
        function get_page($id = 0){
            global $_W;
            global $_D;
            
            if(!$_D->search_one("pages",array(),"`id`=$id AND (NOW() BETWEEN `publish_from` AND `publish_to`)"))return array();
            $page = $_D->record;
            $page["text"] = html_entity_decode ($page["text"]);
            return $page;
        }


        /**
         * Получение tega по id
         */
        function get_tag($id = 0){
            global $_W;
            global $_D;
            
            if(!$_D->search_one("tags",array(),"`id` IN($id)"))return array();
            $tag = $_D->record;
            return $tag;
        }
        
        /**
            Получение ЧПУ по ID раздела
        */
        function get_tag_chpu($tag_id){
            global $_D;
        
            if($_D->search_one("mod_rewrite",array("to_url"=>"/tags/view/$tag_id/","status"=>200))){
                return $_D->record["from_url"];
            }

            $result = '/';
            $tmp = $this->get_tags_path($tag_id);
            for($i=1,$c=count($tmp);$i<$c;$i++)$result .= $tmp[$i]["folder"].'/';
            return $result;

        }

        /**
            Получение ЧПУ по ID статьи
        */
        function get_page_chpu($page_id){
            global $_D;
            global $_H;

            // Ищем ближайший тег
            if($_D->search_one("page_tags",array("page_id"=>$page_id))){
                $tag_id = $_D->record["tag_id"];
            }

            // Ищем сраницу
            if($_D->search_one("pages",array("id"=>$page_id))){
                $page = $_D->record;
            }

            if($_D->search_one("mod_rewrite",array("to_url"=>"/pages/view/$page_id/","status"=>200))){
                return $_D->record["from_url"];
            }
            
            $result = '/';
            $tmp = $this->get_tags_path($tag_id);
            for($i=1,$c=count($tmp);$i<$c;$i++)$result .= $tmp[$i]["folder"].'/';
            
            $result.=$_H->translit($page["name"])."/";
               
            return $result;

        }
        
        /**
            Получение СЕО-параметров по ЧПУ
        */
        function get_chpu_seo($chpu){
            global $_D;

            if($_D->search_one("seo",array("request_uri"=>$chpu))){
                return $_D->record;
            }
            return false;
        }
        
        /**
            Получение хлебных крошек по ЧПУ (если не указано - по текущему request_uri)
        */
        function get_bread($chpu=''){
            global $_D;
            if(!$chpu)$chpu = $_SERVER["REQUEST_URI"];
            $tmp = explode("?",$chpu);$chpu = $tmp[0];
            
            $parts = explode("/",$chpu);
            $module =   isset($parts[1])?$parts[1]:'';
            $section =  isset($parts[2])?$parts[2]:'';
            $action =   isset($parts[3])?$parts[3]:'';
            $item =     isset($parts[4])?$parts[4]:'';

            switch($module){
                case "tags":
                    switch($section){
                        case "view":
                            if(!intval($action))return false;
                            $path = $this->get_tags_path($action);
                        break;
                    }
                break;
                case "pages":
                    switch($section){
                        case "view":
                            if(!intval($action))return false;
                            if(!$_D->search_one("page_tags",array("page_id"=>$action)))return false;
                            $tag_id = $_D->record["tag_id"];
                            $path = $this->get_tags_path($tag_id);
                            if(!$_D->search_one("tags",array("id"=>$tag_id)))return false;
                            $path[] = array("name"=>$_D->record["name"],"url"=>$this->get_tag_chpu($tag_id));
                        break;
                    }
                break;
            }
            
            if(!$path)return false;
            
            $bread = array();
            foreach($path as $item_num=>$path_item){
                if(!$item_num){
                    $bread[] = array(
                        "name"=>"Главная",
                        "url"=>"/"
                    );
                    continue;
                }
                if($item_num>=count($path)-1)break;
                
                $bread[] = array(
                    "name"=>$path_item["name"],
                    "url"=>$this->get_tag_chpu($path_item["id"])
                );
            }
            
            return $bread;
            
        }
        
        
        /**
            Получение массива главного меню
        */
        function get_main_menu(){
            global $_D;
            
            $_D->search(
                array("a"=>"tags"),
                array(),
                array(),
                "`a`.`menu`='Y' AND `a`.`show`='Y'","`a`.`parent_id` ASC, `a`.`order_by` ASC"
            );
            $rows = $_D->rows;
            $items = array();
            foreach($rows as $row){
                $items[$row["id"]] = $row;
                $items[$row["id"]]["text"] = $row["name"];
                $items[$row["id"]]["url"]   = $this->get_tag_chpu($row["id"]);
                $items[$row["id"]]["childs"] = array();
            }
            foreach($items as $id=>$item){
                if($item["parent_id"]){
                    if(isset($items[$item["parent_id"]]))
                        $items[$item["parent_id"]]["childs"][$id] = $item;
                    unset($items[$id]);
                }
            }

            return $items;
            
        }
        

    }
?>
