<?php

include_once("../../wirix.class.php");
$answer = array();
$answer['redirect'] = "";
$answer['error'] = '';


$form = get_object_vars(json_decode(
    base64_decode(
    $_SESSION['wirix'][
        'wirix_html_form'
    ][
        $_SERVER['http']->post('form_id')
    ]
    )
));

$ins_fields = array(
    "name"=>$_H->post("name"),
    "text"=>$_H->post("text"),
    "anons"=>$_H->post("anons"),
    "publish_from"=>$_H->post("publish_from"),
    "publish_to"=>$_H->post("publish_to")
);

if($page_id = $_D->insert("pages",$ins_fields)){
    $answer['redirect'] = "/cms/pages/edit/$page_id/?back_url=".$_H->post("back_url");
    $answer['success'] = "Запись добавлена";
}
else{
    $answer['error'] = 'Форма не была отправлена. Попробуйте позже';
}

$tag_id = intval($_H->post('tag_id'));
// Добавляем связь с разделом
$ins = array("tag_id"=>$tag_id,"page_id"=>$page_id);
if(!$_D->search_one("page_tags",$ins))
    $_D->insert("page_tags",$ins);

echo json_encode($answer);
?>
