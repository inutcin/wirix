<?php

include_once("../../wirix.class.php");
$answer = array();
$answer['redirect'] = '';
$answer['error'] = '';


$form = get_object_vars(json_decode(
    base64_decode(
    $_SESSION['wirix'][
        'wirix_html_form'
    ][
        $_SERVER['http']->post('form_id')
    ]
    )
));

$ins_fields = array(
    "name"=>$_H->post("name"),
    "parent_id"=>intval($_H->post("parent_id"))
);

if($page_id = $_D->insert("tags",$ins_fields)){
    $answer['redirect'] = "/cms/tags/".intval($_H->post("parent_id"))."/";
    $answer['success'] = "Запись добавлена";
}
else{
    $answer['error'] = 'Форма не была отправлена. Попробуйте позже';
}

echo json_encode($answer);
?>
