<?php

class wirix_svn extends wirix{

    var $url = '';          //!< URL репозитория
    
    public function __construct() {
        parent::__construct(__CLASS__);
    }    
    
    /**
        Создание каталога в репозитории
        $dirname задаётся относительно url репозитория, например для создания
        "file:///home/projects/00001/mydir" при $this->url = "file:///home/projects/00001"
        надо передать "mydir"
        
        Возвращает номер ревизии
    */
    function mkdir($dirname){
        $path = $this->url."/$dirname";
        exec("svn mkdir $path -m 'Creating folder $dirname' 2>&1", $output);
        if(isset($output[1]) && preg_match("/^\s*Committed\s+revision\s+(\d+).*$/", $output[1], $m)){
            return $m[1];
        }
        else{
            $this->error = implode("\n", $output);
            return 0;
        }
    }
    
    /**
        Список элементов каталога в репозитории
        $dirname задаётся относительно url репозитория, например для просмотра
        "file:///home/projects/00001/mydir" при $this->url = "file:///home/projects/00001"
        надо передать "mydir"
        
        Возвращает список элементов
    */
    function ls($dirname){
        $path = $this->url."/$dirname";
        exec($comand = "svn ls $path 2>&1", $output);
        return $output;
    }
    
    /**
        Чекаут указанного каталога репозитория в указанный локальный каталог
        $rep_dirname задаётся относительно url репозитория, например для чекаута
        "file:///home/projects/00001/mydir" при $this->url = "file:///home/projects/00001"
        надо передать "mydir"
        
        Если ревизия не указана - обновляется до текущей
    */
    function co(
        $rep_dirname,
        $local_dirname,
        $revision = ''
    ){
        $path = $this->url."/$rep_dirname";
        exec($comand = "svn co $path $local_dirname ".($revision?"-r ".$revision:"")." 2>&1", $output);
echo "$comand<pre>";
print_r($output);
        return $output;
    }
    
    /**
        Обновление репозитория до определённой ревизии
    */
    function update($dirname, $revision = ''){
        exec($comand = "svn update $dirname ".($revision?"-r ".$revision:"")." 2>&1", $output);
        return $output;
    }
    
    /**
        Получение списка файлов, изменённых в указанную редакцию
        $dirname задаётся относительно url репозитория, например для просмотра
        "file:///home/projects/00001/mydir" при $this->url = "file:///home/projects/00001"
        надо передать "mydir"
    */
    function changes($dirname, $revision = 0){
        $revision = intval($revision);
        $path = $this->url."/$dirname";
        exec($comand = "svn log --xml -v ".($revision?"-r ".$revision:"")." $path 2>&1", $output);
        $output = str_replace("\n", "{break}", implode("", $output));
        if(!preg_match_all("/<path.*?action=\"(.*?)\".*?>(.*?)<\/path>/", $output, $m))return array();
        $result = array();
        foreach($m[0] as $k=>$v)$result[$m[2][$k]] = $m[1][$k];
        return $result;
    }
    
}
