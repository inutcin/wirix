<?php

    class wirix_statistic extends wirix{
        function __construct(){
            parent::__construct(__CLASS__);
        }
        
        /**
            Вычисление тренда
            @param $points - массив точек для вычисления тренда
            @returns Массив array("k"=>"наклон","b"=>"смещение")
        */
        function trend($points){
            $count = count($points);
            if(!$count)return array("k"=>0,"b"=>0);
            
            $s1 = 0;
            foreach($points as $i=>$p)$s1 += $i;
            
            $s2 = 0;
            foreach($points as $i=>$p)$s2 += $i*$p;
                
            $s3 = 0;
            foreach($points as $i=>$p)$s3 += $p;

            $s4 = 0;
            foreach($points as $i=>$p)$s4 += pow($i,2);
            
            if(($count*$s4-pow($s1,2))==0)
                $k=0;
            else
                $k = ($count*$s2 - $s1*$s3)/($count*$s4-pow($s1,2));
            
            $b = ($s3-$k*$s1)/$count;
            
            return array("k"=>$k,"b"=>$b);
        }
    }
