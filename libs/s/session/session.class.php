<?php

class wirix_session extends wirix{

    var $DB;
    var $path;
    var $lifetime = 86400;
    
    public function __construct() {
    
        $this->path = $_SERVER['DOCUMENT_ROOT']."/tmp/sessions/";
    
        ini_set("session.name", "WXSESSION");
        ini_set("session.hash_function", 1);
        ini_set("session.hash_bits_per_character", 6);
        ini_set("session.gc_maxlifetime", 1000000);
        session_set_cookie_params($this->lifetime, '/', ".".$_SERVER['HTTP_HOST']);

        session_set_save_handler(
            array($this, "open"),
            array($this, "close"),
            array($this, "read"),
            array($this, "write"),
            array($this, "destroy"),
            array($this, "gc")
        );

        // the following prevents unexpected effects when using objects as save handlers
        //register_shutdown_function('session_write_close');
        if(!session_id())session_start();
    }
 
    function open($savePath, $sessionName) {
        global $_H;
        if(!$sessid = session_id())
            session_id($sessid = sha1($_SERVER['HTTP_HOST'].time().rand(0,1000000)));

        if(!$_SERVER['db']->search_one("sessions", array("session_id"=>$sessid))){
            $_SERVER['db']->insert(
                "sessions",
                array(
                    "session_id"=>$sessid,
                    "ip"=>ip2long($_SERVER['REMOTE_ADDR'])
                )
            );
        }
        return true;
    }

    public function close() {
        // your code if any
        return true;
    }

    public function read($id) {
        $_SERVER['db']->delete("sessions",array(),"UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(`ctime`)>".$this->lifetime,0);
        $_SERVER['db']->search_one("sessions", array("session_id"=>$id));
        $data = $_SERVER['db']->record['data'];
        return $data;
    }

    public function write($id, $data) {
        $_SERVER['db']->delete("sessions",array(),"UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(`ctime`)>".$this->lifetime,0);
        if(!$_SERVER['db']->link)$_SERVER['db'] = $_SERVER['wirix']->lib_load("db");
        $_SERVER['db']->update("sessions", array("data"=>$data), array("session_id"=>$id));
        //file_put_contents($this->path."sess_$id", $data);
        return true;


        // your code
    }

    public function destroy($id) {
        // your code
    }

    public function gc($maxlifetime) {
        // your code
    }    
}
