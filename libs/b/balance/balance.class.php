<?php

    /**
        Класс для авторизации через логинзу
    */
    class wirix_balance extends wirix{
    
        var $user_id = 0;       
        
        function __construct(){
            parent::__construct();
        } 
        
        /**
            Получение баланса лицевого счета
        */
        function get(){
            if(!$_SERVER['db']->search_one("balance", array("user_id"=>$this->user_id))){
                return '0';
            }
            
            if(!isset($_SERVER['db']->record['sum'])){
                return 0;
            }
            
            return $_SERVER['db']->record['sum'];
        }
        
        /**
            Подсчет баланса за всё время
        */
        function calc(){
            $query = "
                SELECT 
                    SUM(`sum`) as `sum` 
                FROM 
                    `balance_incomes` 
                WHERE 
                    `accept_date`!='0000-00-00 00:00:00' 
                    AND 
                    `cancel_date`='0000-00-00 00:00:00'
                    AND
                    `user_id`=".$this->user_id."
                GROUP BY 
                    `user_id`
                LIMIT 1
            ";
            $res = $_SERVER['db']->sql_query($query, "select");
            $row = $res->fetch_assoc();
            $sum = $row['sum'];
            
            if($_SERVER['db']->search_one("balance", array("user_id"=>$this->user_id))){
                $_SERVER['db']->update("balance", array("sum"=>$sum), array("user_id"=>$this->user_id));
            }
            else{
                $_SERVER['db']->insert("balance", array("sum"=>$sum, "user_id"=>$this->user_id));
            }            
                
        }
        
        /**
            Создание нового платёжного поручения (счета)
            (возвращает номер счета).
        */
        function create_inpayment($payment_system_id = 1, $sum= 10){
            return $_SERVER['db']->insert("balance_incomes",
                array(
                    "user_id"           =>  $this->user_id,
                    "payment_system_id" =>  $payment_system_id,
                    "sum"               =>  $sum
                )
            );
        }
        
        
        
        /**
            Получение информации о платеже по его ID
        */
        function get_inpayment($bill_id){
            if(!$_SERVER['db']->search_one("balance_incomes", array("id"=>$bill_id))){
                return array();
            }
            return $_SERVER['db']->record;
        }

        /**
            Принятие платежа от платёжной системы
        */
        function accept_inpayment($bill_id){
            return $_SERVER['db']->update("balance_incomes",
                array("accept_date"=>date("Y-m-d H:i:s")),
                array("id"          =>  $bill_id)
            );
        }        
        

        /**
            Проверка платежа от платёжной системы
        */
        function check_inpayment(
          $bill_id, 
          $code //!< Код ответа платёжной системы
        ){
            return $_SERVER['db']->update("balance_incomes",
                array("check_date"=>date("Y-m-d H:i:s"),"state"=>$code),
                array("id"          =>  $bill_id)
            );
        }        

        /**
            Естановка для платежа суммы
        */
        function set_payment_sum($bill_id, $sum){
            if(!$sum = floatval($sum))return 0;
            if(!$bill_id = intval($bill_id))return 0;

            $_SERVER['db']->update("balance_incomes",
                array("sum"         =>  $sum),
                array("id"          =>  $bill_id)
            );
            return 1;
        }

        /**
            Установка для платежа аккаунта, с которого заплатили
        */
        function set_payment_sum_account($bill_id, $sum_account){
            if(!$bill_id = intval($bill_id))return 0;

            $_SERVER['db']->update("balance_incomes",
                array("sum_account"         =>  $sum_account),
                array("id"          =>  $bill_id)
            );
            return 1;
        }


        /**
            Установка для платежа валюты, в которой заплатил клиент
        */
        function set_payment_sum_cur($bill_id, $sum_cur){
            if(!$bill_id = intval($bill_id))return 0;

            $_SERVER['db']->update("balance_incomes",
                array("sum_cur"         =>  $sum_cur),
                array("id"          =>  $bill_id)
            );
            return 1;
        }

        /**
            Установка для платежа суммы, попавшей в итоге на кошелёк
        */
        function set_payment_income($bill_id, $income){
            if(!$bill_id = intval($bill_id))return 0;

            $_SERVER['db']->update("balance_incomes",
                array("income"         =>  $income),
                array("id"          =>  $bill_id)
            );
            return 1;
        }

        /**
            Получение информации о платеже по его ID
        */
        function get_payment($payment_id){
            if(!$_SERVER['db']->search_one(
                "balance_incomes", 
                array("id"=>$payment_id)
            ))return array();
            return $_SERVER['db']->record;
        }

        /**
            Получить несколько непроверенных оплат
        */
        function get_unchecked($count = 5){
            if(!$_SERVER['db']->search(
                array("a"=>"balance_incomes"), 
                array(), 
                array(), 
                "`check_date`='0000-00-00 00:00:00' AND `accept_date`!='0000-00-00 00:00:00'", 
                "`ctime` DESC", $count
            ))return array();
            return $_SERVER['db']->rows;
        }


    }
