-- --------------------------------------------------------

--
-- Table structure for table `balance`
--

DROP TABLE IF EXISTS `balance`;
CREATE TABLE IF NOT EXISTS `balance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(11) unsigned NOT NULL COMMENT 'ID пользователя',
  `sum` float(10,4) NOT NULL COMMENT 'Сумма на счете',
  PRIMARY KEY (`id`),
  KEY `ctime` (`ctime`),
  KEY `mtime` (`mtime`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Баланс лицевого счета пользователей' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `balance_incomes`
--

DROP TABLE IF EXISTS `balance_incomes`;
CREATE TABLE IF NOT EXISTS `balance_incomes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(11) unsigned NOT NULL COMMENT 'ID пользователя',
  `sum` float(10,4) NOT NULL COMMENT 'Сумма на счете',
  `tax` float(5,3) NOT NULL COMMENT 'Комиссия посреднику',
  `cancel_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Дата отмены платежа',
  `accept_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Дата подтверждения платежа платёжной системой',
  `check_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Дата проверки платежа',
  PRIMARY KEY (`id`),
  KEY `ctime` (`ctime`),
  KEY `mtime` (`mtime`),
  KEY `user_id` (`user_id`),
  KEY `cancel` (`cancel_date`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Пополнения баланса' AUTO_INCREMENT=1 ;
