<?php
    include_once(dirname(__FILE__)."/../../wirix.class.php");
    
    /**
     * Класс движка сайта(отработка контроллеров и шаблонизаторов)
     */
    $_SERVER['wirix']->lib_load('html');
    class wirix_engine extends wirix_html{
        
        var $compile_templates = '';//!< Путь каталогу с компилированными шаблонами
        var $theme = '';            //!< Тема оформления
        var $status = 200;          //!< Статус страницы
        var $breadcrumbs = null;    //!< Хлебные крошки
        var $cron_lock_timeout = 180;//!< Число секунд, после которых блокировка срон-скрипта снимается по таймауту
        var $cron_clear_timeout = 600;//!< Число секунд, после которых блокировка срон-скрипта удаляется
        var $path = array();
        
        
        function __construct(){
            parent::__construct(__CLASS__);
            if(!$this->theme)$this->theme = "default";
            
            $this->breadcrumbs = $_SERVER['wirix']->lib_load('html_breadcrumbs');
            $this->breadcrumbs->id = 'breadcrumbs';
            $this->breadcrumbs->add("Главная","/");
            
            if(is_dir(__THEMES__."/".$this->theme."/tmpl_c"))
                $this->compile_templates = __THEMES__."/".$this->theme."/tmpl_c";
            else
                $this->compile_templates = __WRX_THEMES__."/".$this->theme."/tmpl_c";


            $_SERVER['permissions'] = $_SERVER['wirix']->lib_load('permissions');
            // Блокируем доступ на просмотр к текущему контроллеру, если пользователь не имеет право на просмотр
            if(!$_SERVER['permissions']->is_access
                (
                    $_SERVER['permissions']->get_route_name(), 
                    isset($_SERVER['auth']->user['groups_ids'])?$_SERVER['auth']->user['groups_ids']:"0"
                )
            ){
                $_SERVER['http']->status(403);
                $this->status = 403;
            }
            
            register_shutdown_function(array($this, "cron"));
        }
        
        /**
            Функция крона от пользователя
        */
        function cron(){
            // Блокируем такое использование cron
            return true;
            $cron_folder_hd38cb85ycddd = $_SERVER['DOCUMENT_ROOT']."/cron";
            if(!is_dir($cron_folder_hd38cb85ycddd))return false;
            $hd38cb85ycddd = opendir($cron_folder_hd38cb85ycddd);
            while($filename_hd38cb85ycddd = readdir($hd38cb85ycddd)){
                //if($filename_hd38cb85ycddd!='load_metrica.cron.php')continue;
                if(!preg_match("/^.*\.cron\.php$/", $filename_hd38cb85ycddd))continue;
                $lockname_hd38cb85ycddd = $filename_hd38cb85ycddd;
                $filename_hd38cb85ycddd = "$cron_folder_hd38cb85ycddd/$filename_hd38cb85ycddd";
                // Удаляем логи сроком более указанного времени
                $_SERVER['db']->delete("cron_locks", array(),
                "UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(`ctime`)>".$this->cron_clear_timeout, 0);
                // Снимаем блокировку у всех просроченных и отмечаем как "прибитые"
                $_SERVER['db']->update("cron_locks", array("mtime"=>date("Y-m-d H:i:s"),"killed"=>1), array(),
                "UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(`ctime`)>".$this->cron_lock_timeout);
                // Проверяем не заблокирован ли этот скрипт
                if($_SERVER['db']->search_one("cron_locks",array("script"=>$lockname_hd38cb85ycddd,"mtime"=>'0000-00-00 00:00:00')))continue;
                // Ставим блокировку исполнения
                $script_id_hd38cb85ycddd = $_SERVER['db']->insert("cron_locks",array("script"=>$lockname_hd38cb85ycddd));
                ob_start();
                if(file_exists($filename_hd38cb85ycddd))include($filename_hd38cb85ycddd); 
                $output = ob_get_contents();
                ob_end_clean();
                echo $output;
                // Снимаем блокировку исполнения
                $_SERVER['db']->update("cron_locks", array(
                    "mtime" =>date("Y-m-d H:i:s"),
                    "output"    => $_SERVER['db']->link->real_escape_string($output)
                ), array("id"=>$script_id_hd38cb85ycddd));
            }
            closedir($hd38cb85ycddd);
        }
        
        // Функция проверки необходимого для запуска движка
        function check(){
            // Проверка наличия каталога компилированных шаблонов
            $_SERVER['error']->check_dir($this->compile_templates."/", true, 0002);            
            // Проверка наличия класса Smarty
            $_SERVER['error']->check_file($smarty_file = dirname(__FILE__)."/../../common/smarty/Smarty.class.php", true, 0004);
            include_once($smarty_file);
            // Проверка наличия класса
            $_SERVER['error']->check_class("Smarty");
        }
        
        /**
         * Исполнение кода контроллеров
        */
        function exec($check = true){
            
            global $_W;
            global $_H;
            global $_D;
            global $_L;
            global $_A;

            $_SERVER['http']->get_post_clean();
      
            // Признак наличия нужной страницы
            $page_exists = false;
      
            if($check && $this->check()){
                echo error::message("Движок не может быть запущен из-за ошибок.");
            }
            
            if(!isset($_SESSION['wirix'])){
                $_SESSION['wirix'] = array();
                $_SESSION['wirix']['theme'] = 'default';
            }
            else{
            }
            if($this->status==403)return false;
      
            if($_D->search_one("seo",array("request_uri"=>isset($_SERVER["REQUEST_URI_ORIGIN"])?$_SERVER["REQUEST_URI_ORIGIN"]:$_SERVER["REQUEST_URI"]))){
                $this->title        = $_D->record['title'];
                $this->description  = $_D->record['description'];
                $this->keywords     = $_D->record['keywords'];
            }
            
            $this->path = $this->path();
            if(file_exists($filename = __ACTS__."/before.php"))
                include($filename);
            elseif(file_exists($filename = __WRX_ACTS__."/before.php"))
                include($filename);
            
            // Подключение контроллеров-префиксов
            foreach($this->path as $path)
                if(file_exists($filename = __ACTS__.$path."/before.php")){
                    include($filename);
                    $page_exists |= true;
                }
                elseif(file_exists($filename = __WRX_ACTS__.$path."/before.php")){
                    include($filename);
                    $page_exists |= true;
                }
                
            $rpath = array_reverse($this->path);
            // Подключение контроллеров-постфиксов
            foreach($rpath as $path)
                if(file_exists($filename = __ACTS__.$path."/after.php")){
                    include($filename);
                    $page_exists |= true;
                }
                elseif(file_exists($filename = __WRX_ACTS__.$path."/after.php")){
                    include($filename);
                    $page_exists |= true;
                }
                    
            if(file_exists($filename = __ACTS__."/after.php"))
                include($filename);
            elseif(file_exists($filename = __WRX_ACTS__."/after.php"))
                include($filename);
            
            $this->data['breadcrumbs'] = $this->breadcrumbs->fetch();
            if(!isset($this->data['h1']) || !$this->data['h1'])
                $this->data['h1'] = $this->breadcrumbs->last()->title;
                    
            // 404-я
            if($_SERVER['REQUEST_URI']!='/' && !$page_exists){
                $this->status = 404;
                if(file_exists($filename = __ACTS__."/404.php"))
                    include($filename);
                elseif(file_exists($filename = __WRX_ACTS__."/404.php"))
                    include($filename);
                $_SERVER['http']->status(404);
            }
            
            session_write_close();
            
        }
        
        function display(){
            // magic
            $html = $this->fetch_content();
            $length = strlen($html);
            header('Connection: close');
            //header("Content-Length: " . $length);
            header("Content-Encoding: none");
            header("Accept-Ranges: bytes");
            echo $html;
            ob_flush();
            flush();
            return $this->status;
        }
        
        function fetch_content(){
            
            $html = '';
            
            // Подключаем CSS файл из каталога темы
            $css_files = '/themes/'.$this->theme.'/tmpl/css/style.css';
            if(file_exists(__DR__.$css_files))
                $this->css_files[] = $css_files;

            // Подключаем CSS файл из каталога темы
            $js_files = '/themes/'.$this->theme.'/tmpl/js/script.js';
            if(file_exists(__DR__.$js_files))
                $this->js_scripts[] = $js_files;

            // Выводим заголовок html-страницы
            $html .= $this->page_head();

            $this->data['head']['theme'] = $this->theme; 

            $smarty = new Smarty();
            $smarty->compile_dir = $this->compile_templates;
            $this->data['http'] = $_SERVER['http'];
            $smarty->assign("data", $this->data);
            

            if(file_exists(__THEMES__."/".$this->theme."/tmpl/before.html"))
                $smarty->template_dir = __THEMES__."/".$this->theme."/tmpl";
            elseif(file_exists(__WRX_THEMES__."/default/tmpl/before.html"))
                $smarty->template_dir = __WRX_THEMES__."/default/tmpl";
            
            if($this->status==404){
                $html .= $smarty->fetch("404.html");
                return $html;
            }
            elseif($this->status==403){
                $html .= $smarty->fetch("403.html");
                return $html;
            }
            
            $html .= $smarty->fetch("before.html");
            
            // Подключение шаблонов-префиксов
            foreach($this->path as $path)
                if(file_exists(__THEMES__."/".$this->theme."/tmpl".$path."/before.html")){
                    $smarty->template_dir = __THEMES__."/".$this->theme."/tmpl";
                    $path = preg_replace("/^\/(.*)$/","$1",$path);
                    $html .= $smarty->fetch($path."/before.html");
                }
                elseif(file_exists(__WRX_THEMES__."/default/tmpl".$path."/before.html")){
                    $smarty->template_dir = __WRX_THEMES__."/default/tmpl";
                    $path = preg_replace("/^\/(.*)$/","$1",$path);
                    $html .= $smarty->fetch($path."/before.html");
                }
                //else
                //    echo $_SERVER['error']->alert("Нет шаблона <b>before.html</b> для пути <b>$path</b>");
            

            $rpath = array_reverse($this->path);
            // Подключение шаблонов-постфиксов
            foreach($rpath as $path)
                if(file_exists(__THEMES__."/".$this->theme."/tmpl".$path."/after.html")){
                    $smarty->template_dir = __THEMES__."/".$this->theme."/tmpl";
                    $path = preg_replace("/^\/(.*)$/","$1",$path);
                    $html .= $smarty->fetch($path."/after.html");
                }
                elseif(file_exists(__WRX_THEMES__."/default/tmpl".$path."/after.html")){
                    $smarty->template_dir = __WRX_THEMES__."/default/tmpl";
                    $path = preg_replace("/^\/(.*)$/","$1",$path);
                    $html .= $smarty->fetch($path."/after.html");
                } 
                //else
                //    echo $_SERVER['error']->alert("Нет шаблона <b>after.html</b> для пути <b>$path</b>");
                


            if(file_exists(__THEMES__."/".$this->theme."/tmpl/after.html"))
                $smarty->template_dir = __THEMES__."/".$this->theme."/tmpl";
            elseif(file_exists(__WRX_THEMES__."/default/tmpl/after.html"))
                $smarty->template_dir = __WRX_THEMES__."/".$this->theme."/tmpl";

            $html .= $smarty->fetch("after.html");
            
            // Выводим хвостовик html-страницы
            $html .= $this->page_footer();
	    $html .='<!-- Memory usage='.round(memory_get_peak_usage()/1024/1024,2).'MiБ  -->';
            return $html;
        }
        
        /**
         * Получение url-пути
         */
        private function path(){
            $path = explode("/", $_SERVER['REQUEST_URI']);
            unset($path[0]);
            $tmp = explode("?", $path[count($path)]);
            $path[count($path)] = $tmp[0];
            if(!trim($path[count($path)]))unset($path[count($path)]);
            $cpath = '';
            foreach($path as $k=>$p){
                $cpath .= "/$p";
                $path[$k] = $cpath;
            }
            
            return $path;
        }
        
        /**
         * Получение html-кода таблицы правил
         */
        function fetch_rules_table(){
            global $_W;

            $table = $_W->lib_load("html_db_table");
            $table->id = $this->id."_list_table";
            $table->title = "Правила SEO-тегов";
            $table->show_search_form = false;

            $table->tables = array("a"=>"seo");
            $column = array();
            $column['title'] = 'REQUEST_URI';
            $column['url'] =   '$row["request_uri"]';
            $column['text'] =   '$row["request_uri"]';
            $column['align'] = 'left';
            $table->add_column($column);
            unset($column);

            $table->tables = array("a"=>"seo");
            $column = array();
            $column['title'] = 'Title';
            $column['text'] =   '$row["title"]';
            $column['align'] = 'left';
            $table->add_column($column);
            unset($column);

            $table->tables = array("a"=>"seo");
            $column = array();
            $column['title'] = 'Description';
            $column['text'] =   '$row["description"]';
            $column['align'] = 'left';
            $table->add_column($column);
            unset($column);

            $table->tables = array("a"=>"seo");
            $column = array();
            $column['title'] = 'Keywords';
            $column['text'] =   '$row["keywords"]';
            $column['align'] = 'left';
            $table->add_column($column);
            unset($column);

            $column = array();
            $column['title'] = 'Править';
            $column['url'] =   '"/cms/seo/edit/".$row["id"]."/"';
            $column['icon'] =   '"icon-pencil"';
            $column['width'] =   '10px';
            $column['align'] =   'center';
            $table->add_column($column);
            unset($column);

            $column = array();
            $column['title'] = 'Удалить';
            $column['url'] =   '"/cms/seo/delete/".$row["id"]."/"';
            $column['icon'] =   '"icon-trash"';
            $column['width'] =   '10px';
            $column['align'] =   'center';
            $column['onclick'] =   '"return confirm(\'Точно удалить?\')"';
            $table->add_column($column);
            unset($column);


            $table->search();
            
            return $table->fetch();
        }


        function fetch_edit_form($id){
            global $_W;
            global $_D;
            
            if(!$_D->search_one("seo",array("id"=>$id)))return '';
            $rule = $_D->record;
            
            $form = $_W->lib_load("html_form");
            $form->id = $this->id."_add_form";
            $form->title = "Редактирование SEO-параметров";
            $form->use_back_button = false;
            $form->send_button_text = 'Редактировать';
            
            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'request_uri';
            $input->title = 'REQUEST_URI';
            $input->regexp = "/^.+$/";
            $input->placeholder = '';
            $input->value = $rule["request_uri"];
            $form->add_input($input);
            unset($input);

            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'title';
            $input->title = 'Title';
            $input->regexp = "/^.+$/";
            $input->placeholder = '';
            $input->value = $rule["title"];
            $form->add_input($input);
            unset($input);

            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'description';
            $input->title = 'Description';
            $input->regexp = "/^.+$/";
            $input->placeholder = '';
            $input->value = $rule["description"];
            $form->add_input($input);
            unset($input);

            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'keywords';
            $input->title = 'Keywords';
            $input->regexp = "/^.+$/";
            $input->placeholder = '';
            $input->value = $rule["keywords"];
            $form->add_input($input);
            unset($input);

            $form->submit_url = "/wirix/libs/e/engine/edit_rule.ajax.php?id=".$rule["id"];
            
            return $form->fetch();
            
        }

        function fetch_add_form(){
            global $_W;
            
            $form = $_W->lib_load("html_form");
            $form->id = $this->id."_add_form";
            $form->title = "Добавление SEO-параметров";
            $form->use_back_button = false;
            $form->send_button_text = 'Редактировать';
            
            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'request_uri';
            $input->title = 'REQUEST_URI';
            $input->regexp = "/^.+$/";
            $input->placeholder = '';
            $form->add_input($input);
            unset($input);

            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'title';
            $input->title = 'Title';
            $input->regexp = "/^.+$/";
            $input->placeholder = '';
            $form->add_input($input);
            unset($input);

            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'description';
            $input->title = 'Description';
            $input->regexp = "/^.+$/";
            $input->placeholder = '';
            $form->add_input($input);
            unset($input);

            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'keywords';
            $input->title = 'Keywords';
            $input->regexp = "/^.+$/";
            $input->placeholder = '';
            $form->add_input($input);
            unset($input);

            $form->submit_url = "/wirix/libs/e/engine/add_rule.ajax.php";
            
            return $form->fetch();
            
        }


        
    }
?>
