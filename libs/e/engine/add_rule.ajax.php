<?php

include_once("../../wirix.class.php");
$answer = array();
$answer['redirect'] = '';
$answer['error'] = '';


$form = get_object_vars(json_decode(
    base64_decode(
    $_SESSION['wirix'][
        'wirix_html_form'
    ][
        $_SERVER['http']->post('form_id')
    ]
    )
));

$ins_fields = array(
    "request_uri"=>$_H->post("request_uri"),
    "title"=>$_H->post("title"),
    "description"=>$_H->post("description"),
    "keywords"=>$_H->post("keywords"),
    "auto"=>"N"
);

$id = intval($_H->get('id'));

if($page_id = $_D->insert("seo",$ins_fields)){
    $answer['redirect'] = "/cms/seo/";
    $answer['success'] = "Запись добавлена";
}
else{
    $answer['error'] = "Запись не добавлена";
}

echo json_encode($answer);
?>
