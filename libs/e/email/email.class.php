<?php
    include_once(dirname(__FILE__)."/../../wirix.class.php");
    
    /**
     * Класс работы с почтой
     */
    class wirix_email extends wirix{
        
        var $from_email     = '';
        var $to_email       = '';
        var $from_title     = '';
        var $to_title       = '';
        var $subject        = '';
        var $charset        = 'utf-8';
        var $attachments    = array();      //!< Массив имён прикрепляемых файлов, каждый элемент array("name"=>"...", "path"=>'...')
        var $content_type   = 'text/plain'; //!< Тип тела сообщения
        var $cc_email       = '';
        var $cc_title       = '';
        var $return_path_email = '';
        var $return_path_title = '';
        
        function __construct(){
            parent::__construct(__CLASS__);
        }
        
        function send($body){
            
            $from_title = base64_encode(mb_convert_encoding($this->from_title, "utf-8", $this->charset));
            $to_title = base64_encode(mb_convert_encoding($this->to_title, "utf-8", $this->charset));
            $cc_title = $from_title;
            if($this->cc_title)
                $cc_title = base64_encode(mb_convert_encoding($this->cc_title, "utf-8", $this->charset));
            $return_path_title = $from_title;
            if($this->return_path_title)
                $return_path_title = base64_encode(mb_convert_encoding($this->return_path_title, "utf-8", $this->charset));
                
            if(!$this->cc_email)$this->cc_email = $this->from_email;
            if(!$this->return_path_email)$this->return_path_email = $this->from_email;
            
            $subject = "=?UTF-8?B?".base64_encode(mb_convert_encoding($this->subject, "utf-8", $this->charset))."?=";

            $boundary = md5(time().rand());

            $headers =  "From: =?UTF-8?B?$from_title?= <".$this->from_email.">\r\n";
            $headers .= "Cc: =?UTF-8?B?$cc_title?= <".$this->cc_email.">\r\n";
            $headers .= "Return-path: =?UTF-8?B?$return_path_title?= <".$this->return_path_email.">\r\n";
            $headers .= "Content-Type: multipart/mixed; boundary=\"".$boundary."\"\n";

            $body = chunk_split(base64_encode($body));

            $hbody  = "\r\n--".$boundary."\r\n";
            $hbody .= "Content-type: ".$this->content_type."; charset=\"".$this->charset."\"\r\n";
            $hbody .= "MIME-Version: 1.0\r\n";
            $hbody .= "Content-Transfer-Encoding: base64\r\n\r\n";
            
            $body = $hbody.$body;
            
            foreach($this->attachments as $file){
                $body .= "\r\n--".$boundary."\r\n";
                $body .= "Content-Type: application/octet-stream\r\n";
                $body .= "Content-Transfer-Encoding: base64\r\n";
                $body .= "Content-Disposition: attachment; filename = \"".$file['name']."\"\r\n\r\n";
                $body .= chunk_split(base64_encode(file_get_contents($file['path'])));
            }
            
            if(!mail(
                "=?UTF-8?B?$to_title?= <".$this->to_email.">",
                $subject,
                $body,
                $headers    
            )){
                $this->error = _t("Email error");
                return false;
            }
            
            return true;
        }
        
        /**
            Добавление вложения
        */
        function add_attachment($name, $path){
            if(!file_exists($path))return false;
            $this->attachments[] = array(
                "name"=>$name,
                "path"=>$path
            );
            return true;
        }
    }
?>
