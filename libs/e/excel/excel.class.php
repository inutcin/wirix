<?php
    include_once(dirname(__FILE__)."/../../wirix.class.php");
    include_once(dirname(__FILE__)."/excel_reader/reader.php");
    
    /**
     * Класс работы с excell
     
        Пример использования


        $excel = $_SERVER['wirix']->lib_load("excel");
        $excel->filename = 'Файл.xls';
        $excel->set_data("Лист1", "B2", "ляля");
        $excel->set_fontface("Лист1", "B2", "Arial Cyr"); 
        $excel->set_fontsize("Лист1", "B2", 12); 
        $excel->set_fontbold("Лист1", "B2",true);
        $excel->set_color("Лист1", "B2", "FF0000");
        $excel->set_fill("Лист1", "B2", "88FF88");
        $excel->set_halign("Лист1", "B2", "center");
        $excel->set_valign("Лист1", "B2", "top");
        $excel->set_borders("Лист1", "B2", array("left"=>"00000","right"=>"0000FF","top"=>"FFFF00"));
        $excel->output();
     
     */
    $_SERVER['wirix']->lib_load('html');
    class wirix_excel extends wirix{
        
        var $charset = "cp1251";
        var $data = null;
        var $sheets = array();
        var $max_rows = 500;
        var $max_cols = 100;
        
        var $filename = 'filename.xls';
        private $cells_data = array();
        private $wrap = array();
        private $urls = array();
        private $cells_fontface = array();
        private $cells_fontsize = array();
        private $cells_fontbold = array();
        private $cells_valign = array();
        private $cells_halign = array();
        private $cells_color = array();
        private $cells_fill = array();
        private $cells_borders = array();
        private $width = array();
        private $height = array();
        private $merges = array();
        
        // Шрифт по умолчанию
        var $default_fontname = 'Sans Serif';
        // Размер шрифта по умолчанию
        var $default_fontsize = '10';
        
        function __construct(){
            error_reporting(E_ALL ^ E_NOTICE);        
        }
        
        /**
            Загрузка EXCEL
        */
        function load($filename){
            $this->data = new Spreadsheet_Excel_Reader($filename, true, $this->charset);
            $this->sheets = $this->data->boundsheets;
            foreach($this->sheets as $k=>$sheet)$this->sheets[$k]['name'] = mb_convert_encoding($sheet['name'], "utf-8", $this->charset);
        }

        /**
            Установка гиперссылки
            @param $sheet - лист, например "sheet1"
            @param $cell Адрес ячейки вида 'C2','D4' и т.п.
            @param $url - гиперссылка
        */
        function set_url($sheet, $cell, $url){
            if(!isset($this->urls[$sheet]))$this->urls[$sheet] = array();            $this->wrap[$sheet][$cell] = $wrap;
            $this->urls[$sheet][$cell] = $url;
        }

        /**
            Установка переноса текста для ячейки
            @param $sheet - лист, например "sheet1"
            @param $cell Адрес ячейки вида 'C2','D4' и т.п.
            @param $wrap - переносить true/false
        */
        function set_wrap($sheet, $cell, $wrap){
            if(!isset($this->wrap[$sheet]))$this->wrap[$sheet] = array();
            $this->wrap[$sheet][$cell] = $wrap;
        }
        
        /**
            Установка данных для ячейки
            @param $sheet - лист, например "sheet1"
            @param $cell Адрес ячейки вида 'C2','D4' и т.п.
            @param $data - данные
        */
        function set_data($sheet, $cell, $data){
            if(!isset($this->cells_data[$sheet]))$this->cells_data[$sheet] = array();
            $this->cells_data[$sheet][$cell] = $data;
        }

        /**
            Установка бордеров ячейки
            @param $sheet - лист, например "sheet1"
            @param $cell Адрес ячейки вида 'C2','D4' и т.п.
            @param $borders - массив цветов бордеров вида array("top"=>"EEEEEE","bottom"=>"000000","left"=>"111222", "right"=>"de9a56")
        */
        function set_borders($sheet, $cell, $borders){
            if(!isset($this->cells_borders[$sheet]))$this->cells_borders[$sheet] = array();
            $this->cells_borders[$sheet][$cell] = $borders;
        }


        /**
            Установка имени шрифта для ячейки
            @param $sheet - лист, например "sheet1"
            @param $cell Адрес ячейки вида 'C2','D4' и т.п.
            @param $fontface - имя шрифта, например 'Arial Cyr'
        */
        function set_fontface($sheet, $cell, $fontface){
            if(!isset($this->cells_fontface[$sheet]))$this->cells_fontface[$sheet] = array();
            $this->cells_fontface[$sheet][$cell] = $fontface;
        }

       /**
            Установка размера шрифта для ячейки
            @param $sheet - лист, например "sheet1"
            @param $cell Адрес ячейки вида 'C2','D4' и т.п.
            @param $fontsize - размер шрифта
        */
        function set_fontsize($sheet, $cell, $fontsize){
            if(!isset($this->cells_fontsize[$sheet]))$this->cells_fontsize[$sheet] = array();
            $this->cells_fontsize[$sheet][$cell] = $fontsize;
        }

       /**
            Установка жЫрности шрифта для ячейки
            @param $sheet - лист, например "sheet1"
            @param $cell Адрес ячейки вида 'C2','D4' и т.п.
            @param $bold - жирность (true/false)
        */
        function set_fontbold($sheet, $cell, $bold){
            if(!isset($this->cells_fontbold[$sheet]))$this->cells_fontbold[$sheet] = array();
            $this->cells_fontbold[$sheet][$cell] = $bold;
        }

        /**
            Установка выравнивания для ячейки по горизонтали
            @param $sheet - лист, например "sheet1"
            @param $cell Адрес ячейки вида 'C2','D4' и т.п.
            @param $halign - выравнивание: "center", "left", "right"
        */
        function set_halign($sheet, $cell, $halign){
            if(!isset($this->cells_halign[$sheet]))$this->cells_halign[$sheet] = array();
            $this->cells_halign[$sheet][$cell] = $halign;
        }

        /**
            Установка выравнивания для ячейки по вертикали
            @param $sheet - лист, например "sheet1"
            @param $cell Адрес ячейки вида 'C2','D4' и т.п.
            @param $valign - выравнивание: "top", "middle", "bottom"
        */
        function set_valign($sheet, $cell, $valign){
            if(!isset($this->cells_valign[$sheet]))$this->cells_valign[$sheet] = array();
            $this->cells_valign[$sheet][$cell] = $valign;
        }

        /**
            Установка цвета для ячейки
            @param $sheet - лист, например "sheet1"
            @param $cell Адрес ячейки вида 'C2','D4' и т.п.
            @param $color - RGB, например "FC67DA"
        */
        function set_color($sheet, $cell, $color){
            if(!isset($this->cells_color[$sheet]))$this->cells_color[$sheet] = array();
            $this->cells_color[$sheet][$cell] = $color;
        }

        /**
            Установка заливки для ячейки
            @param $sheet - лист, например "sheet1"
            @param $cell Адрес ячейки вида 'C2','D4' и т.п.
            @param $color - RGB, например "FC67DA"
        */
        function set_fill($sheet, $cell, $color){
            if(!isset($this->cells_fill[$sheet]))$this->cells_fill[$sheet] = array();
            $this->cells_fill[$sheet][$cell] = $color;
        }

        /**
            Устанавливаем ширину колонки
            @param $sheet - лист, например "sheet1"
            @param $colnum - Буквенный индекс колонки
            @param $width - ширина колонки в странных единицах. Имоговая ширина будет примерно $width*10 пикселей
        */
        function set_width($sheet, $colnum, $width){
            if(!isset($this->width[$sheet]))$this->width[$sheet] = array();
            $this->width[$sheet][$colnum] = $width;
        }

        /**
            Устанавливаем высоту строки
            @param $sheet - лист, например "sheet1"
            @param $rownum - индекс строки
            @param $height - высота строки
        */
        function set_height($sheet, $colnum, $height){
            if(!isset($this->height[$sheet]))$this->height[$sheet] = array();
            $this->height[$sheet][$colnum] = $height;
        }

        /**
            Объединяет ячейки
            @param $sheet - лист, например "sheet1"
            @param $cells - диапазон ячеек, например "A1:C2"
        */
        function add_merge($sheet, $cells){
            if(!isset($this->merges[$sheet]))$this->merges[$sheet] = array();
            $this->merges[$sheet][] = $cells;
        }

        /**
            Добавление картинки
        */
        function add_image(
            $sheet_name,
            $filename, 
            $cell="A1", 
            $offset_x=0,
            $offset_y=0,
            $width = 0,
            $height = 0
        ){
            if(!isset($this->images[$sheet_name]))
                $this->images[$sheet_name] = array();
                
            $this->images[$sheet_name][] = array(
                "filename"  =>  $filename,
                "cell"      =>  $cell,
                "offset_x"  =>  $offset_x,
                "offset_y"  =>  $offset_y,
                "width"     =>  $width,
                "height"    =>  $height
            );
        }
        
        
        function show(){
            include("show.inc.php");
        }
        
        function output(){
            
            //подключаем и создаем класс PHPExcel
            include_once 'Classes/PHPExcel.php';
            $pExcel = new PHPExcel();
            // Выстаиваем индексы
            $sheet_indexes = array();
            $k = 0;
            foreach($this->cells_data as $sheet_name=>$sheet){
                $sheet_indexes[$sheet_name] = $k;
                $k++;
            }
            
            foreach($this->cells_data as $sheet_name=>$sheet){
                $pExcel->setActiveSheetIndex($sheet_indexes[$sheet_name]);
                $aSheet = $pExcel->getActiveSheet();
                $aSheet->getDefaultStyle()->getFont()->setName($this->default_fontname);
                $aSheet->getDefaultStyle()->getFont()->setSize($this->default_fontsize);

                if($this->images[$sheet_name]){
                    include_once 'Classes/PHPExcel/Worksheet/Drawing.php';
                    
                    foreach($this->images[$sheet_name] as $image){
                        $img = new PHPExcel_Worksheet_Drawing();
                        
                        $img->setPath($image['filename']);
                        $img->setCoordinates($image['cell']);             
                        $img->setOffsetX($image['offset_x']);
                        $img->setOffsetY($image['offset_y']);    
                        if(isset($image['width']) && $image['width'])$img->setWidth($image['width']);
                        if(isset($image['height']) && $image['height'])$img->setHeight($image['height']);    
                        $img->setWorksheet($aSheet);
                        
                        unset($img);
                    }
                }


                // Проставляем ширину колонок
                if(is_array($this->width[$sheet_name]))foreach($this->width[$sheet_name] as $colnum=>$width){
                    $aSheet->getColumnDimension($colnum)->setWidth($width);
                }
                
                // Проставляем высоту строк
                if(is_array($this->height[$sheet_name]))foreach($this->height[$sheet_name] as $colnum=>$height){
                    $aSheet->getRowDimension($colnum)->setRowHeight($height);
                }

                // Объединяем ячейки
                if(is_array($this->merges[$sheet_name]))foreach($this->merges[$sheet_name] as $cells)
                    $aSheet->mergeCells($cells);

                $aSheet->setTitle($sheet_name);
                foreach($sheet as $cell=>$value){
                    
                    if(isset($this->cells_color[$sheet_name][$cell]) && $this->cells_color[$sheet_name][$cell])
                        $aSheet->getStyle($cell)->getFont()->getColor()->setRGB($this->cells_color[$sheet_name][$cell]);
                        
                    if(isset($this->cells_fill[$sheet_name][$cell]) && $this->cells_fill[$sheet_name][$cell]){
                        $aSheet->getStyle($cell)->getFill()->getStartColor()->setRGB($this->cells_fill[$sheet_name][$cell]);
                        $aSheet->getStyle($cell)->getFill()->getEndColor()->setRGB($this->cells_fill[$sheet_name][$cell]);
                        $aSheet->getStyle($cell)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                    }


                    if(isset($this->cells_fontface[$sheet_name][$cell]))
                        $aSheet->getStyle($cell)->getFont()->setName($this->cells_fontface[$sheet_name][$cell]);

                    if(isset($this->wrap[$sheet_name][$cell]))
                        $aSheet->getStyle($cell)->getAlignment()->setWrapText($this->wrap[$sheet_name][$cell]);

                    if(isset($this->cells_fontsize[$sheet_name][$cell]))
                        $aSheet->getStyle($cell)->getFont()->setSize($this->cells_fontsize[$sheet_name][$cell]);
                        
                    if(isset($this->cells_fontbold[$sheet_name][$cell]))
                        $aSheet->getStyle($cell)->getFont()->setBold($this->cells_fontbold[$sheet_name][$cell]);

                    if(isset($this->urls[$sheet_name][$cell])){
                        
                        $aSheet->getCell($cell)->getHyperlink()->setUrl($this->urls[$sheet_name][$cell]);
                        $aSheet->getStyle($cell)->getFont()->getColor()->setRGB('0000FF');
                    }


                    if(isset($this->cells_halign[$sheet_name][$cell])){
                        switch($this->cells_halign[$sheet_name][$cell]){
                            case 'left':
                                $halign = PHPExcel_Style_Alignment::HORIZONTAL_LEFT;
                            break;
                            case 'right':
                                $halign = PHPExcel_Style_Alignment::HORIZONTAL_RIGHT;
                            break;
                            case 'justify':
                                $halign = PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY;
                            break;
                            case 'center':
                                $halign = PHPExcel_Style_Alignment::HORIZONTAL_CENTER;
                            break;
                            default:
                                $halign = PHPExcel_Style_Alignment::HORIZONTAL_GENERAL;
                            break;
                        }
                        $aSheet->getStyle($cell)->getAlignment()->setHorizontal($halign);
                    }

                    if(isset($this->cells_valign[$sheet_name][$cell])){
                        switch($this->cells_valign[$sheet_name][$cell]){
                            case 'top':
                                $valign = PHPExcel_Style_Alignment::VERTICAL_TOP;
                            break;
                            case 'bottom':
                                $valign = PHPExcel_Style_Alignment::VERTICAL_BOTTOM;
                            break;
                            case 'justify':
                                $valign = PHPExcel_Style_Alignment::VERTICAL_JUSTIFY;
                            break;
                            case 'middle':
                                $valign = PHPExcel_Style_Alignment::VERTICAL_CENTER;
                            break;
                            default:
                                $valign = PHPExcel_Style_Alignment::VERTICAL_BOTTOM;
                            break;
                        }
                        $aSheet->getStyle($cell)->getAlignment()->setVertical($valign);
                    }

                    if(isset($this->cells_borders[$sheet_name][$cell])){
                        if(isset($this->cells_borders[$sheet_name][$cell]['left'])){
                            $aSheet->getStyle($cell)->getBorders()->getLeft()->getColor()->setRGB($this->cells_borders[$sheet_name][$cell]['left']);
                            $aSheet->getStyle($cell)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                        }
                            
                        if(isset($this->cells_borders[$sheet_name][$cell]['right'])){
                            $aSheet->getStyle($cell)->getBorders()->getRight()->getColor()->setRGB($this->cells_borders[$sheet_name][$cell]['right']);
                            $aSheet->getStyle($cell)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                        }

                        if(isset($this->cells_borders[$sheet_name][$cell]['top'])){
                            $aSheet->getStyle($cell)->getBorders()->getTop()->getColor()->setRGB($this->cells_borders[$sheet_name][$cell]['top']);
                            $aSheet->getStyle($cell)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                        }

                        if(isset($this->cells_borders[$sheet_name][$cell]['bottom'])){
                            $aSheet->getStyle($cell)->getBorders()->getBottom()->getColor()->setRGB($this->cells_borders[$sheet_name][$cell]['bottom']);
                            $aSheet->getStyle($cell)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                        }
                    }
                    
                    $aSheet->setCellValue($cell, $value);
                }
            }
            include("Classes/PHPExcel/Writer/Excel5.php");
            $objWriter = new PHPExcel_Writer_Excel5($pExcel);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="'.$this->filename.'"');
            header('Cache-Control: max-age=0');
            $objWriter->save('php://output');
            
        }
    }
