<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<script src=""></script>
<link rel="stylesheet" href="/wirix/libs/e/excel/css/show.css" type="text/css"/>
<body>
<style>
    .excel{
        font-size: <?php echo $this->default_fontsize;?>pt !important;
        font-family: <?php echo $this->default_fontname;?>!important;
    }
</style>
<?php

    $max_rows = $this->max_rows;
    $max_cols = $this->max_cols;

    $sheet = array_pop($this->cells_data);
    $borders = array_pop($this->cells_borders);
    $haligns = array_pop($this->cells_halign);
    $valigns = array_pop($this->cells_valign);
    $widths = array_pop($this->width);
    $heights = array_pop($this->height);
    $cells_fills = array_pop($this->cells_fill);
    $cells_colors = array_pop($this->cells_color);
    $merges = array_pop($this->merges);
    $images = array();
    if(is_array($this->images))$images = array_pop($this->images);
    $cells_fontsize = array_pop($this->cells_fontsize);
    $cells_fontface = array_pop($this->cells_fontface);
    $cells_fontbold = array_pop($this->cells_fontbold);
    
    $merges_table = array();
    $merges_skip = array();
    if(is_array($merges))foreach($merges as $merge){
        list($start_m, $end_m) = explode(":",$merge);
        $merges_table[$start_m] = array();
        if(!preg_match("/^(\w+?)(\d+)$/", $start_m, $m))continue;
        $row = $m[2];
        $st = -1;
        for($i=0;$i<$max_cols;$i++){
            if(get_col_name($i).$row==$start_m)$st = $i;
        }
        $en = -1;
        for($i=0;$i<$max_cols;$i++){
            if(get_col_name($i).$row==$end_m)$en = $i;
        }
        
        for($i=$st+1;$i<=$en;$i++){
            $merges_table[$start_m][get_col_name($i).$row]=1;
            $merges_skip[get_col_name($i).$row] = 1;
        }
        
    }
    
    $images_cell = array();
    foreach($images as $image){
        $images_cell[$image['cell']] = array(
            "url"   =>  str_replace($_SERVER['DOCUMENT_ROOT'],"", $image['filename']),
            "x"     =>  $image['offset_x'],
            "y"     =>  $image['offset_y']
        );
    }
?>
    <div class="excel">
    <div class="watermark">
        Макет
    </div>
    <div class="layout">&#160;</div>
        
    <table>
        <?php
        ?>
        <tr>
            <th>&#160;</th>
            <?php for($i=0;$i<$max_cols;$i++){?>
            <th
                style="<?php
                echo isset($widths[get_col_name($i)])?"width: ".($widths[get_col_name($i)]*10)."px;":"";
                ?>"
            ><?php
                echo get_col_name($i);
            ?></th>
            <?php }?>
        </tr>
        <?php for($row=0;$row<$max_rows;$row++){?>
        <tr>
            <th><?php echo ($row+1);?></th>
            <?php for($col=0;$col<$max_cols;$col++){?>
            <?php
                $celname = get_col_name($col).($row+1);
                if($merges_skip[$celname])continue;
            ?>
            <td <?php 
                if(isset($merges_table[$celname])){
                    echo 'colspan="'.(count($merges_table[$celname])+1).'"';
                }?>
                style="<?php
                echo isset($borders[$celname]['bottom'])?"border-bottom: 1px #".$borders[$celname]['bottom']." solid;":"";
                echo isset($borders[$celname]['top'])?"border-top: 1px #".$borders[$celname]['top']." solid;":"";
                echo isset($borders[$celname]['left'])?"border-left: 1px #".$borders[$celname]['left']." solid;":"";
                echo isset($borders[$celname]['right'])?"border-right: 1px #".$borders[$celname]['right']." solid;":"";
                echo isset($haligns[$celname])?"text-align: ".$haligns[$celname].";":"";
                echo isset($valigns[$celname])?"vertical-align: ".$valigns[$celname].";":"";
                echo isset($widths[get_col_name($col)])?"width: ".($widths[get_col_name($col)]*6.4)."pt;":"";
                echo isset($cells_fills[$celname])?"background-color: #".($cells_fills[$celname]).";":"";
                ?>"
            >
                <?php
                    $celname = get_col_name($col).($row+1);
                    echo '<div class="cell" style="'.
                        (
                        isset($widths[get_col_name($col)])
                        ?
                        "width: ".($widths[get_col_name($col)]*6.4)."px;"
                        :
                        ""
                        ).
                        (
                        isset($heights[$row+1])
                        ?
                        "height: ".($heights[$row+1])."pt;"
                        :
                        ""
                        ).
                        (
                        isset($cells_fontsize[$celname])
                        ?
                        "font-size: ".$cells_fontsize[$celname]."pt;"
                        :
                        ""
                        ).
                        (
                        isset($cells_fontface[$celname])
                        ?
                        "font-family: ".$cells_fontface[$celname].";"
                        :
                        ""
                        ).
                        (
                        isset($cells_fontbold[$celname])
                        ?
                        "font-weight: bold;"
                        :
                        ""
                        ).
                        (
                        isset($haling[$celname])
                        ?
                        "text-align: ".$haling[$celname].";"
                        :
                        ""
                        ).
                        (
                        isset($cells_colors[$celname])
                        ?
                        "color: #".$cells_colors[$celname].";"
                        :
                        ""
                        ).
                       
                       
                        '">'.(isset($sheet[$celname])?str_replace("\n","<br/>",$sheet[$celname]):"").
                        
                        (
                            isset($images_cell[$celname])
                            ?
                            '<img class="pic" src="'.$images_cell[$celname]['url'].'" style="margin-left:'.$images_cell[$celname]['x'].'px;margin-top:'.$images_cell[$celname]['y'].'px;">'
                            :
                            ''
                        ).
                        
                        '</div>';
                            
                ?>
            </td>
            <?php }?>
        </tr>
        <?php }?>
    </table>
    </div>
</body>
</html>
<?php

function get_col_name($i){
    $alphabet = array(
        "A","B","C","D","E","F","G","H","I","J","K","L","M",
        "N","O","P","Q","R","S","T","U","V","W","X","Y","Z"
    );
    return (
        isset($alphabet[floor($i / count($alphabet))-1])
        ?
        $alphabet[floor($i / count($alphabet))-1]
        :
        ""
    )
    .
    $alphabet[$i % count($alphabet)];
}

