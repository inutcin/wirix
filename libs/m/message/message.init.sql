DROP TABLE IF EXISTS `messages`;
CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `from` int(11) NOT NULL COMMENT 'От кого',
  `to` int(11) NOT NULL COMMENT 'Кому',
  `read_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Дата прочтения',
  `reply_id` int(11) NOT NULL COMMENT 'ID сообщения на которое отвечаем',
  `deleted` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'Удалено',
  PRIMARY KEY (`id`),
  KEY `ctime` (`ctime`),
  KEY `mtime` (`mtime`),
  KEY `from` (`from`),
  KEY `to` (`to`),
  KEY `reply_id` (`reply_id`),
  KEY `deleted` (`deleted`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Группа' AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `message_bodies`;
CREATE TABLE IF NOT EXISTS `message_bodies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `message_id` int(11) NOT NULL COMMENT 'ID сообщение',
  `body` longtext NOT NULL COMMENT 'Тело сообщения',
  `file_id` int(11) NOT NULL COMMENT 'Приложенный файл',
  PRIMARY KEY (`id`),
  KEY `ctime` (`ctime`),
  KEY `mtime` (`mtime`),
  KEY `file_id` (`file_id`),
  KEY `message_id` (`message_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Группа' AUTO_INCREMENT=1 ;
