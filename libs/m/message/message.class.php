<?php

class wirix_message extends wirix{

    var $from = 0;      //!< Отправитель
    var $to = 0;        //!< Получатель
    var $reply_message_id = 0;  //!< ID сообщения на которое отвечаем
    var $to_user_email = 0;     //!< Email получателя
    var $limit = 30;            //!< Сообщений на страницу

    public function __construct() {
        parent::__construct(__CLASS__);
        $this->from = isset($_SERVER['auth']->user['id'])?$_SERVER['auth']->user['id']:0;
    }
    
    /**
        Отправка сообщения
        @param $body - ID пользователя, которому отправляется сообщение
        @param $file_upload_array - массив из $_FILES, например $_FILES['attach']
        
        @returns Возвращает ID добавленного сообщения
    */
    function send($body, $file_upload_array = array()){
        // Проверка наличия получателя
        if(!intval($this->to)){
            $this->error = _t("Recipient is empty");
            return false;
        }
        // Проверка наличия такого пользователя
        if(!$to_user = $_SERVER['db']->search_one("users", array("id"=>$this->to))){
            $this->error = _t("Recipient is not valid user");
            return false;
        }
        // Проверка наличия отправителя
        if(!intval($this->from)){
            $this->error = _t("Recipient is empty");
            return false;
        }
        // Проверка наличия такого отправителя
        if(!$from_user = $_SERVER['db']->search_one("users", array("id"=>$this->from))){
            $this->error = _t("Recipient is not valid user");
            return false;
        }

        $message_id = $_SERVER['db']->insert("messages", array(
            "from"      =>  $this->from,
            "to"        =>  $this->to,
            "reply_id"  =>  $this->reply_message_id
        ));
        
        $_SERVER['db']->insert("message_bodies", array(
            "message_id"    =>  $message_id,
            "body"          =>  $_SERVER['db']->link->real_escape_string($body)
        ));

        if(isset($file_upload_array['error'])){
            foreach($file_upload_array['error'] as $k=>$v){
                
                $ua = array();
                foreach($file_upload_array as $key=>$value)
                    $ua[$key] = $file_upload_array[$key][$k];
                
                $file_obj = $_SERVER['wirix']->lib_load("file");
                if(!$file_id = $file_obj->upload($ua)){
                    $this->error = $file_obj->error;
                    unset($file_obj);
                    continue;
                }
                unset($file_obj);
                
                $return = $_SERVER['db']->insert("message_bodies", array(
                    "message_id"    =>  $message_id,
                    "body"          =>  "",
                    "file_id"       =>  $file_id
                ));
            }
        }
        
        if($message_id){
            return $message_id;
        }
        else{
            $this->error = $_SERVER['db']->error;
            return false;
        }
    }
    
    /**
        Получение списка непрочитанных сообщений
        
        @param $user_id - ID пользователя, для которого получаем список непрочитанных
        @param @offset - смещение, с которого начинается список сообщений, количество сообщений на страницу определчется $this->
    */
    function unread($user_id, $offset=0){
        return $this->message_list($user_id, $type='unread', $offset);
    }
    

    /**
        Получение списка отправленных сообщений
        
        @param $user_id - ID пользователя, для которого получаем список отправлненных
        @param @offset - смещение, с которого начинается список сообщений, количество сообщений на страницу определчется $this->
    */
    function sent($user_id, $offset=0){
        return $this->message_list($user_id, $type='sent', $offset);
    }

    /**
        Получение списка полученных сообщений
        
        @param $user_id - ID пользователя, для которого получаем список полученных
        @param @offset - смещение, с которого начинается список сообщений, количество сообщений на страницу определчется $this->
    */
    function inbox($user_id, $offset=0){
        return $this->message_list($user_id, $type='inbox', $offset);
    }


    /**
        Получение списка удалённых сообщений
        
        @param $user_id - ID пользователя, для которого получаем список полученных
        @param @offset - смещение, с которого начинается список сообщений, количество сообщений на страницу определчется $this->
    */
    function deleted($user_id, $offset=0){
        return $this->message_list($user_id, $type='deleted', $offset);
    }
    
    /**
        Получение списка сообщений
        
        @param $user_id - ID пользователя, для которого получаем список непрочитанных
        @param $type - тип сообщений 'unread'. Unread, если не указано
    */
    private function message_list($user_id, $type='', $offset=0){
        if(!$type)$type = 'unread';
        
        switch($type){
            case 'unread':
                $search = array("`a`.`to`"=>$user_id);
                $search["`a`.`read_date`"] = '0000-00-00 00:00:00';
                $search["`a`.`deleted`"]=0;
            break;
            case 'inbox':
                $search = array("`a`.`to`"=>$user_id);
                $search["`a`.`deleted`"]=0;
            break;
            case 'sent':
                $search = array("`a`.`from`"=>$user_id);
                $search["`a`.`deleted`"]=0;
            break;
            case 'deleted':
                $search = array("`a`.`to`"=>$user_id);
                $search["`a`.`deleted`"]=1;
            break;
        }
        
        $_SERVER['db']->search(
            array(
                "a"=>"messages",
                "b"=>"user_prop_values",
                "c"=>"user_prop_values",
                "d"=>"users",
                "e"=>"users"
            ),
            array(
                "`a`.`from`=`b`.`user_id` AND `b`.`user_prop_id`=2"=>"LEFT",
                "`a`.`to`=`c`.`user_id` AND `c`.`user_prop_id`=2"=>"LEFT",
                "`a`.`from`=`d`.`id`"=>"LEFT",
                "`a`.`to`=`e`.`id`"=>"LEFT",
            ),
            $search, "", "`a`.`ctime` DESC", $this->limit, $offset,
            array(
                "`a`.`id`"          =>  "id",
                "`a`.`ctime`"       =>  "date",
                "`a`.`read_date`"   =>  "read_date",
                "`a`.`reply_id`"    =>  "reply_id",
                "`a`.`from`"        =>  "from_id",
                "`b`.`value`"       =>  "from_fio",
                "`a`.`to`"          =>  "to_id",
                "`c`.`value`"       =>  "to_fio",
                "`d`.`name`"        =>  "from_username",
                "`e`.`name`"        =>  "to_username"
            )
        );
        return array("messages"=>$_SERVER['db']->rows, "pages"=>$_SERVER['db']->pages);
    }
    
    /**
        Просмотр сообщения
        @param $message_id - ID сообщения
        @param $user_id - ID пользователя, открвающего письмо
        @param $set_view_date - отмечать сообщение как прочитанное: 1 - если открывающий получатель, 0 - не отмечать
    */
    function view($message_id, $user_id, $set_view_date = 1){
        
        global $_D;
        
        $_D->search(
            array(
                "a"=>"messages",
                "b"=>"message_bodies",
                "c"=>"users",
                "d"=>"users",
                "e"=>"user_prop_values",
                "f"=>"user_prop_values"
            ),
            array(
                "`a`.`id`=`b`.`message_id`" =>"LEFT",
                "`a`.`from`=`c`.`id`"       =>"LEFT",
                "`a`.`to`=`d`.`id`"         =>"LEFT",
                "`a`.`from`=`e`.`user_id` AND `e`.`user_prop_id`=2"  =>"LEFT",
                "`a`.`to`=`f`.`user_id` AND `f`.`user_prop_id`=2"  =>"LEFT"
            ),
            array(
                "`a`.`id`"=>intval($message_id)
            ),"","",1,0,
            array(
                "`a`.`id`"          =>  "id",
                "`a`.`ctime`"       =>  "date",
                "`a`.`read_date`"   =>  "read_date",
                "`a`.`deleted`"     =>  "deleted",
                "`a`.`reply_id`"    =>  "reply_id",
                "`b`.`body`"        =>  "body",
                "`b`.`file_id`"     =>  "file_id",
                "`a`.`from`"        =>  "from_id",
                "`a`.`to`"          =>  "to_id",
                "`c`.`name`"        =>  "from_username",
                "`d`.`name`"        =>  "to_username",
                "`e`.`value`"       =>  "from_fio",
                "`f`.`value`"       =>  "to_fio",
            )
        );
        
        $message = $_SERVER['db']->rows;
        if(isset($message[0]))$message = $message[0];
        
        // Получаем все файлы сообщения
        $_D->search(
            array("a"=>"message_bodies"),
            array(),
            array(),
            "`a`.`message_id`=$message_id AND `a`.`file_id`!=0"
        );
        
        $message['files'] = array();
        foreach($_D->rows as $file){
            $file_obj = $_SERVER['wirix']->lib_load("file");
            $file_info = $file_obj->get_file_info($file['file_id']);
            $file_info['size_h']    = $file_obj->human_size_format($file_info['size']);
            $file_info['pic']       = $file_obj->get_mime_icon($file_info['mime'], 16);
            $message['files'][] = $file_info;
            unset($file_obj);
        }
        
        if($set_view_date && $message['to_id']==$user_id){
            $_SERVER['db']->update(
                "messages",
                array("read_date"=>date("Y-m-d H:i:s")),
                array("id"=>$message_id)
            );
        }
        
        return $message;
    }
 
    /**
        Удаление сообщения
    */
    function remove($message_id){
        $_SERVER['db']->update(
            "messages",
            array("deleted"=>1),
            array("id"=>$message_id)
        );
    }
    
    /**
        Статистика по сообщениям для пользователя
    */
    function statistic($user_id){
        
        $user_id = intval($user_id);
        
        $_SERVER['db']->search(
            array("a"=>"messages"),
            array(),
            array(
            ),"`a`.`to`=$user_id AND `a`.`read_date`='0000-00-00 00:00:00' AND `a`.`deleted`='0'","",0,0,
            array("count(`a`.`id`)"=>"count")
        );
        
        $result = array();
        
        $rows = $_SERVER['db']->rows;
        $result['unread'] = isset($rows[0]['count'])?$rows[0]['count']:0;

        $_SERVER['db']->search(
            array("a"=>"messages"),
            array(),
            array(
            ),"`a`.`to`=$user_id AND `a`.`deleted`='0' AND `a`.`read_date`='0000-00-00 00:00:00'","",0,0,
            array("count(`a`.`id`)"=>"count")
        );

        $rows = $_SERVER['db']->rows;
        $result['inbox'] = isset($rows[0]['count'])?$rows[0]['count']:0;
        

        $_SERVER['db']->search(
            array("a"=>"messages"),
            array(),
            array(
            ),"`a`.`from`=$user_id AND `a`.`deleted`='0' AND `a`.`read_date`='0000-00-00 00:00:00'","",0,0,
            array("count(`a`.`id`)"=>"count")
        );

        $rows = $_SERVER['db']->rows;
        $result['sent'] = isset($rows[0]['count'])?$rows[0]['count']:0;

        $_SERVER['db']->search(
            array("a"=>"messages"),
            array(),
            array(
            ),"`a`.`to`=$user_id AND `a`.`deleted`='1' AND `a`.`read_date`='0000-00-00 00:00:00'","",0,0,
            array("count(`a`.`id`)"=>"count")
        );

        $rows = $_SERVER['db']->rows;
        $result['deleted'] = isset($rows[0]['count'])?$rows[0]['count']:0;

        return $result;
    }
    
    /** 
        Выводит древовидную историю сообщений между двумя пользователями
    */
    function history($from_id, $to_id, $offset){
        $from_id    = intval($from_id);
        $to_id      = intval($to_id);
        $_SERVER['db']->search(
            array(
                "a"=>"messages",
                "b"=>"user_prop_values",
                "c"=>"user_prop_values",
                "d"=>"users",
                "e"=>"users"
            ),
            array(
                "`a`.`from`=`b`.`user_id` AND `b`.`user_prop_id`=2"=>"LEFT",
                "`a`.`to`=`c`.`user_id` AND `c`.`user_prop_id`=2"=>"LEFT",
                "`a`.`from`=`d`.`id`"=>"LEFT",
                "`a`.`to`=`e`.`id`"=>"LEFT",
            ),
            array(),
            "
            `a`.`deleted` =  '0'    
            AND
            (
                (
                    `a`.`from`  =  $from_id
                    AND
                    `a`.`to`    =  $to_id
                )
                OR
                (
                    `a`.`to`    =  $from_id
                    AND
                    `a`.`from`  =  $to_id
                )
            )
            ",
            "`a`.`ctime` ASC",$this->limit, $offset,
            array(
                "`a`.`id`"          =>  "id",
                "`a`.`ctime`"       =>  "date",
                "`a`.`read_date`"   =>  "read_date",
                "`a`.`reply_id`"    =>  "reply_id",
                "`a`.`from`"        =>  "from_id",
                "`b`.`value`"       =>  "from_fio",
                "`a`.`to`"          =>  "to_id",
                "`c`.`value`"       =>  "to_fio",
                "`d`.`name`"        =>  "from_username",
                "`e`.`name`"        =>  "to_username"
            )
        );
        
        $messages = $_SERVER['db']->rows;
        return array("messages"=> $messages, "pages"=>$_SERVER['db']->pages);
    }
 
}
