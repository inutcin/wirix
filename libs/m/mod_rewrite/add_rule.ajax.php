<?php

include_once("../../wirix.class.php");
$answer = array();
$answer['redirect'] = '';
$answer['error'] = '';


$form = get_object_vars(json_decode(
    base64_decode(
    $_SESSION['wirix'][
        'wirix_html_form'
    ][
        $_SERVER['http']->post('form_id')
    ]
    )
));

$ins_fields = array(
    "from_url"=>$_H->post("from_url"),
    "to_url"=>$_H->post("to_url"),
    "status"=>intval($_H->post("status")),
    "regex"=>intval($_H->post("regex")),
);

if($page_id = $_D->insert("mod_rewrite",$ins_fields)){
    $answer['redirect'] = "/cms/mod_rewrite/";
    $answer['success'] = "Запись добавлена";
}
else{
    $answer['error'] = "Запись не изменена";
}

echo json_encode($answer);
?>
