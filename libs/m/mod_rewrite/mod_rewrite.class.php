<?php

class wirix_mod_rewrite extends wirix{

    public function __construct() {
        parent::__construct(__CLASS__);
        global $_D;
        
        $ru = $_SERVER['REQUEST_URI'];
        // Если адрес надо перекинуть 301-й
        if($_D->search_one("mod_rewrite",array("to_url"=>$ru,"regex"=>0,"status"=>200))){
            $this->uri_action(301, $_D->record["from_url"]);
        }
        // Если есть адрес не из регулярок
        elseif($_D->search_one("mod_rewrite",array("from_url"=>$ru,"regex"=>0))){
            $uri = $_D->record;
            $this->uri_action($uri["status"], $uri["to_url"]);
        }
        // Перебираем регулярки
        elseif($_D->search(array("a"=>"mod_rewrite"),array(),array("regex"=>1))){
            $regexps = $_D->rows;
            foreach($regexps as $regexp){
                $pattern = "#".$regexp["from_url"]."#i";
                if(preg_match($pattern,$ru)){
                    $new_uri = preg_replace($pattern,$regexp["to_url"], $ru);
                    $this->uri_action($regexp["status"], $new_uri);
                }
            }
        }
        
        
        
    }

    private function uri_action($status, $url){
        switch($status){
            case 301:
                header("HTTP/1.1 301 Moved Permanently");
                header("Location: ".$url);
                die;
            break;
            case 302:
                header("Location: ".$url);
                die;
            break;
            default:
                $_SERVER['REQUEST_URI_ORIGIN'] = $_SERVER['REQUEST_URI'];
                $_SERVER['REQUEST_URI'] = $url;
                $tmp = explode("?",$url);
                $tmp = $tmp[count($tmp)-1];
                $_SERVER['QUERY_STRING'] = $tmp;
                $tmp = explode("&",$tmp);
                foreach($tmp as $v){
                    $t = explode("=",$v);
                    if(count($t)<=1)
                        $_GET[urldecode($t[0])] = '';
                    else
                        $_GET[urldecode($t[0])] = urldecode($t[1]);
                }
            break;
        }
    }


        /**
         * Получение html-кода таблицы правил
         */
        function fetch_rules_table(){
            global $_W;

            $table = $_W->lib_load("html_db_table");
            $table->id = $this->id."_list_table";
            $table->title = "Правила переписи адресов";
            $table->show_search_form = false;

            $table->tables = array("a"=>"mod_rewrite");
            $column = array();
            $column['title'] = 'Куда переходим';
            $column['url'] =   '$row["from_url"]';
            $column['text'] =   '$row["from_url"]';
            $column['align'] = 'left';
            $table->add_column($column);
            unset($column);
            
            $table->tables = array("a"=>"mod_rewrite");
            $column = array();
            $column['title'] = 'Куда попадаем';
            $column['url'] =   '$row["to_url"]';
            $column['text'] =   '$row["to_url"]';
            $column['align'] = 'left';
            $table->add_column($column);
            unset($column);

            $table->tables = array("a"=>"mod_rewrite");
            $column = array();
            $column['title'] = 'HTTP-статус';
            $column['text'] =   '$row["status"]';
            $column['align'] = 'center';
            $table->add_column($column);
            unset($column);

            $table->tables = array("a"=>"mod_rewrite");
            $column = array();
            $column['title'] = 'Регулярное выражение';
            $column['icon'] =   '($row["regex"]?"icon-ok":"")';
            $column['align'] = 'center';
            $table->add_column($column);
            unset($column);

            $column = array();
            $column['title'] = 'Править';
            $column['url'] =   '"/cms/mod_rewrite/edit/".$row["id"]."/"';
            $column['icon'] =   '"icon-pencil"';
            $column['width'] =   '10px';
            $column['align'] =   'center';
            $table->add_column($column);
            unset($column);

            $column = array();
            $column['title'] = 'Удалить';
            $column['url'] =   '"/cms/mod_rewrite/delete/".$row["id"]."/"';
            $column['icon'] =   '"icon-trash"';
            $column['width'] =   '10px';
            $column['align'] =   'center';
            $column['onclick'] =   '"return confirm(\'Точно удалить?\')"';
            $table->add_column($column);
            unset($column);


            $table->search();
            
            return $table->fetch();
        }


        function fetch_edit_form($id){
            global $_W;
            global $_D;
            
            if(!$_D->search_one("mod_rewrite",array("id"=>$id)))return '';
            $rule = $_D->record;
            
            $form = $_W->lib_load("html_form");
            $form->id = $this->id."_add_form";
            $form->title = "Редактирование ЧПУ";
            $form->use_back_button = false;
            $form->send_button_text = 'Редактировать';
            
            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'from_url';
            $input->title = 'Куда переходим';
            $input->regexp = "/^.+$/";
            $input->placeholder = '';
            $input->value = $rule["from_url"];
            $form->add_input($input);
            unset($input);

            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'to_url';
            $input->title = 'Куда попадаем';
            $input->regexp = "/^.+$/";
            $input->placeholder = '';
            $input->value = $rule["to_url"];
            $form->add_input($input);
            unset($input);

            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'regex';
            $input->type = 'checkbox';
            $input->title = 'Обрабатывать как регулярное выражение';
            $input->regexp = "/^.+$/";
            $input->placeholder = '';
            $input->value = $rule["regex"];
            $form->add_input($input);
            unset($input);

            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'status';
            $input->type = 'select';
            $input->title = 'Код ответа сервера';
            $input->regexp = "/^.+$/";
            $input->placeholder = '';
            $input->value = $rule["status"];
            $input->values = array(
                array("title"=>"200(ок)","value"=>200),
                array("title"=>"301(постоянный редирект)","value"=>301),
                array("title"=>"302(временный редирект)","value"=>302),
                array("title"=>"403(доступ закрыт)","value"=>403)
            );
            $form->add_input($input);
            unset($input);

            $form->submit_url = "/wirix/libs/m/mod_rewrite/edit_rule.ajax.php?id=".$rule["id"];
            
            return $form->fetch();
            
        }


        function fetch_add_form(){
            global $_W;
            global $_D;
            
            $form = $_W->lib_load("html_form");
            $form->id = $this->id."_add_form";
            $form->title = "Добавление ЧПУ";
            $form->use_back_button = false;
            $form->send_button_text = 'Редактировать';
            
            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'from_url';
            $input->title = 'Куда переходим';
            $input->regexp = "/^.+$/";
            $input->placeholder = '';
            $form->add_input($input);
            unset($input);

            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'to_url';
            $input->title = 'Куда попадаем';
            $input->regexp = "/^.+$/";
            $input->placeholder = '';
            $form->add_input($input);
            unset($input);

            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'regex';
            $input->type = 'checkbox';
            $input->title = 'Обрабатывать как регулярное выражение';
            $input->placeholder = '';
            $form->add_input($input);
            unset($input);

            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'status';
            $input->type = 'select';
            $input->title = 'Код ответа сервера';
            $input->regexp = "/^\d+$/";
            $input->placeholder = '';
            $input->values = array(
                array("title"=>"200(ок)","value"=>200),
                array("title"=>"301(постоянный редирект)","value"=>301),
                array("title"=>"302(временный редирект)","value"=>302),
                array("title"=>"403(доступ закрыт)","value"=>403)
            );
            $form->add_input($input);
            unset($input);

            $form->submit_url = "/wirix/libs/m/mod_rewrite/add_rule.ajax.php";
            
            return $form->fetch();
            
        }
    
}
