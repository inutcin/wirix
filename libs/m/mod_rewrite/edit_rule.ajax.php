<?php

include_once("../../wirix.class.php");
$answer = array();
$answer['redirect'] = '';
$answer['error'] = '';


$form = get_object_vars(json_decode(
    base64_decode(
    $_SESSION['wirix'][
        'wirix_html_form'
    ][
        $_SERVER['http']->post('form_id')
    ]
    )
));

$ins_fields = array(
    "from_url"=>$_H->post("from_url"),
    "to_url"=>$_H->post("to_url"),
    "status"=>intval($_H->post("status")),
    "regex"=>intval($_H->post("regex")),
);

$id = intval($_H->get('id'));

if($page_id = $_D->update("mod_rewrite",$ins_fields,array("id"=>$id))){
    $answer['redirect'] = "/cms/mod_rewrite/";
    $answer['success'] = "Запись отредактирована";
}
else{
    $answer['redirect'] = "/cms/mod_rewrite/";
    $answer['success'] = "Запись не изменена";
}

echo json_encode($answer);
?>
