SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
DROP TABLE IF EXISTS `mod_rewrite`;
CREATE TABLE IF NOT EXISTS `mod_rewrite` (`id` int(11) NOT NULL,  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,  `mtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',  `from_url` char(255) DEFAULT 'noname' COMMENT 'URL, который переписываем',  `to_url` char(255) NOT NULL COMMENT 'URL на который переписываем',  `status` smallint(5) NOT NULL DEFAULT '200' COMMENT 'Код ответа',  `regex` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Ищем по регулярному выражению') ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Правила переписи URL'  AUTO_INCREMENT=1 ;
ALTER TABLE `mod_rewrite` ADD PRIMARY KEY (`id`), ADD KEY `ctime` (`ctime`), ADD KEY `mtime` (`mtime`), ADD KEY `to_url` (`to_url`), ADD KEY `from_url` (`from_url`), ADD KEY `status` (`status`), ADD KEY `regexp` (`regex`);
ALTER TABLE `mod_rewrite` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
