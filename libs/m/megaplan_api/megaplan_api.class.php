<?php
    
    include("Request.php");

    class wirix_megaplan_api extends wirix{
        
        var $account        = 'none';
        protected $login          = 'username';
        protected $md5password    = 'password';
        protected $row_data       = '';
        protected $access_id  = '';
        private $secret_key = '';
        var $user_id = 0;
        protected $timeout = 10;
        protected $use_ssl = true;
        
        var $megaplan_domain = "megaplan.ru";
        var $data;
        
        function __construct(){
            parent::__construct(__CLASS__);
        }
        
        
        function post_comment(
            $SubjectType    //!< task (задача), project (проект), contractor (клиент), deal (сделка)
            ,$SubjectId     //!< ID комментируемого объекта
            ,$Text
            ,$Work
        ){
            $params = array(
                "SubjectType"   =>  $SubjectType,
                "SubjectId"     =>  $SubjectId,
                "Model[Text]"   =>  $Text,
                "Model[Work]"   =>  $Work
            );
            $answer = $this->request("/BumsCommonApiV01/Comment/create.api",$params);
            if($this->error)return false;
            return true;
        }
        
        function get_project($project_id){
            $params = array(
                "Id"=>$project_id
            );
            $answer = $this->request("/BumsProjectApiV01/Project/card.api",$params);
            if($this->error)return array();
            
            $project = array();
            if(is_object($answer) && property_exists($answer, "project"))
                $project = $answer->project;
            unset($answer);
            
            return $project;
        }
        
        function get_task_card($task_id){
            $params = array(
                "Id"=>$task_id
            );
            $answer = $this->request("/BumsTaskApiV01/Task/card.api",$params);
            $task = array();
            if(is_object($answer) && property_exists($answer, "task"))
                $task = $answer->task;
            unset($answer);
            
            return $task;
        }
        
        function get_comments(
            $SubjectType    //!< task (задача), project (проект), contractor (клиент), deal (сделка)
            ,$SubjectId     //!< ID комментируемого объекта
            ,$Order = 'desc' //!< Направление сортировки по дате 
            ,$Limit =   50
            ,$Offset = 0
        ){
            $params = array(
                "SubjectType"   =>  $SubjectType,
                "SubjectId"     =>  $SubjectId,
                "Limit"         =>  $Limit,
                "Offset"        =>  $Offset,
                "Order"    => urlencode($Order)
            );
            $answer = $this->request("/BumsCommonApiV01/Comment/list.api",$params);
            if($this->error)return array();
            $comments = array();
            if(is_object($answer) && property_exists($answer, "comments") && is_array($answer->comments))
                $comments = $answer->comments;
                
            foreach($comments as $k=>$v){
                $comments[$k]->SubjectId = $SubjectId;
                $t = date_parse($comments[$k]->TimeCreated);
                $t['month'] = $t['month']<10?'0'.$t['month']:$t['month'];
                $t['day'] = $t['day']<10?'0'.$t['day']:$t['day'];
                $t['hour'] = $t['hour']<10?'0'.$t['hour']:$t['hour'];
                $t['minute'] = $t['minute']<10?'0'.$t['minute']:$t['minute'];
                $t['second'] = $t['second']<10?'0'.$t['second']:$t['second'];
                $comments[$k]->created = $t['year'].'-'.$t['month'].'-'.$t['day']." ".$t['hour'].":".$t['minute'].":".$t['second'];
            }
            unset($answer);
            return $comments;
            
        }

            
        function get_all_comments(
            $OnlyActual=false,
            $Limit=50, 
            $Offset=0, 
            $SubjectType = "task"   // task (задача), project (проект), contractor (клиент), deal (сделка)
            ,$SortBy = 'id' // Сортировка id (идентификатор), name (наименование), activity (активность), deadline (дата дедлайна), responsible (ответственный), owner (постановщик), contractor (заказчик), start (старт), plannedFinish (плановый финиш), plannedWork (запланировано), actualWork (отработано), completed (процент завершения), bonus (бонус), fine (штраф), plannedTime (длительность)
            ,$SortOrder = 'desc' //asc (по возрастанию), desc (по убыванию)
            
        ){
            $params = array(
                "OnlyActual"    =>  $OnlyActual,
                "SubjectType"   =>  $SubjectType,
                "Limit"         =>  $Limit,
                "Offset"        =>  $Offset,
                "SortBy"    => urlencode($SortBy),
                "SortOrder" => urlencode($SortOrder)
            );
            $answer = $this->request("/BumsCommonApiV01/Comment/all.api",$params);
            if($this->error)return array();

            $comments = array();
            if(is_object($answer) && property_exists($answer, "comments") && is_array($answer->comments))
                $comments = $answer->comments;
            unset($answer);
            
            return $comments;
            
        }
        
        function get_tasks(
            $Folder = 'all', //!< incoming (входящие), responsible (ответственный), executor (соисполнитель), owner (исходящие), auditor (аудируемые), all (все)
            $Status = 'any', //!< actual (актуальные), inprocess (в процессе), new (новые), overdue (просроченные), done (условно завершенные), delayed (отложенные), completed (завершенные), failed (проваленные), any (любые)
            $FavoritesOnly = 0, //!< Только избранное
            $Search = '',       //!< 	 Строка поиска
            $Detailed = false, //!<  Нужно ли показывать в списке задач все поля из карточки задачи
            $OnlyActual = false //!< Если true, то будут выводиться только незавершенные задачи
            ,$TimeUpdated = ''  //!< Возвращать только те объекты, которые были изменены после указанный даты
            ,$SortBy = 'id' //!< Сортировка id (идентификатор), name (наименование), activity (активность), deadline (дата дедлайна), responsible (ответственный), owner (постановщик), contractor (заказчик), start (старт), plannedFinish (плановый финиш), plannedWork (запланировано), actualWork (отработано), completed (процент завершения), bonus (бонус), fine (штраф), plannedTime (длительность)
            ,$SortOrder = 'desc' //!< asc (по возрастанию), desc (по убыванию)
            ,$Limit
            ,$Offset
        ){
            /*
            $params = "Folder=".urlencode($Folder)
                ."&Status=".urlencode($Status)
                ."&FavoritesOnly=".urlencode($FavoritesOnly)
                ."&Search=".urlencode($Search)
                ."&Detailed=".urlencode($Detailed)
                ."&OnlyActual=".urlencode($OnlyActual)
            ;
            */
            
            $params = array(
                "Folder" => urlencode($Folder),
                "Status"=> urlencode($Status),
                "FavoritesOnly"=> urlencode($FavoritesOnly),
                "Search"=> urlencode($Search),
                "Detailed"=> urlencode($Detailed),
                "OnlyActual"=> urlencode($OnlyActual),
                "Limit" => $Limit,
                "Offset" => $Offset
            );
            
            if($TimeUpdated)$params["TimeUpdated"]=$TimeUpdated;
            if($SortBy)$params["SortBy"]=urlencode($SortBy);
            if($SortOrder)$params["SortOrder"]=urlencode($SortOrder);
            
            $answer = $this->request("/BumsTaskApiV01/Task/list.api",$params);
            if($this->error)return array();
                
            
            $tasks = array();
            if(is_object($answer) && property_exists($answer, "tasks") && is_array($answer->tasks))
                $tasks = $answer->tasks;
            unset($answer);
            
            $result = array();
            foreach($tasks as $task)$result[] = get_object_vars($task);
            foreach($result as $k=>$v)
                foreach($v as $k1=>$v1)
                    if(is_object($v1))$result[$k][$k1] = get_object_vars($v1);
                    
            $return = array();
            foreach($result as $k=>$v)
                $return[$v['Id']] = $v;
            
            return $return;
        }
        
        function auth(){
            global $_H;
            
            $_H->timeout= $this->timeout;

            $parameters = array(
                "Login"     =>  $this->login,
                "Password"  =>  $this->password
            );
            
            $request = "";
            foreach($parameters as $k=>$v)$request .= urlencode($k)."=".urlencode($v)."&";
            
            $url = 
                ($this->use_ssl?"https":"http")."://".
                $this->account.".".
                $this->megaplan_domain.
                "/BumsCommonApiV01/User/authorize.api";
            
            $_H->post_request = $request;
            $_H->url = $url;
            $_H->request();
            
            $answer = @json_decode($_H->content);
            $this->rowdata = $_H->content;

            if(!is_object($answer)){
                $this->error = _t('Unknown json-error in the file %f at the line %l',array("%f"=>__FILE__,"%l"=>__LINE__));
                return 0;
            }
            $answer = get_object_vars($answer);

            if(!isset($answer['status']) || !is_object($answer['status'])){
                $this->error = _t('Status error in the file %f at the line %l',array("%f"=>__FILE__,"%l"=>__LINE__));
                return 0;
            }
            $answer['status'] = get_object_vars($answer['status']);
            
            if(!isset($answer['status']['code']) || $answer['status']['code']!='ok'){
                $this->error = isset($answer['status']['message'])?$answer['status']['message']:"megaplan_api auth error";
                return 0;
            }
            
            $this->access_id = $answer['data']->AccessId;
            $this->secret_key = $answer['data']->SecretKey;
            $this->user_id = $answer['data']->UserId;
        }
        
        
        function request($Uri, $parameters=NULL, $Method = "GET", $ContentType = ''){
            $request = new SdfApi_Request($this->access_id, $this->secret_key, $this->account.".".$this->megaplan_domain, $this->use_ssl, $this->timeout);
            $data = $request->get($Uri, $parameters);
            $this->error==$request->getError();
            if($this->error)return false;
                

            $answer = json_decode($data);

            if(!is_object($answer)){
                $this->error = _t('Unknown json-error in the file %f at the line %l',array("%f"=>__FILE__,"%l"=>__LINE__));
                $this->data = $data;
                return 0;
            }
            $answer = get_object_vars($answer);

            if(!isset($answer['status']) || !is_object($answer['status'])){
                $this->error = _t('Status error in the file %f at the line %l',array("%f"=>__FILE__,"%l"=>__LINE__));
                return 0;
            }
            $answer['status'] = get_object_vars($answer['status']);
            
            if(!isset($answer['status']['code']) || $answer['status']['code']!='ok'){
                $this->error = isset($answer['status']['message'])?$answer['status']['message']:"megaplan_api auth error";
                return 0;
            }
            return $answer['data'];
        }
    }
