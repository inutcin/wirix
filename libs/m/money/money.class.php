<?php
    include_once(dirname(__FILE__)."/../../wirix.class.php");

    /**
     * Класс по работе с деньгами
     */
    class wirix_money extends wirix{
        
        function __construct(){
            parent::__construct(__CLASS__);
        }
        
        /**
            Функция принимает значение денег в виде float и переводит в удобочитаемый формат
            
            1) Пятьсот шестьдесят три тысячи девятьсот двадцать два рубля двадцать три копейки
            2) Пятьсот шестьдесят три тысячи девятьсот двадцать два рубля 23 копейки
        */
        function translate($value, $format=1, $head=1, $currency='RUR'){
            $symbolycs1 = array(
                "19" => array(
                    array("девятнадцать","копеек"),
                    array("девятнадцать","рублей"),
                    array("девятнадцать","тысяч"),
                    array("девятнадцать","миллионов")
                ),
                "18" => array(
                    array("восемнадцать","копеек"),
                    array("восемнадцать","рублей"),
                    array("восемнадцать","тысяч"),
                    array("восемнадцать","миллионов")
                ),
                "17" => array(
                    array("семнадцать","копеек"),
                    array("семнадцать","рублей"),
                    array("семнадцать","тысяч"),
                    array("семнадцать","миллионов")
                ),
                "16" => array(
                    array("шеснадцать","копеек"),
                    array("шеснадцать","рублей"),
                    array("шеснадцать","тысяч"),
                    array("шеснадцать","миллионов")
                ),
                "15" => array(
                    array("пятнадцать","копеек"),
                    array("пятнадцать","рублей"),
                    array("пятнадцать","тысяч"),
                    array("пятнадцать","миллионов")
                ),
                "14" => array(
                    array("четырнадцать","копеек"),
                    array("четырнадцать","рублей"),
                    array("четырнадцать","тысяч"),
                    array("четырнадцать","миллионов")
                ),
                "13" => array(
                    array("тринадцать","копеек"),
                    array("тринадцать","рублей"),
                    array("тринадцать","тысяч"),
                    array("тринадцать","миллионов")
                ),
                "12" => array(
                    array("двенадцать","копеек"),
                    array("двенадцать","рублей"),
                    array("двенадцать","тысяч"),
                    array("двенадцать","миллионов")
                ),
                "11" => array(
                    array("одинадцать","копеек"),
                    array("одинадцать","рублей"),
                    array("одинадцать","тысяч"),
                    array("одинадцать","миллионов")
                ),
                "10" => array(
                    array("десять","копеек"),
                    array("десять","рублей"),
                    array("десять","тысяч"),
                    array("десять","миллионов")
                ),
                "9" => array(
                    array("девять","копеек"),
                    array("девять","рублей"),
                    array("девять","тысяч"),
                    array("девять","миллионов")
                ),
                "8" => array(
                    array("восемь","копеек"),
                    array("восемь","рублей"),
                    array("восемь","тысяч"),
                    array("восемь","миллионов")
                ),
                "7" => array(
                    array("семь","копеек"),
                    array("семь","рублей"),
                    array("семь","тысяч"),
                    array("семь","миллионов")
                ),
                "6" => array(
                    array("шесть","копеек"),
                    array("шесть","рублей"),
                    array("шесть","тысяч"),
                    array("шесть","миллионов")
                ),
                "5" => array(
                    array("пять","копеек"),
                    array("пять","рублей"),
                    array("пять","тысяч"),
                    array("пять","миллионов")
                ),
                "4" => array(
                    array("четыре","копейки"),
                    array("четыре","рубля"),
                    array("четыре","тысячи"),
                    array("четыре","миллиона")
                ),
                "3" => array(
                    array("три","копейки"),
                    array("три","рубля"),
                    array("три","тысячи"),
                    array("три","миллиона")
                ),
                "2" => array(
                    array("два","копейки"),
                    array("два","рубля"),
                    array("две","тысячи"),
                    array("два","миллиона")
                ),
                "1" => array(
                    array("одна","копейка"),
                    array("один","рубль"),
                    array("одна","тысяча"),
                    array("один","миллион")
                ),
                "0" => array(
                    array("","копеек"),
                    array("","рублей"),
                    array("","тысяч"),
                    array("","миллионов")
                ),
            );
            
            $symbolycs2 = array(
                "2"=>"двадцать",
                "3"=>"тридцать",
                "4"=>"сорок",
                "5"=>"пятьдесят",
                "6"=>"шестьдесят",
                "7"=>"семьдесят",
                "8"=>"восемьдесят",
                "9"=>"девяносто"
            );
            
            $symbolycs3 = array(
                "1"=>"сто",
                "2"=>"двести",
                "3"=>"триста",
                "4"=>"четыреста",
                "5"=>"пятьсот",
                "6"=>"шестьсот",
                "7"=>"семьсот",
                "8"=>"восемьсот",
                "9"=>"девятьсот"
            );
            
            $num = $value;
            $result = array();
            $ths = array();
            while($num>0){
                $ths[] = $num % 1000;
                $num = floor($num/1000);
                
            }
            
            foreach($ths as $n=>$num){
                $result[$n] ='';
                $d3 = floor($num/100);
                $d2 = floor(($num-$d3*100)/10);
                $d1 = isset($symbolycs2[$d2])?$num % 10:$num % 100;
                if($d3 && isset($symbolycs3[$d3]))$result[$n] .= $symbolycs3[$d3]." ";
                if($d2 && isset($symbolycs2[$d2]))$result[$n] .= $symbolycs2[$d2]." ";
                if(isset($symbolycs1[$d1]))$result[$n] .= $symbolycs1[$d1][$n+1][0]." ".$symbolycs1[$d1][$n+1][1]." ";
                if(isset($result[$n-1]) && !trim($result[$n-1]))$result[$n] .= " рублей";
            }
            
            $fraction = floor(($value-floor($value))*100);
            
            $res = '';
            $d2 = floor($fraction/10);
            $d1 = isset($symbolycs2[$d2])?$fraction % 10:$fraction % 100;
            if($format==1){
                if($d2 && isset($symbolycs2[$d2]))$res .= $symbolycs2[$d2]." ";
                if($d1 && isset($symbolycs1[$d1]))$res .= $symbolycs1[$d1][0][0]." ".$symbolycs1[$d1][0][1]." ";
            }
            elseif($format==2){
                $res .= sprintf("%02d", $fraction);
                if($d1 && isset($symbolycs1[$d1]))
                    $res .= " ".$symbolycs1[$d1][0][1];
                elseif(!$fraction)
                    $res .= " копеек";
                
            }
        
            $result = array_reverse($result);
            $result = implode($result)." ".$res;
            $alphabet = array("д"=>"Д","в"=>"В","с"=>"С","ш"=>"Ш","п"=>"П","ч"=>"Ч","т"=>"Т","о"=>"О");
            if($head)$result = mb_strtoupper(mb_substr($result,0,1,'UTF-8'), 'UTF-8').mb_substr($result,1,mb_strlen($result,'UTF-8')-1,'UTF-8');
            return $result;
        }
        
    }

?>
