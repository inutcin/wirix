<?php
    include_once(dirname(__FILE__)."/../../wirix.class.php");


    /**
        Класс для рисования графиков
        
        
        $plot = $_SERVER['wirix']->lib_load("plot");

        $plot->setTitle(
            "Динамика посещений",
            'verdana.ttf', 
            14
        );
        
        $plot->set_size(580, 330);
        
        $plot->add_series("Посетители в день       ",$series1);
        $plot->add_series("Просмотренных страниц в день",$series2);
        $plot->set_font_color(array(0,0,0));
        
        $plot->set_axis("","", array(0,0,0));
        
        //$plot->add_png(470,240, $_SERVER['DOCUMENT_ROOT']."/images/logo.png");
        
        //$plot->set_gradient(array(180,0,0),array(50,0,0),100);
        $plot->set_gradient(array(255,255,255),array(255,255,255),100);
        
        $plot->add_points($labels, 7);
        $plot->add_trend("Тренд",$series1,0.5,3);
        
        $plot->prepare();
        $plot->display();

     */
    class wirix_plot extends wirix{
        
        var $font_dir = '';
        protected $font_color = array(0,0,0);
        protected $palette_filename = '';
        
        protected $plot_type = 'spline';
        
        protected $title = '';
        protected $title_fontname = 'verdana.ttf';
        protected $title_fontsize = 20;
        protected $title_x    = 50;
        protected $title_y    = 35;
        
        protected $width      = 700;
        protected $height     = 400;
        
        protected $border     = array(0,0,0);
        
        protected $left_margin    = 40;
        protected $top_margin     = 60;
        protected $right_margin   = 40;
        protected $bottom_margin  = 70;
        
        protected $default_fontname = 'verdana.ttf';
        protected $default_fontsize = 8;
        
        // Массивы серий
        protected $serieses   = array();
        protected $weights   = array();
        protected $tics   = array();
        // Точки оси обсцисс
        protected $points     = array();
        protected $visibled_points_count = 0;//!< Количество видимых точек
        
        protected $axis_x_title   = 'X';
        protected $axis_y_title   = 'Y';
        protected $axis_color     = array(0,0,0);
        
        protected $grid_color = array(200,200,200);
        
        protected $fill_start_color = array(255,255,255);
        protected $fill_end_color = array(200,200,200);
        protected $fill_alpha = 100;
        
        protected $png = array();
        protected $trends = array();
        
        var $picture = false;
        var $data = false;
        
        var $show_legend = true;
        
        protected $min_y = '';
        protected $max_y = '';
        protected $label_rotation = 0;
        
        
        /**
            Поворот меток
        */
        function set_label_rotation($angle){
            $this->label_rotation = $angle;
        }
        
        /**
            Устанока диапазона по оси X
        */
        function set_y_range($min,$max){
            $this->min_y = $min;
            $this->max_y = $max;
        }
        
        /**
            Установка типа диаграммы
            * line
            * spline
            * bar
        */
        function set_type($plot_type = 'spline'){
            $this->plot_type = $plot_type;
        }
        
        /**
            Загрузка собственной палитры
        */
        function loadPalette($filename){
            $this->palette_filename = $filename;
        }
        
        
        /**
            Добавить линию линейного тренда По МНК
            курим тут
            http://www.cleverstudents.ru/mnk.html
            
        */
        function add_trend($name, $points=array(), $weight=1, $tic=0){
            //return 0;
            
            $count = count($points);
            
            $s1 = 0;
            foreach($points as $i=>$p)$s1 += $i;
            
            $s2 = 0;
            foreach($points as $i=>$p)$s2 += $i*$p;
                
            $s3 = 0;
            foreach($points as $i=>$p)$s3 += $p;

            $s4 = 0;
            foreach($points as $i=>$p)$s4 += pow($i,2);
            
            $k = ($count*$s2 - $s1*$s3)/($count*$s4-pow($s1,2));
            
            $b = ($s3-$k*$s1)/$count;
            
            $trend_points = array();
            for($i=0;$i<count($points);$i++)$trend_points[] = $k*$i+$b;
            
            $this->add_series($name, $trend_points, $weight, $tic);
        }
        
        function set_font_color($color=array(0,0,0)){
            $this->font_color = $color;
        }
        
        function __construct(){
            parent::__construct();
            $this->font_dir = __DIR__."/pchart/fonts";
        }
        
        function add_png($x, $y, $path){
            $this->png[] = array("x"=>$x, "y"=>$y, "path"=>$path);
        }
        
        function set_isspline($val){
            $this->isspline = $val;
        }
        
        function set_gradient($from_color, $end_color, $alpha){
            $this->fill_start_color = $from_color;
            $this->fill_end_color = $end_color;
            $this->fill_alpha = $alpha;
        }
        
        function set_axis($title_x, $title_y, $color = array(0,0,0)){
            $this->axis_x_title = $title_x;
            $this->axis_y_title = $title_y;
            $this->axis_color   = $color;
        }
        
        function set_grid($color=array(200,200,200)){
            $this->grid_color = $color;
        }
        
        function add_points(
            $points = array(),
            $visibled_points = 0    //!< 0 - all points
        ){
            $this->points = $points;
            $this->visibled_points_count = $visibled_points;
        }
        
        function add_series($name, $points=array(), $weight=1, $tic=0){
            $this->serieses[$name] = $points;
            $this->weights[$name] = $weight;
            $this->tics[$name] = $tic;
        }
        
        function set_size($width, $height){
            $this->width = $width;
            $this->height = $height;
        }
        
        /**
            Заголовок графика
        */
        function setTitle(
            $title, 
            $title_fontname='verdana.ttf', 
            $title_fontsize=20, 
            $title_x=50, 
            $title_y=35
        ){
            $this->title = $title;
            $this->title_fontname=$title_fontname; 
            $this->title_fontsize=$title_fontsize;
            $this->title_x=$title_x;
            $this->title_y=$title_y;
        }
        
        function prepare(){
            include("pchart/class/pData.class.php"); 
            include("pchart/class/pDraw.class.php"); 
            include("pchart/class/pImage.class.php"); 
            include("pchart/class/pScatter.class.php"); 
            
            /* Create and populate the pData object */ 
            $this->data = new pData(); 
            if($this->palette_filename){
                $this->data->loadPalette($this->palette_filename, true);
            }
            foreach($this->serieses as $name=>$data){
                $this->data->addPoints($data, $name); 
                $this->data->setSerieWeight($name, $this->weights[$name]);
                if($this->tics[$name])$this->data->setSerieTicks($name, $this->tics[$name]); 
            }

            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            $this->data->setAxisName(0,$this->axis_x_title); 
            $this->data->setAbscissaName($this->axis_y_title);
            
            // Прореживаем, если метки наслаиваются
            if($this->visibled_points_count!=0){
                foreach($this->points as $k=>$v)
                    if($k % $this->visibled_points_count!=0)
                        $this->points[$k] = '';
                
            }
            
            $this->data->addPoints($this->points,"Labels"); 
            $this->data->setSerieDescription("Labels","Months"); 
            $this->data->setAbscissa("Labels"); 
            
            /* Create the pChart object */ 
            $this->picture = new pImage($this->width,$this->height,$this->data); 
            
            /* Turn of Antialiasing */ 
            $this->picture->Antialias = true; 
            
            /* Add a border to the picture */ 
            if($this->border){
                $this->picture->drawRectangle(0,0,$this->width-1,$this->height-1,array("R"=>$this->border[0],"G"=>$this->border[1],"B"=>$this->border[2])); 
            }
            
            if($this->fill_start_color && $this->fill_end_color && $this->fill_alpha){
                $Settings = array(
                    "StartR"    =>$this->fill_start_color[0], 
                    "StartG"    =>$this->fill_start_color[1], 
                    "StartB"    =>$this->fill_start_color[2], 
                    "EndR"      =>$this->fill_end_color[0], 
                    "EndG"      =>$this->fill_end_color[1], 
                    "EndB"      =>$this->fill_end_color[2], 
                    "Alpha"     =>$this->fill_alpha 
                );
                $this->picture->drawGradientArea(0,0,$this->width,$this->height,DIRECTION_VERTICAL,$Settings); 
            }
            
            /* Write the chart title */  
            if($this->title){
                $this->picture->setFontProperties(array(
                    "R"=>$this->font_color[0],
                    "G"=>$this->font_color[1],
                    "B"=>$this->font_color[2],
                    "FontName"=>$this->font_dir."/".$this->title_fontname,
                    "FontSize"=>$this->title_fontsize
                )); 
                $this->picture->drawText(
                    $this->title_x,
                    $this->title_y,
                    $this->title,array(
                        "FontSize"=>$this->title_fontsize
                    )
                ); 
            }
            
            /* Set the default font */ 
            $this->picture->setFontProperties(array(
                "FontName"=>$this->font_dir."/".$this->default_fontname,
                "FontSize"=>$this->default_fontsize
            )); 


            $this->picture->setGraphArea(
                $this->left_margin,
                $this->top_margin-($this->show_legend?0:25),
                $this->width - $this->right_margin,
                $this->height - $this->bottom_margin
            ); 
            
            /* Draw the scale */ 
            $scaleSettings = array(
                "XMargin"=>1,
                "YMargin"=>10,
                "Floating"=>TRUE,
                "GridR"=>$this->grid_color[0],
                "GridG"=>$this->grid_color[1],
                "GridB"=>$this->grid_color[2],
                "DrawSubTicks"=>TRUE,
                "CycleBackground"=>TRUE,
                "AxisR"=>$this->axis_color[0],
                "AxisG"=>$this->axis_color[1],
                "AxisB"=>$this->axis_color[2]
            ); 

            if($this->min_y!='' || $this->max_y!=''){
                $boundary = array();
                $boundary['Min'] = $this->min_y;
                $boundary['Max'] = $this->max_y;
                $scaleSettings['ManualScale'] = array(0=>$boundary);
                $scaleSettings['Mode']=SCALE_MODE_MANUAL;
            }
            
            if($this->label_rotation){
                //$scaleSettings['Mode']=SCALE_MODE_MANUAL;
                $scaleSettings['LabelRotation']=$this->label_rotation;
            }

            $this->picture->drawScale($scaleSettings); 
            
            /* Turn on Antialiasing */ 
            $this->picture->Antialias = TRUE; 
            
            /* Define the chart area */ 
            /* Draw the line chart */ 
            if($this->plot_type == 'spline')
                $this->picture->drawSplineChart(array(
                    "DisplayColor"=>DISPLAY_AUTO,
                    "PlotBorder"=>TRUE,
                    "BorderSize"=>2,
                    "Surrounding"=>-60,
                    "BorderAlpha"=>80
                )); 
            elseif($this->plot_type == 'line')
                $this->picture->drawLineChart(array(
                    "DisplayValues"=>FALSE,
                    "PlotBorder"=>TRUE,
                    "BorderSize"=>2,
                    "Surrounding"=>-60,
                    "BorderAlpha"=>80
                )); 
            elseif($this->plot_type == 'bar')
                $this->picture->drawBarChart(array(
                    "PlotBorder"=>TRUE,
                    "BorderSize"=>2,
                    "BorderAlpha"=>80,
                    "DisplayPos"=>LABEL_POS_INSIDE,
                    "DisplayValues"=>TRUE,
                    "Surrounding"=>10
                )); 
            
            /* Write the chart legend */ 
            if($this->show_legend)
                $this->picture->drawLegend(45,43,array(
                    "Style"=>LEGEND_NOBORDER,"Mode"=>LEGEND_HORIZONTAL
                )); 

            foreach($this->png as $png){
                $this->picture->drawFromPNG(
                    $png['x'],$png['y'],$png['path']
                );
            }
            
            /* Render the picture (choose the best way) */ 
            //$myPicture->autoOutput("pchart/pictures/example.drawLineChart.simple.png");         
        }
        
        function display($type = "line"){
            $this->picture->autoOutput();
        }
        
    }

?>
