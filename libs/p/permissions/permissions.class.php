<?php
    
    /**
        Система прав на дерево контроллеров
    */
    class wirix_permissions extends wirix{
        
        function __construct(){
            parent::__construct(__CLASS__);
        }
        
        
        /**
            Функция возвращает имя контроллера(с путём) исходя из REQUEST_URI
            Если REQUEST_URI - конкретный файл, то вместо REQUEST_URI используется HTTP_REFERER
            (для провеерки с какой страницы этот скрипт был вызван)
        */
        function get_route_name(){
            
            $filename = $_SERVER['DOCUMENT_ROOT'].$_SERVER['REQUEST_URI'];
            $filename = preg_replace("/^(.*?)\?.*$/", "$1", $filename);
            
            if(file_exists($filename)){
                if(!isset($_SERVER['HTTP_REFERER']))$_SERVER['HTTP_REFERER'] = '';
                $RU = str_replace("http://".$_SERVER['HTTP_HOST'], "", $_SERVER['HTTP_REFERER']);
                $RU = str_replace("https://".$_SERVER['HTTP_HOST'], "", $RU);
		if(preg_match("/https?:\/\/.*?(\/.*)$/",$RU,$tmp))
		    $RU = $tmp[1];
            }
            else{
                $RU = $_SERVER['REQUEST_URI'];
            }
            
            $RU = preg_replace("/^\/acts\/(.*)$/i", "$1", $RU);
            $RU = preg_replace("/^(.*)\/$/i", "$1", $RU);
            $RU = preg_replace("/^(.*?)\?.*$/", "$1", $RU);
            
            return $RU;
        }
        
        /**
            Функция блокирует просмотр к контроллеру, если пользователь не имеет 
            права на просмотр (ни одна из групп переданного списка не имеет доступа 
            или явно запрещена)
        */
        function access($groups, $path=''){
            $path = $path?$path:$this->get_route_name();
            if(!$this->is_access($path,$groups)){
                echo _t('Access denied');
                $_SERVER['http']->status(403);
                die;
            }
        }
        
        /**
            Определение имеет ли пользователь с указанным вхождением в группы
            право доступа в указанный путь
            
            Пользователю закрыт доступ в path, если есть хотя бы одна группа, 
            в которую он входит, для которой закрыт доступ или если среди групп, 
            которые он входит, нет ни одной из тех, что упопянуты среди разрешенных
            
            @param $path - путь, к которому определяется доступ, например /locale/langs
            @groups - список групп, например "0,12,6" или array(0,12,6)
            
        */
        function is_access($path, $groups){
            if(!is_array($groups)){
                $tmp = explode(",", $groups);
                $groups = array();
                foreach($tmp as $gid)$groups[] = trim($gid);
            }

            $access = false;
            $access_groups = $this->groups_access($path);
            foreach($access_groups as $gid=>$type){
                
                if(array_search($gid, $groups)!==false && $type=='deny')$access=false;
                if(array_search($gid, $groups)!==false && $type=='allow')$access=true;
            }
            
            return $access;
        }
        
        /**
            Функция выводит массив ID групп, которым разрешен доступ к указанному path
        */
        function groups_access($path){
            if(!$path)$path = '/';
            $path = explode("/", $path);
            $current_path = "";
            $groups = array();
            foreach($path as $item){
                $current_path .= "/".$item;
                $current_path = str_replace("//", "/", $current_path);
                
                $_SERVER['db']->search(array("a"=>"permissions"), array(), array("path"=>$current_path), "","`a`.`order` ASC");
                $rows = $_SERVER['db']->rows;
                foreach($rows as $row)$groups[$row['group_id']] = $row['access'];
            }
            return $groups;
        }
        
        
        /**
            Получение списка контроллеров на указанном уровне
        */
        function get_routes(
            $path = ''  //!< Путь до контроллеров (пустое - корень)
        ){
            if(!$path = trim($path))$path = "";
            if(!preg_match("/^(\/[\w\d\-\_]+)*$/i", $path))$path = "";
            $path = preg_replace("/^\/(.*)$/", "$1", $path);
            
            $routes = array();
            $controlles_path = $_SERVER['DOCUMENT_ROOT']."/wirix/acts/";
            $route_path = $controlles_path.$path;
            if(is_dir($route_path)){
                $dd = opendir($route_path);
                while($file = readdir($dd)){
                    if(preg_match("/^\..*/i", $file))continue;
                    if(!is_dir("$route_path/$file"))continue;
                    $file = str_replace($controlles_path,"",trim($file));
                    $routes[] = array(
                        "name"  =>  "&#8195;&#8195;&#8627;".$file,
                        "path"  =>  preg_replace("/^\/\/(.*)$/", "/$1", "/$path/$file"),
                    );
                }
                closedir($dd);
            }

            $controlles_path = $_SERVER['DOCUMENT_ROOT']."/acts/";
            $route_path = $controlles_path.$path;
            
            if(is_dir($route_path)){
                $dd = opendir($route_path);
                while($file = readdir($dd)){
                    if(preg_match("/^\..*/i", $file))continue;
                    $file = "$route_path$file";
                    if(!is_dir($file))continue;
                    $file = str_replace($controlles_path,"",trim($file));
                    $routes[] = array(
                        "name"  =>  "&#8195;&#8195;&#8627;".$file,
                        "path"  =>  preg_replace("/^\/\/(.*)$/", "/$1", "/$path/$file"),
                    );
                }
                closedir($dd);
            }
            return $routes;
        }
        
        
    }
?>
