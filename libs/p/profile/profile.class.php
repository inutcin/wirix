<?php
    include_once(dirname(__FILE__)."/../../wirix.class.php");
    $_SERVER['wirix']->lib_load("html_form", false);

    class wirix_profile extends wirix_html_form{

        var $user_id = 0;             //!< Пользователь, для которого правим профиль
        
        
        function __construct(){
            $this->called_class = __CLASS__ ;
            $this->user_id = $_SERVER['auth']->is_auth()?$_SERVER['auth']->user['id']:0;
        }


		

        /**
            Получение свойств пользователя для определённой вкладки (типа)
        */
        function get_props(
            $user_id,       //!< Пользователь, для которого получаем свойства
            $type_id = 0    //!< ID вкладки (типа)
        ){
            // Получаем свойства этого 
            $fields = array();
            if($type_id)$fields = array("a.type_id"=>$type_id);
            $_SERVER['db']->search(
                array("a"=>"user_props","b"=>"user_prop_values"),
                array("a.id=b.user_prop_id AND b.user_id=$user_id"=>"LEFT"),
                $fields,
                "",
                "a.order ASC",
                0,
                0,
                array(
                    "a.id"=>"user_prop_id",
                    "a.pattern"=>"pattern",
                    "a.default"=>"default",
                    "b.value"=>"value",
                    "a.name"=>"name",
                    "a.title"=>"title",
                    "a.data"=>"json_data",
                    "a.comment"=>"comment",
                    "a.type"=>"type"
                )
            );
            foreach($_SERVER['db']->rows as $v)$data[$v['name']] = $v;

            return $data;
        }

       /**
            Получение типов параметров(вкладок) профиля
        */
        function get_types(){
            $_SERVER['db']->search(
                array("a"=>"user_prop_types"),
                array()
            );
            return $_SERVER['db']->rows;
        }
        
        
        /**
            Сохранение данных профиля из массива полученного от вконтакта
        */
        function save_from_vk($vk_user_info, $user_id){
            $this->save_property(2, $user_id, $vk_user_info['last_name']." ".$vk_user_info['first_name']);
            $this->save_property(5, $user_id, $vk_user_info['uid']);
        }
        
        /**
            Сохранение фото пользователя. Возвращает текст ошибки. Если без ошибок - пустую строку
        */
        function save_userphoto(
            $prop_id, //!< ID свойства в БД
            $user_id, 
            $name = 'photo',   //!< Имя поля 
            $tmp_filename = '' //!< Временный файл для скачивания вместо стандартного
        ){
            $file_path_url = "/images/users_photos/";
            $file_path = $_SERVER['DOCUMENT_ROOT'].$file_path_url;
            $file_url = $file_path_url.$user_id.".png";
            $file_url_tiny = $file_path_url.$user_id."_tiny.png";
            
            // Проверяем возможность записи в каталог
            if(is_dir($file_path)){
                @touch($file_path."test.png");
                if(!file_exists($file_path."test.png"))
                    return 'Недостаточно прав для создания файлов в каталоге <b>'.$file_path."</b>";
                
            }
            elseif(@mkdir($file_path)){
                if(!chmod($file_path, 0777))
                    return 'Недостаточно привилегий для смены прав каталога <b>'.$file_path."</b>";
                touch($file_path."test.png");
                if(!file_exists($file_path."test.png"))
                    return 'Недостаточно прав для создания файлов в каталоге <b>'.$file_path."</b>";
            }
            else
                return $this->error = 'Не могу создать каталог <b>'.$file_path.'</b>';
            
            // Проверяем загрузился ли файл
            if(!$tmp_filename && !isset($_FILES[$name]['error']))
                return 'Ошибка загрузки файла';

            // Если файл для загрузки не выбрали
            if(!$tmp_filename && $_FILES[$name]['name']=='')return '';
            
            // Подменяем стандартные имена файлов на указанный руками
            if($tmp_filename){
                $_FILES[$name]['tmp_name'] = $tmp_filename;
                $_FILES[$name]['name'] = $tmp_filename;
            }
            
            $ext = preg_replace("/^.*\.([\d\w]+)$/","$1", $_FILES[$name]['name']);
            
            $ext = strtolower($ext);
            if($ext!='jpg' && $ext!='jpeg' && $ext!='gif' && $ext!='png')
                return 'Недопустимое расширение файла( допускаются .jpg .jpeg .png .gif)';

            $filename = $file_path.$user_id.".png";
            $filename_tiny = $file_path.$user_id."_tiny.png";
               
            if(!$tmp_filename && $_FILES[$name]['error'])
                return $_SERVER['error']->upload_error($_FILES[$name]['error']);
                
            if(!@copy($_FILES[$name]['tmp_name'], $filename))
                return "Не могу скопировать файл <b>$filename</b>";

            if(!@chmod($filename, 0777))
                return "Не могу изменить права у файла <b>$filename</b>";
            
            // Создаём превью картинки
            $image = $_SERVER['wirix']->lib_load('image');
            $image->filename = $filename;
            $image->thrumb = $filename;
            $image->thrumb_width = 300;
            $image->thrumb_height = 300;
            $image->create_thrumb($ext);
            
            $image->filename = $filename;
            $image->thrumb = $filename_tiny;
            $image->thrumb_width = 50;
            $image->thrumb_height = 50;
            $image->create_thrumb();
            
            
            $this->save_property($prop_id, $user_id, $filename);
            return '';
        }


        /**
            Получение свойства профиля. 
        */
        function get_property($prop_id, $user_id){
            $cond = array(
                "user_prop_id"  =>  $prop_id,
                "user_id"       =>  $user_id
            );
            
            if(!$_SERVER['db']->search_one('user_prop_values', $cond)){
            	return false;
            }

            return $_SERVER['db']->record;
        }

        
        /**
            Сохранение свойства профиля. Возвращает текст ошибки. Если без ошибок - пустую строку
        */
        function save_property($prop_id, $user_id, $value){
            $cond = array(
                "user_prop_id"  =>  $prop_id,
                "user_id"       =>  $user_id
            );
            if($_SERVER['db']->search_one('user_prop_values', $cond)){
                $_SERVER['db']->update("user_prop_values", array("value"=>$value), $cond);
            }
            else{
                $cond['value'] = $value;
                $_SERVER['db']->insert('user_prop_values', $cond);
            }
            return '';
        }
        
        /**
            Получение html-кода формы внесения полей определённого типа для
            определённого пользователя
        */
        function fetch_types_form($type_id, $user_id = 0){
            
            $this->id = "profile_form_".$type_id;
            $this->title = '';
            $this->send_button_text = 'Сохранить';
            $this->use_back_button = false;
            $this->submit_url = "/wirix/libs/p/profile/save.ajax.php?back_url=".
                urlencode(str_replace($_SERVER['QUERY_STRING'],"",$_SERVER['REQUEST_URI']));
            
            $rows = $this->get_props($user_id, $type_id);
            
            foreach($rows as $row){
                $input  = $_SERVER['wirix']->lib_load("html_input");
                $input->type = $row['type'];
                $input->title = $row['title'];
                $input->regexp = $row['pattern'];
                $input->comment = $row['comment'];
                $input->name = $row['name'];
                $input->default = $row['default'];
                $input->value   = $row['value'];
                $this->add_input($input);
                $input->db_id = $row['user_prop_id'];
                unset($input);
            }

            $this->use_ajax = true;
            return $this->fetch();
        }


        
    }

?>
