<?php
    include("../../wirix.class.php");
    $_SERVER['wirix']->lib_load('http',     false);
    $profile = $_SERVER['wirix']->lib_load('profile');
    // Проверка валидности полей формы
    include("../../h/html_form/check.ajax.php");
    
    $answer = array();
    $answer['error'] = '';
    
    $form = get_object_vars(json_decode(
	base64_decode($_SESSION['wirix']['wirix_profile'][$_SERVER['http']->post('form_id')])
    ));
    
    $answer['success'] = 'Данные сохранены';
    if(!$answer['error'] && isset($form['inputs'])){
        foreach($form['inputs'] as $input){
            $input = get_object_vars($input);
            if($input['name']=='photo')
                $answer['error'] .= $profile->save_userphoto($input['db_id'], $_SERVER['auth']->is_auth()?$_SERVER['auth']->user['id']:0);
            // Проверяем авторизован ли сохраняющий, админ ли и его ли это профиль
            elseif(
                $_SERVER['auth']->is_auth() && (
                    $_SERVER['auth']->user['id']==$form['user_id'] 
                    || 
                    $_SERVER['auth']->in_group(1)
                )
            ){
                $answer['error'] .= $profile->save_property(
                    $input['db_id'], 
                    $form['user_id'],
                    $_SERVER['http']->post($input['name'])
                );
            }
            
        }
    }
    unset($profile);
    
    $answer['redirect'] = "";
    if($answer['error'])$answer['success'] = '';

    echo json_encode($answer);
    /*
?>
<script src="/wirix/libs/h/html/js/jquery.min.js"></script>
<script>
parent.$('#html_form_<?php echo $_SERVER['http']->post('form_id');?>_submit').attr('class','submit btn-primary');
parent.$('#html_form_<?php echo $_SERVER['http']->post('form_id');?>_submit').attr('class','submit btn-primary');

<?php if($answer['error']){?>
parent.$('#<?php echo $_SERVER['http']->post('form_id');?>_alert_error').html('<?php echo $answer['error'];?>');
parent.$('#<?php echo $_SERVER['http']->post('form_id');?>_alert_success').fadeOut();
parent.$('#<?php echo $_SERVER['http']->post('form_id');?>_alert_error').fadeIn();
<?php }elseif($answer['success']){?>
parent.$('#<?php echo $_SERVER['http']->post('form_id');?>_alert_success').html('<?php echo $answer['success'];?>');
parent.$('#<?php echo $_SERVER['http']->post('form_id');?>_alert_error').fadeOut();
parent.$('#<?php echo $_SERVER['http']->post('form_id');?>_alert_success').fadeIn();
<?php }?>
<?php if($answer['redirect']){?>
parent.document.location.href='<?php echo $answer['redirect'];?>';
<?php }?>
</script>
<?php */ 
?>
