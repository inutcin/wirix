<?php
    include_once(dirname(__FILE__)."/../../wirix.class.php");

    include_once(dirname(__FILE__)."/../../p/plot/plot.class.php");
    class wirix_plot_pie extends wirix_plot{
        
        var $by_percent = false;
        
        var $DataGapRadius = 5;
        var $DataGapAngle = 5;
        var $Radius = 0;
        var $pie3d = false;
        var $ring = false;
        
        function prepare(){
            include(dirname(__FILE__)."/../../p/plot/pchart/class/pData.class.php"); 
            include(dirname(__FILE__)."/../../p/plot/pchart/class/pDraw.class.php"); 
            include(dirname(__FILE__)."/../../p/plot/pchart/class/pImage.class.php"); 
            include(dirname(__FILE__)."/../../p/plot/pchart/class/pPie.class.php"); 
            include(dirname(__FILE__)."/../../p/plot/pchart/class/pScatter.class.php"); 

            /* Create and populate the pData object */ 
            $this->data = new pData();   
            if($this->palette_filename){
                $this->data->loadPalette($this->palette_filename, true);
            }
            foreach($this->serieses as $name=>$data){
                $this->data->addPoints($data, $name); 
                $this->data->setSerieWeight($name, $this->weights[$name]);
                if($this->tics[$name])$this->data->setSerieTicks($name, $this->tics[$name]); 
            }

            $this->data->addPoints($this->points,"Labels"); 
            $this->data->setAbscissa("Labels"); 

            /* Create the pChart object */ 
            $this->picture = new pImage($this->width,$this->height,$this->data); 

            /* Turn of Antialiasing */ 
            $this->picture->Antialias = true; 
            
            /* Add a border to the picture */ 
            if($this->border){
                $this->picture->drawRectangle(0,0,$this->width-1,$this->height-1,array("R"=>$this->border[0],"G"=>$this->border[1],"B"=>$this->border[2])); 
            }
            
            if($this->fill_start_color && $this->fill_end_color && $this->fill_alpha){
                $Settings = array(
                    "StartR"    =>$this->fill_start_color[0], 
                    "StartG"    =>$this->fill_start_color[1], 
                    "StartB"    =>$this->fill_start_color[2], 
                    "EndR"      =>$this->fill_end_color[0], 
                    "EndG"      =>$this->fill_end_color[1], 
                    "EndB"      =>$this->fill_end_color[2], 
                    "Alpha"     =>$this->fill_alpha 
                );
                $this->picture->drawGradientArea(0,0,$this->width,$this->height,DIRECTION_VERTICAL,$Settings); 
            }
            
            /* Write the chart title */  
            if($this->title){
                $this->picture->setFontProperties(array(
                    "R"=>$this->font_color[0],
                    "G"=>$this->font_color[1],
                    "B"=>$this->font_color[2],
                    "FontName"=>$this->font_dir."/".$this->title_fontname,
                    "FontSize"=>$this->title_fontsize
                )); 
                $this->picture->drawText(
                    $this->title_x,
                    $this->title_y,
                    $this->title,array(
                        "FontSize"=>$this->title_fontsize
                    )
                ); 
            }
            
            /* Set the default font */ 
            $this->picture->setFontProperties(array(
                "FontName"=>$this->font_dir."/".$this->default_fontname,
                "FontSize"=>$this->default_fontsize
            )); 


            $this->picture->setGraphArea(
                $this->left_margin,
                $this->top_margin,
                $this->width - $this->right_margin,
                $this->height - $this->bottom_margin
            ); 


            /* Create the pPie object */  
            $PieChart = new pPie($this->picture,$this->data); 
             
            $radius = $this->Radius?$this->Radius:($this->height-$this->top_margin-$this->bottom_margin)/2 - 10;
            if($this->pie3d && !$this->ring)
                $PieChart->draw3DPie(
                    $this->left_margin+$this->width/2-$radius/2 ,
                    $this->height/2,
                    array(
                        "DrawLabels"=>TRUE,
                        "LabelStacked"=>TRUE,
                        "WriteValues"=>$this->by_percent?PIE_VALUE_PERCENTAGE:PIE_VALUE_NATURAL,
                        "DataGapAngle"=>$this->DataGapAngle,
                        "DataGapRadius"=>$this->DataGapRadius,
                        "Radius"    =>$radius*1.5,
                        "Border"=>TRUE,
                        "BorderR"=>255,
                        "BorderG"=>255,
                        "BorderB"=>255,
                        "LabelColor"=>PIE_LABEL_COLOR_AUTO
                    )
                );
            elseif(!$this->pie3d && !$this->ring)
                $PieChart->draw2DPie(
                    $this->left_margin+$this->width/2-$radius/2 ,
                    $this->height/2+10,
                    array(
                        "DrawLabels"=>TRUE,
                        "LabelStacked"=>TRUE,
                        "WriteValues"=>$this->by_percent?PIE_VALUE_PERCENTAGE:PIE_VALUE_NATURAL,
                        "DataGapAngle"=>$this->DataGapAngle,
                        "DataGapRadius"=>$this->DataGapRadius,
                        "Radius"    =>$radius,
                        "Border"=>TRUE,
                        "BorderR"=>255,
                        "BorderG"=>255,
                        "BorderB"=>255,
                        "LabelColor"=>PIE_LABEL_COLOR_AUTO
                    )
                );
            if($this->pie3d && $this->ring)
                $PieChart->draw3DRing(
                    $this->left_margin+$this->width/2-$radius/2 ,
                    $this->height/2,
                    array(
                        "DrawLabels"=>TRUE,
                        "WriteValues"=>$this->by_percent?PIE_VALUE_PERCENTAGE:PIE_VALUE_NATURAL,
                        "DataGapAngle"=>$this->DataGapAngle,
                        "DataGapRadius"=>$this->DataGapRadius,
                        "Radius"    => $radius*1.5,
                        "Border"=>TRUE,
                        "BorderR"=>255,
                        "BorderG"=>255,
                        "BorderB"=>255
                    )
                );
            elseif(!$this->pie3d && $this->ring)
                $PieChart->draw2DRing(
                    $this->left_margin+$this->width/2-$radius/2 ,
                    $this->height/2+10,
                    array(
                        "DrawLabels"=>TRUE,
                        "LabelStacked"=>TRUE,
                        "WriteValues"=>$this->by_percent?PIE_VALUE_PERCENTAGE:PIE_VALUE_NATURAL,
                        "DataGapAngle"=>$this->DataGapAngle,
                        "DataGapRadius"=>$this->DataGapRadius,
                        "Radius"    => $radius,
                        "InnerRadius"    => $radius*0.25,
                        "Border"=>TRUE,
                        "BorderR"=>255,
                        "BorderG"=>255,
                        "BorderB"=>255,
                        "LabelColor"=>PIE_LABEL_COLOR_AUTO
                    )
                );
        
            
            /* Turn on Antialiasing */ 
            $this->picture->Antialias = TRUE; 

            foreach($this->png as $png){
                $this->picture->drawFromPNG(
                    $png['x'],$png['y'],$png['path']
                );
            }
            
        }
    }
