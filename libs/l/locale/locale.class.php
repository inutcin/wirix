<?php

    /**
        Класс локалазации
    */
    class wirix_locale extends wirix{
        
        var $dict_size = 10;    //!< Размер загружаемого словаря
        var $lang = 'ru_RU';        //!< Язык локализации
        var $dict = array();    //!< Словарь терминов
        
        function __construct(){
            parent::__construct();
            $_SERVER['db']->search(array("a"=>"locales"), array(), array("lang"=>$this->lang), "", "last_use DESC", $this->dict_size);
            foreach($_SERVER['db']->rows as $row)
                $this->dict[$row['msgid']] = array("msgstr"=>$row['msgstr'], "use"=>0, "id"=>$row['id']);
        }
        
        
        // Импорт PO-файла в БД
        function import_po_file($filename){
            if(!file_exists($filename)){
                $this->error = _t("File not found");
                return false;
            }
            $fd = fopen($filename);
            while(!feof($fd)){
                $line = fgets($fd);
                if(!preg_match("/^msgid \"(.*)\"$/i", $line, $m1))continue;
                if(!preg_match("/^msgstr \"(.*)\"$/i", $line, $m2))continue;
                echo "$m1[1]=>$m2[1]\n";
            }
            fclose($fd);
            return true;
        }
        
        
        // Отметить слово как использовавшееся
        function set_use($msgid){
            $this->dict[$msgid]['use'] = 1;
        }        
        
        /**
            Получаем перевод для фразы
        */
        function get($msgid, $params = array()){
            $msgstr = '';
            if(isset($this->dict[$msgid]['msgstr']))$msgstr = $this->dict[$msgid]['msgstr'];
            
            // Если в кэше перевода нет - лезем за ним в БД
            if(!$msgstr && $_SERVER['db']->search_one("locales", array("msgid"=>$msgid, "lang"=>$this->lang))){
                $this->dict[$msgid] = array(
                    "msgstr"=>$_SERVER['db']->record['msgstr'], 
                    "use"=>0, 
                    "id"=>$_SERVER['db']->record['id']
                );
                $msgstr = $_SERVER['db']->record['msgstr'];
            }
            
            // Отмечаем строку как использованную
            if(isset($this->dict[$msgid]["use"]))$this->dict[$msgid]["use"] = 1;
            

            // Если перевод нашелся - нго и возвращаем, иначе - саму строку         
            $msgstr = trim($msgstr);
            $msgstr = $msgstr?$msgstr:$msgid; 


            foreach($params as $key=>$val){
                $msgstr = str_replace($key, $val, $msgstr);
            }

            return $msgstr;
            
        }
        
        function flush(){
            // Обновляем в БД время последнего использования использованных переводов
            $ids = "0";
            $date = date("Y-m-d H:i:s");
            foreach($this->dict as $item)if($item['use'])$ids .= ",".$item['id'];
            $_SERVER['db']->update("locales", array("last_use"=>$date), array(), "`id` IN ($ids)", 0);
        }
        
        
        /*
            Импорт строк перевода из PO-файла
        */
        function import_from_po_file(
            $filename,  //!< Файл со строками перевода
            $lang       //!< Код языка
        ){
            if(!file_exists($filename)){    
                $this->error = _t("No such file exists! Double check the name and try again.");
                return false;
            }
            
            if(!$_SERVER['db']->search_one("locale_langs", array("lang"=>$lang))){
                $this->error = _t("Language %s not exists", array("%s"=>htmlspecialchars($lang)));
                return false;
            }
            
            $inserted   = 0;
            $updated    = 0;
            $fd = fopen($filename, "r");
            while(!feof($fd)){
                $line = trim(fgets($fd));
                if(!preg_match('/^msgid\s+"(.+)"$/i', $line, $m1))continue;
                $line = trim(fgets($fd));
                if(!preg_match('/^msgstr\s+"(.+)"$/i', $line, $m2))continue;
                $msgid = $m1[1];
                $msgstr = $m2[1];
                if($_SERVER['db']->search_one("locales", array("msgid"=>$msgid, "lang"=>$lang))){
                    $_SERVER['db']->update("locales", array("msgstr"=>$msgstr), array("msgid"=>$msgid, "lang"=>$lang));
                    $updated++;
                }
                else{
                    $_SERVER['db']->insert("locales", array("msgstr"=>$msgstr,"msgid"=>$msgid, "lang"=>$lang));
                    $inserted++;
                }
            }
            fclose($fd);
            return array($inserted, $updated);
        }
        
        /**
            Export po-файла
        */
        function export_po_file($lang='ru_RU', $filename = ''){
            if(!$filename)$filename = "$lang.po";
            $_SERVER['http']->content_type("text/x-gettext-translation","utf-8");
            $_SERVER['http']->attach($filename);
            $query = "SELECT * FROM `locales` WHERE `lang`='".mysqli_real_escape_string($_SERVER['db']->link, $lang)."'";
            $res = $_SERVER['db']->sql_query($query, "select");
            while($row = $res->fetch_assoc()){
                echo 'msgid "'.$row['msgid'].'"'."\r\n";
                echo 'msgstr "'.$row['msgstr'].'"'."\r\n\r\n";
            }
            die;
        }
        
        
         
    }
