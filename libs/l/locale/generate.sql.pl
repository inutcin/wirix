#!/usr/bin/perl
# Простейший скрипт для обработки po-шайла и преобразования его
# в SQL-дамп таблицы переводов. Обрабатываются только переводы в 1 строку
# В качестве параметра передаётся обозначения языка(например ru), при этом
# файл ru.po должен лежать в папке po. После работы, скрипт положит в папку po
# файл ru.sql. То же и для других языков.
$LANG = $ARGV[0];

die "\nLanguage not defined\n" unless $LANG;

$lang_filename = "./po/$LANG.po";

die "\nPO-file '$lang_filename' not found\n" unless -e $lang_filename;

open (A, $lang_filename);
open (B, ">./po/$LANG.sql");
$key = '';
$value = '';
$strid = '';
LUNE:while(<A>){
    chomp($_);
    if($_=~/^msgid\s+"(.*)"$/i && $1 ne ''){
	$msgid = $1;
	$msgid=~s/\'/\\\'/ig;
	$line = <A>;
	chomp($line);
	if($line=~/^msgstr\s+"(.*)"/i){
	    $msgstr = $1;
	    $msgstr=~s/\'/\\\'/ig;
	    chomp($msg);
	    print B "INSERT INTO `locales`(`lang`,`msgid`,`msgstr`) VALUES('$LANG', '$msgid', '$msgstr');\n";
	}
    }
}
close(B);
close(A);