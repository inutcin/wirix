<?php

    /**
        Класс для авторизации через логинзу
    */
    class wirix_auth_loginza extends wirix_auth{
        var $token_url = '';
        var $widget_id = '';        //!< ID виджета логинзы
        var $secret_key = '';       //!< Секретный ключ виджета
        function __construct(){
            parent::__construct();
            $this->called_class = __CLASS__;
            $this->token_url = "http://".$_SERVER['HTTP_HOST'];
            
            $data = '';
            if(0){
                $json_file = $_SERVER['DOCUMENT_ROOT']."/wirix/libs/a/auth_loginza/auth_data.json.txt";
                $data = file_get_contents($json_file);
            }
            elseif(isset($_POST['token'])){
                $url = "http://loginza.ru/api/authinfo?token=".$_POST['token']."&id=".$this->widget_id."&sig=".md5($_POST['token'].$this->secret_key);
                $_SERVER['http']->url = $url;
                $_SERVER['http']->request();
                $data = $_SERVER['http']->content;
            }
            
            // Если были получены данные от логинзы
            if($data){
                $data = @json_decode(
		    base64_decode($data)
		);
                $data = @get_object_vars($data);
                $data['name'] = @get_object_vars(@$data['name']);
                
                $provider_url   = trim(isset($data['provider'])?$data['provider']:'');
                $identity   = trim(isset($data['identity'])?$data['identity']:'');
                $email      = trim(isset($data['email'])?$data['email']:'');
                $first_name = trim(isset($data['name']['first_name'])?$data['name']['first_name']:'');
                $last_name = trim(isset($data['name']['last_name'])?$data['name']['last_name']:'');
                $dob = trim(isset($data['dob'])?preg_replace("/^(\d+)\-(\d+)\-(\d+)$/", "$3.$2.$1", $data['dob']):"");
                    
                // Если не определён тип провайдера - нахер
                if(!$provider_url){
                    $this->error = 'Не определён url провайдера';
                    return false;
                }

                // Если не определён идентификатор пользователя - нахер
                if(!$identity){
                    $this->error = 'Не определён идентификатор пользователя';
                    return false;
                }
                
                // Если такой провайдер не присутствует в БД - добавляем его туда
                if(trim($provider_url) && !$provider_info = $this->get_provider_by_url($provider_url)){
                    $provider_info = $this->add_provider_by_url($provider_url);
                    if(!$provider_info){
                        $this->error = 'Не удалось получить информацию о провайдере';
                        return false;
                    }
                }

                $profile = $_SERVER['wirix']->lib_load('profile');
                
                // Если пользователь не авторизован и такой провайдер какому-то юзеру прикреплён
                // -- в совершаем вход через соотв провайдера
                if(
                    !$this->is_auth() 
                    && 
                    $user_info = $this->user_info($provider_info['id'], $identity)
                ){
                    if(!$this->auth($user_info)){
                        return false;
                    }
                    $_SERVER['http']->status("302","/");
                }
                
                // Если пользователь не авторизован и такого провайдера с этим ID ни у кого нет
                // -- регистрирум нового пользователя
                if(
                    !$this->is_auth() && isset($provider_info['id']) &&  $provider_info['id'] 
                    && !$user_info = $this->user_info($provider_info['id'], $identity)
                ){
                    // Вычисляем ID нового пользователя
                    if(!$_SERVER['db']->search_one("users", array(), "", "*", "", "`id` DESC")){
                        $this->error = 'Не могу получить ID последнего пользователя';
                        return false;
                    }
                    // Генерим пароль
                    $password = rand(1,10000000);
                    // Создаём аккаунт
                    $user_id = $this->account_create($username, $password);
                    $username = "u".str_replace("0","", date("Y"))."$user_id";
                    
                    if($identity && isset($provider_info['id']) && $provider_info['id']){
                        // Прикрепляем к аккаунту провайдера входа
                        $profile->save_property($provider_info['id'], $user_id, $identity);
                    }
                    
                    if($email){
                        // Прикрепляем к аккаунту почту
                        $profile->save_property(1, $user_id, $email);
                        // Сбрасываем пароль и отсылаем на почту
                        $password = $this->send_new_password($user_id);
                    }
                    
                    // Сохраняем собственное имя пользователя (если указаны хотя бы имя или фамилия)
                    if($first_name || $last_name){
                        $profile->save_property(2, $user_id, $first_name." ".$last_name);
                    }
                    
                    if($dob){
                        $profile->save_property(3, $user_id, $dob);
                    }

                    
                    // Сохраняем логин и пароль пользователя после изменения пароля или без него
                    $this->account_save($username, $password, '', $user_id);
                    
                    if(!$password){
                        $this->error = "Не могу отослать пароль пользователю";
                        return false;
                    }
                    
                    if(!$this->enter($username, $password)){
                        return false;
                    }
                    $_SERVER['http']->status("302", '/account/');
                }

                // Если пользователь авторизован, но такого провайдера для пользователя нет
                // -- добавляем новый альтернативный вход для пользователя
                if(
                    $this->is_auth() && isset($provider_info['id']) &&  $provider_info['id'] 
                    && !$user_info = $this->user_info($provider_info['id'], $identity)
                ){
                    
                    if($identity && isset($provider_info['id']) && $provider_info['id']){
                        // Прикрепляем к аккаунту провайдера входа
                        $profile->save_property($provider_info['id'], $this->user['id'], $identity);
                    }
                    $_SERVER['http']->status("302");
                }
                
                $_SERVER['http']->status("302","/?message=OpenID or OAuth error");

            }
            else{
                $this->error = 'Данные не получены';
                return false;
            }
            
        } 
        
        /**
            Сбрасываем пароль и отсылаем его пользователю с указанным ID 
            и возвращаем новый пароль
        */
        function send_new_password($user_id){
            
            $user_info = $this->user_exists_by_id($user_id);
            if(!$user_info)return false;
            
            $new_password = substr(md5(rand(1,1000000000)),0,8);
            //
            
            // Отправка письма
            
            return $new_password;            
        }
        
        /**
            Получение информации о пользователе по ID провайдера и идентификатору внутри провайдера
        */
        function user_info($provider_id, $identity){
            if(
                !$_SERVER['db']->search_one("user_prop_values", array("user_prop_id"=>$provider_id, "value"=>$identity))
            ){
                return false;
            }
            $user_id  = $_SERVER['db']->record['user_id'];

            if(
                !$_SERVER['db']->search_one("users", array("id"=>$user_id))
            ){
                return false;
            }

            return $_SERVER['db']->record;
        }
        
        /**
            Получение информации о провайдере по его url, полученному от логинзы
        */
        function get_provider_by_url($provider_url){
            if($_SERVER['db']->search_one("user_props", array("name"=>$_SERVER['http']->get_domain($provider_url), "comment"=>$provider_url, "type_id"=>3))){
                return $_SERVER['db']->record;
            }
            return false;
        }
        
        /**
            Добавление нового провайдера по его url
        */
        function add_provider_by_url($provider_url){
            $_SERVER['db']->insert("user_props", array("name"=>$_SERVER['http']->get_domain($provider_url), "title"=>$_SERVER['http']->get_domain($provider_url), "comment"=>
            "OpenID или OAuth - провайдер, доступный по адресу ".$provider_url.". Значения не правятся вручную.", "type_id"=>3, "pattern"=>"/^$/"));
            return $this->get_provider_by_url($provider_url);
        }
        
        function fetch_widget(){
            return '<script src="http://loginza.ru/js/widget.js" type="text/javascript"></script>
<iframe src="http://loginza.ru/api/widget?overlay=loginza&token_url='.$this->token_url.'" 
style="width:359px;height:300px;" scrolling="no" frameborder="no"></iframe>';

        }
        
        function fetch_login_form($user_register=true){
            
            
            /**
            // Добавляем ссылки на регистрацию
            $link = $_SERVER['wirix']->lib_load("html_href");
            $link->text = 'Регистрация';
            $link->url = '/register_loginza/';
            $link->title = 'Регистрация';
            $this->add_link($link);
            unset($link);

            // Добавляем ссылки на вход
            $link = $_SERVER['wirix']->lib_load("html_href");
            $link->text = 'Вход из соцсети';
            $link->url = '/enter_loginza/';
            $link->title = 'Регистрация';
            $this->add_link($link);
            unset($link);
            */
            
            return parent::fetch_login_form(false);
        }
        
    }
