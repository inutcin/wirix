<?php
    include("../../wirix.class.php");
    
    $answer = array();
    $answer['error'] = '';
    $answer['redirect'] = '/profile/';
    
    // Подключаем проверку полей
    include(dirname(__FILE__)."/../../h/html_form/check.ajax.php");
    
    // Проверка пароля и подтверждения
    if(!$answer['error'] && $_SERVER['http']->post("password")!=$_SERVER['http']->post("repassword"))
            $answer['error'] = 'Пароль и подтверждение пароля не совпадают';
    
    // Проверка существования пользователя
    if(!$answer['error'] && $_SERVER['http']->post('send') && $userinfo = $_SERVER['auth']->user_exists_by_username($_SERVER['http']->post('username')))
        $answer['error'] = "Пользователь <b>".$userinfo['name']."</b> уже существует";
        
    // Проверка капчи
    if(
        !$answer['error'] && 
        $_SERVER['http']->post('send') && 
        isset($_SESSION['wirix']['captcha']['captcha']) && 
        $_SERVER['http']->post('captcha_reg')!=$_SESSION['wirix']['captcha']['captcha']
    )
    $answer['error'] = "Цифры с картинки введены неверно ".$_SERVER['http']->post('captcha');
        
    if(
        !$answer['error'] && 
        $_SERVER['http']->post('send') && 
        !$_SERVER['auth']->account_create($_SERVER['http']->post("username"), $_SERVER['http']->post("password"))
    )
    $answer['error'] = $_SERVER['auth']->error;
    
    
    echo json_encode($answer);
?>
