<?php
    include("../../wirix.class.php");
    
    $answer = array();
    $answer['error'] = '';
    $answer['redirect'] = $_SERVER['HTTP_REFERER'];
    
    if(
        $_SERVER['http']->post('send') && 
        !$_SERVER['auth']->enter(
            $_SERVER['http']->post('username'), 
            $_SERVER['http']->post('password')
        )
    )
    $answer['error'] = $_SERVER['auth']->error;

    // Если ошибки - остаёмся на месте
    if($answer['error']){
        $answer['redirect'] = '';
    }

    echo json_encode($answer);
?>
