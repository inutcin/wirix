<?php
    include("../../wirix.class.php");
    
    $answer = array();
    $answer['error'] = '';
    $answer['redirect'] = '';
    
    // Подключаем проверку полей
    include(dirname(__FILE__)."/../../h/html_form/check.ajax.php");
    
    // Проверка пароля и подтверждения
    if(!$answer['error'] && $_SERVER['http']->post('send') && $_SERVER['http']->post("password")!=$_SERVER['http']->post("repassword")){
        $answer['error'] = 'Пароль и подтверждение пароля не совпадают';
    }
    
    // Проверка существования пользователя
    if(
        !$answer['error'] 
        && 
        $userinfo = $_SERVER['auth']->user_exists_by_id($_SERVER['auth']->user['id']) 
    )if($userinfo['id']!=$_SERVER['auth']->user['id'])
        $answer['error'] = "Пользователь <b>".$userinfo['name']."</b> уже существует";
        
    if(
        !$answer['error'] && 
        !$_SERVER['auth']->account_save(
            $_SERVER['http']->post("username"), 
            $_SERVER['http']->post("password"),
            $_SERVER['auth']->user['locked'],
            $_SERVER['auth']->user['id']
        )
    ){
        $answer['error'] = $_SERVER['auth']->error;
        $user_id = $_SERVER['auth']->user['id'];
        $_SERVER['auth']->quit();
        $_SERVER['auth']->enter('', '', $user_id);
        $answer['redirect'] = '?status=Change password success';
    }
    $answer['ok'] = 1;
    echo json_encode($answer);
?>
