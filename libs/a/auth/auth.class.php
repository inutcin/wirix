<?php
    include_once(dirname(__FILE__)."/../../wirix.class.php");
    $_SERVER['wirix']->lib_load("html_form");

    /**
        Форма авторизации
        Пример применения
        
        
        $_SERVER['auth']   = $_SERVER['wirix']->lib_load("auth");
        
        $_SERVER['auth']->id = 'auth_form';
        if($_SERVER['auth']->is_auth()){
            $this->data['auth'] = $_SERVER['auth']->fetch_exit_form();
        }
        else{
            $this->data['auth'] = $_SERVER['auth']->fetch_login_form();
        }
        
    */
    class wirix_auth extends wirix_html_form{

        var $attrs = array();     //!< Массив атрибутов пользователя
        var $user = array();   //!< Данные пользователя
        var $ldap_auth_url = '';
        var $ldap_auth_timeout = 10;
	var $reversedisplayname = false;	//Использовать для импорта из ldap Фамилию Имя
        
        function __construct(){
    	    $this->called_class= __CLASS__;
            parent::__construct();
            // Восстанавливаем из сессии
            $this->title = '';
            $this->use_back_button = false;

            $this->user = isset($_SESSION['wirix']['auth']['user'])?$_SESSION['wirix']['auth']['user']:array();
        }
        
        /**
            Проверка на авторизацию
        */
        function is_auth(){
            if(isset($this->user['id'])) return $this->user['id'];
            return 0;
        }
        
        /**
            Проверка, состоит ли пользователь в указанной группе
        */
        function in_group($group_id){
            if(!isset($this->user['groups_ids']))$this->user['groups_ids'] = "0";
            $groups = explode(",", $this->user['groups_ids']);
            return array_search($group_id, $groups);
        }
        /**
            Псевдоним in_group
        */
        function is_group($group_id){
            return $this->in_group($group_id);
        }
        
        /**
         * Проверерка пары пользователь-пароль, если указан ID, 
         * то имя и пароль игнорируются
         */
        function user_check($username, $password, $id = 0){
            
            if($id = intval($id)){
                // Проверяем есть ли такой пользователь с таким паролем
                $_SERVER['db']->search_one("users", array("id"=>$id));
            }
            else{
                // Проверяем есть ли такой пользователь с таким паролем
                $_SERVER['db']->search_one("users", array(
                    "name"=>$username,
                    "password"=>crypt($password, base64_encode($password))
                ));
            }
            // Если нет идём на выход
            if(!$_SERVER['db']->record){
                $this->error = 'Ошибка авторизации';
                return false;
            }
            elseif(!$_SERVER['db']->record && $_SERVER['db']->record['locked']){
                $this->error = 'Поьзователь заблокирован';
                return false;
            }
            
            // Если есть - сохраняем его основные данные в сессии(ЛОГИН)
            $this->user                 = $_SERVER['db']->record;
            return $this->user;
        }
        
        /**
         * Получение массива атрибутов пользователя
         */
        function get_user_attrs($user_id = 0){
            if(!$user_id)
                $user_id = $this->user['id'];
            // Получаем список атрибутов пользователя из БД
            if(!$_SERVER['db']->search(
                array("a"=>"user_props","b"=>"user_prop_values"),
                array("`a`.`id`=`b`.`user_prop_id`"=>"LEFT"),
                array("`b`.`user_id`"=> $user_id)
            ))return false;

            // Вставить код формирования списка групп
            $this->attrs['groups_id'] = '2';
            
            $this->attrs = array();
            foreach($_SERVER['db']->rows as $row)$this->attrs[$row['name']] = $row;
            return true;
        }
        
        /**
            Проверка существования пользователя с указанным именем, возвращает его
            краткую информацию
        */
        function user_exists_by_username($username){
            $_SERVER['db']->search_one('users', array("name"=>$username));
            return $_SERVER['db']->record;
        }

        /**
            Проверка существования пользователя с указанным ID, возвращает его
            краткую информацию
        */
        function user_exists_by_id($id){
            $_SERVER['db']->search_one('users', array("id"=>$id));
            return $_SERVER['db']->record;
        }

        /**
            Проверка существования группы с указанным именем, возвращает её
            краткую информацию
        */
        function group_exists_by_name($name){
            $_SERVER['db']->search_one('groups', array("name"=>$name));
            return $_SERVER['db']->record;
        }

        /**
            Проверка существования пользователя с указанным ID, возвращает его
            краткую информацию
        */
        function group_exists_by_id($id){
            $_SERVER['db']->search_one('groups', array("id"=>$id));
            return $_SERVER['db']->record;
        }

        /**
            Проверка существования пользователя с указанным ID пользователя vkontakte
        */
        function user_exists_by_vk_id($vid){
            if($_SERVER['db']->search_one('user_prop_values', array("user_prop_id"=>5,"value"=>$vid)))
                if($_SERVER['db']->search_one('users', array("id"=>$_SERVER['db']->record['user_id']))){
                    $this->user = $_SERVER['db']->record;
                    return $_SERVER['db']->record;
                }
            return false;
        }

        /**
            Сохранение логина и пароля аккаунта пользователя
        */
        function account_save($username, $password, $locked = '', $id = 0){
            
            $fields = array("name"=>$username);
            

            if($password)$fields['password'] = crypt($password, base64_encode($password));
            
            if($locked)
                $fields['locked'] = $locked;
            else
                $fields['locked'] = '';
            
            // Если задан ID пользователя - ориентируемся на него. Иначе - на имя пользователя
            if($id)
                $cond = array("id"=>$id);
            else
                $cond = array("name"=>$username);
            
            // Обновляем запись пользователя
            if($_SERVER['db']->update(
                "users", 
                $fields,
                $cond
            ))$this->error = $_SERVER['db']->error;
            
            if(!$this->error)
                $this->error = '';
            else
                $this->error = 'Ошибка регистрации';
                
            return false;
        }


        /**
            Добавление группы
        */
        function group_add($name){
            if(!$user_id = $_SERVER['db']->insert("groups", array("name"=>$name)))
                $this->error = $_SERVER['db']->error;
            return $user_id;
        }
        
        /**
            Удаление группы и всех её связей с пользователями
        */
        function group_del($name){
            $group_id = $this->group_id($name);
            $_SERVER['db']->delete("groups",array("id"=>$group_id));
            $_SERVER['db']->delete("groups_of_users",array("group_id"=>$group_id));
        }

        /**
            Получение ID группы по имени
        */
        function group_id($name){
            // Получаем ID группы
            $group_id = 0;
            if(!$_SERVER['db']->search_one("groups",array("name"=>$name)))
                return $group_id;
            if(isset($_SERVER['db']->record['id']))$group_id = $_SERVER['db']->record['id'];
            return $group_id;
            
        }

        function is_user_group($user_id, $group_id){
            $_SERVER['db']->search_one("groups_of_users",array("user_id"=>$user_id,"group_id"=>$group_id));
            return $_SERVER['db']->record;
        }


        /**
            Добавление пользователя в группу
        */
        function user_group_add($user_id, $group_id){
            if(!$user_id = $_SERVER['db']->insert(
                "groups_of_users", 
                array("user_id"=>$user_id,"group_id"=>$group_id))
            )$this->error = $_SERVER['db']->error;
            return $user_id;
        }
        /**
            Удаление пользователя из группы
        */
        function user_group_del($user_id, $group_id){
            $_SERVER['db']->delete("groups_of_users", 
                array("user_id"=>$user_id,"group_id"=>$group_id)
            );
        }




        /**
            Создание аккаунта пользователя с мастер-группой. 
            Возвращает ID пользователя
        */
        function account_create($username, $password, $enter = true){
            
            
            $crypt_password = crypt($password, base64_encode($password));
            
            if(!$user_id = $_SERVER['db']->insert(
                "users", 
                array("name"=>$username,"password"=>$crypt_password)
            ))$this->error = $_SERVER['db']->error;
            
            // В группу all его
            if(!$group_id = $_SERVER['db']->insert(
                "groups_of_users", 
                array("group_id"=>0, "user_id"=>$user_id)
            ))$this->error = $_SERVER['db']->error;
            
            if(!$this->error && $enter && $this->enter($username, $password, $user_id))
                $this->error = '';
            else
                $this->error = 'Ошибка регистрации';
                
            return $user_id;
        }
        
        function account_delete($id){
            $_SERVER['db']->delete("users", array("id"=>$id));
            $_SERVER['db']->delete("user_prop_values", array("user_id"=>$id));
            $_SERVER['db']->delete("groups_of_users", array("user_id"=>$id));
            return true;
        }
        
        /**
            Произведение входа по логину и паролю, или по ID пользователя(если указать ID, 
            то логин-пароль игнорируются)
        */
        function enter($username, $password, $id = 0){
            
            // Если не заданы логин и пароль, но задан ID пользователя - входим по ID
            if(!$username && !$password && $id=intval($id) && $check = $this->user_exists_by_id($id)){
                $this->auth($check);
                return true;
            }
            // Если задан url для ldap-авторизации - пробуем логиниться через LDAP
            if(!$id && $this->ldap_auth_url)
                $user_info = $this->ldap_auth($username, $password);
            else
                $user_info = array("error"=>"Не указан URL авторизации");
            
            // Если успешно авторизовлись - смотрим есть ли такой пользователь в БД
            if(
                !$user_info['error'] && 
                !$this->user_exists_by_username($username)
            ){
                // Создаём новый аккаунт
                $user_id = $this->account_create($username, $password);
            }
            elseif(
                !$user_info['error'] && 
                $user = $this->user_exists_by_username($username)
            ){
                // меняем данные аккаунта
                $this->account_save($username, $password);
                $user_id = $user['id'];
            }
            
            if(!$user_info['error']){
                ////// ============= Загружаем фото =================
                //Определяем ID свойства-фото
                $profile = $_SERVER['wirix']->lib_load('profile');
                $props = $profile->get_props($user_id);
                // Сохраняем фотку во временной папке
                $tmp_filename = sys_get_temp_dir()."/".md5($user_id).".jpg";
                file_put_contents($tmp_filename, property_exists($user_info['data'], "jpegphoto")?base64_decode($user_info['data']->jpegphoto):"");
                // Сделаем из неё аватару
                $profile->save_userphoto(7,$user_id,'photo',$tmp_filename);
                // Удалим временный файл
                unlink($tmp_filename);
                
                /////   ============== Синхронизируем группы ==============
                // Если группы не существует - создаём её
                foreach($user_info['groups'] as $group)
                    if(!$this->group_exists_by_name($group))
                        $group_id = $this->group_add($group);
                
                // Если групы есть в системе, но нет в LDAP - удаляем их и их привязки к пользователям
                /*
                $groups = $this->groups_array();
                foreach($groups as $group);
                    if(!array_search($group, $user_info['groups']) && $group!='admin')
                        $this->group_del($group);
                */
                        
                // Если привязки к группе у пользователя нет, а в LDAP есть - 
                // привязываем группу к пользоватедю
                foreach($user_info['data']->memberof as $group){
                    $group_id = $this->group_id($group);
                    if($group_id && !$this->is_user_group($user_id, $group_id))
                        $this->user_group_add($user_id, $group_id);
                }
                
                // Если привязка к группе есть, а в LDAP её нет - удаляем привязку
                /*
                $groups = $this->groups_array($user_id);
                foreach($groups as $group){
                    $group_id = $this->group_id($group);
                    if(!array_search($group, $user_info['data']->memberof))
                        if($group_id && $user_id)
                            $this->user_group_del($user_id, $group_id);
                }
                */
                
                //////================ Стягивание параметров учетки
                $fields = array("email"=>"mail","phone"=>"telephonenumber",
                    "fio"=>$this->reversedisplayname?"reversedisplayname":"displayname","birthdate"=>"pager");
                
                
                $user_info['data']->pager = preg_replace("/^(\d+)\-(\d+)\-(\d+)$/", "$3.$2.$1", $user_info['data']->pager);
                foreach($fields as $fromf=>$tof)
                        $profile->save_property($props[$fromf]['user_prop_id'], $user_id, $user_info['data']->$tof);
                
            }
            
            if($check = $this->user_check($username, $password))
                $this->auth($check);
            else
                $this->error = 'Ошибка авторизации';

            return $check;
        }
        
        
        function ldap_auth($username, $password){
            $_SERVER['http']->url = $this->ldap_auth_url;
            $_SERVER['http']->timeout = $this->ldap_auth_timeout;
            $_SERVER['http']->post_params = array(
                "username"=>$username, 
                "password"=>$password
            );
            $_SERVER['http']->request();

            if(!$data = @json_decode(
		base64_decode($_SERVER['http']->content)
	    ))
                $data = array("error"=>"Ошибка LDAP-авторизации");
            else
                $data = get_object_vars($data);

            return $data;
        }
        
        /**
            Авторизация пользователя ($this->user должен содержать его данные)
        */
        function auth($check){
            if(!$check['locked']){
                $_SESSION['wirix']['auth']['user'] = $check;
                $_SESSION['wirix']['auth']['user']['groups_ids'] = $this->groups_list($check['id']);
            }
            else{
                $this->error = 'Пользователь заблокирован';
                return false;
            }
                
            return true;
        }

        /**
            Возвращает список групп, в которые входит пользователь(массив имён)
            Если пользователь не указан - массив всех групп
        */
        function groups_array($user_id = 0){
            $user_id = intval($user_id);
            $fields = array("`a`.`group_id`"=>"group_id","`b`.`name`"=>"group_name");
            $conds = array();
            if($user_id)$conds = array("`a`.`user_id`"=>$user_id);
            $_SERVER['db']->search(
                array("a"=>"groups_of_users","b"=>"groups"), 
                array("`a`.`group_id`=`b`.`id`"=>$user_id?"LEFT":"RIGHT"), 
                $conds,
                "","",0,0,$fields
            );
            $result = array();
            foreach($_SERVER['db']->rows as $row)$result[] = $row['group_name'];
            return $result;
        }

        /**
            Возвращает список групп, в которые входит пользователь(через запятую)
        */
        function groups_list($user_id = 0){
            $user_id = intval($user_id);
            $fields = array();
            if($user_id)$fields = array("`a`.`user_id`"=>$user_id);
            $_SERVER['db']->search(
                array("a"=>"groups_of_users"), 
                array(), 
                $fields,
                ""
            );
            $result = "0";
            foreach($_SERVER['db']->rows as $row)$result .= ",".$row['group_id'];
            return $result;
        }
        
        /**
            Разлогин пользователя
        */
        function quit(){
            $_SESSION['wirix']['auth']['user'] = array();
            $this->user = array();
        }
        
        /**
         * Получение HTML-кода формы авторизации
         */
        function fetch_login_form($user_register=true){
            $html = '';
            $this->send_button_text = 'Войти';

            $this->submit_url = '/wirix/libs/a/auth/auth.ajax.php';

            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'username';
            $input->title = 'Логин';
            $input->regexp = "/^.{1,32}$/";
            $input->default = '';
            $input->comment = 'Имя пользователя должно содержать от 4 до 32 любых символов.';
            $this->add_input($input);
            unset($input);
        
            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->type='password';
            $input->title = 'Пароль';
            $input->regexp = "/^.+$/";
            //$input->comment = 'пароль должен содержать от 6 до 32 различных символов';
            $input->name = 'password';
            $input->default = '';
            $this->add_input($input);
            unset($input);


            if($user_register){
                // Добавляем ссылки на регистрацию
                $link = $_SERVER['wirix']->lib_load("html_href");
                $link->text = 'Регистрация';
                $link->url = '/register/';
                $link->title = 'Регистрация';
                $this->add_link($link);
                unset($link);
            }

            return $this->fetch();
        }

        /**
         * Получение HTML-кода формы регистрации
         */
        function fetch_register_form(){
            $html = '';
            $this->id = 'register_form';
            $this->title = 'Регистрация';
            $this->send_button_text = 'Зарегистрироваться';

            $this->submit_url = '/wirix/libs/a/auth/register.ajax.php';
            
            $input = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'username';
            $input->title = 'Пользователь';
            $input->regexp = "/^.{4,32}$/";
            $input->default = 'Введите имя пользователя';
            $input->comment = 'Имя пользователя должно содержать от 4 до 32 любых символов.';
            $this->add_input($input);
            unset($input);
            
            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->type='password';
            $input->title = 'Пароль';
            $input->regexp = "/^.+$/";
            $input->comment = 'пароль должен содержать от 6 до 32 различных символов';
            $input->name = 'password';
            $input->default = '';
            $this->add_input($input);
            unset($input);
        
            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->type='password';
            $input->title = 'Повтор пароля';
            $input->regexp = "/^.+$/";
            $input->comment = 'Повтор пароля должен совпадать с паролем';
            $input->name = 'repassword';
            $input->default = '';
            $this->add_input($input);
            unset($input);
            
        
            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->type='captcha';
            $input->title = 'Защита от авторегистраций';
            $input->regexp = "/^\d{5}$/";  
            $input->comment = '';
            $input->name = 'captcha_reg';
            $input->default = 'Введите цифры с картинки';
            $this->add_input($input);
            unset($input);
        
            return $this->fetch();
        }


        /**
         * Получение HTML-кода формы изменения регистрационных данных
         */
        function fetch_account_form(){
            $html = '';
            $this->title = '';

            $this->submit_url = '/wirix/libs/a/auth/account.ajax.php';
            
            $input = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'username';
            $input->title = _t('Username');
            $input->regexp = "/^.{4,32}$/";
            $input->default = 'Введите имя пользователя';
            $input->value = $_SERVER['auth']->user['name'];
            $input->comment = 'Имя пользователя должно содержать от 4 до 32 любых символов.';
            $this->add_input($input);
            unset($input);
            
            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->type='password';
            $input->title = _t('Password');
            $input->regexp = "/^.{6,32}$/";
            $input->comment = 'пароль должен содержать от 6 до 32 различных символов. Если не хотите менять пароль - оставьте поле пустым.';
            $input->name = 'password';
            $input->default = '';
            $this->add_input($input);
            unset($input);
        
            $input  = $_SERVER['wirix']->lib_load("html_input");
            $input->type='password';
            $input->title = _t('Confirm new password');
            $input->regexp = "/^.{6,32}$/";
            $input->comment = 'Повтор пароля должен совпадать с паролем';
            $input->name = 'repassword';
            $input->default = '';
            $this->add_input($input);
            unset($input);
            
            return $this->fetch();
        }

        /**
         * Получение HTML-кода формы выхода
         */
        function fetch_exit_form($use_profile = true, $use_accout = true){
            $profile = $_SERVER['wirix']->lib_load('profile');
            $profile = $profile->get_props($_SERVER['auth']->user['id'], 2);
            $this->title = '<i class="icon-user"></i><a href="/profile/">'. (isset($profile['fio']['value']) && $profile['fio']['value']?$profile['fio']['value']:$_SERVER['auth']->user['name'])."</a>";
            $this->send_button_text = _t('Log Out');
            
            
            $input = $_SERVER['wirix']->lib_load("html_input");
            $input->name = 'class';
            $input->type = 'hidden';
            $input->value = $this->called_class;
            $this->add_input($input);
            unset($input);
            
            $html = '';

            $this->submit_url = '/wirix/libs/a/auth/exit.ajax.php';
            
            unset($profile);

            return $this->fetch();
        }
        
        /**
            Экспорт паролей пользователей в htpasswd-файл
        */
        function htpasswd_export($filename=''){
            if(!$filename)$filename = $_SERVER['DOCUMENT_ROOT']."/.htpasswd";
            
            $_SERVER['db']->search(array("a"=>"users"), array(), array("locked"=>""));
            $fd = fopen($filename, "w");
            foreach($_SERVER['db']->rows as $row)
                fwrite($fd, $row['name'].":".$row['password']."\n");
            fclose($fd);
        }



        
    }

?>
