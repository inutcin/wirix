<?php

    /**
        Скрипт авторизации. Принимает $_POST['username'], $_POST['password']
        возвращает в случае успешной авторизации.
        
        - ФИО
        - email
        - телефон
        - картинка
        - дата рождения
        ?????- факт блокировки пользователя(уволен)?????
        
        
    */
    $error = '';
    if(!isset($_POST['username']) || !isset($_POST['password'])){
        $error = 'Не указан логин или пароль';
        $_POST['username'] = '';
        $_POST['password'] = '';
    }
    elseif(!preg_match("/^[\w\d\-\.\_]+$/i", $_POST['username'])){
        $error = 'Некорректное имя пользователя';
        $_POST['username'] = '';
    }
    
    define("LDAP_SERVER","cit.local");
    define("LDAP_BASE","ou=Inform-S, ou=d-users, dc=cit,dc=local");
    define("LDAP_FILTER","(&(objectclass=user)(objectcategory=Person)(sAMAccountName=".$_POST['username']."))");
    $LDAP_THERE = array(
        'mail',             // email
        'displayname',      // ФИО
        'mobile',           // Мобильник
        'pager',            // Дата рождения
        'telephonenumber',  //Внут.номер тел.
        'jpegphoto'         // Фотка(bin)
    );
    
    $_POST['username'] = $_POST['username']."@".LDAP_SERVER;

    /**
        Получение информации о пользователе по LDAP
    */
    function get_ldap_users($username, $password){
        global $LDAP_THERE;
        global $error;

        if(!$ldapconn = @ldap_connect(LDAP_SERVER))
            $error = 'Не могу соединиться с LDAP-сервером';
        if($error)return array();
        
        ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($ldapconn, LDAP_OPT_REFERRALS, 0);

        if(!$ldapbind = @ldap_bind($ldapconn, $username, $password))
            $error = 'Ошибка авторизации';
        if($error)return array();
        
        if(!$sr=@ldap_search($ldapconn, LDAP_BASE, LDAP_FILTER, $LDAP_THERE))
            $error = 'Ошибка применения LDAP-фильтра';
        if($error)return array();
            
        if(!$info = @ldap_get_entries($ldapconn, $sr))
            $error = 'Ошибка применения LDAP-данных';
        if($error)return array();

        return $info;
    }
    
    
    if(!$error)
        $user_data = get_ldap_users($_POST['username'], $_POST['password']);

    $data = array();
    foreach($LDAP_THERE as $field){
        if(!isset($user_data[0][$field][0]))continue;
        $data[$field] = $user_data[0][$field][0];
    }
    if(isset($data['jpegphoto']))$data['jpegphoto'] = base64_encode($data['jpegphoto']);
    echo json_encode(array("error"=>$error, "data"=>$data));   
?>
