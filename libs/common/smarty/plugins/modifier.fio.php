<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */
function smarty_modifier_fio($string,$countchars)
{
	$string = iconv("UTF-8","windows-1251",$string);
	//iconv("windows-1251","UTF-8",
	$string = trim($string);
	$StringLength = strlen($string);
	$Array = explode(" ",$string);
	$Position = strpos($string," ");
	if($StringLength > $countchars)
	{
		$Temp = explode(" ",$string);

		if($Position > $countchars)
		{
			if(strlen($Temp[0]) > $countchars)
			{
				return iconv("windows-1251","UTF-8",substr($Temp[0],0,$countchars))."...";
			}
			else 
				return iconv("windows-1251","UTF-8",$Temp[0]);
		}
		else 
		{ 
			if($Position == 0)
				return iconv("windows-1251","UTF-8",substr($Temp[0],0,$countchars))."...";
			for($i = strlen($string);$i > 0; $i--)
			{
				if($string{$i} == " ")
				{
					$Position = $i;
					break;
				}
			}
			$Temp = substr($string,0,$Position);
			return iconv("windows-1251","UTF-8",$Temp);
		}
		
	}
	else 
	{
		return iconv("windows-1251","UTF-8",$string);
	}

}

/* vim: set expandtab: */

?>
