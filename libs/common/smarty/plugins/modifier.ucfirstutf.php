<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

/**
 * Smarty plugin
 *
 * Type:     modifier<br>
 * Name:     grins<br>
 * Date:     March 6, 2007
 * Purpose:  converts smilies to img tags
 * Input:<br>
 *         - string: input string to replace
 * Example:  {$text|grins}
 * @version  1.0
 * @author   Jelena Pavlovic <jelena@arraystudio.com>
 * @param string
 * @return string
 */
function smarty_modifier_ucfirstutf($string)
{
	$e = "utf-8";
    $fc = mb_strtoupper(mb_substr($string, 0, 1, $e), $e);
    return $fc.mb_substr($string, 1, mb_strlen($string, $e), $e);
}
?>
