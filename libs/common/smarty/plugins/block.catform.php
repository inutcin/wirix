<?php
/**
 * Project: Formcat, form client-side validate class.
 * 
 * (c) 2004-2005 Justto.com Propagator Team
 * Author: Joey Wong [joey@justto.com][MSN:gzjoey@hotmail.com]
 * Website: http://formcat.justto.com
 * Released under both BSD and GNU Lesser GPL library license. 
 * This means you can use it in proprietary products.
 *
 * For questions, help, comments, discussion, etc.,please contact
 * joey@justto.com
 * 
 * The latest version of Formcat can be obtained from:
 * http://formcat.justto.com/ or http://formcat.joey.cn
 *
 * @link http://formcat.justto.com/ or http://formcat.joey.cn
 * @copyright 2004,2005 Joey Wong.
 * @author Joey <joey@163.com> [msn:gzjoey@hotmail.com]
 **/
 
 /*
 * Smarty plugin
 * -------------------------------------------------------------
 * Type:     block
 * Name:     catform
 * Purpose:  calculate the multi-form and place the onSubmit string.
 * -------------------------------------------------------------
 */
 
function smarty_block_catform($params, $content, &$smarty){
	
	//set the current form
	$form_name = (isset($params['form']))?$params['form']:$params['FORM'];
    $GLOBALS['_currentForm'] = $form_name;
    
    //store the form name list to session 
    if(is_array($_SESSION['catformList'])){
    	if(!in_array($form_name,$_SESSION['catformList'])){
    	
    		array_push($_SESSION['catformList'],$form_name);
    	}
    }else{
    	$_SESSION['catformList'] = array($form_name);
	}

   
    //process the form tags
    if(preg_match('/<(form|FORM|Form)[^>]*>/', $content, $regs)) $formTags = $regs[0];
   
    $replaceTags = rtrim($formTags,">")." onsubmit='return pigcatFCValidate_".$form_name."(this);'>";   
    $content = str_replace($formTags,$replaceTags,$content);
	echo $content;
	return;

}
?>