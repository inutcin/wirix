<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

function smarty_modifier_httpcheck($string)
{
	if(!preg_match("/http:\/\//",$string))
	{
		return "http://".$string;
	}
	else 
	{
		return $string;
	}
}
?>
