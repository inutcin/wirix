<?
/**
 * Project: Formcat, form client-side validate class.
 * 
 * (c) 2004-2005 Justto.com Propagator Team
 * Author: Joey Wong [joey@justto.com][MSN:gzjoey@hotmail.com]
 * Website: http://formcat.justto.com
 * Released under both BSD and GNU Lesser GPL library license. 
 * This means you can use it in proprietary products.
 *
 * For questions, help, comments, discussion, etc.,please contact
 * joey@justto.com
 * 
 * The latest version of Formcat can be obtained from:
 * http://formcat.justto.com/ or http://formcat.joey.cn
 *
 * @link http://formcat.justto.com/ or http://formcat.joey.cn
 * @copyright 2004,2005 Joey Wong.
 * @author Joey <joey@163.com> [msn:gzjoey@hotmail.com]
 **/
 
 /*
 * Smarty plugin
 * -------------------------------------------------------------
 * Type:     function
 * Name:     formcat
 * Purpose:  store the form validation to session
 * -------------------------------------------------------------
 */
function smarty_function_formcat($params, &$smarty) {
	
	$form_name = $GLOBALS['_currentForm'];
	if(!is_array($_SESSION['catform_'.$form_name])) $_SESSION['catform_'.$form_name] = array();
	
	$_SESSION['catform_'.$form_name] = array_merge($_SESSION['catform_'.$form_name],array($params));
	
	
}
?>