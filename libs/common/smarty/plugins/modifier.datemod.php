<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

/**
 * Smarty plugin
 *
 * Type:     modifier<br>
 * Name:     grins<br>
 * Date:     March 6, 2007
 * Purpose:  converts smilies to img tags
 * Input:<br>
 *         - string: input string to replace
 * Example:  {$text|grins}
 * @version  1.0
 * @author   Jelena Pavlovic <jelena@arraystudio.com>
 * @param string
 * @return string
 */
function smarty_modifier_datemod($string)
{
		$RussianNames = array(
		"01" 	=>	"января",
		"02" 	=>	"февраля",
		"03" 	=>	"марта",
		"04" 	=>	"апреля",
		"05" 	=>	"мая",
		"06" 	=>	"июня",
		"07" 	=>	"июля",
		"08" 	=>	"августа",
		"09" 	=>	"сентября",
		"10" 	=>	"октября",
		"11" 	=>	"ноября",
		"12" 	=>	"декабря",
		);
	$Year = substr($string,0,4);
	$Month = substr($string,5,2);
	$Day = substr($string,8,2);
	if($Day  < 10)
	{
		$Day = substr($Day,1,1); 
	}
    return $Day." ".$RussianNames[$Month];
}
?>
