<?php

    /**
        Класс схемы данных
    */
    class wirix_datascheme extends wirix{

        var $title = 'Схема данных';        //!< Заголовок схемы данных
        var $fields             = array();  //!< Поля
        var $keys               = array();  //!< Ключи
        var $primary_key        = "id";     //!< Имя поля - первичного ключа 
        var $condition          = '';       //!< Условия выборки объектов формат переопределяет потомками класса
        var $uniques            = array();  //!< Уникальные ключи. 
        var $title_field        = "name";   //!< Титульное поле
        var $enabled_fields     = array("id","name");  //!< Поля, которые надо показывать при выборке
        var $hidden_fields      = array();
        var $sort               =  '`a`.`id` ASC';  //!< Сортировка результата 
        var $patterns = array(
            "int"           => "^[\+\-]?\d+$",
            "smallint"      => "^[\+\-]?\d+$",
            "tinyint"       => "^[\+\-]?\d+$",
            "timestamp"     => "^\d{4}\-\d{2}\-\d{2} \d{2}:\d{2}:\d{2}$",
            "datetime"      => "^\d{4}\-\d{2}\-\d{2} \d{2}:\d{2}:\d{2}$",
            "date"          => "^\d{4}\-\d{2}\-\d{2}$",
            "time"          => "^\d{2}:\d{2}:\d{2}$",
            "enum"          => "^.*$",
            "char"          => "^.*$",
            "varchar"       => "^.*$"
        ); //!< Предопределённые шаблоны регекспов для проверки данных
        /*
        Массив, вида
            $this->uniques = array(
                "fields"    => array("fields_name1","fields_name2"),
                "error"     => "Текст ошибки при попытке нарушить уникальность"
            )
        */


        function __construct(){
            $this->called_class = __CLASS__;
        }

        /**
            Установка комментария для поля
        */
        function set_comment($field_name, $comment){
            $this->fields[$field_name]['comment'] = $comment;
        }


        /**
            Установка описания для поля
        */
        function set_description($field_name, $descriprion){
            $this->fields[$field_name]['description'] = $descriprion;
        }

        /**
            Проверка валидности полей, полученных в виде массива имя_поля=>значение
            по регулярным выражениям из $this->regexps
        */
        function check_validate($fields){
            foreach($fields as $k=>$v)
                if(
                    isset($this->regexps[$k]['regexp']) 
                    && 
                    !preg_match("/".$this->regexps[$k]['regexp']."/iu", $v)
                ){
                    $this->error = $this->regexps[$k]['error_message'];
                    return false;
                }
            return true;
        }

        /**
            Установка видимых полей
        */
        function set_enabled_fields($fields = array()){
            $this->enabled_fields = $fields;
        }


        /**
            Определяем какие поля будут скрытыми
        */
        function set_hidden_fields($fields){
            $this->hidden_fields = $fields;
        }

        /**
            Установка валидации поля
        */
        function set_validate(
            $error_message, //!<  
            $field_name, 
            $pattern,       //!< Имя предопределённого шаблона из $this->patterns
            $regexp = ""    //!< Конкретный регексп, если не указан шаблон $pattern
        ){
            if(!$pattern){
                $this->regexps[$field_name] = array(
                    "regexp"=>$regexp,
                    "error_message"=>$error_message
                );
            }
            elseif($pattern && isset($this->patterns[$pattern])){
                $this->regexps[$field_name] = array(
                    "regexp"=>$this->patterns[$pattern],
                    "error_message"=>$error_message
                );
            }
            
        }

        /**
            Установка заголовка схемы данных
        */
        function set_title($title){
            $this->title = $title;
            return true;
        }

        /**
            Установка первичного ключа
        */
        function set_primary($val){
            $this->primary_key = $val;
        }

        /**
            Установка титульного поля
        */
        function set_title_field($field_name){
            $this->title_field = $field_name;
        }


        /**
            Установка условия выборки данных
        */
        function set_condition($cond){
            $this->condition = $cond;
        }

        /**
            Сохранение объекта схемы, для того, чтобы установить сохраняемые 
            значения полей и первичный ключ, используется метод set()
        */
        function save(){
        }
        
        
        /**
            Очищает все значения полей
        */
        function clear(){
            foreach($this->fields as $field_name=>$field)$this->set($field_name, '');
        }
        
        /**
            Получение значений полей в виде массива
        */
        function get_as_array(){
            $result = array();
            foreach($this->fields as $k=>$v)
                if(array_search($k, $this->enabled_fields)!==false)
                    $result[$k] = isset($v['value'])?$v['value']:'';
                    
            return $result;
        }
        
        /**
            Получение значения поля
        */
        function get($field_name){
            if(!isset($this->fields[$field_name])){
                $this->error = 'Нет поля с именем '.$field_name;
                return false;
            }
            if(!isset($this->fields[$field_name]['value']))
                $this->fields[$field_name]['value'] = '';
            return $this->fields[$field_name]['value'];
        }
        
        /**
            Установка значения поля
        */
        function set(
            $field_name,    //!< Имя поля или массив значений вида ключ=>значение
            $field_value='' //!< Значение поля (не нужно при назанчении массивом)
        ){
            
            if(!is_array($field_name)){
                if(!isset($this->fields[$field_name])){
                    $this->error = "Поля $field_name не существует";
                    return false;
                }
                $this->fields[$field_name]['value'] = $field_value;
            }
            else{
                foreach($field_name as $k=>$v){
                    $this->set($k, $v);
                }
            }
            
            return true;
        }
        
        /**
            Сохранение объекта схемы, для того, чтобы установить 
            значения полей додавяемого оъекта и первичный ключ, 
            используется метод set()
        */
        function add(){
        }
        
        /**
            Удаление объекта схемы
        */
        function delete(){
        }
        
        /**
            Поиск объектов схемы, производится по значениям фильтров для полей, 
            либо по условию в $this->condition
        */
        function find($type = 'all'){
        }


        /**
            Инициализация схемы данных
        */
        function init(){
        }
        

        /**
            Преобразование объекта в массив
        */
        function array_from_object($obj){
            $arr = get_object_vars($obj);
            foreach($arr as $k => $v)
                if(is_object($v))$arr[$k] = $this-> array_from_object($v);
            return $arr;
        }
        
        
        /**
            Загрузка схемы данных из сессеии
        */
        function load_from_session(){
            // Провеляем есть ли объект схемы данных в сессии
            if(!isset($_SESSION['wirix'][$this->called_class])){
                wirix_error::alert("Нет информации о схеме данных в сессии");
                return false;
            }
            
            foreach($_SESSION['wirix'][$this->called_class] as $item)break;
            
            $scheme = $this->array_from_object(json_decode(
		base64_decode($item)
	    ));
            
            // Импортируем общие настройки
            foreach($scheme as $k=>$v)
                if(is_scalar($this->$k) && is_scalar($scheme[$k]))$this->$k = $v;
                
            // Импортируем поля формы
            foreach($scheme['fields'] as $field_name=>$field){
                foreach($field as $k=>$v){
                    if(
                        (is_scalar($this->fields[$field_name][$k]) && is_scalar($v))
                        ||
                        (is_array($this->fields[$field_name][$k]) && is_array($v))
                    ){$this->fields[$field_name][$k] = $v;continue;}
                }
            }
            
            // Импортируем досткпные поля
            $this->enabled_fields = $scheme['enabled_fields'];
            $this->hidden_fields = $scheme['hidden_fields'];
            $this->regexps = $scheme['regexps'];
            $this->patterns = $scheme['patterns'];
            
            return true;
        }
    }
