<?php
    include_once(dirname(__FILE__)."/../../wirix.class.php");

    class wirix_dates extends wirix{
        
        function __construct(){
        }
        
        /**
            Перевод даты из одного формата в другой. Типы форматов
            
            0) Неизвестный
            1) 2012-04-06 00:00:00
            2) 06 Апр 2012 00:00:00
            3) 06.04.2012
            4) 06.04.2012 00:00:00
            5) 06 Апр 2012
            6) Timestamp
            7) 2012-04-06
            8) April 2012
            9) 06 апреля 2012 г.
            10) 06.Апр.12
            11) Апрель 2012
        */
        static function translate(
            $date,          //!< Дата
            $to_type =2,    //!< Тип в который преобразуем
            $from_type = 0  //!< Тип из котрого преобразуем
        ){
            $mon = array(
                "1"=>"Jan",
                "2"=>"Feb",
                "3"=>"Mar",
                "4"=>"Apr",
                "5"=>"May",
                "6"=>"Jun",
                "7"=>"Jul",
                "8"=>"Aug",
                "9"=>"Sep",
                "10"=>"Oct",
                "11"=>"Nov",
                "12"=>"Dec",
                "01"=>"Jan",
                "02"=>"Feb",
                "03"=>"Mar",
                "04"=>"Apr",
                "05"=>"May",
                "06"=>"Jun",
                "07"=>"Jul",
                "08"=>"Aug",
                "09"=>"Sep",
                "10"=>"Oct",
                "11"=>"Nov",
                "12"=>"Dec"
            );

            $mond = array(
                "1"=>"January",
                "2"=>"February",
                "3"=>"March",
                "4"=>"April",
                "5"=>"May",
                "6"=>"June",
                "7"=>"July",
                "8"=>"August",
                "9"=>"September",
                "10"=>"October",
                "11"=>"November",
                "12"=>"December",
                "01"=>"January",
                "02"=>"February",
                "03"=>"March",
                "04"=>"April",
                "05"=>"May",
                "06"=>"June",
                "07"=>"July",
                "08"=>"August",
                "09"=>"September",
                "10"=>"October",
                "11"=>"November",
                "12"=>"December"
            );
            
            $monh = array(
                "1"=>"Января",
                "2"=>"Февраля",
                "3"=>"Марта",
                "4"=>"Апреля",
                "5"=>"Мая",
                "6"=>"Июня",
                "7"=>"Июля",
                "8"=>"Августа",
                "9"=>"Сентября",
                "10"=>"Октября",
                "11"=>"Ноября",
                "12"=>"Декабря",
                "01"=>"Января",
                "02"=>"Февраля",
                "03"=>"Марта",
                "04"=>"Апреля",
                "05"=>"Мая",
                "06"=>"Июня",
                "07"=>"Июля",
                "08"=>"Августа",
                "09"=>"Сентября",
                "10"=>"Октября",
                "11"=>"Ноября",
                "12"=>"Декабря"
            );

            $monh1 = array(
                "1"=>"Январь",
                "2"=>"Февраль",
                "3"=>"Март",
                "4"=>"Апрель",
                "5"=>"Май",
                "6"=>"Июнь",
                "7"=>"Июль",
                "8"=>"Август",
                "9"=>"Сентябрь",
                "10"=>"Октябрь",
                "11"=>"Ноябрь",
                "12"=>"Декабрь",
                "01"=>"Январь",
                "02"=>"Февраль",
                "03"=>"Март",
                "04"=>"Апрель",
                "05"=>"Май",
                "06"=>"Июнь",
                "07"=>"Июль",
                "08"=>"Август",
                "09"=>"Сентябрь",
                "10"=>"Октябрь",
                "11"=>"Ноябрь",
                "12"=>"Декабрь"
            );
            
            switch($from_type){
                case 1:
                    $dates = date_parse($date);
                break;
                case 6:
                    $dates = array(
                        "year"      => date("Y", $date),
                        "month"     => date("m", $date),
                        "day"       => date("d", $date),
                        "hour"      => date("H", $date),
                        "minute"    => date("i", $date),
                        "second"    => date("s", $date)
                    );
                break;
                default:
                    $dates = date_parse($date);
                break;
            }

            if(!isset($dates['year']) || !$dates['year'])
                $timestamp = 0;
            else
                $timestamp = mktime($dates['hour'],$dates['minute'],$dates['second'],$dates['month'],$dates['day'],$dates['year']);

            if(!$timestamp){
                switch($to_type){
                    case 1:
                        $date = "NULL";
                    break;
                    case 2:
                        $date = _t("Undefined");
                    break;
                    case 3:
                        $date = "";
                    break;
                    case 4:
                        $date = "";
                    break;
                    case 5:
                        $date = _t("Undefined");
                    break;
                    case 6:
                        $date = _t("Undefined");
                    break;
                }
            }
            else{
                switch($to_type){
                    case 1:
                        $date = date("Y-m-d H:i:s", $timestamp);
                    break;
                    case 2:
                        $date = date("d",$timestamp)." "._t($mon[$dates['month']])." ".date("Y H:i:s", $timestamp);
                    break;
                    case 3:
                        $date = date("d.m.Y", $timestamp);
                    break;
                    case 4:
                        $date = date("d.m.Y H:i:s", $timestamp);
                    break;
                    case 5:
                        $date = date("d",$timestamp)." "._t($mon[$dates['month']])." ".date("Y", $timestamp);
                    break;
                    case 6:
                        $date = $timestamp;
                    break;
                    case 7:
                        $date = date("Y-m-d", $timestamp);
                    break;
                    case 8:
                        $date = _t($mond[$dates['month']])." ".date("Y", $timestamp);
                    break;
                    case 9:
                        $date = date("d",$timestamp)." "._t($monh[$dates['month']])." ".date("Y", $timestamp)." г.";
                    break;
                    case 10:
                        $date = date("d",$timestamp)."."._t($mon[$dates['month']]).".".date("y", $timestamp);
                    break;
                    case 11:
                        $date = $monh1[$dates['month']]." ".date("Y", $timestamp);
                    break;
                }
            }

            return $date;
        }
        
        
        /**
            Функция возвращает в виде timestamp дату, получающую  в любом формате, который осили php-функция date_parse
        */
        static function date_parse($date){
            $dates = date_parse($date);

            if(!isset($dates['year']) || !$dates['year'])
                $timestamp = 0;
            else
                $timestamp = mktime($dates['hour'],$dates['minute'],$dates['second'],$dates['month'],$dates['day'],$dates['year']);
            
            return $timestamp;
        }
        
        /**
            Функция возвразает значение timestamp даты, идущей через $days дней после указанной
        */
        function add_date(
            $date,      //!< Дата в любом формате, который осили php-функция date_parse
            $days=1     //!< Количество дней, которые надо прибавить к дате       
        ){
            $holidays = array('01.01', '02.01', '23.02', '08.03', '01.05', '02.05', '09.05', '10.05');
            $timestamp = wirix_dates::date_parse($date);
            
            $t=0;
            while($t<$days){
                $timestamp = $timestamp+24*60*60;
                if(
                    date("D" ,$timestamp)=="Sun"
                    ||
                    date("D" ,$timestamp)=="Sat"
                    ||
                    array_search(date("d.m",$timestamp), $holidays)!==false
                )continue;
                $t++;
            }
            
            return $timestamp;
        }


        /**
            Функция возвращает количество рабочих дней между двумя датами,
            представленными как timestamp
            
        */
        function get_duration(
            $start,      //!< Дата в любом формате, который осили php-функция date_parse
            $end=1     //!< Количество дней, которые надо прибавить к дате       
        ){
            $holidays = array('01.01', '02.01', '23.02', '08.03', '01.05', '02.05', '09.05', '10.05');
            $timestamp = $start;
            
            $t=0;
            while($timestamp<$end){
                $timestamp = $timestamp+24*60*60;
                if(
                    date("D" ,$timestamp)=="Sun"
                    ||
                    date("D" ,$timestamp)=="Sat"
                    ||
                    array_search(date("d.m",$timestamp), $holidays)!==false
                )continue;
                $t++;
            }
            
            return $t;
        }
        
        /*
            Переводит время в часах(обычная дробь) к другим форматам
            
            1) 1ч 12м
            2) 01:12
            3) 01:12:00
        */
        function hours_to_time($hours, $format = 1){
            $whole = floor($hours);
            $frac = $hours - $whole;
            
            $min = floor($frac/(1/60));
            $sec = $hours*60*60 % 60;
            
            $result = '';
            switch($format){
                case 1:
                    $result = $whole."ч ".$min."м";
                break;
                case 2:
                    $result = sprintf("%02d",$whole).":".sprintf("%02d",$min);
                break;
                case 3:
                    $result = 
                        sprintf("%02d",$whole)
                        .":".sprintf("%02d",$min)
                        .":".sprintf("%02d",$sec);
                break;
                default:
                break;
            }
            
            return $result;
        }

    }
?>
