<?php

    include(dirname(__FILE__)."/../datascheme/datascheme.class.php");

    class wirix_datascheme_mysql extends wirix_datascheme{
        
        var $table    = "";     //!< Имя основной таблицы
        var $per_page = 10;     //!< Количество записей на страницу
        var $pages = array();   //!< Страницы постраничной навигации
        var $offset = 0;        //!< Смещение вывода
        var $sort = "id";         //!< Имя поля, по которому производится сортировка результата
        var $group_by = '';
        var $order = "ASC";     //!< Порядок сортировки (по умолчанию по возрастанию)
        var $alphabet = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','r','s','t','u','v','w','x','y','z');

        function __construct(){
            $this->called_class = get_called_class();
        }

        /**
            Проверка на уникальные поля
        */
        private function check_unique($fields){
            foreach($this->keys as $key){
                if($key['type']!='UNIQUE')continue;
                // Формируем условие на поиск записи по условиям в очередном ключе
                $search = array();
                foreach($key['fields'] as $field_name)$search[$field_name] = $fields[$field_name];
                if($_SERVER['db']->search_one($this->table, $search)){
                    if($_SERVER['db']->record[$field_name]==$fields[$field_name])continue;
                    $this->error = $key['error_message'];
                    return false;
                }
            }
            return true;
        }
        
        /**
            Проверка перичных ключей в связанных таблицах
        */
        function check_pk_in_join_tables($fields){
            foreach($fields as $field_name=>$v){
                if(
                    isset($this->fields[$field_name]['join_schema']) &&
                    is_object($this->fields[$field_name]['join_schema'])
                ){
                    $pk = $this->fields[$field_name]['join_schema']->primary_key;
                    $table = $this->fields[$field_name]['join_schema']->table;
                    $table_title = $this->fields[$field_name]['join_schema']->title;
                    
                    if(!$_SERVER['db']->search_one($table, array($pk=>$v))){
                        $this->error = "В таблице '".$table_title."'($table) нет записи с ключом '$v'";
                        return false;
                    }
                }
            }
            return true;
        }

        /**
            Удаление запаси из таблицы
        */
        function delete(){
            
            $id = $this->get('id');
            $search = array("id"=>$id);
            
            // Проверяем наличие такой записи
            if(!$_SERVER['db']->search_one($this->table, $search)){
                $this->error = "Объекта '".$this->title."' с ID=$id нет";
                return false;
            }
            
            return $_SERVER['db']->delete($this->table, $search);
        }
        
        /**
            Добавление записи в таблицу БД
        */
        function add(){
            
            // Получаем значения полей схемы в виде массива
            $fields = $this->get_as_array();
            
            // PK не надо ни проверять ни вставлять
            unset($fields[$this->primary_key]);
            
            // Проверяем валидность по регекспам
            if(!$this->check_validate($fields))return false;
            
            // Проверяем наличие первычных ключей в связных таблицах
            if(!$this->check_pk_in_join_tables($fields))return false;
            
            // Проверяем уникальные ключи
            if(!$this->check_unique($fields))return false;
            
            foreach($fields as $field_name=>$field_value)
                $fields[$field_name] = html_entity_decode($field_value);
            
            // Добавляем запись и устанавливаем в поле первичного ключа ID новой записи
            $insert_id = 0;
            $this->set($this->primary_key, $insert_id = $_SERVER['db']->insert($this->table, $fields));
            
            return $insert_id;
        }
        
        /**
            Сохранение записи в таблице БД по установленному первичному ключу
        */
        function save(){
            // Получаем значения полей схемы в виде массива
            $fields = $this->get_as_array();
            $pk = $this->primary_key;
            $pk_val = $fields[$pk];
            
            // PK не надо ни проверять ни вставлять
            unset($fields[$pk]);

            // Проверяем валидность по регекспам
            if(!$this->check_validate($fields))return false;
            
            // Проверяем наличие первычных ключей в связных таблицах
            if(!$this->check_pk_in_join_tables($fields))return false;

            // Проверяем уникальные ключи
            if(!$this->check_unique($fields))return false;

            foreach($fields as $field_name=>$field_value)
                $fields[$field_name] = html_entity_decode($field_value);
            // Изменяем запись
            $_SERVER['db']->update($this->table, $fields, array($pk=>$pk_val));
            
            return true;
            
        }
        
        /**
            Выборка объектов из БД
            'all' - все
            'page' - одна страница, согласно $this->per_page
            'first' - только 1-й элемент
        */
        function find($type = 'all'){
            // Устанавливаем главную таблицу
            $tables = array("a"=>$this->table);
            $join = array();
            $fields = array();
            
            // Задаём параметры поиска из полей
            $searchs = $this->get_as_array();
            $search = array();
            foreach($searchs as $k=>$v)if(trim($v))$search["`a`.`$k`"] = $v;
            
            // Добавляем связи таблиц и выводимые поля
            $i=1;
            
            foreach($this->enabled_fields as $field_name){
                $fields["`a`.`".$field_name."`"] = $field_name;
            }
            
            foreach($this->fields as $field_name=>$field){
                if(isset($field['join_schema'])){
                    foreach($field['join_schema']->enabled_fields as $f_name){
                        $fields["`".$this->alphabet[$i]."`.`".$f_name."`"] = $field_name."_".$f_name;
                    }
                    $fields["`".$this->alphabet[$i].
                        "`.`".$field['join_schema']->title_field.
                        "`"] = $field_name."_title";
                    $tables[$this->alphabet[$i]] = $field['join_schema']->table;
                    $join[
                        "`a`.`".$field_name."`=`".
                        $this->alphabet[$i]."`.`".$field['join_schema']->primary_key."`"
                        ." AND ".$field['join_schema']->condition
                    ] = "LEFT";
                    $i++;
                }
            }
            
            if(count($tables)<=1 && !$this->sort){
                $this->sort = $this->primary_key;
            }
            elseif(count($tables)>1 && !$this->sort){
                $this->sort = "`a`.`".$this->primary_key."`";
            }
            
            $sort = $this->sort." ".$this->order;
            
            // Определяем с количеством выбираемых записей
            if($type=='first'){
                $limit = 1;
            }
            elseif($type=='page'){
                $limit = $this->per_page;
            }
            elseif($type=='all'){
                $limit = 0;
                $this->offset = 0;
            }

            $_SERVER['db']->search( 
                $tables, $join, $search, $this->condition, 
                $sort, $limit, $this->offset, $fields, $this->group_by
            );
            $this->pages = $_SERVER['db']->pages;
            
            // Заполняем поля схемы полученными из БД значениями
            if(isset($_SERVER['db']->rows[0]))foreach($_SERVER['db']->rows[0] as $k=>$v)
                $this->set($k, $v);

            
            // Определяемся с тем, сколько записей вернуть
            if($type=='first' && isset($_SERVER['db']->rows[0]))
                return $_SERVER['db']->rows[0];
            elseif($type=='all' || $type=='page')
                return $_SERVER['db']->rows;
                
            return false;
        }


        /**
            Определение связной таблицы-справочника для поля
        */
        function set_join(
            $join_table,                    //!< Имя подсоединяемой 
            $foreign_key_name,              //!< Имя поля основной таблицы, используемое как внешний ключ
            $primary_key_name,              //!< Имя поля соединённой таблицы, используемое как первичный ключ
            $join_table_title,              //!< Выражение из полей таблицы справочника, используемое как заголовок строки
            $join_table_show = array(),     //!< Выводимые поля
            $cond = '1'                     //!< Дополнительное условие связывания
        ){
            if(!isset($this->fields[$foreign_key_name])){
                $this->error = 'В схеме данных нет поля $foreign_key_name';
                return false;
            }
            
            $this->fields[$foreign_key_name]['join_schema'] = new wirix_datascheme_mysql();
            $this->fields[$foreign_key_name]['join_schema']->table = $join_table;
            $this->fields[$foreign_key_name]['join_schema']->condition = $cond;
            $this->fields[$foreign_key_name]['join_schema']->set_primary($primary_key_name);
            $this->fields[$foreign_key_name]['join_schema']->set_title_field($join_table_title);
            $this->fields[$foreign_key_name]['join_schema']->set_enabled_fields($join_table_show);
            $this->fields[$foreign_key_name]['join_schema']->init();
        }

        /**
            Инициализация
        */
        function init(){
            
            $this->table = $_SERVER['db']->link->real_escape_string($this->table);
            
            $query = "SHOW CREATE TABLE `".$this->table."`";
            
            if(!$stha = $_SERVER['db']->sql_query($query, 'select')){
                $this->error = 'Ошибка получения данных о таблице '.
                    $this->table.': '.$_SERVER['db']->error;
                return false;
            }
            $row = $stha->fetch_assoc();
            
            $rows = explode("\n",$row['Create Table']);
            
            
            // Выделяем поля
            $this->enabled_fields = array();
            foreach($rows as $k=>$row){
                if(!$k)continue;
                if(!preg_match("/^\s*`(.*?)`/", $row, $m))continue;
                if($m[1]=='ctime' || $m[1]=='mtime')continue;
                $this->fields[$m[1]] = array("comment"=>'',"description"=>'', 'default'=>'', 'value'=>'');
                $this->enabled_fields[] = $m[1];
            }
            
            // Выделяем комменты
            foreach($rows as $row){
                if(!preg_match("/`(.*?)`.* COMMENT '(.*)'/i", $row, $m))continue;
                if(!isset($this->fields[$m[1]]))continue; 
                $this->fields[$m[1]]['comment'] = _t($m[2]);
            }
            
            //Выделяем типы данных
            foreach($rows as $row){
                if(!preg_match("/`(.*?)`\s+(.*?)\s+/i", $row, $m))continue;
                if(!isset($this->fields[$m[1]]))continue; 
                
                // Выделяем размер
                preg_match("/([\w\d]+)(\(?.*\)?)/", $m[2], $m1);
                $m1[1] = trim($m1[1]);
                if($m1[2])$m1[2] = preg_replace("/^\s*\((.*)\)\s*$/", "$1", $m1[2]);

                $this->fields[$m[1]]['type'] = $m1[1];
                $this->fields[$m[1]]['size'] = $m1[2];

                
                // Выделяем набор значений для enum
                if($m1[1]=='enum'){
                    $values = explode(",", $m1[2]); 
                    foreach($values as $c=>$value)$values[$c] = preg_replace("/\s*'(.*)'\s*/", "$1", $value);
                    foreach($values as $c=>$value)$values[$c] = _t($value);
                    $this->fields[$m[1]]['values'] = $values;
                    unset($this->fields[$m[1]]['size']);
                }
                
                
            }
            
            // Выделяем значения по-умолчанию
            foreach($rows as $row){
                if(!preg_match("/`(.*?)`.*?DEFAULT\s+'?(.*?)'?\s+/i", $row, $m))continue;
                if(!isset($this->fields[$m[1]]))continue; 
                $this->fields[$m[1]]['default'] = $m[2];
            }
            
            // Выделяем ключи
            $k=0;
            foreach($rows as $row){
                if(!preg_match("/(.*?\s)KEY\s+`?(.*)`\s*\((.*)\)/i", $row, $m))continue;
                if($m[2]=='ctime' || $m[2]=='mtime')continue;
                $this->keys[$k] = array(
                    "type"=>trim($m[1]),
                    "name"=>trim($m[2]),
                    "fields"=>array()
                );
                $fields = explode(",", $m[3]);
                $this->keys[$k]['fields'] = $fields;
                foreach($this->keys[$k]['fields'] as $c=>$field){
                    $this->keys[$k]['fields'][$c] = str_replace('`', "", $field);
                }
                
                if($this->keys[$k]['type']=='UNIQUE' && count($this->keys[$k]['fields'])==1){
                    $this->keys[$k]['error_message'] = "Поле '".$this->fields[$this->keys[$k]['fields'][0]]['comment']."' должно быть уникальным";
                }
                elseif($this->keys[$k]['type']=='UNIQUE'){
                    $comments = array();
                    foreach($this->keys[$k]['fields'] as $field_name)
                        $comments[] = $this->fields[$field_name]['comment'];
                    $this->keys[$k]['error_message'] = "Сочетание полей '".implode("','", $comments)."' должно быть уникальным";
                }
                $k++;
            }
            
            // Назначаем регекспы
            foreach($this->enabled_fields as $field_name){
                $field = $this->fields[$field_name];
                if(isset($this->patterns[$field['type']])){
                    $this->regexps[$field_name] = array(
                        "regexp"=>$this->patterns[$field['type']], 
                        "error_message"=>"Ошибка при заполнении поля ".
                            (isset($this->fields[$field_name]['comment'])?"'".$this->fields[$field_name]['comment']."'":"").
                            "($field_name)"
                    );
                }
                else{
                    $this->regexps[$field_name] = array(
                        "regexp"=>"^.*$", 
                        "error_message"=>"Ошибка при заполнении поля ".
                            (isset($this->fields[$field_name]['comment'])?"'".$this->fields[$field_name]['comment']."'":"").
                            "($field_name)"
                    );
                }
            }
            // Выделяем коммент к таблице
            if(preg_match("/COMMENT='(.*)'/", $rows[count($rows)-1], $m))
                if(trim($m[1]))$this->title = $m[1];
            
        }
        
    }
