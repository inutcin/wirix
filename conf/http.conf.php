<?php
    $this->timeout = '{{http_timeout}}'; // Таймаут соединения : 10
    
    $this->proxy["host"] = '{{http_proxy_host}}'; // Хост прокси :
    $this->proxy["port"] = '{{http_proxy_port}}'; // Порт прокси :
    $this->proxy["username"] = '{{http_proxy_username}}'; // Пользователь прокси :
    $this->proxy["password"] = '{{http_proxy_password}}'; // Пароль прокси :
    
    $this->http_auth = '{{http_auth}}'; // Пользователь пароль для http-авторизации(через двоеточие) : 
    